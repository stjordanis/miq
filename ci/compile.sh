#!/bin/bash

# Three is the right number, babe!
if ! [[ -n $1 && -n $2 && -n $3 ]]; then
    echo "You McFucked up!"
    exit 1
fi

MODE=$1                 # so - shared object; exe - executable
FILE=${2%.*}            # Strip suffic
FILE=${FILE##*/}        # Remove path

SOURCE=$2               # Source has to be the original target
TARGET="$3/${FILE}.so"  # Target is the directory/library.so

if   [[ $MODE == "so"  ]]; then OPTS="-O3 -shared -fPIC";
elif [[ $MODE == "exe" ]]; then OPTS="";
else echo "Invalid mode: $MODE"; exit 2;
fi

gcc -DKXVER=3 ${OPTS} ${SOURCE} -o ${TARGET}

