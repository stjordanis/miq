// ========== \\
//  miQ base  \\
// ========== \\

\d .miq
/ Meta
M.FAMILY  :`kernel;
M.NAME    :`miq;
M.VERSION :`0.001.0;
M.COMMANDS:`command`env`ls`gdb`realpath;

LOADED:`file`lzp!(-1!`$first -3#(get {});.z.p);

/ miQ pre-configuration
ROOT.BB :`.bb  ^ `$getenv`MIQ_ROOT_BB ;
ROOT.CFG:`.cfg ^ `$getenv`MIQ_ROOT_CFG;

if[not (1#0) ~ where "."=string ROOT.BB ;  '"ROOT.BB namespace invalid: ",-3!ROOT.BB ];
if[not (1#0) ~ where "."=string ROOT.CFG; '"ROOT.CFG namespace invalid: ",-3!ROOT.CFG];

/ Special
SELF:system"d";                       / Me <3
BB  :` sv ROOT.BB,`$1_string SELF;    / .miq.bb.new[SELF]

/ Config
/ NOTE 'Config' section defined near the end of the script to allow use of routines defined by miQ kernel

// ===========
//  Constants
// ===========
INIT:(` sv BB,`init) set 0b;      / INIT set to 0b on every reload
WARM:` sv BB,`warm;               / Unresolvable (cold) on first run, resolvable (warm) subsequently
MAIN:` sv BB,`main;               / .miq.main[] will peek into `QINIT on first execution to allow seemless embedding to q

LOG.LEVELS    :(` sv BB,`log`levels) set `debug`show`info`warn`error`fatal;

LIBRARY       :` sv BB,`library;
QK.DESCRIPTION:` sv BB,`qk`description;
GISTS         :` sv BB,`gists;
OBJECTS       :` sv BB,`objects;
HANDLERS      :` sv BB,`handlers;
TEMPLATES     :` sv BB,`templates;

ITER.GIST     :` sv BB,`iter`gist;
TMP.PATH      :` sv BB,`tmp`path;

CONSOLE.CODE  :` sv BB,`console`code;
CONSOLE.ACTIVE:` sv BB,`console`active;
CONSOLE.OUT   :` sv BB,`console`out;
CONSOLE.CH    :` sv BB,`console`ch;
CONSOLE.D     :` sv BB,`console`d;

FN.TEMPLATE   :` sv BB,`fn`template;
FN.ORIGINAL   :` sv BB,`fn`original;

HOOK.HOOKS    :` sv BB,`hook`hooks;
HOOK.PRIMARY  :` sv BB,`hook`primary;
HOOK.STACK    :` sv BB,`hook`stack;

/ Table templates
TT.LIBRARY    :3!flip`directory`name`extension`class`loaded`lzp`request!"SSSSBP*"$\:();
TT.DESCRIPTION:1!flip`name`body`return!"S*S"$\:();
TT.GISTS      :1!flip`name`context`zp`gist!"S*P*"$\:();
TT.OBJECTS    :1!flip`class`signature`types!"S**"$\:();
TT.HANDLERS   :1!flip`name`implementer`arity`description`attached!"SSJ**"$\:();
TT.TEMPLATES  :2!flip`name`interface`order`implementer`arity!"S*JSJ"$\:();

// ==========
//  Messages
// ==========
.miq.i.messages:{[]
  .is.generate[`warn`error;;1;]./:\:.[;(::;0);` sv SELF,`colour`is,](
    (`colour      ;"Invalid colour name: "                  )
   ;(`mode        ;"Invalid colour mode: "                  )
   ;(`rgb         ;"Invalid RGB value: "                    )
  );

  .is.generate[`warn`error;;1;]./:\:.[;(::;0);` sv SELF,`system`is,](
    (`command     ;"System command unavailable: "           )
   ;(`variable    ;"No such environment variable: "         )
   ;(`terminal    ;"Process not attached to a terminal: "   )
  );

  .is.generate[`warn`error;;1;]./:\:.[;(::;0);` sv SELF,`module`is,](
    (`class       ;"Not a module class: "                   )
   ;(`loaded      ;"Module not loaded: "                    )
  );

  .is.generate[`warn`error;;1;]./:\:.[;(::;0);` sv SELF,`bb`is,](
    (`valid       ;"Not a valid blackboard address: "       )
   ;(`present     ;"No such blackboard address: "           )
   ;(`bb          ;"No such blackboard: "                   )
   ;(`item        ;"No such blackboard item: "              )
  );

  .is.generate[`warn`error;;1;]./:\:.[;(::;0);` sv SELF,`ns`is,](
    (`present     ;"No such namespace address: "            )
   ;(`context     ;"No such context: "                      )
   ;(`name        ;"No such name: "                         )
   ;(`gist        ;"No such namespace gist: "               )
   ;(`ancestor    ;"Context is not an ancestor: "           )
  );

  .is.generate[`warn`error;;1;]./:\:.[;(::;0);` sv SELF,`fs`is,](
    (`present     ;"No such file or directory: "            )
   ;(`file        ;"No such file: "                         )
   ;(`dir         ;"No such directory: "                    )
   ;(`softlink    ;"Not a softlink: "                       )
   ;(`absolute    ;"Path is not absolute: "                 )
   ;(`relative    ;"Path is not relative: "                 )
   ;(`ancestor    ;"Directory is not an ancestor: "         )
  );

  .is.generate[`warn`error;;1;]./:\:.[;(::;0);` sv SELF,`tmp`is,](
    (`enabled     ;"TMP not enabled"                        )
   ;(`tmp         ;"Not in TMP: "                           )
  );

  .is.generate[`warn`error;;1;]./:\:.[;(::;0);` sv SELF,`syntax`name`is,](
    (`constant    ;"Name not a constant: "                  )
   ;(`literal     ;"Name not a literal: "                   )
   ;(`internal    ;"Name not an internal definition: "      )
   ;(`public      ;"Name not a public definition: "         )
   ;(`handler     ;"Name not a handler: "                   )
   ;(`template    ;"Name not a template: "                  )
   ;(`hook        ;"Name not a hook: "                      )
  );

  .is.generate[`warn`error;;1;]./:\:.[;(::;0);` sv SELF,`fn`is,](
    (`locked      ;"Function locked: "                      )
   ;(`address     ;"Not an address to executable object: "  )
   ;(`executable  ;"Not an executable object: "             )
   ;(`arity       ;"Invalid arity: "                        )
  );

  .is.generate[`warn`error;;1;]./:\:.[;(::;0);` sv SELF,`object`is,](
    (`defined     ;"Object class not defined: "             )
   ;(`valid       ;"Not a valid object: "                   )
   ;(`object      ;"Not an instance of known class: "       )
   ;(`class       ;"Object not of class ${sCLass}: "        )
  );

  .is.generate[`warn`error;;1;]./:\:.[;(::;0);` sv SELF,`handler`is,](
    (`defined     ;"Handler not defined: "                  )
   ;(`implemented ;"Handler not implemented: "              )
   ;(`stale       ;"Handler implementer stale: "            )
   ;(`attached    ;"Handler not attached: "                 )
  );

  .is.generate[`warn`error;;1;]./:\:.[;(::;0);` sv SELF,`template`is,](
    (`present     ;"No such template: "                     )
   ;(`defined     ;"No such template interface: "           )
   ;(`interface   ;"Not a valid template interface: "       )
   ;(`implementer ;"Not an interface implementer: "         )
   ;(`blueprint   ;"Not a valid template blueprint: "       )
  );

  .is.generate[`warn`error;;1;]./:\:.[;(::;0);` sv SELF,`hook`is,](
    (`defined     ;"Hook not defined: "                     )
   ;(`name        ;"Not a hook name: "                      )
   ;(`label       ;"No such label for ${nsHook}: "          )
  );
 };

// ====================
//  Template blueprint
// ====================
.miq.i.templates:{[]
  .miq.template.blueprint flip`name`interface`implementer!flip(
    / system routines
    (`.miq.system.require   ;`s           ;`.miq.system.t.requireResource           )
   ;(`.miq.system.require   ;`s`          ;`.miq.system.t.requireNamedResource      )
    / object routines
   ;(`.miq.object.define    ;`s`sl`       ;`.miq.object.t.define                    )
   ;(`.miq.object.define    ;`b`s`sl`     ;`.miq.object.t.redefine                  )
   ;(`.miq.object.extend    ;`s`s`sl`     ;`.miq.object.t.extend                    )
   ;(`.miq.object.extend    ;`b`s`s`sl`   ;`.miq.object.t.reextend                  )
   ;(`.miq.object.export    ;`s           ;`.miq.object.t.export                    )
   ;(`.miq.object.export    ;`b`s         ;`.miq.object.t.exportWithRedefine        )
    / handler routines
   ;(`.miq.handler.define   ;`s`j`cl      ;`.miq.handler.t.define                   )
   ;(`.miq.handler.define   ;`s`ns`j`cl   ;`.miq.handler.t.defineWithImplementer    )
   ;(`.miq.handler.define   ;`b`s`j`cl    ;`.miq.handler.t.redefine                 )
   ;(`.miq.handler.define   ;`b`s`ns`j`cl ;`.miq.handler.t.redefineWithImplementer  )
  )
 };

// =========
//  General
// =========
.miq.init:{[fnConfigLoader]
  / Initiate contants on first execution
  LIBRARY         set @[get;LIBRARY       ;TT.LIBRARY   ];
  QK.DESCRIPTION  set @[get;QK.DESCRIPTION;.miq.config.i.qk.descriptions[]];
  GISTS           set @[get;GISTS         ;TT.GISTS     ];
  OBJECTS         set @[get;OBJECTS       ;TT.OBJECTS   ];
  HANDLERS        set @[get;HANDLERS      ;.miq.config.i.handler[]];
  TEMPLATES       set @[get;TEMPLATES     ;TT.TEMPLATES ];

  / NOTE Constants not using .miq.bb.new[] call so we can keep 'Constants' section at the top
  CONSOLE.CODE    set @[get;CONSOLE.CODE  ;1#.q         ];
  CONSOLE.ACTIVE  set @[get;CONSOLE.ACTIVE;0b           ];
  CONSOLE.OUT     set @[get;CONSOLE.OUT   ;0N           ];
  CONSOLE.CH      set @[get;CONSOLE.CH    ;0i           ];
  CONSOLE.D       set @[get;CONSOLE.D     ;(1#0i)!1#`.  ];

  FN.TEMPLATE     set @[get;FN.TEMPLATE   ;1#.q         ];
  FN.ORIGINAL     set @[get;FN.ORIGINAL   ;1#.q         ];

  HOOK.HOOKS      set @[get;HOOK.HOOKS    ;1#.q         ];
  HOOK.PRIMARY    set @[get;HOOK.PRIMARY  ;1#.q         ];
  HOOK.STACK      set @[get;HOOK.STACK    ;`            ];

  / Import handlers
  .miq.handler.import[`stl.log ;` sv SELF,`h];
  .miq.handler.import[`stl.trap;` sv SELF,`h];
  .miq.handler.import[`stl.ipc ;` sv SELF,`h];

  .miq.h.log.info "Initialising: ",-3!SELF;

  / Load & apply configuration
  .miq.cfg.loader[fnConfigLoader;last` vs SELF];
  .miq.config.apply[];

  / .is & .to contexts
  .miq.is.init[];
  .miq.to.init[];

  / Standard warn & error messages
  .miq.i.messages[];

  / Function templating
  .miq.i.templates[];

  INIT set 1b;
  .miq.h.log.info "Initialisation completed: ",-3!SELF;

  / Silent startup completion
  if[.miq.system.is.variable`MIQ_SILENT_BOOT; `MIQ_SILENT_BOOT setenv ""; .miq.log.init[]];
 };

/ After-initiation default
.miq.i.hook.init:{[]
  / Disable default HTTP access
  if[not()~key`.z.ph; if[.miq.z.dd.ph~.z.ph; .miq.hook.primary[`.z.ph;`$".miq.i.hook.init@@";{(first get .h.he)"Be gone, foul creature!"}]]];
  / Set default hooks
  {if[()~key x; .miq.hook.expunge x]} each
    `.miq.z.p`.miq.z.d`.miq.z.z`.miq.z.n`.miq.z.t`.miq.z.console.prompt
 };

/ Functions without their own home :<
/ Return 1b if called directly by a script that is being executed, as opposed to a simple code load
/ Executing 'q script.q'  ->  .miq.main[] will yield 1b
/ Starting up q session and '\l script.q'  ->  .miq.main[] will yield 0b
.miq.main:{[]
  / Trick to only execute if both definitions exist, guaranteeing at least one function execution
  if[@[{get C.MAIN.ONCE; L.MAIN.COUNT; 1b};::;0b];
    if[C.MAIN.ONCE[] & L.MAIN.COUNT>0; :0b]
  ];

  main:`file`source!(')[.miq.fs.path.real;.miq.fs.to.absolute]'[-1!'(.z.f;`$.miq.u.nFrame[1][1;1])];

  / Take a peek into `QINIT on first load to see if we are attempting embedding miQ into q session
  if[@[get;MAIN;1b];
    MAIN set 0b;
    main[`qinit]:.miq.fs.path.real .miq.fs.to.absolute -1!`$getenv`QINIT;
    :$[(main[`qinit]~main[`source]); [`QINIT setenv ""; 1b]; (main[`file]~main[`source])]
  ];

  / Order matters - embedding logic above should not eat into 'execute once' counter
  L.MAIN.COUNT+:1;
  main[`file]~main[`source]
 };

/ Resolves membership of element in set
.miq.member:{[qsElement;qslSet]
  .is.error[`qs`qsl]@'(qsElement;qslSet);
  $[                `  in qslSet; 1b;     / every element matches a null symbol
            qsElement  in qslSet; 1b;     / explicit membership of set
        (-1!qsElement) in qslSet; 0b;     / explicitly excluded from set
                                  0b      / not a member of set, should never hit
  ]
 };

// Accessors
.miq.gists    :{[] .miq.ns.gist.info[]};
.miq.objects  :{[] OBJECTS[]};
.miq.handlers :{[] HANDLERS[]};
.miq.templates:{[] TEMPLATES[]};
.miq.hooks    :{[] .miq.hook.info[]};

// =========
//  Console
// =========
/ TODO inspect stack and pull context from previous frame to apply it
.miq.console:{[]
  if[not .miq.system.is.terminal[]&0i~.z.w;
    .z.w({if[not @[{.miq.i.console.out x;1b};x;{0b}];-1 x]};'"Console: Can only run on local terminal!");
  ];

  $[CONSOLE.ACTIVE[]; [.miq.i.console.out "Console: Running already!"; :()]; CONSOLE.ACTIVE set 1b];
  if[not null CONSOLE.OUT[]; hclose CONSOLE.OUT[]];
  CONSOLE.OUT set hopen .miq.fd.stdin[];

  mli:0b; sync:1b; clear:0b; run:0b; quit:0b; code:""; ns:system"d";
  if[not C.CONSOLE.REMEMBER.D[]; CONSOLE.D set (1#0i)!1#ns];
  if[not C.CONSOLE.REMEMBER.CH[]; CONSOLE.CH set 0i];
  lang :C.CONSOLE.INPUT.LANG[];
  debug:C.CONSOLE.DEBUG[];
  block:C.CONSOLE.INPUT.BLOCK[];
  .miq.i.console.block[block];

  while[1b;
    if[not .is.any[`ch`chl;CONSOLE.CH[]]; CONSOLE.CH set 0i];
    if[1<count CONSOLE.CH[]; sync:0b];
    if[null d:first CONSOLE.D[CONSOLE.CH[]]; CONSOLE.D set CONSOLE.D[],((),CONSOLE.CH[])!count[CONSOLE.CH[]]#d:`.];
    CONSOLE.OUT[] .miq.z.console.prompt (CONSOLE.CH[];d;lang;mli);
    input:read0 0;

    $[input like "handle *"; .miq.i.console.handle[input];
      input   ~  "handle"  ; 0N!CONSOLE.CH[];
      input   ~  "handles" ; 0N!key .z.W;
      input   ~  "mli"     ; mli:not run:mli;
      input   ~  "debug"   ; debug:not debug;
      input   ~  "block"   ; .miq.i.console.block[block:not block];
      input   ~  1#"\\"    ; lang:$[lang~"q";"k";"q"];
      input   ~  "sync"    ; sync:1b;
      input   ~  "async"   ; $[all CONSOLE.CH[];sync:0b;.miq.i.console.out "Console: Cannot async locally"];
      input like "save *"  ; code:.miq.i.console.save[input;code];
      input like "load *"  ; code:.miq.i.console.load[input];
      input   ~  "code"    ; 0N!'1_` vs code;
      input   ~  "reset"   ; [mli:0b; CONSOLE.CH set 0i; clear:1b];
      input   ~  "clear"   ; clear:1b;
      input   ~  "run"     ; run:1b;
      input   ~  "done"    ; [run:1b; quit:1b];
      input   ~  "exit"    ; quit:1b;
      input   ~  "\\\\"    ; $[C.CONSOLE.INPUT.QUIT[]~`local ; [quit:1b; input:""];
                               C.CONSOLE.INPUT.QUIT[]~`remote; [.miq.i.console.out "Console: Terminating q process on handle: ",-3!CONSOLE.CH[]; code,:"\n",input; run:1b];
                                                             .miq.i.console.out "Console: Invalid console.quit configuration: ",-3!C.CONSOLE.INPUT.QUIT[]
                             ];
                             [code,:"\n",input; run:not mli]
    ];

    if[run & not""~code;
      if[not null d; code:"\n(.)\"\\\\d ",string[d],"\";",code];
      processed:.miq.qk.process[`STDIN;-1;";",code];
      message:(.miq.qk.interpret[lang];processed);
      / TODO below assumes that remote runs on the same version as local! This will break!
      / TODO devise a system to determine kdb version of remote on first message dispatch!
      return:enlist$[.z.K<4f;("\\d";"STDIN";-1);("STDIN";-1;"\\d")];  / "qk" changed argument order in kdb 4.0
      $[0i~CONSOLE.CH[]; .miq.i.console.local[debug;message[0]] each message[1];
        sync           ; .miq.i.console.sync[debug;CONSOLE.CH[];message[0]';message[1],return];
                         .miq.h.ipc.async[CONSOLE.CH[];(message[0]';message[1])]
      ];
      run:0b; clear:1b
    ];
    if[clear; code:""; clear:0b];
    if[quit;
      system"d ",string ns;
      if[C.CONSOLE.REMEMBER.LANG[]; C.CONSOLE.INPUT.LANG set lang];
      hclose CONSOLE.OUT[]; CONSOLE.OUT set 0N;
      CONSOLE.ACTIVE set 0b;
      :()
    ]
  ]
 };

.miq.i.console.argument:{[x] 1_" "vs x};
.miq.i.console.out:{[x] neg[CONSOLE.OUT[]] x;};
.miq.i.console.show:{[x] CONSOLE.OUT[] .Q.s x;};

.miq.i.console.block:{[x]
/  $[x;
/    .miq.hook.remove[`.z.pi;`.miq.i.console.z.pi];
/    .miq.hook.register[`.z.pi;`.miq.i.console.z.pi;.miq.i.console.z.pi]
/  ];
 };

.miq.i.console.z.pi:{[x]
  $[first r:.miq.h.trap.apply[get;x];
      CONSOLE.OUT[] .miq.h.trap.print . r[1];
      CONSOLE.OUT[]  .Q.s r[1]
  ]
 };


.miq.i.console.local:{[debug;cmd;arg]
  if[first r:.miq.h.trap.apply[cmd;arg];
    error:r[1;0];
    stack:{x[3]:x[3]-8;x}each -8_r[1;1];
    if[0~count stack; if[debug; '`console.debug]; .miq.i.console.out "Console: Internal failure!"; :()];
    :CONSOLE.OUT[] .miq.h.trap.print[error;stack]
  ];

  CONSOLE.D set CONSOLE.D[],(1#CONSOLE.CH[])!1#system"d";
  .miq.i.console.show last r; last r
 };

/ TODO execute loop in .trap.remote and send partial results back on handle?
.miq.i.console.sync:{[debug;ch;cmd;arg]
  r:.miq.h.trap.apply[.miq.h.ipc.sync .; (ch;(get .miq.h.trap.apply;cmd;arg))];

  / handle local cmd failure
  if[first r;
    .miq.i.console.out "'",r[1;0];
    .miq.i.console.out "Console: Sync request failed!";
    if[debug; '`console.debug];
    :()
  ];

  / handle remote cmd failure
  r:last r;
  if[first r;
    error:r[1;0];
    stack:{x[3]:x[3]-4;x}each -4_r[1;1];
    if[0~count stack; if[debug; '`console.debug]; .miq.i.console.out "Console: Internal failure!"; :()];
    :CONSOLE.OUT[] .miq.h.trap.print[error;stack]
  ];

  / handle output from successful remote call
  out:last r;
  CONSOLE.D set CONSOLE.D[],(1#CONSOLE.CH[])!1#last out;
  .miq.i.console.show each -1_out; -1_out
 };

.miq.i.console.handle:{[clInput]
  arg:.miq.i.console.argument[clInput];
  ch:distinct`int$@'[get;arg;{0N}];

  if[not .is.chl[ch]; .miq.i.console.out "Console: Handle not valid: ",-3!ch; :()];
  if[(1<count ch) & 0i in ch; .miq.i.console.out "Console: Cannot mix local and remote handles: ",-3!ch; :()]

  if[1~count ch;ch:first ch];
  CONSOLE.CH set ch;
 };

.miq.i.console.load:{[clInput]
  label:`$.miq.i.console.argument[clInput];
  $[.miq.bb.is.item[CONSOLE.CODE;label]; .miq.bb.get[CONSOLE.CODE;label]; [.miq.i.console.out "Console: Label undefined: ",-3!label; ""]]
 };

.miq.i.console.save:{[clInput;cllCode]
  label:`$.miq.i.console.argument[clInput];
  .miq.bb.set[CONSOLE.CODE;label;cllCode];
  ""
 };

// ========
//  Colour
// ========
.miq.colour.is.colour:{[sColour] .is.error.s[sColour]; sColour in key C.COLOUR.CODES[]};
.miq.colour.is.mode  :{[sMode] .is.error.s[sMode]; sMode in key C.COLOUR.MODES[]};
.miq.colour.is.rgb   :{[jlRGB] .is.error.jl[jlRGB]; (3~count jlRGB) & all within[jlRGB;0 255]};

.miq.colour.codes:{[] C.COLOUR.CODES[]};
.miq.colour.names:{[] key C.COLOUR.CODES[]};
.miq.colour.modes:{[] key C.COLOUR.MODES[]};

.miq.colour.add:{[sColour;jlRGB]
  if[.miq.colour.is.colour[sColour]; '.miq.h.log.error "Colour already defined: ",-3!sColour];
  .miq.colour.is.error.rgb[jlRGB];
  C.COLOUR.CODES set C.COLOUR.CODES[],(1#sColour)!enlist jlRGB;
  sColour
 };

.miq.colour.remove:{[sColour]
  .miq.colour.is.error.colour[sColour];
  C.COLOUR.CODES set sColour _ C.COLOUR.CODES[];
  sColour
 };

.miq.colour.fg:{[sColour;clText]
  .miq.colour.is.error.colour[sColour];
  .miq.colour.i.mode[C.COLOUR.MODES`off] .miq.colour.i.apply[38;C.COLOUR.CODES[sColour];clText]
 };

.miq.colour.bg:{[sColour;clText]
  .miq.colour.is.error.colour[sColour];
  .miq.colour.i.mode[C.COLOUR.MODES`off] .miq.colour.i.apply[48;C.COLOUR.CODES[sColour];clText]
 };

.miq.colour.i.apply:{[jTextbg;jlRGB;clText]
  colour:";" sv string jTextbg,2,jlRGB;
  "\033[",colour,"m",clText
 };

.miq.colour.mode:{[sMode;clText]
  .is.error[`s`cl]@'(sMode;clText);
  .miq.colour.is.error.mode[sMode];

  .miq.colour.i.mode[C.COLOUR.MODES[sMode];clText]
 };

.miq.colour.i.mode:{[jMode;clText]
  $[-1~jMode; clText;
     0~jMode; clText,"\033[",string[jMode],"m";
              "\033[",string[jMode],"m",clText
  ]
 };

/ Inline tag resolution
.miq.colour.process:{[clText]
  .is.error.cl[clText];

  raze {[x]
    if[1~count x; :first x];
    rule:" "vs x[0]; tag:`$rule[0]; text:x[1];
    if[.miq.colour.is.mode[tag]; :.miq.colour.i.mode[C.COLOUR.MODES[tag];""],text];
    if[not tag in `bg`fg; .miq.h.log.warn "No such tag: ",-3!tag; :"${",x[0],"}",x[1]];
    t:$[tag~`fg;38;48];
    c:$[
      2~count rule; [.miq.colour.is.error.colour[colour:`$last rule]; C.COLOUR.CODES[colour]];
      4~count rule; [.miq.colour.is.error.rgb[rgb:"J"$1_rule]; rgb];
                    '.miq.h.log.error "Invalid tag argument: "," " sv x[0]
    ];
    .miq.colour.i.apply[t;c;text]
  } each "}" vs'"${" vs clText
 };

// ========
//  System
// ========
/ TODO shed system into handler?
.miq.system.is.command:{[s]
  / Cannot rely on itself due to recursion, so have to handle the command dependency specially
  if[not `command in L.SYSTEM.COMMAND.VERIFIED;
    $[@[{system x;1b};"command &>/dev/null";0b];
      L.SYSTEM.COMMAND.VERIFIED,:`command;
      '.miq.h.log.error "System command unavailable: `command"
    ]
  ];

  $[not .is.s[s]                                              ; 0b;
    s in L.SYSTEM.COMMAND.VERIFIED                            ; 1b;
    @[{system x;1b};"command -v ",string[s]," &>/dev/null";0b]; [L.SYSTEM.COMMAND.VERIFIED,:s; 1b];
                                                                0b
  ]
 };

.miq.system.is.variable:{[s] $[.is.s[s]; not ""~getenv s; 0b]};
.miq.system.is.terminal:{[] ":/dev/pts"~9#string .miq.fd.stdin[]};

.miq.system.t.requireResource:{[sResource] .miq.system.i.require[sResource;::]};
.miq.system.t.requireNamedResource:{[sResource;name] .miq.system.i.require[sResource;name]};

.miq.system.i.require:{[sResource;name]
  .is.error.s[sResource];

  $[sResource in `cmd`command     ; .miq.system.is.error.command[name];
    sResource in `var`variable    ; .miq.system.is.error.variable[name];
    sResource in `tty`pts`terminal; .miq.system.is.error.terminal[];
                                    '.miq.h.log.error "Invalid resource: ",-3!sResource
  ];

  name
 };

.miq.system.env:{[] .miq.system.is.error.command[`env]; (!)."S=*\001"0:raze (system"env"),\:"\001"};
/ TODO .miq.system.exec[]

// ==================
//  File descriptors
// ==================
/ STD linux streams shorthand
.miq.fd.stdin :{[] .miq.fd.i.stream[C.FD.STDIN[] ]};
.miq.fd.stdout:{[] .miq.fd.i.stream[C.FD.STDOUT[]]};
.miq.fd.stderr:{[] .miq.fd.i.stream[C.FD.STDERR[]]};

/ Attach to local q process / Does not work :(((
/.miq.fd.attach:{[iPid]
/  .is.error.i[iPid];
/  if[.z.i~iPid; '.miq.h.log.error "Can attach to foreign q process only"];
/  if[not "q"~last last system"ps | grep ",string iPid; '.miq.h.log.error "Can attach to q process only"];

/  .miq.h.log.info "Attaching q terminal to PID ",-3!iPid;
/  tty:.miq.fd.stdin[];
/  .miq.fd.i.redirect[.z.i;0 1 2i;`:/dev/null];
/  .miq.fd.i.redirect[iPid;0 1 2i;tty]
/ };

.miq.fd.i.stream:{[jStream]
  info:.miq.fd.info[jStream];
  $[info`stale;`;info`target]
 };

.miq.fd.list:{[]
  "I"$string fds where not ()~/:key each` sv'L.FD.DIR,'fds:key[L.FD.DIR]
 };

.miq.fd.info:{[fd]
  .is.error.any[`none`fd;fd];
  .miq.system.is.error.command[`ls];

  $[.is.none[fd];
    `fd xasc .miq.fd.i.info'[.miq.fd.list[]];
    .miq.fd.i.info[fd]
  ]
 };

.miq.fd.i.info:{[fd]
  file:` sv L.FD.DIR,`$string fd;
  out:last " -> "vs first system"ls -l ",1_string file;
  $[count ss[out;"(deleted)"];
      [stale:1b; target:raze -1_" " vs out];
      [stale:0b; target:out]
  ];
  htype:(),$[0=fd;`fh`ch;fd in key .z.W;`ch;`fh];

  `fd`htype`stale`file`target!(`int$fd;htype;stale;file;-1!`$target)
 };

.miq.fd.listener:{[]
  first exec fd from .miq.fd.info[] where `fh in/:htype, target like ":socket:*"
 };

.miq.fd.redirect:{[fhl;fsFile]
  .is.error[`fhl`fs]@'((),fhl;fsFile);
  .miq.h.log.info "Redirecting handles (",(-3!fhl),") to ",-3!fsFile;
  .miq.fd.i.redirect[.z.i;fhl;fsFile]
 };

.miq.fd.i.redirect:{[iPid;fhl;fsFile]
  .miq.system.is.error.command[`gdb];

  fd:"fd";
  dup2:{"p (int) dup2((int) fileno($",x,"), ",string[y],")"};

  file:.miq.u.text.qw 1_string fsFile;
  mode:.miq.u.text.qw 1#$[C.FD.REDIRECT.APPEND[];"a";"w"];
  script:enlist "set variable $",fd," = (int) fopen (",file,", ",mode,")";
  script,:dup2[fd] each (),fhl;
  script,:(
    "p (int) close ($",fd,")"
   ;"detach"
   ;"quit"
  );
  script:"'",/:script,\:"'";
  script:(enlist "gdb -q -p ",string[iPid]),script;
  script:" -ex " sv script;

  system script, " &>/dev/null;"
 };

// ========
//  Module
// ========
// Handler implementation
.miq.module.is.class:{[sClass] .is.error.s[sClass]; sClass in C.MODULE.CLASSES[]};

.miq.module.is.loaded:{[sClass;sName]
  .is.error.s[sClass];
  .is.error.any[`s`sl;sName];
  .miq.module.is.error.class[sClass];

  libs:.miq.module.i.resolve[sClass;sName];
  $[1~count libs; LIBRARY[first libs][`loaded]; 0b]
 };

.miq.module.classes:{[] C.MODULE.CLASSES[]};
.miq.module.library:{[] LIBRARY[]};
.miq.module.load   :{[sClass;sName] .miq.module.i.load[0b;sClass;sName]};
.miq.module.reload :{[sClass;sName] .miq.module.i.load[1b;sClass;sName]};

.miq.module.path:{[qtLib]
  .is.error.qt[qtLib];
  {` sv x,` sv y,z}.'flip get flip qtLib
 };

.miq.module.resolve:{[sClass;sName]
  .is.error.s[sClass];
  .is.error.any[`s`sl;sName];
  .miq.module.is.error.class[sClass];

  .miq.module.i.resolve[sClass;sName]
 };

.miq.module.i.resolve:{[sClass;sName]
  if[not count[sName] in 1 2; '.miq.h.log.error "Invalid name: ",-3!sName];

  dirs:.miq.config.get` sv `module,sClass,`directory;
  conds:`directory`class!(dirs;sClass); ops:(in;=);

  $[2~count sName      ; [conds,:`name`extension!(first;last)@\:sName; ops,:(=;=)];
    "." in string sName; [conds,:`name`extension!(first;last)@\:.miq.fs.file.split[sName]; ops,:(=;=)];
                         [conds,:(1#`name)!1#sName; ops,:(=)]
  ];

  libs:key?[LIBRARY;.miq.fq.d2cond[ops;conds];0b;()];
  libs[`order]:(dirs!til count dirs)[libs`directory];
  flip first `order xgroup libs
 };

.miq.module.i.load:{[bReload;sClass;sName]
  .is.error[`b`s]@'(bReload;sClass);
  .is.error.any[`s`sl;sName];
  .miq.h.log.info $[bReload;"Reloading";"Loading"]," module: ",-3!`class`name!(sClass;sName);

  libs:.miq.module.resolve[sClass;sName];
  if[0~count libs; '.miq.h.log.error "No matching module: ",-3!`class`name!(sClass;sName)];
  if[1<count libs; '.miq.h.log.error "Module name ambiguous: ",-3!`class`name!(sClass;sName)];
  if[bReload&not LIBRARY[first libs][`loaded]; .miq.h.log.warn "Module not loaded: ",-3!first libs];

  .miq.module.i.request lib:first libs;
  class:LIBRARY[lib;`class];

  $[`so~class;
      2:[` sv lib`directory`name];
    [
      if[LIBRARY[lib;`loaded]&not bReload; :(::)];
      file:` sv lib[`directory],.miq.fs.file.join[lib[`name`extension]];
      system"l ",1_string file;
      .miq.module.i.setLoaded[lib];
    ]
  ]
 };

.miq.module.i.setLoaded:{[qdLib] LIBRARY upsert qdLib,`loaded`lzp!(1b;.z.p)};

/ Record request
.miq.module.i.request:{[qdLib]
  caller:.miq.u.stack[][0;1;0];
  caller:$[(2~count caller)|()~caller;`process;""~caller;`user;`$caller];   / ("q";`) hack on new kdb 4.0 behaviour
  LIBRARY upsert qdLib,enlist[`request]!enlist LIBRARY[qdLib][`request],caller;
 };

// Required for configuration application
.miq.module.i.checkEnv:{[]
  .miq.h.log.info "No module classes configured";

  .miq.h.log.info "Inspecting presence of internal LOADED flags";
  loaded:` sv'(SELF,CFG.EXPLICIT),\:`LOADED;
  classes:`module`config;
  $[count w:where .miq.ns.is.present each loaded;
    [
      .miq.h.log.info "Found internal LOADED flags: ",-3!loaded w;
      .miq.h.log.info "Module classes added to configuration: ",-3!classes w
    ];
      .miq.h.log.info "No internal LOADED flags found"
  ];
  loaded:classes w;

  .miq.h.log.info "Inspecting environment variables";
  classes:`module`config`role`so;
  envvars:`MIQ_MOD`MIQ_CFG`MIQ_ROLE`MIQ_SO;
  $[count w:where not ""~/:getenv each envvars;
    [
      .miq.h.log.info "miQ envvars detected: ",-3!envvars w;
      .miq.h.log.info "Module classes added to configuration: ",-3!classes w
    ];
      .miq.h.log.info "No miQ envvars detected"
  ];

  classes[w] union loaded
 };

.miq.module.i.configure:{[sClass;qdConfig]
  .miq.h.log.info "Resolving configuration for class: ",-3!sClass;

  if[not `extension in key qdConfig; '.miq.h.log.error "Class extension configuration missing: ",-3!sClass];
  if[not any `directory`envvar in key qdConfig;
    '.miq.h.log.error "No configuration for class directory or envvar: ",-3!sClass
  ];

  / LOADED hack
  miq:` sv SELF,`LOADED;
  miqcfg:` sv CFG.EXPLICIT,`LOADED;
  m:.miq.ns.is.present[miq]  &  sClass~`module;
  c:.miq.ns.is.present[miqcfg] & sClass~`config;
  / LOADED hack

  config:qdConfig;
  if[not `directory in key config;
    config[`directory]:0#`;
    $[""~envval:getenv config`envvar;
      if[not m|c; '.miq.h.log.error "Class envvar empty: ",-3!`class`envvar!(sClass;envvar)];
      config[`directory]:$["`:"~2#envval;(),get envval;-1!'`$":"vs envval]
    ];
  ];

  config[`directory]:distinct .miq.fs.path.real'[config`directory];
  if[m;config[`directory]:distinct config[`directory],first` vs miq`file];
  if[c;config[`directory]:distinct config[`directory],first` vs miqcfg`file];

  config
 };

/ Returns a table of libraries available for loading
.miq.module.i.populate:{[sClass]
  config:.miq.config.get[` sv `module,sClass];

  .miq.h.log.info "Generating library for class: ",-3!sClass;
  if[0~count config[`directory]; '.miq.h.log.error "Class directory empty: ",-3!sClass];
  if[not .is.fsl[config`directory]; '.miq.h.log.error "Class directory invalid: ",-3!sClass];

  libs:raze{[dir;ext]
    .miq.fs.is.error.dir[dir];
    files:files where any each flip (files:key dir) like/:ext;
    if[0~count files;
      .miq.h.log.warn "No modules found: ",-3!`directory`extension!(dir;ext); :()
    ];
    flip`directory`name`extension!(dir,flip .miq.fs.file.split each files)
  }[;config[`extension]] each config[`directory];

  libs:TT.LIBRARY upsert update class:sClass from libs;

  / LOADED hack
  line:{[x;y] x:` vs x;
    `directory`name`extension`loaded`lzp!(first[x],.miq.fs.file.split[last x],1b,y)
  };
  miq:` sv SELF,`LOADED;
  miqcfg:` sv CFG.EXPLICIT,`LOADED;
  if[.miq.ns.is.present[miq]  &  sClass~`module;libs:libs upsert line . get miq[]];
  if[.miq.ns.is.present[miqcfg] & sClass~`config;libs:libs upsert line . get miqcfg[]];
  / LOADED hack

  LIBRARY upsert libs
 };

// ============
//  Blackboard
// ============
/ Checks
.miq.bb.is.valid  :{[s] $[.is.ns[s]; $[ROOT.BB~s; 1b; .miq.ns.is.ancestor[ROOT.BB;s]]; 0b]};
.miq.bb.is.present:{[s]  $[.is.ns[s]; .miq.ns.is.present[s]; 0b]};
.miq.bb.is.bb     :{[sBoard] $[.is.any[`s`ns;sBoard]; $[.miq.bb.is.valid board:.miq.bb.i.realpath[sBoard]; .miq.ns.is.context[board]; 0b]; 0b]};
.miq.bb.is.item   :{[sBoard;sName] $[.is.any[`s`ns;sBoard] & .is.s[sName]; $[.miq.bb.is.valid board:.miq.bb.i.realpath[sBoard]; .miq.ns.is.name[` sv board,sName]; 0b]; 0b]};

/ Common
.miq.bb.root:{[] ROOT.BB};
.miq.bb.realpath:{[s] .is.error.any[`s`ns;s]; .miq.bb.i.realpath[s]};
.miq.bb.i.realpath:{[x] $[.miq.bb.is.valid[x]; x; ` sv ROOT.BB,`$$["."~first x:string x; 1_x; x]]};

/ Blackboard
.miq.bb.new:{[sBoard]
  .is.error.any[`s`ns;sBoard];
  board:.miq.bb.i.realpath[sBoard];

  $[.miq.bb.is.bb[board];
      .miq.h.log.info "Blackboard exists, skipping: ",-3!board;
    .miq.ns.is.present[board];
      '.miq.h.log.error "Blackboard context taken: ",-3!board;
      [.miq.h.log.info "Creating new blackboard: ",-3!board; .miq.ns.set[board;1#.q]]
  ];
  board
 };

.miq.bb.delete:{[sBoard]
  .is.error.any[`s`ns;sBoard];
  board:.miq.bb.i.realpath[sBoard];
  .miq.bb.is.error.bb[board];

  parts:` vs board;

  .miq.h.log.info "Deleting blackboard: ",-3!` sv -1#parts;
  ![` sv -1_parts;();0b;-1#parts]
 };

/ Blackboard item
.miq.bb.set:{[sBoard;sItem;content]
  .is.error.any[`s`ns;sBoard];
  .is.error.s[sItem];

  board:.miq.bb.i.realpath[sBoard];
  .miq.bb.is.error.bb[board];

  $[null sItem;
    [
      .miq.h.log.debug "New blackboard content: ",-3!board;
      .miq.ns.set[board;content]
    ];
    [
      .miq.h.log.debug "New blackboard item: ",-3!`board`item!(board;sItem);
      .miq.ns.set[` sv board,sItem;content]
    ]
  ]
 };

.miq.bb.get:{[sBoard;sItem]
  .is.error.any[`s`ns;sBoard];
  .is.error.s[sItem];

  board:.miq.bb.i.realpath[sBoard];
  .miq.bb.is.error.bb[board];

  $[null sItem;
    [
      .miq.h.log.debug "Retrieving blackboard: ",-3!board;
      get board
    ];
    [
      .miq.bb.is.error.item[board;sItem];
      .miq.h.log.debug "Retrieving blackboard item: ",-3!`board`item!(board;sItem);
      get ` sv board,sItem
    ]
  ]
 };

.miq.bb.drop:{[sBoard;sItem]
  .is.error.any[`s`ns;sBoard];
  .is.error.s[sItem];

  board:.miq.bb.i.realpath[sBoard];
  .miq.bb.is.error.bb[board];
  .miq.bb.is.error.item[board;sItem];

  .miq.h.log.debug "Dropping blackboard item: ",-3!`board`item!(board;sItem);
  .miq.ns.delete[` sv board,sItem]
 };

// ===============
//  Configuration
// ===============
/ TODO implement tree mechanism to encode configuration item dependencies to enable targeted re-application
// General
.miq.cfg.root:{[] ROOT.CFG};

/ Return conventional user configuration context
.miq.cfg.explicit:{[sName]
  .is.error.any[`s`ns;sName];
  explicit:` sv ROOT.CFG,$[.is.ns[sName]; `$1_string sName; sName];
  $[.miq.ns.is.present[explicit]; explicit; explicit set 1#.q]
 };

/ TODO Improve configuration implementation
/.miq.cfg.loader:{[oCFG;config;argument]
.miq.cfg.loader:{[config;argument]
  /.miq.o.is.error.CFG[`CFG;oCFG];
  .is.error.any[`none`ns`fn`fp`qc`qd;config];

  if[.is.none[config]; :()];

  if[.is.any[`ns`fn`fp`qc;config];
    .miq.fn.is.error.executable[config];
    if[1<>.miq.fn.arity[config]; '.miq.h.log.error "Configuration loader has to be of arity 1: ",-3!.miq.fn.print[config]];

    .miq.h.log.info "Executing configuration loader: ",-3!`function`argument!(.miq.fn.print[config];argument);
    :config argument
  ];

  if[.is.qd[config]; '.miq.h.log.error "Configuration loading from dictionary not yet implemented: ",-3!config];
 };

// Special
.miq.cfg.default:{[nsDefault;sName;content]
  .is.error[`ns`s]@'(nsDefault;sName);
  (` sv nsDefault,sName) set content
 };

/ Set's a value in cfg context
.miq.cfg.set:{[nsExplicit;nsDefault;sName;content]
  .is.error[`ns`ns`s]@'(nsExplicit;nsDefault;sName);
  name:$[null sName; nsExplicit; (` sv nsExplicit,sName)];
  if[not first[` vs sName] in key nsDefault;
    .miq.h.log.warn "Potentially unimplemented configuration: ",-3!sName
  ];
  get name set content
 };

/ Returns configuration either explicit or default configuration
.miq.cfg.assemble:{[nsExplicit;nsDefault;sName]
  .is.error[`ns`ns`s]@'(nsExplicit;nsDefault;sName);
  e:` sv nsExplicit,sName;
  i:` sv nsDefault,sName;

  .miq.h.log.debug "Configuration (Explicit;Default;Item): ",-3!(nsExplicit;nsDefault;sName);

  if[not()~key e;
    .miq.h.log.debug "Config explicit: ",-3!(e;get e);
    :$[.miq.ns.is.context[e];
        $[@[{get x;1b};i;0b]; i[],e[]; e[]];
        get e
    ];
  ];

  if[not()~key i;
    .miq.h.log.debug "Config default: ",-3!(i;get i);
    :get i;
  ];

  '.miq.h.log.error "No such configuration item: ",-3!sName
 };

/ Moves configuration to blackboard space and assigns the address to name in given context
/ TODO template? so nf can be pre-processed via different interface
.miq.cfg.apply:{[nsContext;nsApplied;fnCfgAssemble;nf]
  .is.error[`ns`ns]@'(nsContext;nsApplied);
  .miq.fn.is.error.executable[fnCfgAssemble];

  if[1<>.miq.fn.arity[fnCfgAssemble]; '.miq.h.log.error "Assembler function not of arity 1: ",.miq.fn.print[fnCfgAssemble]];

  $[1=count nf;
      [.is.error.s[nf]                               ; name:nf   ; fn:fnCfgAssemble       ];
      [.is.error.s[nf 0]; .miq.fn.is.executable[nf 1]; name:nf[0]; fn:nf[1]@fnCfgAssemble@]     / TODO check that custom function nf[1] is of arity 1?
  ];

  (` sv nsContext,upper name) set .miq.ns.set[` sv nsApplied,name;fn[name]]
 };

/ Returns the current (applied) configuration from blackboard
.miq.cfg.get:{[nsApplied;sName]
  .is.error.ns[nsApplied];
  .is.error.any[`none`s;sName];

  term:$[any .is[`none`null]@\:sName; nsApplied; ` sv nsApplied,sName];
  $[.miq.ns.is.present[term]; get term; '.miq.h.log.error "No such configuration: ",-3!term]
 };

// =====================
//  Functional querying
// =====================
/ Transforms dictionary into conditional statement
/ NOTE While I will enlist symbols for you automatically, for use with
/      (in), you need to double enlist strings and mixed lists!
.miq.fq.d2cond:{[op;dict]
  .is.error.any[`op`qi]@/:op;
  .is.error.qd[dict];

  .[;
    (where (type each get[dict]) in -11 11h;2);
    enlist
  ] op,'flip(key;get)@\:dict
 };

/ IDEA Transform into unary function applicator?
/ IDEA And create binary and ternanry applicators?
/ IDEA Or even general applicators?
.miq.fq.not:{[idx;cond]
  .is.error.any[`j`jl;idx];
  .is.error.generic[cond];

  @[;idx;(not),] @[;idx;enlist] cond
 };

// ===========
//  Namespace
// ===========
/ Checks
.miq.ns.is.present:{[ns] $[.is.ns[ns]; $[null ns; 0b; not ()~key ns]; 0b]};
.miq.ns.is.context:{[ns] $[.is.ns[ns]; $[.miq.ns.is.present[ns]; .miq.ns.i.context[get ns]; 0b]; .miq.ns.i.context[ns]]};
.miq.ns.is.name   :{[ns] $[.is.ns[ns]; $[.miq.ns.is.present[ns]; not .miq.ns.i.context[get ns]; 0b]; 0b]};
.miq.ns.is.gist   :{[s] $[.is.s[s]; s in key GISTS; 0b]};

.miq.ns.is.ancestor:{[nsContext;nsName]
  .is.error.ns each (nsContext;nsName);

  c:count a:string nsContext;
  (nsContext<>nsName) & a~c sublist string nsName
 };

/ Actions
.miq.ns.type:{[ns]
  .is.error.ns[ns];
  .miq.ns.is.error.present[ns];
  $[.miq.ns.i.context[get ns];`context;`name]
 };

.miq.ns.i.context:{[qd] $[.is.qd[qd]; $[11h~type key qd; $[any key[qd] like "*.*"; 0b; 0h~type get qd]; 0b]; 0b]};

.miq.ns.path.absolute:{[ns]
  .is.error.any[`s`ns;ns];
  $[.is.ns[ns] & not null ns; ns; ` sv system["d"],ns]
 };

.miq.ns.path.relative:{[ns]
  .is.error.any[`s`ns;ns];
  $[.is.ns[ns]; $[.miq.ns.is.ancestor[system"d";ns]; ` sv 2_` vs ns; ns]; ns]
 };

/ IDEA create safe* variations? also not sure this function is actually needed
.miq.ns.set:{[nsName;content]
  .is.error.ns[nsName];
  nsName set content
 };

.miq.ns.delete:{[nsName]
  .is.error.ns[nsName];

  .miq.h.log.info "Deleting name: ",-3!nsName;
  d:`ns`name!(` sv -1_;-1#)@\:` vs nsName;
  $[`~d[`ns]; [.miq.ns.purge[nsName]; `]; ![d`ns;();0b;d`name]]
 };

.miq.ns.purge:{[nsContext]
  .is.error.ns[nsContext];
  .miq.ns.is.error.context[nsContext];

  .miq.h.log.info "Purging context: ",-3!nsContext;
  nsContext set 1#.q
 };

.miq.ns.inject:{[ch;nsContext]
  .is.error[`ch`ns]@'(ch;nsContext);
  .miq.ns.is.error.context[nsContext];

  .miq.h.log.info "Injecting context [",(-3!nsContext),"] to handle [",(-3!ch),"]";
  .miq.h.ipc.sync[ch;({x set y}.;(nsContext;get nsContext))]
 };

/ TODO .miq.ns.rename[] to rename keys? it would accept a dict mapping current!desired values

/ Namespace inspection
.miq.ns.user:{[] (` sv'`,'key`) except .miq.ns.reserved[]}
.miq.ns.reserved:{[] $[C.NS.RESERVED.ENABLE[]; C.NS.RESERVED.LIST[]; 0#`]};

/ IDEA accept fsl that would mean 'all but'? i.e. `:.bb`:.miq will return full tree bar `.bb`.miq
/ IDEA move all tree routines into .miq.ns.tree.* and split tree[] to flat[] and deep[] routines?
.miq.ns.tree:{[nsContext]
  .is.error.any[`none`qd`ns`nsl;nsContext];

  if[.is.qd[nsContext]; .miq.ns.is.error.context[nsContext]; :{$[x;`context;`name]} each .miq.ns.i.tree[nsContext]];

  context:$[.is.none[nsContext]; ``.; (),nsContext];
  .miq.ns.is.error.present'[((),context) except `];

  names:context where .miq.ns.is.name'[context];
  contexts:context except names;
  (names!count[names]#`name),raze {$[x;`context;`name]}''[.miq.ns.i.tree'[contexts]]
 };

.miq.ns.i.tree:{[ns]
  $[ns~`.       ; n!(count n:` sv'`.,'key`.)#0b;
    ns~`        ; ,[;raze .z.s'[n]] n!(count n:.miq.ns.user[])#1b;
    .is.ns[ns]  ; (!).(` sv'ns,'key@;get)@\:.z.s[get ns];
                  [
                    c:where tree:.miq.ns.i.context'[ns];
                    tree,$[.is.empty[c];();raze{(` sv'x,'key[y])!get[y]}'[c;.z.s'[ns c]]]
                  ]
  ]
 };

/ IDEA allow pruning of dictionaries so I can recursively remove stuff and simply reassign?
.miq.ns.trim:{[slLeaf;qd]
  .is.error[`sl`qd]@'((),slLeaf;qd);
  .is.error[`qsl`sl]@'(key;get)@\:qd;

  _[;qd] (where `name=qd) where (')[last;vs[`]]'[where `name=qd] in\: (),slLeaf
 };

.miq.ns.prune:{[slBranch;qd]
  .is.error[`sl`qd]@'((),slBranch;qd);
  .is.error[`qsl`sl]@'(key;get)@\:qd;

  branches:` vs'(),slBranch;
  contexts:where `context=qd;

  contexts:contexts where (|/) {x~/:y}'[branches;(neg count each branches)#/:\:` vs'contexts];
  _[;qd] key[qd] where (key[qd] in contexts) | any each key[qd] like/:\:string[contexts],\:".*"
 };

/ Gist
.miq.ns.gist.new:{[nsContext]
  name:`$"gist.",string -6!(+:;ITER.GIST;1);
  cont:$[.is.none[nsContext]; ``.; (),nsContext];
  time:.z.p;
  gist:{x!(')[.miq.ns.gist.i.new;get]'[x]} exec name from group .miq.ns.tree[nsContext];
  GISTS upsert (name;cont;time;gist);

  name
 };

.miq.ns.gist.i.new:{[x]
  t:.is.type[x];
  $[.is.empty[x]               ;()                       ;
    t in `nyi`generic          ;()                       ;
    t in `qd`qdl`qk`qkl`qt`qtl ;count x                  ;
    t in `qp`qpl               ;`parted                  ;
    t  ~ `code                 ;md5 (raze/) string get x ;
                                md5 (raze/) string x
  ]
 };

.miq.ns.gist.delete:{[sName] .miq.ns.is.error.gist[sName]; delete from GISTS where name=sName; sName};
.miq.ns.gist.get   :{[sName] .miq.ns.is.error.gist[sName]; GISTS[sName][`gist]};
.miq.ns.gist.info  :{[sName] $[.is.none[sName]; GISTS[]; [.miq.ns.is.error.gist[sName];  exec from GISTS[] where name=sName]]};

.miq.ns.gist.diff:{[sName1;sName2]
  .miq.ns.is.error.gist'[(sName1;sName2)];

  .miq.h.log.info "Generating gist diff: ",-3!`gist1`gist2!(sName1;sName2);
  gist1:GISTS[sName1][`gist];
  gist2:GISTS[sName2][`gist];

  result:`matched`changed`removed`added!"SSSS"$\:();
  result[`removed]:key[gist1] except key[gist2];
  result[`added  ]:key[gist2] except key[gist1];

  common:key[gist1] inter key[gist2];
  result,group {`changed`matched x~y}.'flip common#/:(gist1;gist2)
 };

/ TODO implement .miq.ns.snap[] and .miq.ns.restore[]? It could be an enabler for AUGMENT section in test module

// ============
//  Filesystem
// ============
/ TODO spinoff into fs module? tmp is dependent, and module partially too on file.split[] and file.join[] calls
/ Checks
.miq.fs.is.present :{[fs] not null .miq.fs.type[fs]};
.miq.fs.is.file    :{[fs] `file ~  .miq.fs.type[fs]};
.miq.fs.is.dir     :{[fs] `dir  ~  .miq.fs.type[fs]};
.miq.fs.is.softlink:{[fs] .is.error.fs[fs]; .miq.fs.is.error.present[fs]; "l"~(first/)system"ls -l ",1_string[fs]};
.miq.fs.is.absolute:{[fs] .is.error.fs[fs]; "/"~string[fs][1]};
.miq.fs.is.relative:{[fs] not .miq.fs.is.absolute[fs]};
.miq.fs.is.ancestor:{[fsDir;fs] .is.error.fs each (fsDir;fs); c:count a:string fsDir; (fsDir<>fs) & a~c sublist string fs};

/ Conversion
.miq.fs.to.absolute:{[fs]
  .is.error.any[`fs`fsl;fs];
  $[.is.atom[fs];
    .miq.fs.path.i.absolute[-1!`$system"cd";fs];
    .miq.fs.path.i.absolute[-1!`$system"cd"]'[fs]
  ]
 };

.miq.fs.to.relative:{[fs]
  .is.error.any[`fs`fsl;fs];
  cd:-1!`$system"cd";

  $[.is.atom[fs];
    .miq.fs.path.i.relative[cd;.miq.fs.path.i.absolute[cd;fs]];
    .miq.fs.path.i.relative[cd]'[.miq.fs.path.i.absolute[cd]'[fs]]
  ]
 };

// General routines
.miq.fs.type:{[fs]
  .is.error.fs[fs];
  $[()~k:key fs; `; -11h~type k; `file; `dir]
 };

.miq.fs.tree:{[fs]
  .miq.fs.is.error.dir[fs];
  .miq.fs.i.tree $["/"~last string fs;`$-1_string fs;fs]
 };

.miq.fs.i.tree:{[fs]
  content:` sv'fs,'key fs;
  t:content!(')[type;key] each content;
  files:where -11h=t;
  dirs :where  11h=t;
  ((1#fs)!(1#`dir)),(files!count[files]#`file),raze .z.s'[dirs]
 };

/ Recursive rm, fs tree in order of deletion
.miq.fs.rm:{[fs]
  .is.error.fs[fs];
  .miq.fs.is.error.present[fs];

  .miq.h.log.info "Recursive deletion of: ",-3!fs;
  .miq.h.log.info "Current path: ",-3!-1!`$system"cd";
  $[.miq.fs.is.file[fs];
    (1#.miq.fs.file.i.rm[fs])!1#`file;
    .miq.fs.i.rm[fs]
  ]
 };

.miq.fs.i.rm:{[fs]
  content:reverse .miq.fs.tree[fs];
  {[i;t]
    $[t=`file; .miq.fs.file.i.rm[i]; .miq.fs.dir.i.rm[i]];
  }.'flip(key;get)@\:content;
  content
 };

// Path manipulation
.miq.fs.path.absolute:{[fs1;fs2]
  .is.error.fs[fs1];
  .is.error.any[`fs`fsl;fs2];

  root:$[.miq.fs.is.absolute[fs1]; fs1; .miq.fs.to.absolute[fs1]];

  $[.is.atom[fs2];
    .miq.fs.path.i.absolute[root;fs2];
    .miq.fs.path.i.absolute[root]'[fs2]
  ]
 };

.miq.fs.path.i.absolute:{[fs1;fs2]
  if[.miq.fs.is.absolute[fs2]; :fs2];
  part:`$1_"/" vs string[fs1],"/",1_string fs2;
  ` sv `:,{x where not 1=sum(::;next)@\:`..=x}/[part except ``.]
 };

.miq.fs.path.relative:{[fs1;fs2]
  .is.error.fs[fs1];
  .is.error.any[`fs`fsl;fs2];

  / TODO you need to turn root into cannonical path; this fn may fail if root is absolute but contains `..
  / TODO .miq.fs.path.unwind[] ?
  root:$[.miq.fs.is.absolute[fs1]; fs1; .miq.fs.to.absolute[fs1]];

  $[.is.atom[fs2];
    .miq.fs.path.i.relative[root;.miq.fs.path.i.absolute[-1!`$system"cd";fs2]];
    .miq.fs.path.i.relative[root]'[.miq.fs.path.i.absolute[-1!`$system"cd"]'[fs2]]
  ]
 };

.miq.fs.path.i.relative:{[fs1;fs2]
  path:(`$"/" vs string fs1) except ``.;
  file:(`$"/" vs string fs2) except ``.;

  c:min count@'(path;file);
  extra:count[path]-common:?[;0b](=). c#'(path;file);
  parts:(extra#`..),(common _ file);
  `$":","/" sv string $[.is.empty[parts]; 1#`.; parts]
 };

.miq.fs.path.common:{[fs1;fs2]
  .is.error.fs@\:(fs1;fs2);

  p1:(`$"/" vs string fs1) except ``.;
  p2:(`$"/" vs string fs2) except ``.;
  c:min count@'(p1;p2);
  common:a?[;0b](=). c#'(p1;p2);
  $[common;sv[`];::] common#p1
 };

/ TODO 10x slower than system"realpath ..."; implement alternative cross-platform solutions
.miq.fs.path.real:{[fs]
  .is.error.fs[fs];
  .miq.fs.is.error.present[fs];

  if[.miq.system.is.command[`realpath]; :-1!`$first system"realpath ",1_string fs];

  part:except[;`] `$"/" vs string fs;
  real:$[`:~first part;[part:1_part;`:];0#`];
  while[count part;
    real:-1!` sv real,first part;
    part:1_part;
    if[.miq.fs.is.softlink[real];
      path:-1!`$last " -> " vs first system"ls -l ",1_string real;
      real:$[.miq.fs.is.absolute[path]; path; `:.~first` vs real; path; ` sv (first` vs real),`$1_string path]
    ];
  ];

  part:except[;``.] `$"/" vs string real;
  ` sv {if[(`..~x 0)|((":"~string[x 0][0])&(`..~x 1));:x]; x where not 1=sum(::;next)@\:`..=x}/[part]
 };

// File manipulation
.miq.fs.file.split:{[sFile]
  .is.error.s[sFile];
  $["." in string sFile;
    (` sv -1_;first -1#)@\:` vs sFile;
    (sFile;`)
  ]
 };

/ Given joins file name parts into full name, even if it has no extension
.miq.fs.file.join:{[slNameExt]
  .is.error.sl[slNameExt];
  ` sv $[null last slNameExt; -1_; (::)] slNameExt
 };

.miq.fs.file.new:{[fsDir;sName]
  .is.error[`fs`s]@'(fsDir;sName);
  (` sv fsDir,sName) 0: ()
 };

.miq.fs.file.rm:{[fs]
  .is.error.fs[fs];
  .miq.fs.is.error.file[fs];
  .miq.fs.file.i.rm[fs]
 };

.miq.fs.file.i.rm:{[fs]
  .miq.h.log.info "Deleting file: ",-3!fs;
  hdel fs
 };

/ TODO file copy utilising read1(f;o;n)
/.miq.fs.file.copy:{[fsFrom;fsTo]};

// Directory manipulation
.miq.fs.dir.content:{[fs]
  .miq.fs.is.error.dir[fs];
  c!.miq.fs.type each ` sv'fs,'c:key[fs]
 };

.miq.fs.dir.new:{[fsDir;slName]
  .is.error[`fs`sl]@'(fsDir;(),slName);
  dir:` sv fsDir,slName;
  hdel (` sv dir,`miq) 0: ();
  dir
 };

.miq.fs.dir.rm:{[fs]
  .is.error.fs[fs];
  .miq.fs.is.error.dir[fs];
  .miq.fs.dir.i.rm[fs]

 };

.miq.fs.dir.i.rm:{[fs]
  .miq.h.log.info "Deleting directory: ",-3!fs;
  hdel fs
 };

/ TODO directory copy utilising .miq.fs.file.copy
/.miq.fs.dir.copy:{[fsFrom;fsTo]};

// ======================
//  TMP storage handling
// ======================
/ IDEA tmp should be an independent module?
.miq.tmp.is.enabled:{[] C.TMP.ENABLE[]};
.miq.tmp.is.tmp    :{[fs] .miq.fs.is.ancestor[TMP.PATH[];fs]};

.miq.tmp.enable:{[]
  if[not .miq.fs.is.dir TMP.PATH[];
    $[C.TMP.AUTOCREATE[];
        .miq.fs.dir.new TMP.PATH[];
        '.miq.h.log.error "TMP directory does not exist: ",-3!TMP.PATH[]
    ]
  ];
  C.TMP.ENABLE set 1b;
 };

.miq.tmp.clear:{[]
  .miq.h.log.info "Clearing out TMP directory";
  .miq.fs.rm each ` sv TMP.PATH[],'key TMP.PATH[]
 };

.miq.tmp.disable:{[]
  C.TMP.ENABLE set 0b;
  if[C.TMP.AUTOCLEAR[]; .miq.tmp.clear[]]
 };

.miq.tmp.file.new:{[sName]
  .miq.tmp.is.error.enabled[];
  .is.error.s[sName];
  .miq.fs.file.new[.miq.tmp.dir.new[sName];sName]
 };

.miq.tmp.file.rm:{[fsName]
  .miq.tmp.is.error.enabled[];
  .is.error.fs[fsName];
  .miq.tmp.is.error.tmp[fsName];

  .miq.h.log.info "Deleting TMP file: ",-3!fsName;
  $[.miq.fs.is.file[fsName];
      .miq.fs.file.rm[fsName];
      .miq.h.log.warn "No TMP file to delete: ",-3!fsName
  ];

  d:`dir`file!(first;last)@\:` vs fsName;
  if[.miq.fs.is.dir[d`dir]&0~count key d[`dir];
    .miq.h.log.info "Deleting empty TMP directory: ",-3!d[`dir];
    hdel d[`dir];
  ]
 };

.miq.tmp.dir.path:{[] TMP.PATH[]};

.miq.tmp.dir.new:{[sName]
  .miq.tmp.is.error.enabled[];
  .is.error.s[sName];
  .miq.tmp.is.error.enabled[];
  .miq.fs.dir.new[TMP.PATH[];` sv sName,`$8?.miq.u.text.Aan]
 };

.miq.tmp.dir.rm:{[fsName]
  .miq.tmp.is.error.enabled[];
  .is.error.fs[fsName];
  .miq.tmp.is.error.tmp[fsName];
  if[not .miq.fs.is.dir[fsName]; .miq.h.log.warn "No TMP directory to delete: ",-3!fsName; :()];

  .miq.h.log.info "Deleting TMP directory: ",-3!fsName;
  .miq.fs.dir.rm[fsName];
 };

// =======================================
//  k/q parsing and primitive recognition
// =======================================
// Loading
/ Returns file / list of strings split into interpretable code
/ Accepts file symbol, symbol, and a generic list (why?)
k).miq.qk.load:{[x]
  if[@x;l:x;x:0:x];
  x:("#!"~2#*x)_x:-1!'x;
  :.miq.qk.process[.miq.qk.i.fullPath l;0N;x]
 };

/ TODO figure out how to define code valid for multiple kdb version without pre-processor
/ TODO reorder formal parameters to reflect kdb 4.0 changes (it's better that way anyway)
/ Returns interpretable code
/ Accepts (l) name of the source file
/ Accepts (x) code as list of strings or "\n" delimited string
k).miq.qk.i.process.3.6:{[l;n;x]
  $[^l;l:();l:`$1_$-1!l];
  if[10h~@x;x:`\:x];
  i:(&|1^\|0N 0 1@"/ "?*:'(v:x i),'"/")_i:&~|':(b?-1)#b:+\-/x~\:/:+,"/\\";
  :$[#l;+(;$(#i)#l;$[^n;1+*:'i;n])@;]@"\n"/:'x i
 };

k).miq.qk.i.process.4.0:{[l;n;x]
  $[^l;l:();l:`$1_$-1!l];
  if[10h~@x;x:`\:x];
  i:(&|1^\|0N 0 1@"/ "?*:'(v:x i),'"/")_i:&~|':(b?-1)#b:+\-/x~\:/:+,"/\\";
  :$[#l;+($(#i)#l;$[^n;1+*:'i;n];)@;]@"\n"/:'x i
 };

.miq.qk.process:$[.z.K<4.0f;.miq.qk.i.process.3.6;.miq.qk.i.process.4.0];

k).miq.qk.i.fullPath:{[l]
  file:`$1_$-1!l;
  :$["/"~*$file;file;`$(."\\cd"),"/",$file]
 };

// Function (un)boxing and breaking into statements
.miq.qk.fn.wrap:{[slParam;clCode]
  .is.error[`sl`cl]@'((),slParam;clCode);
  code:$[k:"k)"~2#clCode; 2_; ::] clCode;
  $[k; "k)",; ::] "{[",(";"sv string (),slParam),"]",code,$["\n"~last code;" ";""],"}"
 };

.miq.qk.fn.strip:{[clCode]
  .is.error.cl[clCode];
  .is.error.fn[first .miq.qk.parse[clCode]];
  code:$[k:"k)"~2#clCode; 2_; ::] clCode;

  code:(1+first ss[code;"{"])_(last ss[code;"}"])#code;
  code:((" "=code)?0b)_code;
  if["["~first code; code:(1+first ss[code;"]"])_code];
  $[k; "k)",; ::] code
 };

.miq.qk.fn.break:{[fn]
  .is.error.fn[fn];

  source:.miq.fn.info[fn][`source];
  lines:"\n"vs s:.miq.qk.fn.strip[source];
  lines:lines where not all each " " = lines;
  parsed:-5!s;
  if[";"~first parsed; parsed:1_parsed];

  if[(1~count[lines])|count[parsed]~count[lines]; :lines];

  p:{x:-5!x; $[";"~first x;x 1;x]};
  i:0;
  while[i<count[lines];
    $[parsed[i]~@[p;lines[i];{0b}];
        i+:1;
        [lines[i]:lines[i],lines[i+1]; lines:((i+1)#lines),(i+2)_lines]
    ];
  ];
  lines
 };

// Parsing & Interpretation
.miq.qk.parse:{[clLine] .is.error.cl[clLine]; parse clLine};

.miq.qk.interpret:{[cLanguage;code]
  if[not -10h~type cLanguage; '.miq.h.log.error "c type expected: ",-3!cLanguage];  / NOTE Don't replace, this routine has to work independently of miQ
  if[not cLanguage in "qk"; '.miq.h.log.error "Cannot interpret language: ",-3!cLanguage];

  cLanguage code
 };

/ Expects a list of code lines
.miq.qk.inspect:{[code]
  ns:system"d";
  detail:$[.is.cl code;
    .miq.qk.i.inspect code;
    .miq.qk.i.inspect each code
  ];
  system"d ",string ns;

  detail
 };

.miq.qk.describe:{[nsFunction;sReturn]
  .is.error.any[`s`ns;nsFunction];
  .is.error.s[sReturn];
  .miq.h.log.info "Adding function description: ",-3!`function`return!(nsFunction;sReturn);
  QK.DESCRIPTION upsert (nsFunction;@[get;nsFunction;()];sReturn);
  nsFunction
 };

/ TODO Improve resolution
/ Parse individual lines to uncover details
.miq.qk.i.inspect:{[line]
  tree:$[10h~type (),line; .miq.qk.parse (),line; line];
  if[";"~first tree; :(,'/).z.s'[1_tree]];

  context:enlist 1#system"d";
  `context`definition`datatype`class!context,enlist each $[
    (::) ~ tree                ; (```);
    1<count first tree         ; (```);
    first[tree] ~ (:)          ;
    [
      definition:.miq.ns.path.absolute[tree 1];
      datatype:.miq.qk.i.datatype[tree 2;0];
      classes:.miq.syntax.name.classify[definition];
      class:$[`variable~datatype; classes[0]; `function~datatype; classes[1]; `unknown];
      (definition;datatype;class)
    ];
    first[tree] ~  system      ; [if[{$[0h~type x; any .z.s[x 1]; "d "~2#x]} tree[1]; @[-6!;tree;{.miq.h.log.warn "Failed to change directory!"}]];(``function`system)];
    first[tree] in .miq.u.op.u ; (``function`operator.unary);
    first[tree] in .miq.u.op.b ; (``function`operator.binary);
    first[tree] in .miq.u.op.t ; (``function`operator.ternary);
                                 (```)
  ]
 };

/ Resolve datatype of an assinable
.miq.qk.i.datatype:{[object;args]
  o:object;
  if[.is.any[`s`ns;o];
    o:.miq.ns.path.absolute[o];
    if[not ()~key o; o:get o]
  ];

  $[.miq.fn.is.executable[o] ; $[(args+count[object]-1)~$[a:.miq.fn.arity[o];a;a+1]; `unknown^exec first return from QK.DESCRIPTION[] where body~'o; `function];
    (type[o]~0h) & 0<count[o]; .z.s[first o;args+count[object]-1];
    type[o] < 20h            ; `variable;
                               `unknown
  ]
 };

// ========
//  Syntax
// ========
.miq.syntax.name.classify:{[sName]
  .is.error.any[`s`ns;sName];

  (
    $[.miq.syntax.name.is.constant[sName]; `constant; `literal]
   ,{$[.is.empty[x];x;first x]} {x where .miq.syntax.name.is[x]@\:y}[`hook`handler`template;sName]
   ,$[.miq.syntax.name.is.internal[sName]; `internal; `public]
  )
 };

.miq.syntax.name.is.constant:{[x] .is.error.any[`s`ns;x]; n~upper n:last` vs x};
.miq.syntax.name.is.literal :{[x] .is.error.any[`s`ns;x]; not .miq.syntax.name.is.constant[x]};
.miq.syntax.name.is.internal:{[x] .is.error.any[`s`ns;x]; x like "*.i.*"};
.miq.syntax.name.is.public  :{[x] .is.error.any[`s`ns;x]; not .miq.syntax.name.is.internal[x]};
.miq.syntax.name.is.handler :{[x] .is.error.any[`s`ns;x]; x like "*.h.*"};
.miq.syntax.name.is.template:{[x] .is.error.any[`s`ns;x]; x like "*.t.*"};
.miq.syntax.name.is.hook    :{[x] .is.error.any[`s`ns;x]; (x like "*.z.*") & not x like "*.z.dd.*"};

// ==========
//  Function
// ==========
// Injecting & Injection templates
/ TODO document this family of functions
/ IDEA does code injection belong to debugger module?
.miq.fn.template.add:{[sTemplate;jNum;clCode]
  .is.error[`s`j`cl]@'(sTemplate;jNum;clCode);
  template:FN.TEMPLATE[sTemplate];
  template:$[(::)~template; (); template],enlist[jNum]!enlist[clCode];
  .miq.fn.template.i.set[sTemplate] asc[key template]#template;
 };

.miq.fn.template.remove:{[sTemplate;jlNum]
  .is.error[`s`jl]@'(sTemplate;(),jlNum);
  template:((),jlNum)_.miq.fn.template.i.get[sTemplate];
  $[count template;
      .miq.fn.template.i.set[sTemplate;template];
      FN.TEMPLATE set sTemplate _ FN.TEMPLATE[]
  ];
 };

.miq.fn.template.show:{[x] FN.TEMPLATE[x]};

.miq.fn.template.i.get:{[sTemplate]
  .is.error.s[sTemplate];
  if[not sTemplate in key FN.TEMPLATE[]; '.miq.h.log.error "No such function template: ",-3!sTemplate];
  FN.TEMPLATE[sTemplate]
 };

.miq.fn.template.i.set:{[sTemplate;qdTemplate]
  .is.error[`s`qd]@'(sTemplate;qdTemplate);
  FN.TEMPLATE set FN.TEMPLATE[],enlist[sTemplate]!enlist[qdTemplate];
  sTemplate
 };

.miq.fn.inject:{[fn;sTemplate]
  function:$[.is.ns[fn];@[get;fn;{fn}];fn];
  .is.error.any[`op`fn`fp`qc`qi;fn];
  if[not .is.fn[function]; '.miq.h.log.error "Can inject code to simple function only: ",-3!.miq.fn.print[fn]];

  if[.is.ns[fn];
    $[fn in key FN.ORIGINAL[];
      function:FN.ORIGINAL[fn];
      FN.ORIGINAL set FN.ORIGINAL[],(1#fn)!enlist function
    ];
  ];

  .miq.fn.is.error.locked[function];
  def:`signature`namespace`file`line`source#.miq.fn.info[function];
  /if[def[`source]~"locked"; .miq.h.log.warn "Can't inject code to locked function: ",-3!def`name; :()];

  code:.miq.qk.fn.strip[def`source];
  if[k:"k)"~2#code; code:2_code];

  template:.miq.fn.template.i.get[sTemplate];
  template:" ",/:template,\:";   /  template:",(-3!sTemplate)," \n";
  b:{raze (key[x] where key[x]<0)#x}[template];
  e:{raze (key[x] where key[x]>0)#x}[template];

  lines:"\n"vs code; r:$[(::)~parse last lines;-2;-1];
  code:b,$[1<count lines; raze(r _lines),"\n",e,r#lines; code,e];
  code:$[k;"k)";""],$[.is.s[fn];string[fn],":";""],.miq.qk.fn.wrap[def`signature;code];

  / TODO return to original namespace
  r:.miq.qk.interpret["q"] each ("\\d .",string def[`namespace];(code;1_string def`file;def`line));
  $[.is.s[fn]; fn; r]
 };

.miq.fn.reset:{[sName]
  .is.error.s[sName];
  if[not sName in 1_key FN.ORIGINAL[]; .miq.h.log.warn "No function to reset: ",-3!sName; :()];
  sName set FN.ORIGINAL[sName];
  FN.ORIGINAL set sName _ FN.ORIGINAL[];
  sName;
 };

// Checks
.miq.fn.is.address   :{[fn] $[.miq.ns.is.name[fn]; .is.any[`op`fn`fp`qc`qi`code;get fn]; 0b]};
.miq.fn.is.executable:{[fn] $[.miq.fn.is.address[fn]; 1b; .is.any[`op`fn`fp`qc`qi`code;fn]]};
.miq.fn.is.locked    :{[fn] $[.miq.fn.is.executable[fn]; (~)["locked"] $[.is.qs[fn]; get fn; fn]; 0b]};
.miq.fn.is.arity     :{[j] $[.is.j[j]; j in 0 1 2 3 4 5 6 7 8; 0b]};

// Razing and composing
.miq.fn.flatten:{[fn]
  .miq.fn.is.error.executable[fn];
  $[.is.fp[fn]; $[.is.fp first c:get fn; .z.s[first c],1_c; c];
    .is.qc[fn]; {@[x;where (.is.type'[x])in`fp`qc;.miq.fn.flatten] }.miq.fn.decompose[fn];
                fn
  ]
 };

.miq.fn.decompose:{[fn]
  .miq.fn.is.error.executable[fn];
  $[.is.fp[fn]; .z.s[first get fn],enlist 1_get fn;
    .is.qc[fn]; $[.is.qc first c:get fn; .z.s[first c],1_c; c];
                fn
  ]
 };

.miq.fn.compose:{[fn]
  .miq.fn.is.error.executable[first fn];
  $[1~count fn; fn;
    fn~raze fn; '[;]/[(first[fn],raze 1_fn)];
                ./[first fn;1_fn]
  ]
 };

// Information on functions, operators and their derivations
.miq.fn.info:{[fn]
  .miq.fn.is.error.executable[fn];
  $[.miq.fn.is.address[fn]; fn:get address:.miq.ns.path.absolute[fn]; address:`];

  $[.is.fp[fn];
    [parts:.miq.fn.flatten[fn]; function:first parts; args:1_parts];
    [                           function:fn         ; args:()     ]
  ];

  info:$[
    .is.fn[function]  ; .miq.fn.i.fn[function]  ;
    .is.fp[function]  ; .z.s[function]          ;
    .is.qc[function]  ; .miq.fn.i.qc[function]  ;
    .is.code[function]; .miq.fn.i.code[function];
                        .miq.fn.i.op[function]
  ];

  / Cater for code primary address and `fp`qc address inheritance
  if[null info[`address]; info[`address]:address];

  / Cater for qc recursion, where the last qc component could be a projection
  if[`filled in key[info]; info[`parameters]:info[`parameters] except info[`filled]];

  args:.miq.fn.i.args[info`parameters;args];
  info,args,`function`class!(fn;.miq.fn.i.class[fn])
 };

.miq.fn.i.op:{[op]
  / default value of 2 is meant for applied iterators, types 106h - 111h
  parameters:$[op in .miq.u.op.u; 1; op in .miq.u.op.b; 2; op in .miq.u.op.t; 3; 2]#`x`y`z;
  /parameters:(2^1+first where op in/: .miq.u.op[`u`b`t])#`x`y`z;
  locals    :0#`;
  namespace :`  ;
  globals   :0#`;
  name      :`  ;
  file      :`  ;
  line      :0N ;
  source    :op ;
  address   :`  ;

  `name`parameters`signature`namespace`locals`globals`file`line`source`address!
    (name;parameters;parameters;namespace;locals;globals;file;line;source;address)
 };

.miq.fn.i.fn:{[fn]
  info:get fn;

  parameters:           info[1];
  locals    :           info[2];
  namespace :     first info[3];
  globals   :1_         info[3];
  name      :   `$first -4#info;
  file      :-1!`$first -3#info;
  line      :     first -2#info;
  source    :     first -1#info;
  address   :`                 ;

  if[name~`$(); name:`];
  if[not null namespace; namespace:` sv`,namespace];

  `name`parameters`signature`namespace`locals`globals`file`line`source`address!
    (name;parameters;parameters;namespace;locals;globals;file;line;source;address)
 };

.miq.fn.i.code:{[code]
  info:`arity`address`file`line`name!get[code];

  parameters:info[`arity]#`x`y`z`a`b`c`d`e;
  locals    :0#`            ;
  namespace :`              ;
  globals   :0#`            ;
  name      :`$info`name    ;
  file      :-1!`$info`file ;
  line      :info`line      ;
  source    :"code"         ;
  address   :`$info`address ;

  `name`parameters`signature`namespace`locals`globals`file`line`source`address!
    (name;parameters;parameters;namespace;locals;globals;file;line;source;address)
 };

/ TODO Improve composition source creation, i.e. evaluating of source of .miq.handler.i.export@ does not return identical composition to original
.miq.fn.i.qc:{[qc]
  info:.miq.fn.info[last .miq.fn.decompose qc];

  fns:"," sv string .miq.fn.decompose[qc];
  source:"'[;]/[",fns,"]";

  info[`source]:source;
  info
 };

/ TODO Incorrectly filled arguments!!!
/ q)r:{[a;b;c;d;e] 0N!(a;b;c;d;e); a+b+c+d+e}
/ q)r:r[;2][3]
/ q)t:.miq.fn.compose ({x+y}[1];{x+y}[2];r)
/ q)t[4;5;6]
/ 3 2 4 5 6
/ q).miq.fn.info t[4;5]
/ ...

.miq.fn.i.args:{[parameters;args]
  arguments :args;
  indices   :where not .is.elision each arguments;
  filled    :parameters@indices;
  arguments :arguments@indices;
  signature :parameters except filled;
  arity     :$[null signature[0]; 0; count signature];

  `filled`arguments`signature`arity!(filled;arguments;signature;arity)
 };

// Utilities
.miq.fn.name:{[fn]
  .miq.fn.is.error.executable[fn];
  fn:$[.miq.fn.is.address[fn]; get fn; fn];

  .miq.fn.i.name[fn]
 };

.miq.fn.i.name:{[fn]
  t:.is.type[fn];
  $[`op~t   ;`;
    `fn~t   ;`$first -4#get fn;
    `fp~t   ;.z.s first .miq.fn.flatten[fn];
    `qc~t   ;.z.s last get fn;
    `qi~t   ;.z.s get fn;
    `code~t ;`$get[fn][4];
  ]
 };

.miq.fn.arity:{[fn]
  .miq.fn.is.error.executable[fn];
  fn:$[.miq.fn.is.address[fn]; get fn; fn];

  .miq.fn.i.arity[fn;0]
 };

.miq.fn.i.arity:{[fn;args]
  t:.is.type[fn];
  ($[`op~t   ;$[.is.binary[fn];2;.is.ternary[fn];3;1];
     `fn~t   ;count get[fn][1] except `;
     `fp~t   ;.z.s . {(x 0;count where not .is.elision'[1_x])} .miq.fn.flatten[fn];
     `qc~t   ;.z.s[;0] last get fn;
     `qi~t   ;2;
     `code~t ;get[fn][0];
  ])-args
 };

/ TODO source of compositions is broken, i.e.: .miq.fn.info[`.q.ceiling][`source] will be invalid as I don't handle nested compositions well
.miq.fn.arguments:{[fn] (!). .miq.fn.info[fn][`filled`arguments]};
.miq.fn.namespace:{[fn]      .miq.fn.info[fn][`namespace]       };
.miq.fn.source   :{[fn]      .miq.fn.info[fn][`source]          };

.miq.fn.class:{[fn]
  .miq.fn.is.error.executable[fn];
  fn:$[.miq.fn.is.address[fn]; get fn; fn];

  .miq.fn.i.class[fn]
 };

.miq.fn.i.class:{[fn]
  $[.is.fp[fn]      ;` sv .z.s[first .miq.fn.flatten[fn]],`projection;
    .is.fn[fn]      ;`function         ;
    .is.qc[fn]      ;`composition      ;
    .is.qi[fn]      ;`iterator         ;
    .is.code[fn]    ;`code             ;
    .is.unary[fn]   ;`operator.unary   ;
    .is.binary[fn]  ;`operator.binary  ;
    .is.ternary[fn] ;`operator.ternary ;
                     '"this should never happen"
  ]
 };

.miq.fn.print:{[fn]
  info:`name`signature#.miq.fn.info[fn];
  $[count n:info`name;-3!n;"lambda"],$[101h~type info`signature; ""; "[",(";" sv -3!'info`signature),"]"]
 };

.miq.fn.copy:{[sName;fn]
  .is.error.any[`s`ns;sName];
  .miq.fn.is.error.executable[fn];
  fn:$[.miq.fn.is.address[fn]; get fn; fn];

  .miq.fn.i.copy[sName;fn]
 };

.miq.fn.i.copy:{[sName;fn]
  d:.miq.fn.decompose[fn];

  if[not l:.is.list[d];d:(),d];
  t:type each d;

  if[count w:where t=100h       ; d:@[d;w;:;{-6!(:;x;.miq.u.copy y); get x}[sName] each d w]];
  if[count w:where t in 104 105h; d:@[d;w;:;.z.s[sName]'[d w]]];
  if[count w:where t=0h         ; d:@[d;w;:;.z.s[sName]''[d w]]];

  if[not l;d:first d];
  -6!(:;sName;f:.miq.fn.compose[d]);
  f
 };

.miq.fn.atomic:{[fn;arg]
  .miq.fn.is.error.executable[fn];
  fn:$[.miq.fn.is.address[fn]; get fn; fn];

  if[1<>.miq.fn.arity[fn]; '.miq.h.log.error "Function of arity 1 required: ",.miq.fn.print[fn]];
  .miq.fn.i.atomic[fn;arg]
 };

.miq.fn.i.atomic:{[fn;arg]
  $[.is.qk[arg]          ; (!). .z.s[fn]@/:(key;get)@\:arg;
    type[arg] in 0 98 99h; .z.s[fn]'[arg];
                           fn[arg]
  ]
 };

// ===========
//  Utilities
// ===========
/ Textual
.miq.u.text.A:.Q.A;
.miq.u.text.a:.Q.a;
.miq.u.text.n:.Q.n;
.miq.u.text.Aa:.Q.A,.Q.a;
.miq.u.text.An:.Q.A,.Q.n;
.miq.u.text.an:.Q.a,.Q.n;
.miq.u.text.Aan:.Q.A,.Q.a,.Q.n;
.miq.u.text.ascii:"c"$til 256;
.miq.u.text.printable:.miq.u.text.ascii[9 10 13,32+til 95];

.miq.u.text.clean:{[cl] .is.error.cl[cl]; cl inter .miq.u.text.printable};

.miq.u.text.uncomment:{[c;clLine]
  .is.error.any[`c`cl;c];
  .is.error.cl[clLine];

  $[last[c]~first[clLine];
      0#clLine;
    1~count c;
      (clLine?first[c])#clLine;
      $[0^w:first w where(clLine -1+w:where last[c]=clLine) in -1_c; w#clLine; clLine]
  ]
 };

.miq.u.text.dedup:{[chars;clLine]
  .is.error.any[`c`cl;chars];
  .is.error.cl[clLine];

  $[1~count chars;
    c:where 1<>deltas w:where not first[chars]=clLine;
    c:where 1<>deltas w:where not (|/) chars=\:clLine
  ];
  first[chars] sv clLine c cut w
 };

.miq.u.text.upper:{[x]
  t:type[x];
  $[t in -10 10h       ; .Q.Aa x;
    t in -11 11h       ; `$.Q.Aa string x;
    t within 20 76h    ; `$.Q.Aa string get x;
    .is.qk[x]          ; .z.s[key x]!.z.s[get x];
    t in 0 98 99h      ; .z.s'[x];
                         x
  ]
 };

.miq.u.text.lower:{[x]
  t:type[x];
  $[t in -11 -10 10 11h; (_) x;
    t within 20 76h    ; (_) get x;
    .is.qk[x]          ; .z.s[key x]!.z.s[get x];
    t in 0 98 99h      ; .z.s'[x];
                         x
  ]
 };

.miq.u.text.qw:{[cl] .is.error.cl[cl]; "\"",cl,"\""};

/ Numeric
.miq.u.num.null:{(`$'x)!{first x$()}each x} -1_.Q.t except "bx " ;
.miq.u.num.pinf:{(`$'x)!{min   x$()}each x} .Q.t except "bgxhcs ";
.miq.u.num.ninf:{(`$'x)!{max   x$()}each x} .Q.t except "bgxhcs ";
.miq.u.num.inf :.miq.u.num.ninf,'.miq.u.num.pinf;

.miq.u.num.o2d:8 sv 10 vs;    / octal to decimal
.miq.u.num.d2o:10 sv 8 vs;    / decimal to octal

/ Types
.miq.u.q.types:`short$(-76+til 189) except -3 3;
.miq.u.q.atoms:reverse neg -1_5h$where .Q.t<>" ";
.miq.u.q.lists:-1_5h$0,where .Q.t<>" ";
.miq.u.q.info:{[]
  ctypes:`K`char`U`short`int`int64_t`float`double,`$"char*";
  info:1!flip`n`c`name`empty`null!flip{$[x;(x;.Q.t x;key x$();x$();first x$());(0h;"*";`;();::)]} each .miq.u.q.lists;
  info:update numeric:1b from info where c in "hijefpmdznutv";
  info:update ctype:ctypes[0 1 2 1 3 4 5 6 7 1 8 5 4 4 7 5 4 4 4] from info;
  update size:0N 1 16 1 2 4 8 4 8 1 8 8 4 4 8 8 4 4 4 from info
 }[];

.miq.u.miq.atoms  :`b`g`x`h`i`j`e`f`c`s`p`m`d`z`n`u`v`t`ns`fd`fs`fh`cs`ch`op`fn`fp`qc`qi`qs`qd`qt`qk`qp;
.miq.u.miq.lists  :`generic,`$string[.miq.u.miq.atoms],\:"l";
.miq.u.miq.special:`nyi`enum`anymap`mapped`nested`elision`code`foreign;
.miq.u.miq.types  :.miq.u.miq.atoms,.miq.u.miq.lists,.miq.u.miq.special;

/ Operators
.miq.u.ops:{[]
  n:til 256;
  u:,[;1#()] -1_{@[{-9!x,`byte$y}[x];y;{()}]}[-1_-8!(::)]each n;
  b:{@[{-9!x,`byte$y}[x];y;{()}]}[-1_-8!(:)]each n;
  t:{@[{-9!x,`byte$y}[x];y;{()}]}[-1_-8!(')]each n;

  delete from ([n]u;b;t) where {all ()~/:x} each flip (u;b;t)
 }[];

.miq.u.op.u:exec u from .miq.u.ops where not()~/:u;
.miq.u.op.b:exec b from .miq.u.ops where not()~/:b;
.miq.u.op.t:exec t from .miq.u.ops where not()~/:t;

/ magic value
.miq.u.elision:last get (;);

/ ltrim, rtrim, trim replacement
.miq.u.ltrim:{[x] t:type[x]; $[t in 2 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19h; $[null first x; ((null x)?0b)_x; x]; t~0h ; .z.s'[x]; x]};
.miq.u.rtrim:{[x] t:type[x]; $[t in 2 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19h; $[null last x; (neg(reverse null x)?0b)_x; x]; t~0h ; .z.s'[x]; x]};
.miq.u.trim:{[x] .miq.u.ltrim .miq.u.rtrim x};

/ Deep copy of any object
.miq.u.copy:-9!-8!;

/ Do nothing for a set number of miliseconds
.miq.u.sleep:{[jMilis] end:.z.p; .is.error.j[jMilis]; end+:jMilis*1000000; while[.z.p<end;]};

/ Set a random seed
.miq.u.seed:{[] system"S ",string 0x0 sv 4#0x0 vs first -1?0Ng};

/ Attempt to execute function fn jTimes every jInterval miliseconds
.miq.u.wait:{[jTimes;jInterval;fn;args]
  .is.error[`j`j]@'(jTimes;jInterval);
  .miq.fn.is.executable[fn];

  attempts:jTimes;
  while[not last return:@[{(x y;1b)}[fn];args;{(x;0b)}];
    if[0~attempts-:1; '.miq.h.log.error "Wait attempts exceeded: ",-3!`jTimes`jInterval`fn`args!(jTimes;jInterval;fn;args)];
    .miq.u.sleep[jInterval];
  ];
  first return
 };

// Stack frames shenanigans
.miq.u.stack :{[x] $[(::)~x; .z.s -100!x; type x; .z.s[-103!(0b;x)],enlist -101!x; ()]};    / Return full execution stack
.miq.u.nFrame:{[n] .is.error.j[n]; -101!'(n+1) {-103!(0b;x)}/-100!`};                       / N-th frame
.miq.u.self  :{[] (-101!-103!(0b;-100!`))[1;0]};                                            / Return name of function from caller's perspective
.miq.u.caller:{[] (-101!-103!(0b;-103!(0b;-100!`)))[1;0]};                                  / Return name of caller from caller's perspective

/ TODO function to skip over names that match certain patters, i.e. "*.is.*"
/ TODO move stack functionality into its own .miq.stack context and abstract properly?

/ Returns name of user-defined caller from caller's perspective
/ NOTE If the frame retrieved is of a q native function, the following frame will be used
.miq.u.nCaller:{[n]
  .is.error.j[n];
  frame:(n+1) {-103!(0b;x)}/-100!`;     / +1 as we are one frame below caller
  c:$[".q."~(p:-101!frame)[1;0;0 1 2];  / if we hit q internal, go again
        (-101!-103!(0b;frame));
        p
  ];                                    / Caller's name is here [1;0]
  $[2~count c[1;0];();c[1;0]]           / ("q";`) hack on new kdb 4.0 behaviour
 };

/ Return (n)-th function from stack, skipping over patterns defined by cll at most once
.miq.u.nCallerSkip:{[n;cll]
  .is.error.j[n];
  if[.is.cl[cll]; cll:enlist cll];
  .is.error.cl'[cll];

  patterns:cll;
  name:{[f] n:(-101!f)[1;0]; $[(()~n)|2~count n;"";n]};

  / +1 as we are one frame below caller
  frame:(n+1) {-103!(0b;x)}/-100!`;
  while[any match:name[frame] like/:patterns;
    patterns:patterns where not match;
    frame:-103!(0b;frame);
  ];

  name[frame]
 };

/ Return (n)-th user defined function, skipping all q functions
/ TODO Make this into nCallerSkip with unlimited number of skips
/ NOTE This function is way more expensive than .miq.u.nCaller[]
.miq.u.nCallerStrict:{[n]
  .is.error.j[n];
  stack:-100!`;
  depth:n+1;

  while[depth;
    stack:-103!(0b;stack);
    if[not ".q."~(-101!stack)[1;0;0 1 2]; depth-:1];
  ];
  {$[2~count x;"";x]} (-101!stack)[1;0]
 };

// =========
//  Objects
// =========
/ IDEA add 'keys' attribute to objects? i.e. when creating a table, it will be keyed automatically...
/ IDEA derive objects from other objects?
/ IDEA default values for constructor?

/ Checks
.miq.object.is.defined:{[sClass] $[.is.s[sClass]; sClass in key OBJECTS; 0b]};
.miq.object.is.valid  :{[object] $[.is.qd[object]; all `s=.is.type'[key object]; 0b]};
.miq.object.is.object :{[object] $[.miq.object.is.valid[object]; not`~.miq.object.identify[object]; 0b]};

.miq.object.is.class  :{[sClass;object]
  if[not .miq.object.is.defined[sClass] & .miq.object.is.valid[object]; :0b];

  definition:.miq.object.definition[sClass];
  if[not asc[key definition]~asc[key object]; :0b];

  all .miq.object.i.verify[definition;object]
 };

/ Utils
.miq.object.require:{[sClass] .miq.object.is.error.defined[sClass]};
.miq.object.classes:{[] exec class from OBJECTS};

/ Primary operations on objects
.miq.object.signature :{[sClass] .miq.object.is.error.defined[sClass]; OBJECTS[sClass][`signature]};
.miq.object.definition:{[sClass] .miq.object.is.error.defined[sClass]; (!). OBJECTS[sClass][`signature`types]};

.miq.object.construct :{[sClass;values]
  .miq.object.is.error.defined[sClass];
  .is.error.any[`qd`list;values];

  signature:.miq.object.signature[sClass];
  object:$[.is.list[values];
    [
      if[not count[signature]~count[values]; '.miq.h.log.error "Count of values not matching the count of signature: ",-3!`class`signature`values!(sClass;signature;values)];
      signature!values
    ];
    [
      if[not all signature in key values; '.miq.h.log.error "Cannot construct object from dictionary: ",-3!`class`signature`values!(sClass;signature;values)];
      signature#values
    ]
  ];

  if[.miq.object.is.class[sClass;object]; :object];

  definition:.miq.object.definition[sClass];
  bad:where not .miq.object.i.verify[definition;object];
  '.miq.h.log.error "Object construction failed due to invalid values: ",-3!`class`bad!(sClass;bad#object);
 };

.miq.object.tt:{[sClass]
  .miq.object.is.error.defined[sClass];

  flip {[x]
    t:$[.is.miq.type[x]; first .is.miq2q[x]; 0h];
    .miq.u.q.info[;`empty] $[t<0h; neg t; 0h]
  } each .miq.object.definition[sClass]
 };

.miq.object.verify:{[sClass;object]
  .miq.object.is.error.defined[sClass];
  .miq.object.is.error.valid[object];

  definition:.miq.object.definition[sClass];
  common:key[object] inter key[definition];
  {0b}'[object,definition],.miq.object.i.verify[definition;common#object]
 };

.miq.object.i.verify:{[definition;object]
  key[object]!{[def;k;v]
    $[1<count def[k]               ; any .is[def k]@\:v;
      null[def k]                  ; 1b;
      .is.miq.type[def k]          ; .is[def k][v];
      .miq.object.is.defined[def k]; .miq.object.is.class[def k;v];
                                     0b
    ]
  }[definition].'flip(key;get)@\:object
 };

/ Secondary operations on objects
/ TODO gaps in logic, rework to make it work reliably on objects with list values
.miq.object.diff  :{[o1;o2] {where 2<>count each x} each .miq.object.i.prepare[o1;o2]};
.miq.object.inter :{[o1;o2] {where 2 =count each x} each .miq.object.i.prepare[o1;o2]};
.miq.object.except:{[o1;o2] {where (1#0)~/:x} each .miq.object.i.prepare[o1;o2]};
.miq.object.union :{[o1;o2] key each .miq.object.i.prepare[o1;o2]};

.miq.object.i.prepare:{[o1;o2]
  if[not(~). .miq.object.identify each (o1;o2); '.miq.h.log.error "Objects have to be of same class: ",-3!(o1;o2)];
  (')[group;raze] each flip(o1;key[o1]#o2)    / This transformation is reducing the information available to perform set operations
 };

/ Object definition & handling
.miq.object.t.define:{[sClass;slSignature;types] .miq.object.i.define[0b;sClass;slSignature;types]};
.miq.object.t.redefine:{[bRedefine;sClass;slSignature;types] .miq.object.i.define[bRedefine;sClass;slSignature;types]};

.miq.object.i.define:{[bRedefine;sClass;slSignature;types]
  if[count[slSignature]<>count[types];
    '.miq.h.log.error "Signature and types are of different count: ",-3!`signature`types!(slSignature;types)
  ];

  / Don't define object classes bearing name of miQ types
  if[.is.miq.type[sClass];
    '.miq.h.log.error "Object class name clash with miQ type name: ",-3!sClass
  ];

  / Type has to be either null symbol (catch all), miq.type, or a another class
  if[not .is.empty bad:types where not (`~/:raze types)|.is.miq.type'[raze types]|.miq.object.is.defined'[raze types];
    '.miq.h.log.error "Object class signature types unrecognised: ",-3!bad
  ];

  / Fail if attempting implicit object redefinition
  if[.miq.object.is.defined[sClass] & not bRedefine;
    '.miq.h.log.error "Object class already defined: ",-3!sClass
  ];

  OBJECTS upsert (sClass;slSignature;types);
  sClass
 };

.miq.object.t.extend:{[sClass;sNewClass;slSignature;types] .miq.object.i.extend[0b;sClass;sNewClass;slSignature;types]};
.miq.object.t.reextend:{[bRedefine;sClass;sNewClass;slSignature;types] .miq.object.i.extend[bRedefine;sClass;sNewClass;slSignature;types]};

.miq.object.i.extend:{[bRedefine;sClass;sNewClass;slSignature;types]
  .miq.object.is.error.defined[sClass];

  def:.miq.object.definition[sClass];
  sig:key[def],slSignature;
  typ:get[def],types;

  .miq.object.i.define[bRedefine;sNewClass;sig;typ]
 };

.miq.object.delete:{[sClass]
  .miq.object.is.error.defined[sClass];
  delete from OBJECTS where class=sClass;
  sClass
 };

.miq.object.identify:{[object]
  if[not .miq.object.is.valid[object]; :`];

  classes:exec class from OBJECTS where asc[key object]~/:asc'[signature];
  if[.is.empty[classes]; :`];

  classes:classes where .miq.object.is.class[;object]'[classes];
  /if[1<count classes; .miq.h.log.warn "Object can be identified using multiple classes: ",-3!classes];

  first classes
 };

/ Import & Export
.miq.object.resolve:{[sClass]
  .is.error.s[sClass];
  first .miq.object.i.resolve[sClass;`]
 };

.miq.object.i.resolve:{[sClass;nsContext]
  / When object matches exactly
  if[.miq.object.is.defined[sClass]; :(1#sClass;` sv nsContext,` sv 1_` vs sClass)];

  / Otherwise perform part-wise matching
  pattern,:$[null sClass; "*"; "."~last pattern:string[sClass]; "*"; ".*"];
  classes:exec class from OBJECTS where class like pattern;

  address:` sv'nsContext,'(count[` vs sClass]-1)_'` vs'classes;
  if[not .is.empty root:where 2=count each` vs'address;
    address[root]:` sv'`,'` vs'address[root];
  ];
  (classes;address)
 }

.miq.object.import:{[sClass;nsContext]
  .is.error[`s`ns]@'(sClass;nsContext);

  items:flip .miq.object.i.resolve[sClass;nsContext];
  $[.is.empty[items]; 0#`; .miq.object.i.import .' items]
 };

.miq.object.i.import:{[sClass;nsAddress]
  nsAddress set (!). flip (
    (`class       ;{[x;y] x                          }[sClass]  )
   ;(`signature   ;{[x;y] .miq.object.signature[x]   }[sClass]  )
   ;(`definition  ;{[x;y] .miq.object.definition[x]  }[sClass]  )
   ;(`construct   ;{[x;y] .miq.object.construct[x;y] }[sClass]  )
   ;(`tt          ;{[x;y] .miq.object.tt[x]          }[sClass]  )
   ;(`verify      ;{[x;y] .miq.object.verify[x;y]    }[sClass]  )
  );
  sClass
 };

.miq.object.t.export:{[sClass] .miq.object.i.export[0b;sClass]};
.miq.object.t.exportWithRedefine:{[bRedefine;sClass] .miq.object.i.export[bRedefine;sClass]};

.miq.object.i.export:{[bRedefine;sClass]
  classes:first .miq.object.i.resolve[sClass;`];
  {[b;class]
    definition:.miq.object.definition[class];
    ".miq.object.define[",$[b;(-3!b),";";""],(-3!class),";",(-3!key definition),";",(-3!get definition),"]"
  }[bRedefine]'[classes]
 };

/.miq.o.translate:{[od;qd]
/  .is.error.any[`sl`qd;od];       / TOOD s - object
/  .is.error.qd[qd];

/  form:$[
/    .is.sl[od]; key[qd]!od;
/    .is.qd[od]; $[
/        all key[qd] in key[od]; key[qd]#od;
/                                '.miq.h.log.error "Invalid type translation definition: ",-3!od
/      ];
/  ];

/  .to[get form]@'qd
/ };

// ==========
//  Handlers
// ==========
/ TODO shorthand to define handlers if they are not defined already - maybe it can accept a table and work on that?
/ Checks
.miq.handler.is.defined    :{[s] $[.is.s[s]; s in key HANDLERS; 0b]};
.miq.handler.is.implemented:{[s] $[.miq.handler.is.defined[s]; not null HANDLERS[s][`implementer]; 0b]};
.miq.handler.is.stale      :{[s] $[.miq.handler.is.defined[s]; $[null HANDLERS[s][`implementer]; 0b; not .miq.ns.is.present HANDLERS[s][`implementer]]; 0b]};
.miq.handler.is.attached   :{[ns] $[.is.ns[ns]; ns in raze exec attached from HANDLERS; 0b]};

/ Utils
.miq.handler.info:{[sName]
  .is.error.s[sName];
  .miq.handler.is.error.defined[sName];
  HANDLERS[sName]
 };

.miq.handler.list:{[]
  exec name from HANDLERS
 };

/ TODO .miq.handler.inspect[] to find information on supplied handler??

/ Definition
/ - template interfaces for .miq.handler.define[] & .miq.handler.redefine[]
.miq.handler.t.define:{[sName;jArity;clDescription]
  .miq.h.log.info "Defininig new handler: ",-3!sName;
  .miq.handler.i.define[0b;sName;jArity;clDescription]
 };

.miq.handler.t.defineWithImplementer:{[sName;nsImplementer;jArity;clDescription]
  .miq.handler.t.define[sName;jArity;clDescription];
  .miq.handler.set[sName;nsImplementer]
 };

.miq.handler.t.redefine:{[bRedefine;sName;jArity;clDescription]
  .miq.h.log.info "Redefininig handler: ",-3!sName;
  .miq.handler.i.define[bRedefine;sName;jArity;clDescription]
 };

.miq.handler.t.redefineWithImplementer:{[bRedefine;sName;nsImplementer;jArity;clDescription]
  .miq.handler.t.redefine[bRedefine;sName;jArity;clDescription];
  .miq.handler.set[sName;nsImplementer]
 };

/ - internal implementation
.miq.handler.i.define:{[bRedefine;sName;jArity;clDescription]
  if[not bRedefine; if[.miq.handler.is.defined[sName]; '.miq.h.log.error "Handler already defined: ",-3!sName]];
  if[not jArity in 0N,til 9; '.miq.h.log.error "Invalid handler arity: ",-3!`handler`arity!(sName;jArity)];

  HANDLERS upsert `name`arity`description!(sName;jArity;clDescription);
  sName
 };

/ Implementers
.miq.handler.set:{[sName;nsImplementer]
  .is.error[`s`ns]@'(sName;nsImplementer);
  .miq.handler.is.error.defined[sName];
  .miq.ns.is.error.present[nsImplementer];

  $[null nsImplementer;
      :.miq.handler.unset[sName];
    null arity:HANDLERS[sName][`arity];
      if[.miq.fn.is.address[nsImplementer]; .miq.h.log.warn "Arity of executable implementer not defined: ",-3!`handler`implementer!(sName;nsImplementer)];
      $[.miq.fn.is.address[nsImplementer];
          if[not arity~.miq.fn.arity[nsImplementer]; '.miq.h.log.error "Handler arity does not match that of implementer: ",-3!`handler`implementer!(sName;nsImplementer)];
          '.miq.h.log.error "Not a valid implementer: ",-3!`handler`implementer!(sName;nsImplementer)
      ]
  ];

  .miq.h.log.info "New handler implementer: ",-3!`handler`implementer!(sName;nsImplementer);
  HANDLERS upsert `name`implementer!(sName;nsImplementer);
  .miq.handler.update[sName]
 };

.miq.handler.unset:{[sName]
  .is.error.s[sName];
  .miq.handler.is.error.defined[sName];

  .miq.h.log.info "Removing handler implementer: ",-3!`handler`implementer!(sName;HANDLERS[sName][`implementer]);
  HANDLERS upsert `name`implementer!(sName;`);
  .miq.handler.update[sName]
 };

.miq.handler.get:{[sName]
  .is.error.s[sName];
  .miq.handler.is.error.implemented[sName];
  HANDLERS[sName][`implementer]
 };

/ TODO check that implementer is not attached to itself
/ TODO function to determine what handler is the address attached to (.miq.handler.target[] / .miq.handler.implementer[]?)
/ Attach & Detach
.miq.handler.attach:{[sName;nsAddress]
  .is.error[`s`ns]@'(sName;nsAddress);
  .miq.handler.is.error.defined[sName];

  if[null nsAddress; '.miq.h.log.error "Handler cannot be attached to null address: ",-3!`handler`address!(sName;nsAddress)];
  .miq.handler.i.attach[sName;nsAddress]
 };

.miq.handler.i.attach:{[sName;nsAddress]
  .[HANDLERS;(sName;`attached);distinct@,;nsAddress];

  .miq.h.log.debug "Attaching to ",(-3!sName),": ",-3!nsAddress;
  implementer:HANDLERS[sName][`implementer];
  nsAddress set $[null implementer;
      (')[{[x;y;z] '.miq.h.log.error "Calling an unimplemented handler: ",-3!`handler`caller!(x;y);}[sName;nsAddress];enlist];
      implementer
  ]
 };

.miq.handler.detach:{[nsAddress]
  .is.error.ns[nsAddress];
  if[null nsAddress; '.miq.h.log.error "Handler cannot be detached from null address: ",-3!`handler`address!(sName;nsAddress)];
  .miq.handler.is.error.attached[nsAddress];

  .miq.handler.i.detach[nsAddress]
 };

.miq.handler.i.detach:{[nsAddress]
  handler:first exec name from HANDLERS where nsAddress in/:attached;
  .[HANDLERS;(handler;`attached);except;nsAddress];

  .miq.h.log.debug "Detaching from ",(-3!handler),": ",-3!nsAddress;
  if[.miq.ns.is.present[nsAddress]; .miq.ns.delete[nsAddress]];
  nsAddress
 };

.miq.handler.update:{[sName]
  .miq.handler.is.error.defined[sName];
  address:HANDLERS[sName][`attached];

  .miq.h.log.info "Updating handler: ",-3!sName;
  $[.is.empty[address]; address; .miq.handler.i.attach[sName]'[address]];
  sName
 };

/ Higher order
.miq.handler.import:{[sName;nsContext]
  .is.error[`s`ns]@'(sName;nsContext);
  if[.miq.ns.is.present[nsContext]; .miq.ns.is.error.context[nsContext]];

  items:flip .miq.handler.i.resolve[sName;nsContext];
  $[.is.empty[items]; 0#`; .miq.handler.i.attach .' items]
 };

.miq.handler.export:{[sName;nsContext]
  .is.error[`s`ns]@'(sName;nsContext);
  if[.miq.ns.is.present[nsContext]; .miq.ns.is.error.context[nsContext]];

  items:flip .miq.handler.i.resolve[sName;nsContext];
  $[.is.empty[items]; 0#`; .miq.handler.i.export .' items]
 };

.miq.handler.i.export:{[sName;nsAddress]
  .miq.h.log.info "Exporting ",(-3!sName)," to: ",-3!nsAddress;
  implementer:HANDLERS[sName][`implementer];

  nsAddress set $[null implementer;
      (')[{[x;y;z] if[not .miq.handler.is.implemented[x]; '.miq.h.log.error "Calling an unimplemented handler: ",-3!`handler`caller!(x;y)]; y set get HANDLERS[x][`implementer]; y . z}[sName;nsAddress];enlist];
      get implementer
  ]
 };

.miq.handler.resolve:{[sName]
  .is.error.s[sName];
  first .miq.handler.i.resolve[sName;`]
 };

.miq.handler.i.resolve:{[sName;nsContext]
  / When handler matches exactly
  if[.miq.handler.is.defined[sName]; :(1#sName;` sv nsContext,` sv 1_` vs sName)];

  / Otherwise perform part-wise matching
  pattern,:$[null sName; "*"; "."~last pattern:string[sName]; "*"; ".*"];
  handler:exec name from HANDLERS where name like pattern;

  address:` sv'nsContext,'(count[` vs sName]-1)_'` vs'handler;
  if[not .is.empty root:where 2=count each` vs'address;
    address[root]:` sv'`,'` vs'address[root];
  ];
  (handler;address)
 };

// ==========
//  Template
// ==========
/ TODO allow more complex interface definition, so a single parameter can take on multiple miQ types
/ Checks
.miq.template.is.present    :{[nsName] $[.is.ns[nsName]; nsName in exec name from TEMPLATES; 0b]};
.miq.template.is.defined    :{[nsName;slInterface] $[all .is[`ns`sl]@'(nsName;(),slInterface); (nsName;(),slInterface) in key TEMPLATES; 0b]};
.miq.template.is.interface  :{[slInterface] $[.is.any[`s`sl;slInterface]; all slInterface in .miq.is.functions,`; 0b]};
.miq.template.is.implementer:{[nsImplementer] $[.is.ns[nsImplementer]; nsImplementer in exec implementer from TEMPLATES; 0b]};

.miq.template.is.blueprint:{[qt]
  $[not .is.qt[qt];
      0b;
    not asc[cols qt]~asc`name`interface`implementer;
      0b;
      all .is.ns'[qt`name] & .miq.template.is.interface'[qt`interface] & .miq.fn.is.address'[qt`implementer]
  ]
 };

/ Utils
.miq.template.list:{[]
  distinct exec name from TEMPLATES
 };

.miq.template.info:{[nsName]
  .miq.template.is.error.present[nsName];
  select from TEMPLATES where name=nsName
 };

/ IDEA Define match[] in public and allow customisation?
/.miq.template.match:{[x;y] $[count[x]~count[y]; x~x^y; 0b]}

.miq.template.mock:{[nsName;arguments]
  .miq.template.is.error.present[nsName];

  match:{$[count[x]~count[y];all .is[`nyi^x]@'y;0b]}[;arguments];
  select from TEMPLATES where name=nsName, match'[interface]
 };

/ IDEA configuration option to delete by interface, not name, so we can keep users custom interfaces?
.miq.template.blueprint:{[qtBlueprint]
  .miq.template.is.error.blueprint[qtBlueprint];

  templates:distinct exec name from qtBlueprint;
  .miq.template.delete each templates where .miq.template.is.present'[templates];
  (')[.miq.template.add .;get]'[qtBlueprint]
 };

/ Engine
.miq.template.generate:{[nsName]
  .miq.template.is.error.present[nsName];

  code:select interface,implementer from TEMPLATES where name=nsName;
  code:update {$[","~x 0;"1#",1_x;x]} each -3!'interface, implementer:(string[implementer],\:" . ") from code;
  code:update (max count'[interface])$interface, (max count'[implementer])$implementer from code;
  code:update line:{"    match[",x,"]; ",y,"arguments;"}'[interface;implementer] from code;

  code :code[`line];
  code,:enlist((code[0]?";")#""),"  '.miq.h.log.error \"No matching interface: \",-3!`template`types!(",(-3!nsName),";type'[arguments])";
  code,:("  ]";" }");

  code[0;2 3]:"$[";
  code:("{[arguments]"
   ;"  .miq.fn.is.error.arity[count arguments];"
   ;""
   ;"  match:{$[count[x]~count[y];all .is[`nyi^x]@'y;0b]}[;arguments];"
   ;""
  ),code;

  .miq.qk.interpret["q"] first .miq.qk.process[`$".miq.template.generate@@";-1] ` sv code
 };

/ TODO sllInterface could be useful to call .miq.template.update[] only once
.miq.template.add:{[nsName;slInterface;nsImplementer]
  .miq.template.is.error.interface[slInterface];
  .miq.ns.is.error.present[nsImplementer];
  .miq.fn.is.error.executable[get nsImplementer];

  if[.miq.template.is.defined[nsName;slInterface];
    '.miq.h.log.error "Template interface already defined: ",-3!`template`interface!(nsName;slInterface)
  ];

  .miq.h.log.info "Adding new template interface: ",-3!`template`interface!(nsName;slInterface);
  TEMPLATES upsert (nsName;(),slInterface;0N;nsImplementer;count slInterface);
  .miq.template.i.update[nsName]
 };

.miq.template.remove:{[nsName;slInterface]
  .miq.template.is.error.present[nsName];
  .miq.template.is.error.interface[slInterface];
  .miq.template.is.error.defined[nsName;slInterface];

  / explicit expand to list
  slInterface,:();

  .miq.h.log.info "Removing an existing template interface: ",-3!`template`interface!(nsName;slInterface);
  delete from TEMPLATES where name=nsName, interface~\:slInterface;
  $[.miq.template.is.present[nsName]; .miq.template.i.update[nsName]; .miq.template.i.delete[nsName]];
  nsName
 };

.miq.template.delete:{[nsName]
  .miq.template.is.error.present[nsName];
  .miq.template.i.delete[nsName]
 };

.miq.template.i.delete:{[nsName]
  .miq.h.log.info "Deleting a templated function: ",-3!nsName;
  delete from TEMPLATES where name=nsName;
  .miq.ns.delete[nsName]
 };

.miq.template.update:{[nsName]
  .miq.template.is.error.present[nsName];
  .miq.template.i.update[nsName]
 };

.miq.template.i.update:{[nsName]
  / Sort
  interfaces:`name`ord xasc update ord:(count;count where null@)@\:/:interface from TEMPLATES[];
  TEMPLATES set update order:til count name by name from delete ord from interfaces;

  / Template regeneration
  function:.miq.fn.copy[nsName;.miq.template.generate[nsName]];
  nsName set (')[function;enlist]
 };

// ==========
//  Overload
// ==========
/.miq.overload.

// ========
//  Extend
// ========
/.miq.extend.

/ TODO pending rewrite, should be split into overload and extend?
/ - overload will run all hooks in sequence and won't make their output available in .miq.z.LAST
/ - extend will designate a primary hook and run secondary hooks in its presence and record the output in .miq.z.LAST
/ TODO remove hook name from .miq.z.LAST?
// =======
//  Hooks
// =======
/ Generic hook to invoke all registered functions
/ TODO execute secondary within lambda to guarantee .miq.z.LAST correctness in nested scenario?
\d .
.miq.hook.process:{[nsHook;fnlHooks;x]
  .miq.z.LAST:(nsHook;fnlHooks[0][x]);
  (1_fnlHooks)@\:x;
  .miq.z.LAST[1]
 };
\d .miq

/ TODO split into callbacks and variables, handle appropritelly in expunge and primary / register
/ kdb hooks
.miq.hook.z:{[]
  `.z.vs`.z.exit`.z.zd`.z.ts      / General
 ,`.z.pi`.z.pq                    / Input / qcon
 ,`.z.po`.z.pc`.z.pw`.z.bm        / IPC open / close
 ,`.z.pg`.z.ps                    / IPC sync / async
 ,`.z.wo`.z.wc`.z.ws              / Websockets
 ,`.z.pm`.z.ac`.z.ph`.z.pp        / HTTP
/ ,`.z.p`.z.d`.z.z`.z.n`.z.t       / Variables
 };

/ Checks
.miq.hook.is.defined:{[nsHook]
  .is.error.ns[nsHook];
  .miq.hook.is.error.name[nsHook];
  .miq.ns.is.present[nsHook]
 };

.miq.hook.is.name:{[nsHook]
  .is.error.ns[nsHook];
  z:1+first where `z=v:` vs nsHook;
  $[2~z;nsHook in .miq.hook.z[]; not (z~count v)|null[z]|`dd~v z]
 };

.miq.hook.is.label:{[nsHook;label]
  .is.error.ns[nsHook];
  .is.error.any[`s`ns;label];
  label in key .miq.hook.definitions[nsHook]
 };

/ Primary functionality
.miq.hook.definitions:{[nsHook]
  .is.error.ns[nsHook];
  .miq.hook.is.error.name[nsHook];
  @[get;` sv HOOK.HOOKS,`$1_string nsHook;{(`$())!()}]
 };

.miq.hook.default:{[nsHook]
  .miq.hook.is.error.name[nsHook];
  .miq.hook.i.default[nsHook]
 };

.miq.hook.i.default:{[nsHook]
  z:1+first where `z=v:` vs nsHook;
  ` sv$[`z~v[1]; SELF,`z; (z#v)],`dd,(z _v)
 };

.miq.hook.expunge:{[nsHook]
  .is.error.ns[nsHook];
  .miq.hook.is.error.name[nsHook];

  z:nsHook in .miq.hook.z[];
  default:.miq.hook.i.default[nsHook];

  if[.miq.ns.is.present[default]; :.miq.hook.primary[nsHook;default;get default]];

  $[z; system"x ",string nsHook; .miq.ns.delete[nsHook]];
  nsHook
 };

/ TODO run once and insert into HOOKS, then keep up to date somehow? this defo deserves TT.HOOKS
.miq.hook.info:{[ns]
  .is.error.any[`none`ns;ns];

  names:$[`.z~ns; 0#`; (group .miq.ns.tree[ns])[`name]];
  names:names where not names like string[ROOT.BB],".*";
  parts:` vs'names;
  hooks:names where (not`=last'[parts])&(`z in/:-1_'parts);
  hooks:hooks except .miq.hook.i.default each .miq.hook.z[];
  default:hooks where {`dd~x 1+first where `z=x}each` vs'hooks;
  d2h:{[x] z:1+first where `z=x; ` sv(z#x),(z+1)_x}each` vs'default;

  hooks:distinct asc .miq.hook.z[],d2h,hooks except default;
  default:.miq.hook.i.default each hooks;

  table:([]
    module:(` vs'hooks)[;1]
   ;name:hooks
   ;default:?[.miq.ns.is.present'[default]; default; `]
   ;enabled:.miq.ns.is.present'[hooks]
  );

  constraints:enlist(|),.miq.fq.d2cond[(like;like)] `name`default!2#enlist string[ns],".*";
  $[.is.none[ns]; table; ?[table;constraints;0b;()]]
 };

.miq.hook.primary :{[nsHook;label;fn] .miq.hook.i.add[1b;nsHook;label;fn]};
.miq.hook.register:{[nsHook;label;fn] .miq.hook.i.add[0b;nsHook;label;fn]};

.miq.hook.i.add:{[bPrimary;nsHook;label;fn]
  .is.error[`b`ns]@'(bPrimary;nsHook);
  .miq.hook.is.error.name[nsHook];
  .is.error.any[`s`ns;label];

  .miq.fn.is.error.executable[fn];
  $[.miq.fn.is.address[fn]; fn:get address:.miq.ns.path.absolute[fn]; address:`];

  hook:`$1_string nsHook;
  hooks:$[.miq.bb.is.item[HOOK.HOOKS;hook]; .miq.bb.get[HOOK.HOOKS;hook]; ()],enlist[label]!enlist[fn];
  primary:HOOK.PRIMARY[];

  if[bPrimary;
    if[not label~primary[nsHook];
      hooks:primary[nsHook] _ hooks;
      primary[nsHook]:label
    ];
    hooks:((1#label)#hooks),label _ hooks;
    HOOK.PRIMARY set primary
  ];

  if[not[bPrimary]&label~primary[nsHook];
    '.miq.h.log.error "Attempt to overwrite primary hook by non-primary: ",-3!label
  ];

  .miq.bb.set[HOOK.HOOKS;hook;hooks];
  if[null primary[nsHook]; :()];

  / IDEA rename hook implementers to hook name for stack readability?
  / IDEA set it as option?
  nsHook set $[1=count hooks; fn; .miq.hook.process[nsHook;get(` sv HOOK.HOOKS,hook)[]]]
 };

.miq.hook.remove:{[nsHook;label]
  .is.error.ns[nsHook];
  .is.error.any[`s`ns;label];
  .miq.hook.is.error.name[nsHook];
  hook:`$1_string nsHook;

  hooks:$[.miq.bb.is.item[HOOK.HOOKS;hook]; .miq.bb.get[HOOK.HOOKS;hook]; (0#`)!()];

  if[C.HOOK.REMOVE.SAFE[] & not label in key hooks;
    '.miq.h.log.error "No such hook label: ",-3!`nsHook`label!(nsHook;label)
  ];

  .miq.bb.set[HOOK.HOOKS;hook] hooks:label _ hooks;

  primary:HOOK.PRIMARY[];
  $[null primary[nsHook]; ::;
    primary[nsHook]~label;
    [
      HOOK.PRIMARY set nsHook _ primary;
      .miq.hook.expunge[nsHook]
    ];
    1=count hooks; nsHook set first hooks;
    1<count hooks; nsHook set .miq.hook.process[nsHook;get(` sv HOOK.HOOKS,hook)[]];
                   ::
  ]
 };

.miq.hook.depth:{[nsHook;fnOrigin;arg]
  .is.error.ns[nsHook];
  .miq.fn.is.error.executable[fnOrigin];
  $[.miq.fn.is.address[fnOrigin]; fnOrigin:get address:.miq.ns.path.absolute[fnOrigin]; address:`];

  name:.miq.fn.name[.z.s];
  correction:neg 1=count .miq.hook.definitions[nsHook];
  stack:HOOK.STACK set `;

  .miq.hook.register[nsHook;`$"__HOOK.DEPTH";{y;x set -1_.miq.u.stack[][;1;0]}[stack]];
  fnOrigin arg;
  .miq.hook.remove[nsHook;`$"__HOOK.DEPTH"];

  if[`~stack[]; :0N];   / Return null if hook not part of execution
  stack:`${?[10h=type'[x];x;count[x]#()]} stack[];    / ("q";`) hack to accommodate kdb 4.0
  correction+count[stack]-1+first where name~/:stack
 };

// =================
//  Type resolution
// =================
.miq.is.functions:@[get;`.miq.is.functions;0#`];

.miq.is.init:{[]
  tree:{$[.miq.ns.i.context[get x]; raze .z.s'[` sv'x,'key[x]]; x]};
  fns:{` sv'x where not `=last each x:2_'` vs'x} tree[`.is];
  fns:fns where not any each flip fns like/: ,'[;"*"] string `init`generate`type`meta`generic`neg`miq2q`q2miq`any`one`warn`error;

  / Export a list of implemented plain checks
  .miq.is.functions:fns,`generic;

  / Generic type check messages
  {[depth;fns]
    .is.generate[`warn`error;;depth;]./:\:flip (` sv'`.is,'fns;string[fns],\:" type expected: ");
  }.'flip (key;get)@\:fns group 1+count each where each "."='string fns;

  / Non-generic type check messages
  .is.generate[`warn`error;;1;]./:\: (
    (`.is.any     ;"Any of ${slType} types expected: ")
   ;(`.is.one     ;"One of ${form} types expected: "  )
   ;(`.is.form    ;"Variable not of expected form: "  )
   ;(`.is.generic ;"generic list type expected: "     )
  );
 };

/ Internal .is.meta[] helper functions
/ IDEA fk could be turned into .is.fk[]?
/ NOTE I am probably not covering some special case
.miq.is.i.meta.fk:{[x] $[not .is.enum[x]; `; 97h<type get .miq.ns.path.absolute t:key x; t; `]};
.miq.is.i.meta.first:{[x] $[.is.enum[f:first x]; get f; f]};

/ .is.generate.*[] implementer
.miq.is.i.generate.level:{[bSignal;sSubcontext;nsPrint;nsFunction;jDepth;clMessage]
  depth     :$[.is.none[jDepth];1;jDepth];
  context   :` sv ({$[`~last x;-1_x;x]} neg[depth]_` vs nsFunction),sSubcontext;
  name      :` sv neg[depth]#` vs nsFunction;
  address   :` sv context,name;

  function  :{$[".."~2#x;2_x;x]} string[nsFunction];
  info      :.miq.fn.info[nsFunction];
  signature :{$[`~first x;0#x;x]} info[`signature];
  parameters:$[.is.empty[signature];"";";" sv string signature];
  print     :string[nsPrint];

  message:{[sig;msg]
    matched:sig where 1=count each ss[msg]'["${",/:string[sig],\:"}"];
    printed:sig except matched;
    text:ssr/[.miq.u.text.qw[msg];"${",/:string[matched],\:"}";"\",(-3!",/:string[matched],\:"),\""];
    text,$[.is.empty[printed];"";1~count printed;",-3!",string[printed 0];",-3!",(-3!printed),"!(",(";" sv string printed),")"]
  }[signature;clMessage];

  source:$[bSignal;
      " $[",function,"[",parameters,"];1b;'",print," ",message,"]";
      " $[",function,"[",parameters,"];1b;[",print," ",message,";0b]]"
  ];
  .miq.qk.interpret["q"] string[address],":",.miq.qk.fn.wrap[info`signature] source;
  address
 };

// Check function generators
/ Standardised message generation for checks (and maybe other wrappers)
/ TODO peek into the context the function is defined in and look for imported / exported log handlers?
.is.generate.warn:{[nsFunction;jDepth;clMessage]
  .miq.fn.is.error.address[nsFunction];
  .is.error.any[`none`j;jDepth];
  .is.error.cl[clMessage];
  .miq.is.i.generate.level[0b;`warn;`.miq.h.log.warn;nsFunction;jDepth;clMessage]
 };

.is.generate.error:{[nsFunction;jDepth;clMessage]
  .miq.fn.is.error.address[nsFunction];
  .is.error.any[`none`j;jDepth];
  .is.error.cl[clMessage];
  .miq.is.i.generate.level[1b;`error;`.miq.h.log.error;nsFunction;jDepth;clMessage]
 };

// General purpose routines
.is.type:{[x]
  types:.is.q2miq[t:type x];
  / TODO is there a less FUGLY way to detect an empty generic list?
  / (so I don't attribute it to first of .is.q2miq[0h])
  if[0h~t; if[x~(); :`generic]];
  / while[] is less expensive than shotgun approach due to some more complex types
  while[not .is[first types][x]; types:1_types];
  first types
 };

.is.meta:{[x]
  .is.error.any[`qt`qk`qp;x];
  ([c:key x] t:(')[.is.type;.miq.is.i.meta.first]'[v]; f:.miq.is.i.meta.fk'[v]; a:-2!'v:get x:.Q.V x)
 };

.is.neg:{[x]
  .is.error.any[`s`sl`h`hl;x];

  $[.is.empty[x]|.is.null[x]; x;
    .is.any[`h`hl;x];
    [
      .is.error.q.type'[x];
      ?[x<77h;neg x;0h]
    ];
    .is.any[`s`sl;x];
    [
      .is.error.miq.type'[x];
      $[.is.list[x]; @'[;x]; @[;x]] {[x] $[`generic~x; x; .is.miq.atom[x]; `$string[x],"l"; .is.miq.list[x]; `$-1_string x; x]}
    ]
  ]
 };

.is.miq2q:{[]
  basic :`b`g`x`h`i`j`e`f`c`s`p`m`d`z`n`u`v`t!neg`short$1 2,4+til 16;
  basic,:(`generic,`$raze each string key[basic],'`l)!`short$0 1 2,4+til 16;
  extra :`ns`fd`fs`fh`cs`ch`op`fn`fp`qc`qi`qs`qd`qt`qk`qp`qm!`short$(-11;-6 -7;-11;-6 -7;-11;-6 -7;101+til 3;100;104;105;106+til 6;-11;99;98;99;98;98 99);
  extra,:(`$raze each string key[extra],'`l)!`short$count[extra]#0;
  extra,:`nsl`fdl`fsl`fhl`csl`chl`qsl!`short$(11;6 7;11;6 7;11;6 7;11);
  extra,:`nyi`enum`anymap`mapped`nested`elision`code`foreign!`short$(0N;(-76+til 57),20+til 57;77;78+til 19;97;101;112;112);
  (),/:basic,extra
 }[];

.is.q2miq:{[]
  qt:group {(raze get[count each x]#'key[x])!raze get x} .is.miq2q;
  / Manual correction of type ordering to manage type collisions
  qt[98h] :`qp`qt;
  qt[99h] :`qd`qk`qt;
  qt[0h]  :`opl`fnl`fpl`qcl`qil`qdl`qpl`qkl`qtl`qml`generic;
  asc[key qt]#qt
 }[];

// Type checking
/ Special
.is.any:{[slType;x]
  .is.error.sl[slType];
  any .[.is]'[` vs'slType]@\:x
 };

.is.one:{[form;x]
  .is.error.sl[raze form];
  if[not all raze[form] in .miq.is.functions; '.miq.h.log.error "Form includes unimplemented checks: ",-3!raze[form] except .miq.is.functions];
  any {[form;x] $[.is.atom[form]; .is[form;x]; .is.list[x]; .is.form[form;x]; 0b]}[;x]'[form]
 };

.is.form:{[form;x]
  .is.error[`sl`list]@'(form;x);
  if[not all raze[form] in .miq.is.functions; '.miq.h.log.error "Form includes unimplemented checks: ",-3!raze[form] except .miq.is.functions];
  $[count[form]~count[x]; all .is[form]@'(x); 0b]
 };

/ Generic
.is.none :{any x~/:(::;())};
.is.null :{$[.is.atom[x]; null x; 0b]};
.is.empty:{0~count x};
.is.true :{not .is.false[x]};
.is.false:{$[.is.b[x]; not x; .is.number[x]; 0=x; .is.list[x]; 0~count x; .is.null[x]]};

.is.generic :{0h~type x};

.is.q.type  :{$[.is.h[x]; x in .miq.u.q.types  ; 0b]};
.is.q.atom  :{$[.is.h[x]; x in .miq.u.q.atoms  ; 0b]};
.is.q.list  :{$[.is.h[x]; x in .miq.u.q.lists  ; 0b]};

.is.miq.type:{$[.is.s[x]; x in .miq.u.miq.types; 0b]};
.is.miq.atom:{$[.is.s[x]; x in .miq.u.miq.atoms; 0b]};
.is.miq.list:{$[.is.s[x]; x in .miq.u.miq.lists; 0b]};

.is.atom:{type[x] < 0h};
.is.list:{type[x] in 0 1 2 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19h};

/ Atoms and Lists of basic datatypes
.is.b:{ -1h~type x};   .is.bl:{ 1h~type x};
.is.g:{ -2h~type x};   .is.gl:{ 2h~type x};
.is.x:{ -4h~type x};   .is.xl:{ 4h~type x};
.is.h:{ -5h~type x};   .is.hl:{ 5h~type x};
.is.i:{ -6h~type x};   .is.il:{ 6h~type x};
.is.j:{ -7h~type x};   .is.jl:{ 7h~type x};
.is.e:{ -8h~type x};   .is.el:{ 8h~type x};
.is.f:{ -9h~type x};   .is.fl:{ 9h~type x};
.is.c:{-10h~type x};   .is.cl:{10h~type x};
/.is.s:{-11h~type x};   .is.sl:{11h~type x};  / Replaced by .is.qs[] and .is.qsl[]
.is.p:{-12h~type x};   .is.pl:{12h~type x};
.is.m:{-13h~type x};   .is.ml:{13h~type x};
.is.d:{-14h~type x};   .is.dl:{14h~type x};
.is.z:{-15h~type x};   .is.zl:{15h~type x};
.is.n:{-16h~type x};   .is.nl:{16h~type x};
.is.u:{-17h~type x};   .is.ul:{17h~type x};
.is.v:{-18h~type x};   .is.vl:{18h~type x};
.is.t:{-19h~type x};   .is.tl:{19h~type x};

/ Narrower miQ symbol type
.is.s: {$[-11h~type x;not string[x][0] in ":.";0b]};
.is.sl:{$[11h~type x;all not any each like/:[;(":*";".*")]@\:x;0b]};

/ Atoms and lists of extended and quasi datatypes
/ Operator(s)
.is.op :{$[type[x] in 102 103h;1b;101h~type x;not 0x010000000a00000065ff~-8!x;0b]};
.is.opl:{$[not 0h~type x;0b;all type'[x] in 101 102 103h;not any 0x010000000a00000065ff~/:-8!'x;0b]};

.is.unary  :{$[101h~type x;not .is.elision[x];0b]};    .is.unaryList  :{$[not 0h~type x;0b;all type'[x]=101h;not any .is.elision'[x];0b]};
.is.binary :{102h~type x};                             .is.binaryList :{$[0h~type x;all type'[x]=102h;0b]};
.is.ternary:{103h~type x};                             .is.ternaryList:{$[0h~type x;all type'[x]=103h;0b]};

/ Function
.is.fn :{100h~type x};
.is.fnl:{$[0h~type x;all type'[x]=100h;0b]};
/ Projection
.is.fp :{104h~type x};
.is.fpl:{$[0h~type x;all type'[x]=104h;0b]};
/ Composition
.is.qc :{105h~type x};
.is.qcl:{$[0h~type x;all type'[x]=105h;0b]};
/ Iterator
.is.qi :{type[x] in 106 107 108 109 110 111h};
.is.qil:{$[0h~type x;all type'[x] in 106 107 108 109 110 111h;0b]};
/ Namespace
.is.ns :{$[-11h~type x;null[x]|x like".*";0b]};
.is.nsl:{$[11h~type x;(0=count x)|all null[x]|x like".*";0b]};
/ File Descriptors
.is.fd :{$[type[x] in -6 -7h; x in .miq.fd.list[]; 0b]};
.is.fdl:{$[type[x] in 6 7h; all x in .miq.fd.list[]; not .is.list[x]; 0b; all type'[x] in -6 -7h; all any'[in\:[x;.miq.fd.list[]]]; 0b]};
/ File symbol
.is.fs :{$[-11h~type x;null[x]|x like":?*";0b]};
.is.fsl:{$[11h~type x;all x like":?*";0b]};
/ File handle
.is.fh :{$[type[x] in -6 -7h; x in .miq.fd.list[] except 0i,key .z.W;0b]};
.is.fhl:{$[type[x] in 6 7h;all x in .miq.fd.list[] except 0i,key .z.W;not .is.list[x];0b;all type'[x] in -6 -7h;all any'[in\:[x;.miq.fd.list[] except 0i,key .z.W]];0b]};
/ Connection symbol
.is.cs :{$[not -11h~type x;0b;null[x];1b;(x like":*") & (1_string[x]) like "*:*"]};
.is.csl:{$[11h~type x;all (x like":*") & (1_'string x) like "*:*";0b]};
/ Connection handle
.is.ch :{$[type[x] in -6 -7h;x in 0i,key .z.W;0b]};
.is.chl:{$[type[x] in 6 7h;all x in 0i,key .z.W;not .is.list[x];0b;all type'[x] in -6 -7h;all any'[in\:[x;0i,key .z.W]];0b]};
/ True q symbol type check
.is.qs :{-11h~type x};
.is.qsl:{11h~type x};
/ Dictionary
.is.qd :{$[99h~type[x];not(98h~type key x)&98h~type get x;0b]};
.is.qdl:{$[all type'[x]=99h;not any(98h=(')[type;key]'[x])&98h=(')[type;get]'[x];0b]};
/ Table
.is.qt :{$[98h~type x;not .is.qp[x];0b]};
.is.qtl:{$[0h~type x;all .is.qt'[x];0b]};
/ Keyed table
.is.qk :{$[99h~type x;(98h~type key x)&98h~type get x;0b]};
.is.qkl:{$[(all 99h~type'[x])&0h~type x;all (98h=(')[type;key]'[x])&98h=(')[type;get]'[x];0b]};
/ Partitioned table
.is.qp :{$[not 98h~type x;0b;type t:get flip x;not":"~first string t;0b]};
.is.qpl:{$[0h~type x;all .is.qp'[x];0b]};
/ In-memory table
.is.qm :{$[98h~type x;not .is.qp[x];99h~type x;.is.qk[x];0b]};
.is.qml:{$[0h~type x;all .is.qm'[x];0b]};

/ Numbers
.is.number :{type[x] in -5 -6 -7 -8 -9h};   .is.numberList :{all type'[x] in 5 6 7 8 9h};
.is.whole  :{type[x] in -5 -6 -7h};         .is.wholeList  :{all type'[x] in 5 6 7h};
.is.decimal:{type[x] in -8 -9h};            .is.decimalList:{all type'[x] in 8 9h};

/ Infinities
.is.pinf:{x in raze .miq.u.num.pinf};       .is.pinfList:{all x in raze .miq.u.num.pinf};
.is.ninf:{x in raze .miq.u.num.ninf};       .is.ninfList:{all x in raze .miq.u.num.ninf};
.is.inf :{x in raze .miq.u.num.inf };       .is.infList :{all x in raze .miq.u.num.inf };

/ Special
.is.nyi    :{1b};
.is.enum   :{any type[x] within (-76 20h;-20 76h)};
.is.anymap :{77h~type x};
.is.mapped :{type[x] within 78 96h};
.is.nested :{97h~type x};
.is.table  :{$[99h~type x; .is.qk[x]; 98h~type x]};
.is.elision:{$[101h~type x; 0x010000000a00000065ff~-8!x; 0b]};
.is.code   :{$[112h~type x; "code"~string x; 0b]};
.is.foreign:{$[112h~type x; "foreign"~string x; 0b]};

/ TODO all but .is.temporal should be part of TZ module???
/ Temporal
.is.temporal:{type[x] in -19 -18 -17 -16 -15 -14 -13 -12 12 13 14 15 16 17 18 19h};

.is.year:{$[.is.whole[x]; x within 1000 9999; 0b]};
.is.yearList:{$[.is.wholeList[x]; all x within 1000 9999; 0b]};

.is.dd:{$[.is.whole[x]; x within 1 31; 0b]};
.is.ddList:{$[.is.wholeList[x]; all x within 1 31; 0b]};

.is.mm:{$[.is.whole[x]; x within 1 12; 0b]};
.is.mmList:{$[.is.wholeList[x]; all x within 1 12; 0b]};

.is.hh:{$[.is.whole[x]; x within 0 23; 0b]};
.is.hhList:{$[.is.wholeList[x]; all x within 0 23; 0b]};

.is.uu:{$[.is.whole[x]; x within 0 59; 0b]};
.is.uuList:{$[.is.wholeList[x]; all x within 0 59; 0b]};

.is.ss:{$[.is.whole[x]; x within 0 59; 0b]};
.is.ssList:{$[.is.wholeList[x]; all x within 0 59; 0b]};

/ Attributes
.is.sorted :{`s~-2!x};
.is.unique :{`u~-2!x};
.is.parted :{`p~-2!x};
.is.grouped:{`g~-2!x};

.is.step:{(99h;`s)~(type;-2!)@\:x};

// ==============
//  Type casting
// ==============
.miq.to.init:{[]
  miqtypes:where any each .is.miq2q in\: .miq.u.q.atoms,1_.miq.u.q.lists;
  fns :{".to.",x,":{.miq.to.type[`",x,";x]}"    } each string miqtypes where miqtypes in .miq.u.miq.atoms;
  fns,:{".to.",x,":{.miq.to.type[`",x,";x]}"    } each string miqtypes where miqtypes in .miq.u.miq.lists;
  fns,:{".to.",x,":{.miq.to.temporal[`",x,";x]}"} each string .miq.to.temporalParts;
  fns,:raze {
    (".to.warn.",x,":{v:.to.",x,"[x]; $[.is.",x,"[v]; v; [.miq.h.log.warn \"Casting returned unexpected type: \",-3!`type`arg!(.is.type v;x); v]]}"
    ;".to.error.",x,":{v:.to.",x,"[x]; $[.is.",x,"[v]; v; '.miq.h.log.error \"Casting returned unexpected type: \",-3!`type`arg!(.is.type v;x)]}")
  } each string miqtypes,.miq.to.temporalParts;

  .miq.qk.interpret["q"] each .miq.qk.process[`$".miq.to.init@@";-1;fns];
 };

/ miQ
/ TODO this is tripple fugly and requires complete rework and methodical approach to determining behaviour on every possible combination
/ NOTE atomic casting is atomic, but list casting is strictly depth 1
.miq.to.type:{[sType;arg]
  .is.error.s[sType];
  hType:first .is.miq2q[sType];

  $[.is.miq.list[sType]; .miq.to.i.list[hType;sType;arg];
    .is.miq.atom[sType]; .miq.fn.atomic[.miq.to.i.atom[hType;sType];arg];
                         '.miq.h.log.error "Only miQ fundamental types are supported at the moment"
  ]
 };

/ TODO you will likely have to split internal functions and create non-generic pathways for casting for this to make any sense
/ TODO design the workflow and casting matrix and determine what conversions make sense and what would be the expected outcome
/ TODO think about narrowing and expanding of data types
.miq.to.i.parse:{[h;cl]
  $[not 10h~type cl          ; ();
    () ~ p:@[-5!;cl;()]      ; ();
    all (h;type p) in -11 11h; first p;
                               p
  ]
 };

/ NOTE expands to list if possible by joining with empty generic list
.miq.to.i.list:{[h;s;arg]
  if[.is.type[arg]~s; :arg];

  n:1 4 5 6 7 8 9 12 13 14 15 16 17 18 19h;
  if[all (abs h,type arg) in n; :(),abs[h]$p];

  a:$[not type[arg] in -10 10h; string arg; arg];
  if[not 10h~type[a]; :arg];
  if[10h~h; :a];

  parsed:.miq.to.i.parse[h;a];
  $[()~parsed                           ; (),.miq.to.i.atom[.is.neg h;.is.neg s]'[a];
    all (h;type first parsed) in -11 11h; (),first parsed;
    all (abs h,type parsed) in n        ; (),abs[h]$parsed;
    /h ~ 11h                             ; $[type[first parsed] in -11 11h; (),first parsed; parsed];
    h <> type parsed                    ; (),.miq.to.i.atom[.is.neg h;.is.neg s]'[a];
                                          parsed
  ]
 };

/ TODO fungliest function ever; improve
/      it should be illegal to have so many explicit returns in a single function
.miq.to.i.atom:{[h;s;arg]
  if[.is.type[arg]~s; :arg];

  / Numeric q types, with extra boolean and byte
  n:1 4 5 6 7 8 9 12 13 14 15 16 17 18 19h;
  if[all (abs h,type arg) in n; :abs[h]$arg]

  a:$[not type[arg] in -10 10h; string arg; arg];
  if[not type[a] in -10 10h; :arg];
  if[h in -10 10h; :a];

  parsed:.miq.to.i.parse[h;a];
  $[all (h;type first parsed) in -11 11h; first parsed;
    all (abs h,type parsed) in n        ; abs[h]$parsed;
    h in -11 11h                        ; `$a;
    abs[h] in .miq.u.q.lists            ; (neg abs h)$a;
                                          arg
  ]
 };

/ Temporal
.miq.to.temporalParts:`year`dd`mm`hh`uu`ss;

.miq.to.temporal:{[sPart;arg]
  .is.error.s[sPart];
  {$[0h~type x; .z.s'[x]; .is.error.temporal[x]]} arg;

  if[not sPart in .miq.to.temporalParts; '.miq.h.log.error "Not a recognised part of temporal: ",-3!sPart];

  {[cast;arg]
    $[0h~t:type arg; .z.s[cast]'[arg]; cast$arg]
  }[sPart;arg]
 };

// =====
//  Log
// =====
/ TODO accept proper config loader function / object?
.miq.log.init:{[]
  .miq.log.cfg,:`level`caller`skip!$[@[{C.LOG;1b};::;0b]; get get'[1_C.LOG]; (`show;1b;(".q.*";"*.is.*"))];

  / Silent startup override
  if[.miq.system.is.variable`MIQ_SILENT_BOOT; .miq.log.cfg.level:`warn];

  / TODO should be done as part of configuration item processing (through unimplemented 'processors')
  if[not .miq.log.cfg.level in LOG.LEVELS[];
    -2 m:"Log level invalid: ",-3!.miq.log.cfg.level; 'm
  ];

  / Define caller routine & apply log level
  .miq.log.caller:$[.miq.log.cfg.caller; {" <",.miq.u.nCallerSkip[3;.miq.log.cfg.skip],"> "}; ""];

  idx:LOG.LEVELS[]?.miq.log.cfg.level;
  off:idx # LOG.LEVELS[];
  on :idx _ LOG.LEVELS[];

  @[`.miq.log;off;:;`.miq.log.off[off]];
  @[`.miq.log;on ;:;`.miq.log.on [on ]];
 };

.miq.log.on.debug:{[clMsg] .miq.log.print[-1;`debug;" [DEBUG] ";clMsg]; clMsg};
.miq.log.on.show :{[obj]   1 .Q.s obj; obj};
.miq.log.on.info :{[clMsg] .miq.log.print[-1;`info ;" [INFO]  ";clMsg]; clMsg};
.miq.log.on.warn :{[clMsg] .miq.log.print[-1;`warn ;" [WARN]  ";clMsg]; clMsg};
.miq.log.on.error:{[clMsg] .miq.log.print[-2;`error;" [ERROR] ";clMsg]; clMsg};
.miq.log.on.fatal:{[clMsg] .miq.log.print[-2;`fatal;" [FATAL] ";clMsg]; clMsg};

.miq.log.off.debug:{x};
.miq.log.off.show :{x};
.miq.log.off.info :{x};
.miq.log.off.warn :{x};
.miq.log.off.error:{x};
.miq.log.off.fatal:{x};

.miq.log.print:{[jStream;sLevel;clLevel;clMsg]
  jStream@(string .z.t),clLevel,.miq.log.caller[],clMsg;
 };

// =====
//  IPC
// =====
.miq.ipc.connect:hopen;
.miq.ipc.disconnect:hclose;

.miq.ipc.sync:{[ch;msg]
  .is.error.any[`ch`chl;ch];
  $[.is.atom[ch]; ch@msg; {x@y}[;msg]'[ch]]
 };

.miq.ipc.async:{[ch;msg]
  .is.error.any[`ch`chl;ch];
  $[.is.atom[ch]; neg[ch][msg]; -25!(ch;msg)];
  if[L.IPC.FLUSH; .miq.h.ipc.flush[ch]]
 };

.miq.ipc.flush:{[ch]
  .is.error.any[`ch`chl;ch];
  $[.is.atom[ch]; neg[ch][]; -25!(ch;::)];
 };

/ TODO allow for (cs;tmout) address (since V4.0)
.miq.ipc.oneshot:{[csAddress;msg]
  .is.error.cs[csAddress];
  csAddress msg
 };

// ======
//  Trap
// ======
.miq.trap.apply:{[cmd;arg] .Q.trp[0b,enlist cmd@; arg; {(1b;(x;y))}]};
.miq.trap.print:{[clErr;stack] -1_"'",clErr,"\n", .Q.sbt stack};



// ===========
//  Bootstrap
// ===========
/ NOTE Run only on first file load!
if[not @[get;WARM;0b];
  / Logging subsystem & log handlers
  .miq.log.init[];
  .miq.h.log:1#.q;
  .miq.h.log[LOG.LEVELS[]]:` sv'`.miq.log,'LOG.LEVELS[];

  / TODO any better way of doing this?
  / I need to pre-define below to allow me to use .is.generate.* capabilities
  / .is.generate.*[] needs `sl`cl`any and `.miq.fn.is.error.executable[]
  / .miq.fn.info[] needs .miq.fn.is.error.executable[]
  / .miq.fs.path.real[] needs .miq.fs.is.error.present[] and .is.error.any[]
  `.miq.fn.is.error.executable set {[fn] $[.miq.fn.is.executable[fn];1b;'.miq.h.log.error "Not an executable object: ",-3!fn]};
  `.miq.fn.is.error.address set {[fn] $[.miq.fn.is.address[fn];1b;'.miq.h.log.error "Not an address to executable object: ",-3!fn]};

  `.is.error.sl  set {[x] $[.is.sl[x];1b;'.miq.h.log.error "sl type expected: ",-3!x]};
  `.is.error.cl  set {[x] $[.is.cl[x];1b;'.miq.h.log.error "cl type expected: ",-3!x]};
  `.is.error.any set {[slType;x] $[.is.any[slType;x];1b;'.miq.h.log.error "Any of ",(-3!slType)," types expected: ",-3!x]};

  / .is & .to
  .miq.is.init[];
  .miq.to.init[];

  / Standard warn & error messages
  .miq.i.messages[];

  / Allows usage of .miq.system.is.command[] (dependency of .miq.main[])
  L.SYSTEM.COMMAND.VERIFIED:0#`;

  / Allows usage of un-templated .miq.system.require[]
  TEMPLATES set TT.TEMPLATES;
  .miq.i.templates[];

  / Makes sure this code path won't be run in the future
  WARM set 1b;
 ];



// ========
//  Config
// ========
/ Standard CFG definitions
CFG.EXPLICIT:.miq.cfg.explicit[SELF];
CFG.DEFAULT :` sv SELF,`config`default;
CFG.APPLIED :.miq.bb.new[` sv SELF,`config];
CFG.CONSTANT:` sv SELF,`C;

/ Standard config definition
.miq.fn.copy[`.miq.config.set     ;.miq.cfg.set[CFG.EXPLICIT;CFG.DEFAULT]];
.miq.fn.copy[`.miq.config.assemble;.miq.cfg.assemble[CFG.EXPLICIT;CFG.DEFAULT]];
.miq.fn.copy[`.miq.config.get     ;.miq.cfg.get[CFG.APPLIED]];

.miq.config.i.handler:{[]
  update `$attached from TT.HANDLERS upsert flip `name`implementer`arity`description!flip(
    / log
    (`stl.log.debug      ;`.miq.log.debug       ;1 ;"DEBUG level log output; input is returned unchanged"                    )
   ;(`stl.log.show       ;`.miq.log.show        ;1 ;"SHOW  level log output; input is returned unchanged"                    )
   ;(`stl.log.info       ;`.miq.log.info        ;1 ;"INFO  level log output; input is returned unchanged"                    )
   ;(`stl.log.warn       ;`.miq.log.warn        ;1 ;"WARN  level log output; input is returned unchanged"                    )
   ;(`stl.log.error      ;`.miq.log.error       ;1 ;"ERROR level log output; input is returned unchanged"                    )
   ;(`stl.log.fatal      ;`.miq.log.fatal       ;1 ;"FATAL level log output; input is returned unchanged"                    )
    / trap
   ;(`stl.trap.apply     ;`.miq.trap.apply      ;2 ;".Q.trp wrapper returning (fail;output)"                                 )
   ;(`stl.trap.print     ;`.miq.trap.print      ;2 ;"Format stack trace using .Q.sbt and print it with signal to output"     )
    / ipc
   ;(`stl.ipc.connect    ;`.miq.ipc.connect     ;1 ;"Open new connection to process, returning its handle"                   )
   ;(`stl.ipc.disconnect ;`.miq.ipc.disconnect  ;1 ;"Close an existing connection to process"                                )
   ;(`stl.ipc.sync       ;`.miq.ipc.sync        ;2 ;"Send a synchronous request to handle"                                   )
   ;(`stl.ipc.async      ;`.miq.ipc.async       ;2 ;"Send an asynchronous request to handle"                                 )
   ;(`stl.ipc.flush      ;`.miq.ipc.flush       ;1 ;"Flush asynchronous requests on handle"                                  )
   ;(`stl.ipc.oneshot    ;`.miq.ipc.oneshot     ;2 ;"One-shot synchronous request to handle"                                 )
    / minor
   ;(`miq.seed           ;`.miq.u.seed          ;0 ;"Simple functions setting seed \\S value based of built-in 0Ng random"   )
    / module
   ;(`miq.module.classes        ;`.miq.module.classes         ;0 ;"Return a list of implemented classes"                                  )
   ;(`miq.module.library        ;`.miq.module.library         ;0 ;"Return info on a module, or on all if fed None"                        )
   ;(`miq.module.load           ;`.miq.module.load            ;2 ;"Load a module of specified class and name, but don't reload"           )
   ;(`miq.module.reload         ;`.miq.module.reload          ;2 ;"Explicit module reload"                                                )
   ;(`miq.module.path           ;`.miq.module.path            ;1 ;"Return path to the specified module"                                   )
   ;(`miq.module.resolve        ;`.miq.module.resolve         ;2 ;"Resolve class and module name to an actual registered module"          )
   ;(`miq.module.is.class       ;`.miq.module.is.class        ;1 ;"Return true if argument is a class"                                    )
   ;(`miq.module.is.warn.class  ;`.miq.module.is.warn.class   ;1 ;""                                                                      )
   ;(`miq.module.is.error.class ;`.miq.module.is.error.class  ;1 ;""                                                                      )
   ;(`miq.module.is.loaded      ;`.miq.module.is.loaded       ;1 ;"Return true if the module is loaded"                                   )
  )
 };

.miq.config.i.tmp.dir:{[] @[{get C.TMP.DIR};::;{` sv `$("miQ";8?.Q.an except"_";string .z.i)}]};

.miq.config.i.colour.codes:{[]
  (!). flip (
    (`red    ;255 0   0  )
   ;(`yellow ;255 255 0  )
   ;(`green  ;0   255 0  )
   ;(`cyan   ;0   255 255)
   ;(`blue   ;0   0   255)
   ;(`purple ;255 0   255)
   ;(`black  ;0   0   0  )
   ;(`white  ;255 255 255)
  )
 };

.miq.config.i.colour.modes:{[] ``off`bold`dim`invert`underline!-1 0 1 2 3 4};

/ TODO Add primitives to the table
.miq.config.i.qk.descriptions:{[]
  q:where in[;`op`fn`fp`qc`qi] .is.type each 1_.q;
  f:TT.DESCRIPTION upsert flip (q;.q[q];`variable);
  f:f,update {` sv'`.q,'x}name from f;
  f:f upsert flip (.Q.res;@[get;;()]'[string .Q.res];`variable);
  update return:`unknown from f where name in ({x,` sv'`.q,'x}`get`value`eval`reval`exit)
 };

.miq.config.i.qk.variable:`.miq.bb.new`.miq.cfg.explicit;
.miq.config.i.qk.function:1#`.miq.u.copy;

/ Default values
.miq.cfg.default[CFG.DEFAULT].'(
  / .miq.main[]
  (`main.once                     ;1b                           )
  / module configuration
 ;(`module.classes                ;0#`                          )
 ;(`module.module.envvar          ;`MIQ_MOD                     )
 ;(`module.module.extension       ;("*.[kq]";"*.k_";"*.q_")     )
 ;(`module.config.envvar          ;`MIQ_CFG                     )
 ;(`module.config.extension       ;("*.[kq]";"*.k_";"*.q_")     )
 ;(`module.role.envvar            ;`MIQ_ROLE                    )
 ;(`module.role.extension         ;("*.[kq]";"*.k_";"*.q_")     )
 ;(`module.so.envvar              ;`MIQ_SO                      )
 ;(`module.so.extension           ;enlist"*.so"                 )
  / namespace
 ;(`ns.reserved.list              ;`.q`.Q`.h`.j`.o`.m           )
 ;(`ns.reserved.enable            ;1b                           )
  / tmp storage
 ;(`tmp.enable                    ;0b                           )
 ;(`tmp.root                      ;`:/tmp                       )
 ;(`tmp.dir                       ;.miq.config.i.tmp.dir[]      )
 ;(`tmp.autocreate                ;0b                           )
 ;(`tmp.autoclear                 ;0b                           )
  / console
 ;(`console.debug                 ;0b                           )
 ;(`console.input.lang            ;"q"                          )
 ;(`console.input.quit            ;`local                       )
 ;(`console.input.block           ;0b                           )
 ;(`console.remember.lang         ;0b                           )
 ;(`console.remember.ch           ;0b                           )
 ;(`console.remember.d            ;1b                           )
  / colours
 ;(`colour.codes                  ;.miq.config.i.colour.codes[] )
 ;(`colour.modes                  ;.miq.config.i.colour.modes[] )
  / fd
 ;(`fd.STDIN                      ;0i                           )
 ;(`fd.STDOUT                     ;1i                           )
 ;(`fd.STDERR                     ;2i                           )
 ;(`fd.redirect.append            ;1b                           )
  / qk
 ;(`qk.variable                   ;.miq.config.i.qk.variable    )
 ;(`qk.function                   ;.miq.config.i.qk.function    )
  / hook
 ;(`hook.remove.safe              ;1b                           )
  / log | NOTE .miq.log.init[] holds duplicated values for bootstrapped initialisation
 ;(`log.level                     ;`show                        )
 ;(`log.caller                    ;1b                           )
 ;(`log.skip                      ;(".q.*";"*.is.*")            )
  / ipc
 ;(`ipc.flush                     ;0b                           )
  );

.miq.config.apply:{[]
  .miq.cfg.apply[CFG.CONSTANT;CFG.APPLIED;.miq.config.assemble]
    each `main.once`ns.reserved.list`ns.reserved.enable
   ,`tmp.enable`tmp.root`tmp.dir`tmp.autocreate`tmp.autoclear
   ,`console.debug`console.input.lang`console.input.quit`console.input.block
   ,`console.remember.lang`console.remember.ch`console.remember.d
   ,`colour.modes`log.level`log.caller`log.skip`ipc.flush`hook.remove.safe
   ,`fd.STDIN`fd.STDOUT`fd.STDERR`fd.redirect.append
   ,((`module.classes; {$[count x;x;.miq.module.i.checkEnv[]]})
    ;(`colour.codes  ; {@[{get C.COLOUR.CODES};::;{.miq.config.i.colour.codes[]}],x})
    ;(`qk.variable   ; {.miq.qk.describe'[x;`variable]})
    ;(`qk.function   ; {.miq.qk.describe'[x;`function]})
   );

  .miq.i.hook.init[];

  .miq.cfg.apply[CFG.CONSTANT;CFG.APPLIED;.miq.config.assemble]
    each (` sv'`module,'C.MODULE.CLASSES[]),'.miq.module.i.configure@'C.MODULE.CLASSES[];
  .miq.module.i.populate each C.MODULE.CLASSES[];

  TMP.PATH set ` sv C.TMP.ROOT[],C.TMP.DIR[];
  if[C.TMP.ENABLE[];.miq.tmp.enable[TMP.PATH[]]];

  / Logging subsystem initialisation
  .miq.log.init[];
  .miq.config.local[];
 };

.miq.config.local:{[]
  L.FD.DIR   ::` sv `:/proc,(`$string .z.i),`fd;
  L.IPC.FLUSH::C.IPC.FLUSH[];

  L.SYSTEM.COMMAND.VERIFIED::0#`;
 };



// =========
//  Runtime
// =========
// Default hook definition
/ kdb variables
.miq.z.dd.p:{.z.p};
.miq.z.dd.d:{.z.d};
.miq.z.dd.z:{.z.z};
.miq.z.dd.n:{.z.n};
.miq.z.dd.t:{.z.t};

/ kdb hooks
if[()~key`.miq.z.dd.ph; .miq.z.dd.ph:.z.ph];

/ Console
.miq.z.dd.console.prompt:{[ch;ns;cLang;bMLI]
  .is.error.any[`ch`chl;ch];
  .is.error[`ns`c`b]@'(ns;cLang;bMLI);

  user  :string[.z.u];
  at    :1#"@";
  host  :string[.z.h];
  handle:"${fg purple}",(-3!ch);
  dir   :(-3!ns);
  lang  :"${fg white}",cLang;
  end   :">${off} ";

  .miq.colour.process "${bg 51 51 51}",$[bMLI;"";user,at,host," "]," " sv (handle;dir;lang,end)
 }.;

// ========
//  Script
// ========
\d .
if[.miq.main[];
  if[.miq.system.is.variable`MIQ_MODULE_PATH;
    .miq.config.set[`module.module.directory] (),{$["`:"~2#x;get x;-1!`$x]} getenv`MIQ_MODULE_PATH;
    `MIQ_MODULE_PATH setenv ""
  ];

  .miq.init[];

  -1"miQ v",string[.miq.M.VERSION]," Copyright (C) 2019-2021 Michal Širochman";
  -1" - documentation: https://shahdee.gitlab.io/miq";
  -1" - support: https://gitlab.com/Shahdee/miq/-/issues";
  -1"";

  if[.miq.system.is.variable`MIQ_LOAD_TIME;
    `MIQ_LOAD_TIME setenv "";
    -1"Loaded in: ",-3!.z.p-.miq.LOADED`lzp
  ];
 ];



/
TODO think of a way to "borrow" configuration from other modules,
     i.e. `flush from IPC should be able to check for `ipc.flush of miQ,
     and if miQ has an explicit config, but IPC only implicit, make use of miQ config item
TODO .is.handler.transform[] to dereferrence all handlers in use? turn them from imported to exported? for perfomance reason or any other,
     i.e. .miq.h.ipc.sync --> turn `.miq.ipc.sync to {...}
TODO .miq.fn.info[] special cases for .[] / @[] parameter names? t c a ...
TODO function overloading (similar to current hook but not limited)
TODO hooking to be a special case of function overloading
TODO do something about the FUGLY LOADED HACK!!!
IDEA create a new function to store functions to execute when a definition gets loaded (trigger library?)
     i.e. log lib will ask: .miq.def.loaded? and if not, place a trigger function to .miq.def somewhere
TODO create a build library that will convert tags {[]} and {{[]}} to what's needed
IDEA .miq.in.typeCheck[] to implement ?[(`$())!();();();{x!x}get[.z.s][1]] via a pre-build phase
     maybe put it into .is.auto[]? you could possibly access both formal parameter names and their values, making input checks of simple functions effortless!
IDEA trap.stack.print & trap.stack.depth options?
TODO better module class handling? .miq.z.module.class.exist ...
TODO create more remote-safe functions, akin to .miq.qk.interpret
     i.e. .trap.apply, various .is checks, fd inspection ...

