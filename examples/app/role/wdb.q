/
system"p 9003"
h:hopen 9001
{x[0] set x[1]} h(`.tp.sub;`tab;`)
upd:insert

// EOD to save down tables and wipe them
end:{[d]
  .Q.dpft[`:/kdb/hdb;d;`sym;] each tables`;
  {@[`.;x;0#]}'[tables`.]
  }

/dr:{[s] :`date xcols update date:.z.d from 0!select avg size by `hh$time,sym from tab where sym=s};

//Helpful to debug incoming messages
//.z.ps:{ show .Q.s1 x;value x}

//Websocket hander evaluates queries and returns strings
.z.ws:{neg[.z.w].Q.s value x;}

//A websocket was open on handle 416
//This example published any new rows of data to the websocket as they arrived
//upd:{i:count get x;x insert y;neg[416] .Q.s (neg (count get x)-i)#get x}

//Example of error trapping update
//ALlows for replay of log file (with -11!) even if some messages are incorrect schema
//oldUpd:upd
//upd:{
// .[oldUpd;
//   (x;y);
//   {show "insert to;",string[x]," failed with ",z," data: ",.Q.s1 y}[x;y]
//   ]
// };

\
