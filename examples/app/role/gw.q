\p 9011

/h.rdb:hopen`::9002
/h.hdb:hopen`::9003

/dr:{[s]
/  rr:h.rdb("dr";s);
/  rh:h.hdb("dr";s);
/  :rh,rr
/  }

/ https://code.kx.com/q/cookbook/deferred-response
/ https://code.kx.com/q/ref/internal/#-30x-deferred-response
/ https://kx.com/blog/kdb-q-insights-deferred-response

wh:hopen'[9002 9003] / worker handles
pending:()!() / keep track of received results for each ch

/ each worker calls this with (0b;result) or (1b;errorString)
cb:{[ch;res]  / client handle, results
  pending[ch],:enlist res; / store the received result
  / check whether we have all expected results for this client
  if[count[wh]=count pending ch;
    / test whether any response (0|1b;...) included an error
    isError:0<sum pending[ch][;0];
    res:pending[ch][;1]; / grab the error strings or results
    / send the first error or the reduced result
    r:$[isError;{first x where 10h=type each x};raze]res;
    -30!(ch;isError;r);
    pending[ch]:(); / clear the temp results
  ]
  }

.z.pg:{[query]
  remoteFunction:{[clntHandle;query]
    neg[.z.w](`cb;clntHandle;@[(0b;)value@;query;{[errorString](1b;errorString)}])
  };
  neg[wh]@\:(remoteFunction;.z.w;query); / send the query to each worker
  -30!(::); / defer sending a response message i.e. return value of .z.pg is ignored
  }

