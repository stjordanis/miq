// ================== \\
//  Common code base  \\
// ================== \\

-1 "=== MIQ CORE & DEFINITIONS ===";
system"l ",getenv`MIQ_BASE;
system"l ",getenv`MIQ_BASE_CFG;
.miq.init[];
.miq.handler.export[`stl.log;`];

.log.info "=== SLOT HANDLING ===";
.miq.handler.get[`miq.module.load][`module;`slot];
.slot.init[.miq.handler.get[`miq.module.load][`config]];

.log.info "=== PROCESS NAME ===";
.STL.load`arg;
.STL.load`clan;

.log.info "=== HOOKS ===";
/.DEF.load`process;
.COMMON.load`hook;
.miq.hook.primary[`.log.z.generate.name;`app;.app.hook.log.generate.name];

.log.info "=== OTHER MODULES ===";
/.STL.load'[`log`ipc`clan];
.STL.load'[`log`ipc];
.log.set.level`show;

.log.info "=== OPTIONS SUPPLIED ===";
.log.show .arg.args[];

/
TODO redirect STDOUT and STDERR at the beginning to not lose output

