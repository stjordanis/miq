system"d ",string .miq.cfg.root[];

/ Check for envvars
.miq.system.require[`var;`MIQ_LOG];

// Logging
log.level    :`show;
log.STDOUT   :-1;
log.STDERR   :-2;
log.delimiter:" | ";      / Delimiter for log message components

log.caller.enable :1b;    / Print out the name of calling routine
log.dir.autocreate:1b;    / Autocreate log directory if it does not exist

// Log file
log.file.directory:`:.^-1!`$getenv[`MIQ_LOG];  / Log dir file symbol
log.file.extension:`log;                       / Logfile extension
log.file.mode     :0Nj;

// Log file rotation
log.rotate.enable   :1b;
log.rotate.mode     :`day;
log.rotate.threshold:1;

// Unit definition
/ On disk size
log.KiB:1024*;            / Kibibytes
log.MiB:log.KiB 1024 *;   / Mebibytes
log.GiB:log.MiB 1024 *;   / Gibibytes
log.KB :1000*;            / Kilobytes
log.MB :log.KB 1000 *;    / Megabytes
log.GB :log.MB 1000 *;    / Gigabytes
/ Number of lines
log.K  :1000*;            / Thousands
log.M  :log.K 1024 *;     / Milions

/
// Logging rules for processes (requires: `clan)
/ IDEA Have best match ignore order and simply look for highest level matching component?
log.rules:flip`tier`instance`clan`role`name`level`mode`threshold!flip(
  (` ;` ;`fx ; `    ;`          ; `info ; `day ; 1          )
 ;(` ;` ;`   ; `mat ;`          ; `show ; `day ; 7          )
 ;(` ;` ;`   ; `rdb ;`          ; `info ; `day ; 1          )
 ;(` ;` ;`   ; `fh  ;`          ; `info ; `size; log.MiB 100)
 ;(` ;` ;`ut ; `msg ;`          ; `info ; `line; log.K   10 )
 ;(` ;` ;`   ; `rdb ;`          ; `info ; `day ; 1          )
 ;(` ;` ;`   ; `rdb ;`rdb_ro    ; `debug; `day ; 7          )
 );

/
TODO Incorporate TIMEZONE setting
TODO Implement rules as hook

