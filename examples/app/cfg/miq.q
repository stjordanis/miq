system"d ",string .miq.cfg.root[];

/ Special constant to mark this config as loaded
miq.LOADED:`file`lzp!(-1!`$first -3#(get {});.z.p);

/ Check for envvars
.miq.system.require[`var;]'[`MIQ_SLOT`STL_SLOT];

// Namespace
miq.ns.delete.purge:1b;

// tmp
miq.tmp.enable    :1b;
miq.tmp.dir       :`miQ.app;
miq.tmp.autocreate:1b;

// Log
miq.log.level:`info;
miq.log.caller:1b;

// Module classes
/cfg.module.CLASS.envvar   :`ANY_ENVIRONMENT_VARIABLE
/cfg.module.CLASS.directory:(),`:/path/to/directory;
/cfg.module.CLASS.extension:enlist "*.[qk]"

miq.module.classes          :`module`config`role`so;
miq.module.module.directory :-1!'`$getenv each`MIQ_SLOT`STL_SLOT;
miq.module.config.envvar    :`CFG_SLOT;
miq.module.role.envvar      :`ROLE_SLOT;
miq.module.so.envvar        :`SO_SLOT;


/ TODO Implement switch for object verification
/CFG_O_VERIFICATION:` sv CFG_NS,`miq.o.verification;
/O_VERIFICATION:1b;
/

/
/ TODO allow configuration of module envvar>directory priority?

