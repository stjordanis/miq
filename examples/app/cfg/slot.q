system"d ",string .miq.cfg.root[];

/ Check for envvars
.miq.system.require[`var;]'[`MIQ_SLOT`STL_SLOT`CFG_SLOT`ROLE_SLOT`COMMON_SLOT`SO_SLOT];

// General
slot.load.manager:`;
slot.load.strict :0b;
slot.load.snap   :0b;

slot.parse.enrich:1b;
slot.parse.auto:1b;

slot.snap.seed:0;
slot.snap.default:0Wj;
slot.snap.strict:0b;

/ TODO option to enable / disable repeat code loading
/slot.reload:1b;

slot.classes:1!flip`class`interpreter!flip(
  (`module     ; "qk")
 ;(`config     ; "qk")
 ;(`role       ; "qk")
 ;(`so         ; 2:  )
  );

/ Parsing, extension to interpreter mapping
/slot.parse:1b;
slot.ext2int:1!flip`extension`interpreter!flip(
  (`k  ; "k")
 ;(`k_ ; "k")
 ;(`q  ; "q")
 ;(`q_ ; "q")
 ;(`so ; 2: )
  );

slot.slots:1!update upper name, -1!'`$directory from
  flip`name`class`directory!flip(
  (`MIQ    ;`module     ;getenv[`MIQ_SLOT]   )
 ;(`STL    ;`module     ;getenv[`STL_SLOT]   )
 ;(`CFG    ;`config     ;getenv[`CFG_SLOT]   )
 ;(`ROLE   ;`role       ;getenv[`ROLE_SLOT]  )
 ;(`COMMON ;`role       ;getenv[`COMMON_SLOT])
 ;(`SO     ;`so         ;getenv[`SO_SLOT]    )
  );

/ Functions available to links
/ TODO add exist function
/ TODO implement refresh function
slot.functions:flip`class`function`enabled!flip enlist(
  / Shared Object
  (`so ;`push ; 0b)
  );

// Auto initialisation for modules
/ slot      - slot for the module
/ name      - name of the module
/ extension - extension of the module
/ active    - is the rule active
/ init      - module's init function
/ args      - lambda's output serves as input to init function
slot.auto.enable:1b;
slot.rules:.miq.object.signature[`slot.module] xkey flip`slot`name`extension`active`init`args!flip(
  (`STL ;`slot   ;`q ;1b ;`.slot.init   ; {.CFG.load})
 ;(`STL ;`arg    ;`q ;1b ;`.arg.init    ; {.CFG.load})
 ;(`STL ;`env    ;`q ;1b ;`.env.init    ; {.CFG.load})
 ;(`STL ;`clan   ;`q ;1b ;`.clan.init   ; {.CFG.load})
 ;(`STL ;`ipc    ;`q ;1b ;`.ipc.init    ; {.CFG.load})
 ;(`STL ;`log    ;`q ;1b ;`.log.init    ; {.CFG.load})
 ;(`STL ;`exec   ;`q ;1b ;`.exec.init   ; {.CFG.load})
 ;(`STL ;`mat    ;`q ;1b ;`.mat.init    ; {.CFG.load})
 ;(`STL ;`role   ;`q ;0b ;`.role.init   ; {}         )
 ;(`STL ;`schema ;`q ;1b ;`.schema.init ; {}         )
 ;(`STL ;`seq    ;`q ;1b ;`.seq.init    ; {}         )
  );

/

