system"d ",string .miq.cfg.root[];

// General configuration
clan.timeout  :5000;            / hopen timeout in milliseconds
/clan.server   :`localhost;      / Override for template server definition
/clan.port.definition:`relative;
/clan.port.base:9700;
clan.manager  :`matriarch;

clan.connect.attempts:3;
clan.connect.interval:1000;

// Instance definition table
/ Defines a template from which an environment instance will be produced.
/ `tier - will be set up on .clan.init[] for all processes in scope. If `tier is a:
/   * standard symbol - it is a list of environments in which the process is to be present
/   * handle symbol   - is is a list of environments from which the process is to be excluded
/   * null symbol     - it represents all environments
/ `instance - optional, will be filtered by if exists, or created on the fly if not
/ `clan - defines membership of a process in a clan (multi-membership allowed)
/ `role - defines what role the process will assume
/ `name - process name, has to be unique
clan.instance:update {(),x} each tier, {(),x} each clan, {(),x} each role from
  flip`tier`instance`clan`role`name`server`port!flip(
  / A
/  (`      ;`A ;`fx`st ;`matriarch ;`matriarch;` ;0  )
  (`      ;`A ;`fx    ;`matriarch ;`matriarch;` ;0  )
 ;(`      ;`A ;`st    ;`matriarch ;`matriarch;` ;1  )
 ;(`      ;`A ;`fx`st ;`tp        ;`tp       ;` ;10 )
/ ;(`      ;`A ;`fx`st ;`gw        ;`gw       ;` ;5  )
 ;(`      ;`A ;`fx    ;`rdb       ;`rdb      ;` ;11 )
 ;(`      ;`A ;`fx    ;`wdb       ;`wdb      ;` ;12 )
 ;(`      ;`A ;`fx    ;`hdb       ;`hdb      ;` ;13 )
 ;(`      ;`A ;`fx    ;`fh        ;`fh       ;` ;19 )
 ;(`      ;`A ;`st    ;`rdb       ;`rdb      ;` ;20 )
 ;(`      ;`A ;`st    ;`wdb       ;`wdb      ;` ;21 )
 ;(`      ;`A ;`st    ;`hdb       ;`hdb      ;` ;22 )
 ;(`      ;`A ;`st    ;`fh        ;`fh       ;` ;29 )
  / B
 ;(`      ;`B ;`in    ;`matriarch ;`matriarch;` ;0  )
 ;(`      ;`B ;`in    ;`rdb       ;`rdb      ;` ;31 )
 ;(`      ;`B ;`in    ;`wdb       ;`wdb      ;` ;32 )
 ;(`      ;`B ;`in    ;`hdb       ;`hdb      ;` ;33 )
 ;(`      ;`B ;`in    ;`fh        ;`fh       ;` ;39 )

 ;(`:prod ;`B ;`fx`st ;`sup       ;`support  ;` ;99 )    / support process on all BUT `prod environments
 ;(`:qa   ;`B ;`fx`st ;`ro        ;`readonly ;` ;99 )    / readonly process on all BUT `qa environments
 ;(`prod  ;`B ;`fx`st ;`col       ;`collector;` ;99 )    / collector process only on `prod environment
 ;(`shagi ;`B ;`fx`st ;`sag       ;`shagi    ;` ;99 )    / shit and giggles
 ;(`qa    ;`B ;`fx`st ;`col       ;`test     ;` ;99 )    / test process on QA only
  );


/
TODO test, should be done --> allow `env groups, i.e. `qa`dev or `:qa`:dev
TODO enable referencing processes in the form of `env.instance(.clan.role)?
IDEA Server override for dev env? .cfg.clan.override.server

