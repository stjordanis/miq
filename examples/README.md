Example code
===
Under heavy development but should work out of the box.

App
===
Starts 2 independent clusters of processes, fx and st, controlled by their own matriarch. Not doing much at the moment, used mostly for testing and development.

1. Define `app/bin/q` as a `nohup` wrapper around your q binary, i.e.
```
#!/bin/bash

export QHOME=~/install/q/4.0
export QLIC=~/install/q

nohup ~/install/q/4.0/l64/q $@ 2>&1 &
```

2. Launch the application
```
cd app/
bin/exec.sh ../app.env managed.q -tier prod -instance A -role matriarch -clan fx st -common codebase.q -ports 9700-9800 -debug -p rp,1234
```

Script
===
A simple script to highlight simplicity and power of miQ.

```
cd script/
./script.sh script.q -p :1213 -name "test me!" -port 1212 -date 2020.03.12 -idxs SAP,FTSE,DOW
```

