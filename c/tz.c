#include "k.h"
#include <stdlib.h>     // malloc(), getenv(), setenv(), unsetenv()
#include <time.h>       // gmtime(), localtime(), mktime()

#define OFFSET  946684800       // Seconds from EPOCH to 2020.01.01
#define SUB_SEC 1000000000      // Sub-second precision (10e9 = nanos)

// Wrappers around time.h functions with error detection
struct tm *to_utc(const time_t *timep) {
    struct tm *time = gmtime(timep);
    if (time == NULL) krr("to_utc(): gmtime() failed");
    return time;
}

struct tm *from_utc(const time_t *timep) {
    struct tm *time = localtime(timep);
    if (time == NULL) krr("from_utc(): localtime() failed");
    return time;
}

time_t to_epoch(struct tm *tm) {
    time_t epoch = mktime(tm);
    if (epoch == -1) krr("to_epoch(): mktime() failed");
    return epoch;
}

// Routines intended for use from within K
K tz2utc(K in_tz, K in_time) {
    if (in_tz->t != KC || in_time->t != -KP)
        krr("type");

    char *TZ, *tz;
    tz = malloc(1+in_tz->n);
    tz = kC(in_tz); tz[in_tz->n] = '\0';

    TZ = getenv("TZ");

    J nanos = in_time->j%SUB_SEC;
    time_t from_time = (in_time->j/SUB_SEC)+OFFSET;

    setenv("TZ",tz,1);
    struct tm *from_tm = to_utc(&from_time);
    from_tm->tm_isdst = -1;
    time_t utc = to_epoch(from_tm);

    if (TZ)
        setenv("TZ", TZ, 1);
    else
        unsetenv("TZ");

    return ktj(-KP,(utc-OFFSET)*SUB_SEC+nanos);
}

K utc2tz(K in_tz, K in_time) {
    if (in_tz->t != KC || in_time->t != -KP)
        krr("type");

    char *TZ, *tz;
    tz = malloc(1+in_tz->n);
    tz = kC(in_tz); tz[in_tz->n] = '\0';

    TZ = getenv("TZ");

    J nanos = in_time->j%SUB_SEC;
    time_t from_time = (in_time->j/SUB_SEC)+OFFSET;

    setenv("TZ",tz,1);
    struct tm *to_tm = from_utc(&from_time);
    setenv("TZ","",1);
    time_t to_time = to_epoch(to_tm);

    if (TZ)
        setenv("TZ", TZ, 1);
    else
        unsetenv("TZ");

    return ktj(-KP,(to_time-OFFSET)*SUB_SEC+nanos);
}

K tz2tz(K in_tz1, K in_tz2, K in_time) {
    if (in_tz1->t != KC || in_tz2->t != KC || in_time->t != -KP)
        krr("type");

    // Store current TZ env and convert input timezones to strings
    char *TZ, *tz1, *tz2;
    TZ = getenv("TZ");

    tz1 = malloc(1+in_tz1->n);
    tz1 = kC(in_tz1); tz1[in_tz1->n] = '\0';

    tz2 = malloc(1+in_tz2->n);
    tz2 = kC(in_tz2); tz2[in_tz2->n] = '\0';

    // Decompose input time to EPOCH seconds and nanos
    J nanos = in_time->j%SUB_SEC;
    time_t from_time = (in_time->j/SUB_SEC)+OFFSET;

    // Transform EPOCH seconds @TZ1 to UTC
    setenv("TZ",tz1,1);
    struct tm *from_tm = to_utc(&from_time);
    from_tm->tm_isdst = -1;
    time_t utc = to_epoch(from_tm);

    // Transform UTC time to EPOCH seconds @TZ2
    setenv("TZ",tz2,1);
    struct tm *to_tm = from_utc(&utc);
    //from_tm->tm_isdst = -1;
    setenv("TZ","",1);
    time_t to_time = to_epoch(to_tm);

    // Restore TZ environment variable to original state
    if (TZ)
        setenv("TZ", TZ, 1);
    else
        unsetenv("TZ");

    // Return the result
    return ktj(-KP,(to_time-OFFSET)*SUB_SEC+nanos);
}


/*
TODO "P"$"1612144293" converts unix timestamps, make use of it!
TODO Why is tz2utc() 20x faster than utc2tz?
     mktime() is just slow, look at C++ chrono...
TODO Investigate possibility of C++ implementation
TODO Accept in_tz to be of -KS type (as that is interned as pointer to null terminated string)
TODO Maybe simply cast char lists to symbols using K? It will allow me to remove stdio.h include
TODO Encapsulate logic and get rid of repeated code sequences
*/
