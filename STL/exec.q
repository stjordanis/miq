// ========== \\
//  Executor  \\
// ========== \\

\d .exec
/ Meta
M.FAMILY  :`stl;
M.NAME    :`exec;
M.VERSION :`0.001.0;
M.COMMANDS:(),`nohup;

.miq.system.is.warn.command'[M.COMMANDS];

/ Special
SELF:system"d";
BB  :.miq.bb.new[SELF];

/ Config
CFG.EXPLICIT:.miq.cfg.explicit[SELF];
CFG.DEFAULT :` sv SELF,`config`default;
CFG.APPLIED :.miq.bb.new[` sv SELF,`config];
CFG.CONSTANT:` sv SELF,`C;

// ===========
//  Constants
// ===========
INIT:(` sv BB,`init) set 0b;

// ========
//  Config
// ========
.miq.fn.copy[`.exec.config.set     ;.miq.cfg.set[CFG.EXPLICIT;CFG.DEFAULT]];
.miq.fn.copy[`.exec.config.assemble;.miq.cfg.assemble[CFG.EXPLICIT;CFG.DEFAULT]];
.miq.fn.copy[`.exec.config.get     ;.miq.cfg.get[CFG.APPLIED]];

.miq.cfg.default[CFG.DEFAULT].'(
  (`cmd; 1#.q          )
 ;(`io ; (`int$())!`$())
 );

.exec.config.apply:{[]
  .miq.cfg.apply[CFG.CONSTANT;CFG.APPLIED;.exec.config.assemble]
    each `cmd`io;
 };

// =========
//  General
// =========
.exec.init:{[fnConfigLoader]
  .miq.handler.import[`stl.log;` sv SELF,`h];

  .exec.h.log.info "Initialising: ",-3!SELF;

  .miq.cfg.loader[fnConfigLoader;last` vs SELF];
  .exec.config.apply[];

  INIT set 1b;
  .exec.h.log.info "Initialisation completed: ",-3!SELF;
 };

// System execution
.exec.system:{[qdCmd]
  cmd:.exec.resolve[qdCmd];
  .miq.system.is.error.command `$first" " vs cmd`cmd;

  $[any`server`connector in key cmd;
      .exec.i.remote[cmd];
      .exec.i.local[cmd]
  ]
 };

.exec.resolve:{[qdCmd]
  / Input resolution
  cmd:$[
    .is.cl (),qdCmd; enlist[`cmd]!enlist[(),qdCmd];
    .is.qd    qdCmd; qdCmd;
                     '.exec.h.log.error "Invalid command definition: ",-3!qdCmd
  ];

  / Command resolution
  if[.is.s cmd`cmd; cmd:cmd,(1#`cmd)!enlist C.CMD[cmd`cmd]];
  cmd[`cmd]:(),cmd`cmd;
  if[not .is.cl cmd`cmd; '.exec.h.log.error "Cannot resolve command: ",-3!cmd`cmd];

  / Argument resolution
  if[not `arg in key cmd; cmd:cmd,(1#`arg)!enlist ""];
  if[not .is.cl cmd`arg; '.exec.h.log.error "Cannot resolve argument: ",-3!cmd`arg];

  / Redirection resolution
  cmd:cmd,(1#`io)!enlist $[
    not `io in key cmd; 0#C.IO[];
    .is.none cmd`io   ; C.IO[];
    .is.fs   cmd`io   ; 0 1 2i!3#cmd`io;
    .is.qd   cmd`io   ; cmd`io;
                        '.exec.h.log.error "Invalid io redirection: ",-3!cmd`io
  ];

  cmd[`io]:.exec.i.io[cmd`io];
  if[not .is.cl cmd`io; '.exec.h.log.error "Cannot resolve io redirection: ",-3!cmd`io];

  / Resolve nohup
  cmd:cmd,(1#`nohup)!1#`nohup in key cmd;

  cmd
 };

/ 0N -> append, write modes
/ all other keys are FDs / streams
.exec.i.io:{[qdIO]
  handles:key qdIO;
  $[all .is.any[`i`j]'[handles];
      handles:`int$handles;
      '.exec.h.log.error "Not a handle: ",-3!handles where not .is.any[`i`j]'[handles]
  ];

  / IDEA make the mode configurable per FD?
  / Write mode defauls to 'write'
  $[0Ni in handles;
      [mode:qdIO[0Ni]; io:0Ni _ qdIO];
      [mode:`write   ; io:qdIO]
  ];
  if[not mode in `write`append; '.exec.h.log.error "Not a valid file write mode: ",-3!mode];

  if[not .is.fsl get io; '.exec.h.log.error "Not a file symbol: ",-3!io where not .is.fs each io];
  if[0~count handles; :""];

  / Clumsy way of joining STDOUT and STDERR together, make this generic
  redirect:"";
  if[0i in handles; redirect,:" < ",1_string io[0i]; io:(1#0i)_ io];
  if[all 1 2i in handles;
    if[(=). io[1 2i]; redirect,:" &> ",1_string io[1i]; io:1 2i_ io]
  ];

  redirect,:raze{" ",string[x],"> ",1_string[y]}.'flip(key;get)@\:io;
  $[mode~`append; ssr[redirect;">";">>"]; redirect]
 };

.exec.i.local:{[qdCmd]
  cmd:" "sv qdCmd[`cmd`arg`io];
  cmd:$[qdCmd`nohup; "nohup ",cmd," &"; cmd,";"];

  .exec.h.log.info "Local execution: ",cmd;
  system cmd
 };

.exec.i.remote:{[qdCmd]
  '.exec.h.log.error "Remote execution not yet implemented";
  optional:`user`runas;
  /if `connector; system connector
  /if `server; system ssh server
 };

/
TODO allow defining env vars prior to execution?
TODO bg execution? method nohup / setsid?
TODO Attempt finding command if not defined in CMD
TODO Implement remote execution
TODO Verify that configured cmd.* are of type `cl
TODO Exit status of executed commands

