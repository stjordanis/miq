// ====== \\
//  clan  \\
// ====== \\
/ Clan management

\d .clan
M.FAMILY :`stl;
M.NAME   :`clan;
M.VERSION:`0.001.0;
M.OBJECTS:(),`clan.process;

/ Special
SELF    :system"d";
BB      :.miq.bb.new[SELF];

/ Object definition
O.PROCESS:.miq.object.define[1b;`clan.process;`tier`instance`clan`role`name;(`s;`s;`s`sl;`s`sl;`s)];
.miq.object.import[`clan.;` sv SELF,`o];

/ Config
CFG.EXPLICIT:.miq.cfg.explicit[SELF];
CFG.DEFAULT :` sv SELF,`config`default;
CFG.APPLIED :.miq.bb.new[` sv SELF,`config];
CFG.CONSTANT:` sv SELF,`C;

// ===========
//  Constants
// ===========
INIT    :(` sv BB,`init) set 0b;
IDENTITY:` sv BB,`identity;
INSTANCE:` sv BB,`instance;
UNIVERSE:` sv BB,`universe;

/ Table templates
TT.INSTANCE:o.process.tt[]!flip`server`port!"SJ"$\:();
TT.UNIVERSE:TT.INSTANCE^flip`address`handle`connect!"SIB"$\:();

// ========
//  Config
// ========
.miq.fn.copy[`.clan.config.set     ;.miq.cfg.set[CFG.EXPLICIT;CFG.DEFAULT]];
.miq.fn.copy[`.clan.config.assemble;.miq.cfg.assemble[CFG.EXPLICIT;CFG.DEFAULT]];
.miq.fn.copy[`.clan.config.get     ;.miq.cfg.get[CFG.APPLIED]];

.miq.cfg.default[CFG.DEFAULT].'(
  (`timeout          ;5000        )
 ;(`port.definition  ;`absolute   )
 ;(`port.base        ;0           )
 ;(`manager          ;`           )
 ;(`connect.attempts ;3           )
 ;(`connect.interval ;500         )
 ;(`instance         ;TT.INSTANCE )
  );

.clan.config.apply:{[]
  .miq.cfg.apply[CFG.CONSTANT;CFG.APPLIED;.clan.config.assemble]
    each `timeout`manager`connect.attempts`connect.interval`port.definition
   ,enlist (`port.base;{$[`relative~C.PORT.DEFINITION[];x;0]});

  if[.clan.manager[];
    .miq.cfg.apply[CFG.CONSTANT;CFG.APPLIED;.clan.config.assemble] (`instance;.clan.i.instantiate);
    .clan.universe.colonise[C.INSTANCE[]]
  ];
 };

.clan.config.check:{[]

 };

// =========
//  General
// =========
.clan.init:{[fnConfigLoader]
  .miq.handler.import[;` sv SELF,`h]'[`stl.log`stl.ipc];

  .clan.h.log.info "Initialising: ",-3!SELF;

  UNIVERSE set @[get;UNIVERSE;{TT.UNIVERSE}];
  INSTANCE set @[get;INSTANCE;{TT.INSTANCE}];
  .clan.set.identity[];

  .miq.cfg.loader[fnConfigLoader;last` vs SELF];
  .clan.config.apply[];

  .clan.h.log.info "Process identity: ",-3!.clan.self;
  if[.clan.manager[]; .clan.h.log.info "Number of processes: ",-3!count get C.INSTANCE];

  INIT set 1b;
  .clan.h.log.info "Initialisation completed: ",-3!SELF;
 };

.clan.set.identity:{[] get IDENTITY set .clan.self:o.process.construct .arg.get'[o.process.signature[]]};
.clan.self:()!();
/ IDEA replace self with identity?
/.clan.identity:()!();

/ TODO make this into a custom service discovery call, utilising .miq.z.service.discover
.clan.set.manager:{[sName] get .clan.config.set[`manager;sName]};
.clan.manager:{[]null[C.MANAGER[]]|C.MANAGER[] in IDENTITY[`role]};

/ Template instantiation
/ TODO add protocol column!
/ `tcp`unix`tcps!`:host:port:user:password`:unix://port:user:password`:tcps://host:port:user:password
.clan.i.instantiate:{[qmInstance]
  .is.error.qm[qmInstance];
  /override:@[get;SERVER;{()}];
  self:`tier`instance#.arg.args[];
  base:C.PORT.BASE[];

  template:select from qmInstance where .miq.member[self`tier] each tier;
  TT.INSTANCE upsert update tier:self[`tier]
   ,?[`=server;.z.h;server]
   ,port+base
    from $[`instance in cols template;
        select from template where instance=self[`instance];
        update instance:self[`instance] from template
    ]
 };

// ==================
//  Process handling
// ==================
// User friendly process definition & handle resolution
/ TODO partial object construction & .miq.object.verify on constructed keys instead of this hardcoded object type verification
.clan.resolve:{[procDef]
  .is.error.any[`list`qd;(),procDef];

  pattern:.clan.self,$[.is.list[(),procDef]; (neg[count procDef]#o.process.signature[])!(),procDef; procDef];
  if[not .miq.object.is.class[o.process.class[];pattern];
    '.miq.h.log.error "Process pattern construction failed: ",-3!procDef
  ];

  pattern
 };

.clan.handle:{[ch] exec from UNIVERSE where handle in ch};

// Connect & Disconnect logic
.clan.connect:{[procDef]
  process:0!.clan.select .clan.resolve[procDef];
  $[0~count process; '.clan.h.log.error "No matching process definition: ",-3!procDef;
    1~count process; .clan.i.connect first process;
                     .clan.i.connect each process
  ]
 };

.clan.disconnect:{[procDef]
  process:0!.clan.select .clan.resolve[procDef];
  $[0~count process; '.clan.h.log.error "No matching process definition: ",-3!procDef;
    1~count process; .clan.i.disconnect first process;
                     .clan.i.disconnect each process
  ]
 };

.clan.i.connect:{[qdProc]
  .is.error.qd[qdProc];
  if[all (.is.ch;not null@)@\:qdProc`handle; .clan.h.log.warn "Already connected: ",-3!qdProc; :qdProc`handle];
  if[null qdProc`address; .clan.h.log.error "No address to connect to: ",-3!qdProc; :0Ni];

  .clan.h.log.info "Connecting to process: ",-3!qdProc;
  handle:.miq.u.wait[C.CONNECT.ATTEMPTS[];C.CONNECT.INTERVAL[];.clan.h.ipc.connect;(qdProc`address;C.TIMEOUT[])];

  process:key .clan.i.select[UNIVERSE;o.process.construct[qdProc]];
  process[`handle`connect]:(handle;1b);
  .clan.universe.update o.process.signature[] xkey process;

  handle
 };

.clan.i.disconnect:{[qdProc]
  .is.error.qd[qdProc];
  if[null qdProc`handle; .clan.h.log.warn "Not connected: ",-3!qdProc; :qdProc`handle];

  .clan.h.log.info "Disconnecting from process: ",-3!qdProc;
  .clan.h.ipc.disconnect[qdProc`handle];

  process:key .clan.i.select[UNIVERSE;o.process.construct[qdProc]];
  process[`handle`connect]:(0Ni;0b);
  .clan.universe.update o.process.signature[] xkey process;

  qdProc[`handle]
 };

// Messaging
.clan.sync :{[procDef;message] .clan.i.send[procDef;message;1b]};
.clan.async:{[procDef;message] .clan.i.send[procDef;message;0b]};

.clan.i.send:{[procDef;message;sync]
  process:0!.clan.select .clan.resolve[procDef];
  if[0~count process; '.clan.h.log.error "No matching process definition: ",-3!procDef];

  available:select from process where not null handle;
  unavailable:select from process where null handle;

  if[0~count available  ; '.clan.h.log.error "No process available: ",-3!procDef];
  if[0<count unavailable; .clan.h.log.warn "Some processes are unavailable: ",-3!unavailable; .clan.h.log.show unavailable];

  $[sync;.clan.h.ipc.sync;.clan.h.ipc.async][available`handle;message]
 };

// Process identification
.clan.identify:{[ch]
  .is.error.ch[ch];
  process:.clan.h.ipc.sync[ch;(@;get;`.clan.self;{()})];
  if[()~process; process:.clan.h.ipc.sync[ch;(@;{x!`.arg.get x};o.process.signature[];{()})]];
  if[()~process;
    .clan.h.log.warn "Connection handle identification failed: ",-3!ch;
    :()
  ];

  / NOTE Removing belove as it is dangerous. If there's no clan nor pi module,
  /      there's nothing to control identity and connecting processes may
  /      misrepresent it's identity by adding a new one or removing an existing one
  /if[()~process;
  /  process:.arg.parse .clan.h.ipc.sync[ch;(`.z.X;::)];
  /  if[not all o.process.signature[] in key process; :()];
  /  process:o.process.construct[process];
  /];

  info:.clan.exec process;
  if[not()~info; if[ch~info[`handle]; :process]];

  / TODO don't just pick host and make up an address? This should be done by a dedicated module!
  / TODO use .clan.z.host hook to identify host?
  info:.clan.h.ipc.sync[ch;({(.z.h;system"p")};::)];
  .clan.universe.add enlist[process]!
    enlist`server`port`address`handle`connect!(info[0];info[1];-1!`$":"sv string info;ch;1b);

  process
 };

// ================================
//  Select & Exec & Ungroup & Show
// ================================
// Public
/ Returns a list of matching process!info records from UNIVERSE
.clan.select:{[procDef]
  process:.clan.resolve[procDef];
  .clan.i.reduce[;process] .clan.i.select[UNIVERSE;process]
 };

/ Returns full record for a single process from UNIVERSE
.clan.exec:{[procDef]
  process:.clan.resolve[procDef];
  match:.clan.i.reduce[;process] .clan.i.select[UNIVERSE;process];

  $[1~count match; first 0!match;
    0~count match; [.clan.h.log.warn "No matching process definition: ",-3!process; first 0!match];
                   [.clan.h.log.warn "Ambiguous process definition: ",-3!process; last 0!match]
  ]
 };

.clan.ungroup:{[oProcess]
  .miq.object.is.error.class[O.PROCESS;oProcess];
  /(cross/) flip each enlist'[o.process.signature[]]#\:raze'[oProcess]
  flip key[oProcess]!flip (cross/) raze'[oProcess]
 };

.clan.show:{[sTarget;procDef]
  .is.error.s[sTarget];
  target:get` sv SELF,upper[sTarget];
  if[(::)~procDef; :target[]];

  process:.clan.resolve[procDef];
  .clan.i.select[target;process]
 };

.clan.refresh:{[oProcess]
  m:.clan.resolve[C.MANAGER[],`];
  h:first exec handle from ?[UNIVERSE;enlist(.clan.i.match[m]each key UNIVERSE);0b;()];
  if[null h; '.clan.h.log.error "Manager disconnected: ",-3!m];
  p:$[.is.atom[h];::;raze] .clan.h.ipc.sync[h;(`.clan.select;oProcess)];
  if[count p; .clan.universe.update delete handle,connect from p];
 };

// Internal
/ Returns all matching records
.clan.i.select:{[t;oProcess]
  if[not[.clan.manager[]]; .clan.refresh[oProcess]];
  ?[t;enlist(.clan.i.match[oProcess]each key t);0b;()]
 };

/ Reduce process definition to fit process object definition
.clan.i.reduce:{[qkTable;oProcess]
  if[0~count qkTable; :qkTable];
  {{$[-11h~type x;x;(`~y)|y~1#`;x;x inter y]}.'flip(x;y)}[;oProcess]'[key qkTable]!get[qkTable]
 };

// =================================
//  Process matching and comparison
// =================================
// Public
/ Matches processes 1-to-1, null is treated as a wildcard
/ TODO Allow matching 1-to-many, returning B
.clan.match:{[oProcess1;oProcess2]
  .miq.object.is.error.class[O.PROCESS] each (oProcess1;oProcess2);
  .clan.i.match . o.process.signature[]#/:(oProcess1;oProcess2)
 };

/ Compare processes 1-to-1 and returns count of common values
/ TODO Allow comparing 1-to-many, returning B
.clan.compare:{[oProcess1;oProcess2]
  .miq.object.is.error.class[O.PROCESS] each (oProcess1;oProcess2);
  .clan.i.compare . o.process.signature[]#/:(oProcess1;oProcess2)
 };

// Internal
/ Objects must only contain o.process.signature[] keys AND the key order has to be identical!
.clan.i.match:{[oProcess1;oProcess2]
  matchExact:{(any x in y)|any y in x}.'flip (oProcess1;oProcess2);
  matchNull :(any each null oProcess1) | any each null oProcess2;
  all matchExact | matchNull
 };

/ Returns count of common values of two process objects
.clan.i.compare:{[oProcess1;oProcess2]
  count where {(any x in y)|any y in x}.'flip (oProcess1;oProcess2)
 };

// ==================================
//  Instance & Universe manipulation
// ==================================
// Internal
.clan.universe.i.self:{[]
  self:.clan.select[IDENTITY[]];
  self:update handle:0i from self;

  .clan.h.log.info "Setting 0i as handle to myself";
  .clan.h.log.show self;
  $[0~count self; .clan.h.log.warn "Identity matches no process";
    1<count self; .clan.h.log.warn "Identity matches more than one process";
                  .clan.universe.update[self]
  ]
 };

/ TODO move add & remove to select exec section
/ TODO show looks very much like select
.clan.i.add:{[sTarget;qkTable]
  .is.error[`s`qk]@'(sTarget;qkTable);
  if[not all .miq.object.is.class[o.process.class[]]'[key qkTable]; '.clan.h.log.error "Table key does not conform clan.process definition: ",-3!qkTable];
  target:get` sv SELF,upper[sTarget];

  .clan.h.log.info "Updating ",(-3!count qkTable)," from: ",-3!sTarget;
  .clan.h.log.show qkTable;
  target upsert qkTable;
 };

/ TODO update to remove individual process from a multi-identity process
/ TODO will remove whole row only when all identities have been removed
.clan.i.remove:{[sTarget;oProcess]
  .is.error.s[sTarget];
  .miq.object.is.error.class[O.PROCESS;oProcess];
  target:get` sv SELF,upper[sTarget];

  remove:key .clan.i.select[sTarget;oProcess];
  .clan.h.log.info "Removing [",(-3!count remove),"] members from: ",-3!sTarget;
  .clan.h.log.show remove;
  remove:key[target] in remove;
  delete from target where remove;
 };

// Public
.clan.instance.add   :.clan.i.add[`instance];
.clan.instance.update:.clan.i.add[`instance];
.clan.instance.remove:.clan.i.remove[`instance];
.clan.instance.show  :.clan.show[`instance];

.clan.universe.add   :.clan.i.add[`universe];
.clan.universe.update:.clan.i.add[`universe];
.clan.universe.remove:.clan.i.remove[`universe];
.clan.universe.show  :.clan.show[`universe];

.clan.universe.colonise:{[qmInstance]
  .is.error.qm[qmInstance];
  .clan.h.log.info "Colonising universe";
  /colony:0!update address:-1!'`$":"sv'string flip(server;port) from qmInstance;
  colony:0!update address:.clan.z.address .'flip(server;port) from qmInstance;
  UNIVERSE upsert o.process.signature[] xkey (cols[UNIVERSE] inter cols[colony])#colony;

  .clan.universe.i.self[];
 };

// =======
//  Hooks
// =======
.clan.z.address:{[sServer;jPort]
  .is.error[`s`j]@'(sServer;jPort);
  -1!`$":"sv string(sServer;jPort)
 };

/
IDEA move TT. to object definition file?
IDEA .clan.hook.connect to override IPC behaviour?
IDEA .clan.assume[] will assume a new identity? Or only add additional identity to itself?
TODO When connect=1 process disconnects, add a reconnection hook and timer func to check if
     the connection happened? Or should there be conn hook sent to Matriarch during initial
     connection and persisted for the lifetime?
TODO Locate codeserver within my clan / instance / env? How to do this?
TODO Implement UNIX SOCKETS for communication on the same host

