// =========== \\
//  Matriarch  \\
// =========== \\

\d .mat
VERSION:`0.001.0;
MODULE :enlist(`miq`0.001.0);

// ===========
//  Constants
// ===========
SELF  :system"d";
BB    :.miq.bb.new[SELF];
INIT  :` sv BB,`init;
Z.PROC:` sv SELF,`z.proc;
Z.OPTS:` sv SELF,`z.opts;

/ Config
CFG.EXPLICIT:.miq.cfg.explicit[SELF];
CFG.DEFAULT :` sv SELF,`config`default;
CFG.APPLIED :.miq.bb.new[` sv SELF,`config];
CFG.CONSTANT:` sv SELF,`C;

// ========
//  Config
// ========
.miq.fn.copy[`.mat.config.set     ;.miq.cfg.set[CFG.EXPLICIT;CFG.DEFAULT]];
.miq.fn.copy[`.mat.config.assemble;.miq.cfg.assemble[CFG.EXPLICIT;CFG.DEFAULT]];
.miq.fn.copy[`.mat.config.get     ;.miq.cfg.get[CFG.APPLIED]];

/ TODO figure out object sharing, maybe simply add a config item that tells matriarch what class defines a 'process'?
.miq.cfg.default[CFG.DEFAULT].'(
  (`opts.general     ;.miq.object.signature[`clan.process],`port`debug )    / TODO this is fugly, matriarch needs a complete replacement not a rewrite!!!
 ;(`emerge.sleep     ;100                     )
 /;(`emerge.inject    ;`$()                    )
 ;(`emerge.introduce ;`$()                    )
 ;(`emerge.restrict  ;`$()                    )
 ;(`hb.enable        ;0b                      )
 ;(`hb.interval      ;30000                   )
  );

.mat.config.apply:{[]
  .miq.cfg.apply[CFG.CONSTANT;CFG.APPLIED;.mat.config.assemble]
    each `opts`emerge.sleep`emerge.introduce`emerge.restrict`hb;
 };

// =========
//  General
// =========
/ Initiate
.mat.init:{[fnConfigLoader]
  .miq.handler.import[`stl.log;` sv SELF,`h];
  .miq.handler.import[`stl.ipc;` sv SELF,`h];

  .mat.h.log.info "Initialising: ",-3!SELF;

  .miq.cfg.loader[fnConfigLoader;last` vs SELF];
  .mat.config.apply[];

  / Callback namespace
  if[not .miq.ns.is.present Z.PROC; Z.PROC set 1#.q];
  if[not .miq.ns.is.present Z.OPTS; Z.OPTS set 1#.q];

  INIT set 1b;
  .mat.h.log.info "Initialisation completed: ",-3!SELF;
 };

// Process handling
/ Process materialisation
/ TODO codepath for manager / standard / slave processes
/ TODO allow multiple processes to be started at once for efficiency
.mat.emerge:{[oProcess]
  .miq.object.is.error.class[`clan.process;oProcess];
  .mat.h.log.info "Process emergence: ",-3!oProcess;

  conf:.clan.exec[oProcess];
  if[null conf[`tier]; .mat.h.log.error "No such process: ",-3!oProcess; .mat.h.log.error "Skipping emergence"];

  if[.ipc.test[conf`address]; .mat.introduce[;oProcess] h:.clan.connect[oProcess]; :h];

  opts:opts where not null opts:distinct raze C.OPTS[`general,conf[`role]];
  opts:((opts inter key[conf])#conf),
    (opts except key[conf])#.arg.args[];
  /distinct .mat.config.get[`opts][conf`role]except`;

  / TODO move args composition into exec module?
  args:.arg.compose[opts];
  out:.log.z.generate.file .log.z.generate.name[opts];
  .exec.system`cmd`arg`io!(`q;args;0N 1 2i!`append,2#out);
  .miq.u.sleep C.EMERGE.SLEEP[];
  handle:.clan.connect[oProcess];

  .mat.callback.opts[handle;opts];
  /.mat.callback.proc[handle;`general,conf`role];

  /handle({.miq.module.load[`role] each .arg.get`role};::);
  handle
 };

.mat.introduce:{[ch;oProcess]
  .is.error.ch[ch];
  .miq.object.is.error.class[`clan.process;oProcess];

  self:.mat.h.ipc.sync[ch;(`.clan.self;::)];
  if[not(~). C.EMERGE.RESTRICT[]#(self;oProcess);
    '.mat.h.log.error "Socket taken by foreign environment! ",-3!self
  ];

  missing:.miq.object.except[oProcess;self];
  opts:C.EMERGE.INTRODUCE[] inter where not 0~'count each missing;
  opts:opts#missing;

  if[0~count opts; .mat.h.log.warn "Nothing to introduce: ",-3!oProcess];
  .mat.h.log.info "Introducing following functionality: ",-3!opts;

  .mat.callback.opts[ch;opts];
  if[`role in key opts; .mat.callback.proc[ch;opts`role]];
  .mat.h.ipc.async each ch,'enlist each flip({.arg.set[x] .arg.get[x],y};;).(key;get)@\:opts
 };

.mat.abandon:{[oProcess]

 };

// ====================
//  Callback execution
// ====================
.mat.callback.opts:{[ch;dOpts]
  .is.error.ch[ch];
  cbs:key[Z.OPTS] inter key[dOpts];
  .mat.h.log.info "Opts callback execution: ",-3!cbs;
  (` sv'Z.OPTS,'cbs).'flip(ch;dOpts[cbs]);
 };

/.mat.callback.proc:{[ch;slProcess]
/  .is.error.ch[ch];
/  cbs:key[Z.PROC] inter slProcess;
/  .mat.h.log.info "Proc callback execution: ",-3!cbs;
/  (` sv'Z.PROC,'cbs).'flip(ch;cbs);
/ };


/
TODO have the configurable callbacks to return code to execute on remote handle
     so one can control sync/async nature of the requests
TODO Implement re-execution of libraries when pushing to isolated process
     (i.e. .arg will have incorrect data if only copied accross blindly)
     IDEA simply store parsed files, complete execution on remote...
     load first into .bb.mat.tmp, and when parsing successful, to .bb.mat.parsed
TODO Imlement REQUIRES keyword? As we need .clan
TODO Move all the per option / process callbacks?
     .clan.add[`role;`rdb]
     .clan.add[`clan;`fx]

