// ================= \\
//  Time & Timezone  \\
// ================= \\

\d .tz
/ Meta
M.FAMILY  :`stl;
M.NAME    :`tz;
M.VERSION :`0.001.0;
M.COMMANDS:(),`timedatectl;

.miq.system.is.warn.command'[M.COMMANDS];

/ Special
SELF:system"d";
BB  :.miq.bb.new[SELF];

/ Config
CFG.EXPLICIT:.miq.cfg.explicit[SELF];
CFG.DEFAULT :` sv SELF,`config`default;
CFG.APPLIED :.miq.bb.new[` sv SELF,`config];
CFG.CONSTANT:` sv SELF,`C;

// ===========
//  Constants
// ===========
INIT:(` sv BB,`init) set 0b;

ORIGINAL :` sv BB,`original;
TIMEZONES:` sv BB,`timezones;
SO.TZ    :` sv BB,`so`tz;
SO.LOADED:` sv BB,`so`loaded;

// ========
//  Config
// ========
.miq.fn.copy[`.tz.config.set     ;.miq.cfg.set[CFG.EXPLICIT;CFG.DEFAULT]];
.miq.fn.copy[`.tz.config.assemble;.miq.cfg.assemble[CFG.EXPLICIT;CFG.DEFAULT]];
.miq.fn.copy[`.tz.config.get     ;.miq.cfg.get[CFG.APPLIED]];

.miq.cfg.default[CFG.DEFAULT].'(
  (`so.class  ;`so               )
 ;(`so.name   ;`tz               )
 /;(`so.loader ; `.SO.load        )   / 1st this, if missing
 /;(`so.args   ; `tz              )   / then fallback to so.class/name
 ;(`timezone  ;"local"           )    / TODO make `timezone configuration item to contain an actual timezone!
 ;(`utc       ;("UTC";"Etc/UTC") )
  );

.tz.config.apply:{[]
  .miq.cfg.apply[CFG.CONSTANT;CFG.APPLIED;.tz.config.assemble]
    each `so.class`so.name`timezone`utc;

  / Get my C code
  {[x]
    if[get SO.LOADED set not first x; .tz.so.bind get SO.TZ set last x]
  } .trap.apply[.tz.h.module.load .;get'[C.SO`CLASS`NAME]];

  ORIGINAL set getenv`TZ;
  $[C.TIMEZONE[] in C.UTC[];
    .miq.hook.primary[`.tz.z.now;`.tz.utc  ;.tz.utc  ];
    .miq.hook.primary[`.tz.z.now;`.tz.local;.tz.local]
  ];
 };

// =========
//  General
// =========
.tz.init:{[fnConfigLoader]
  .miq.handler.import[;` sv SELF,`h]'[`stl.log`miq.module];

  .tz.h.log.info "Initialising: ",-3!SELF;

  / Generate a list of available timezones
  .miq.system.is.error.command[`timedatectl];
  TIMEZONES set @[get;TIMEZONES;{system"timedatectl list-timezones"}];

  .is.generate[`warn`error;;1;]./:\:.[;(::;0);` sv SELF,`is,]enlist(
    (`timezone  ;"Invalid TimeZone: " )
  );

  .miq.cfg.loader[fnConfigLoader;last` vs SELF];
  .tz.config.apply[];

  INIT set 1b;
  .tz.h.log.info "Initialisation completed: ",-3!SELF;
 };

/ Binds c functions to q names
.tz.so.bind:{[fpSo]
  / Conversion from and to UTC
  .tz.utc2tz:fpSo(`utc2tz;2);     / .tz.utc2tz[clTZ;pTime]
  .tz.tz2utc:fpSo(`tz2utc;2);     / .tz.tz2utc[clTZ;pTime]

  / Conversion between two arbitrary timezones
  .tz.tz2tz:fpSo(`tz2tz;3);       / .tz.tz2tz[clFromTz;clToTz;pTime]
 };

/ Accessors
.tz.now:{[] .tz.z.now ()};

.tz.timezones:{[clTZ]
  .is.error.any[`none`cl;clTZ]
  $[.is.none[clTZ] | .is.empty[clTZ]; TIMEZONES[]; TIMEZONES[] where TIMEZONES[] like x]
 };

/ Checks
.tz.is.timezone:{[cl] .is.error.cl[cl]; cl in TIMEZONES[]};

// ==========
//  Timezone
// ==========
/ TZ manipulation;
.tz.get:{[] C.TIMEZONE[]};

.tz.set:{[clTZ]
  .is.error.cl[clTZ];
  `TZ setenv clTZ;
  .tz.config.set[`timezone;clTZ];
  .tz.config.apply[]
 };

.tz.unset:{[] .tz.set[""]};
.tz.reset:{[] .tz.set ORIGINAL[]};

// =======
//  Hooks
// =======
/ Now implementations
.tz.local:{[] .z.P};
.tz.utc:{[] .z.p};


/
IDEA Allow creation of on the fly projections so various modules
     can operate at different timezones
IDEA Load C code into .tz.i... functions and wrap around? to convert cl to symbols...
IDEA Optimise TZ so if it behaves like "UTC" use .z.p instead of .z.P ?
TODO Allow both symbols and strings as timezones

