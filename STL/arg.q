// ============================ \\
//  Command-line options parser \\
// ============================ \\

\d .arg
/ Meta
M.FAMILY :`stl;
M.NAME   :`arg;
M.VERSION:`0.001.0;

/ Special
SELF:system"d";
BB  :.miq.bb.new[SELF];

/ Config
CFG.DEFAULT :` sv SELF,`config`default;
CFG.EXPLICIT:.miq.cfg.explicit[SELF];
CFG.APPLIED :.miq.bb.new[` sv SELF,`config];
CFG.CONSTANT:` sv SELF,`C;

// ===========
//  Constants
// ===========
INIT:(` sv BB,`init) set 0b;
OPTS:` sv BB,`opts;
ARGS:` sv BB,`args;

/ Table template
TT.OPTS:1!flip`opt`mandatory`cast`alias`description!"SBS**"$\:();

// ========
//  Config
// ========
.miq.fn.copy[`.arg.config.set     ;.miq.cfg.set[CFG.EXPLICIT;CFG.DEFAULT]];
.miq.fn.copy[`.arg.config.assemble;.miq.cfg.assemble[CFG.EXPLICIT;CFG.DEFAULT]];
.miq.fn.copy[`.arg.config.get     ;.miq.cfg.get[CFG.APPLIED]];

.arg.config.i.opts:{[]
  TT.OPTS upsert flip`opt`cast`description!flip(
    (`b ;`b    ;"Block write-access to a kdb+ database for any handle context other than 0"     )
   ;(`c ;`jl   ;"Set console maximum rows and columns, default 25 80"                           )
   ;(`C ;`jl   ;"Set HTTP display maximum rows and columns"                                     )
   ;(`e ;`j    ;"Set error-trapping mode, default to 0 (off)"                                   )
   ;(`E ;`j    ;"TLS Server Mode"                                                               )
   ;(`g ;`j    ;"Set garbage-collection mode"                                                   )
   ;(`l ;`b    ;"Log updates to filesystem"                                                     )
   ;(`L ;`b    ;"As -l, but sync logging"                                                       )
   ;(`m ;`fs   ;"Memory backed by a filesystem"                                                 )
   ;(`o ;`j    ;"Set local time offset as N hours from UTC, or minutes if abs[N]>23"            )
   ;(`p ;`port ;"Set listening port"                                                            )
   ;(`P ;`j    ;"Display precision for floating-point numbers"                                  )
   ;(`q ;`b    ;"Quiet mode"                                                                    )
   ;(`r ;`cs   ;"Replicate from :host:port"                                                     )
   ;(`s ;`j    ;"Number of secondary threads or processes available for parallel processing"    )
   ;(`t ;`j    ;"Period in milliseconds between timer ticks, default to 0 for no timer"         )
   ;(`T ;`j    ;"Timeout in seconds for client queries, default to 0 for no timeout"            )
   ;(`u ;`user ;"Disable access to system commands and restrict file access"                    )
   ;(`U ;`fs   ;"Set a password file for user authentication and disable \\x"                   )
   ;(`w ;`j    ;"Workspace limit in MB for the heap per thread, default to 0 for no limit"      )
   ;(`W ;`j    ;"Set the start-of-week offset, where 0 is Saturday, default to 2 for Monday"    )
   ;(`z ;`j    ;"Set the format for \"D\"$ date parsing: 0 for mm/dd/yyyy and 1 for dd/mm/yyyy" )
  )
 };

.miq.cfg.default[CFG.DEFAULT].'(
  (`opts                ;TT.OPTS                                    )
 ;(`mandatory           ;0#`                                        )
 ;(`strict              ;0b                                         )
 ;(`set.strict          ;1b                                         )
 ;(`assert.auto         ;0b                                         )
 ;(`assert.exit         ;0b                                         )
 ;(`assert.exitcode     ;1                                          )
 ;(`assert.usage        ;1b                                         )
 ;(`program.description ;"<No program description given>"           )
 ;(`program.usage       ;(string last` vs -1!.z.f)," [options] ..." )
 ;(`program.options     ;::                                         )
 ;(`help.option         ;`help                                      )
 ;(`help.auto           ;0b                                         )
 ;(`help.exitcode       ;0                                          )
  );

.arg.config.apply:{[]
  .miq.cfg.apply[CFG.CONSTANT;CFG.APPLIED;.arg.config.assemble] each
    `opts`mandatory`strict`set.strict
   ,`assert.auto`assert.exit`assert.exitcode`assert.usage
   ,`program.description`program.usage`program.options`help.option`help.auto`help.exitcode;

  / Include custom option definitions
  OPTS set update {`$string(),x} each alias from OPTS[] upsert C.OPTS[];

  / Define -help option
  if[C.HELP.AUTO[]; OPTS upsert `opt`cast`description!(`help;`b;"Print out help and exit")];

  / Update option definition of mandatory options
  ![OPTS;enlist(in;`opt;enlist C.MANDATORY[]);0b;(1#`mandatory)!1#1b];

  .arg.i.hook.init[];
  .arg.process[];

  if[.arg.is.supplied[C.HELP.OPTION[]] & C.HELP.AUTO[]; .arg.usage[]; exit C.HELP.EXITCODE[]];
  if[C.ASSERT.AUTO[]; .arg.assert[]];
 };

// ==========
//  Messages
// ==========
.arg.i.messages:{[]
  .is.generate[`warn`error;;1;]./:\:.[;(::;0);` sv SELF,`is,](
    (`option      ;"No such option: "       )
   ;(`supplied    ;"Option not supplied: "  )
   ;(`mandatory   ;"Option not mandatory: " )
   ;(`optional    ;"Option not optional: "  )
   ;(`alias       ;"No such alias: "        )
  );
 };

// ====================
//  Template blueprint
// ====================
.arg.i.templates:{[]
  .miq.template.blueprint flip`name`interface`implementer!flip(
    (`.arg.usage  ;`cl        ; `.arg.t.usageDescription          )
   ;(`.arg.usage  ;`cl`cl     ; `.arg.t.usageDescriptionUsage     )
   ;(`.arg.usage  ;`cl`sl     ; `.arg.t.usageDescriptionOpt       )
   ;(`.arg.usage  ;`cl`cl`sl  ; `.arg.t.usageDescriptionUsageOpt  )
   ;(`.arg.usage  ;`sl        ; `.arg.t.usageOpt                  )
   ;(`.arg.usage  ;```        ; `.arg.t.usageDescriptionUsageOpt  )
   ;(`.arg.usage  ;`          ; `.arg.t.usageNone                 )
  )
 };

// =========
//  General
// =========
.arg.init:{[fnConfigLoader]
  .miq.handler.import[`stl.log;` sv SELF,`h];
  .arg.i.messages[];
  .arg.i.templates[];

  .arg.h.log.info "Initialising: ",-3!SELF;

  OPTS set @[get;OPTS;.arg.config.i.opts[]];
  ARGS set @[get;ARGS;(0#`)!()            ];

  .miq.cfg.loader[fnConfigLoader;last` vs SELF];
  .arg.config.apply[];

  INIT set 1b;
  .arg.h.log.info "Initialisation completed: ",-3!SELF;
 };

.arg.i.hook.init:{[]
  / Generate functions for miQ type checking
  {[sCast]
    dd:` sv `.arg.z.dd.is,sCast;
    if[not .miq.fn.is.address dd;
      .arg.h.log.debug "Auto-generating miQ type checking hooks";
      code:"{[sOpt;arg] $[.is.",string[sCast],"[arg]; 1b; [.arg.z.message (sOpt;",(-3!sCast),";arg); 0b]]} .";
      fn:.miq.qk.interpret["q"] first .miq.qk.process[`$".arg.i.hook.init";-1;code];
      .miq.fn.copy[dd;fn]
    ];
  } each .miq.u.miq.types;

  / Initialise unitialised hooks that have default implementation
  {[sHook]
    if[not .miq.hook.is.defined sHook;
      .arg.h.log.debug "Initialising hook: ",-3!sHook;
      .miq.hook.expunge sHook
    ]
  } each `.arg.z.message,raze {` sv'`.arg.z,'flip (x;1_key` sv`.arg.z.dd,x)} each `parse`compose`is;
 };

.arg.assert:{[]
  mandatory:exec opt from .arg.mandatory.list[];
  supplied :key .arg.args[];
  missing  :mandatory except supplied;

  if[not .is.empty[missing];
    signal:.arg.h.log.error "Some mandatory options were not supplied: ",-3!missing;
    if[C.ASSERT.USAGE[]; .arg.usage[]];
    $[C.ASSERT.EXIT[]; exit C.ASSERT.EXITCODE[]; 'signal]
  ];
 };

.arg.t.usageDescription        :{[clDescription] .arg.i.usage[clDescription;::;::]};
.arg.t.usageDescriptionUsage   :{[clDescription;clUsage] .arg.i.usage[clDescription;clUsage;::]};
.arg.t.usageDescriptionOpt     :{[clDescription;slOpt] .arg.i.usage[clDescription;::;slOpt]};
.arg.t.usageDescriptionUsageOpt:{[clDescription;clUsage;slOpt] .arg.i.usage[clDescription;clUsage;slOpt]};
.arg.t.usageOpt                :{[slOpt] .arg.i.usage[::;::;slOpt]};
.arg.t.usageNone               :{[] .arg.i.usage[::;::;::]};

.arg.i.usage:{[clDescription;clUsage;slOpt]
  .is.error.any[`none`cl]@'(clDescription;clUsage);
  .is.error.any[`none`sl;slOpt];

  description:$[.is.none[clDescription]; C.PROGRAM.DESCRIPTION[]; clDescription];
  usage:"Usage: ",$[.is.none[clUsage]; C.PROGRAM.USAGE[]; clUsage];
  opts:$[.is.none[slOpt]; C.PROGRAM.OPTIONS[]; slOpt];

  -1 ("";description;"";usage);
  .arg.help[opts]
 };

/ TODO resolve also added aliases by default?
/ IDEA maybe allow passing in `opt1`opt2, and recognise projection, filling in all other user-defined options?
.arg.help:{[slOpt]
  .is.error.any[`none`sl;slOpt];

  opts:$[.is.none[slOpt];
      [distinct C.MANDATORY[],key[OPTS][`opt] except key[.arg.config.i.opts[]][`opt]];
      [.arg.is.error.option'[slOpt]; distinct .arg.resolve'[slOpt]]
  ];

  opts:select from OPTS where opt in opts;
  opts:update combined:{$[.is.empty[y];"-",x;"|" sv "-",/:enlist[x],y]}.'flip string(opt;alias) from opts;
  opts:update argument:("[",/:string[cast],\:"]") from opts;
  opts:update 0#'argument from opts where cast=`b;
  opts:update max[5+count'[combined]]$combined from opts;
  opts:update max[5+count'[argument]]$argument from opts;
  opts:update line:{"     ",x,y,z}.'flip(combined;argument;description) from opts;

  help:();
  if[count mandatory:select line from opts where mandatory;
    help,:("";"Mandatory options:"),mandatory`line
  ];

  if[count optional:select line from opts where not mandatory;
    help,:("";"Optional options:"),optional`line
  ];

  -1 help,enlist"";
 };

// Accessors
.arg.resolve:{[sOpt] .arg.alias.resolve[sOpt]};
.arg.native :{[] .arg.config.i.opts[] lj OPTS[]};
.arg.custom :{[] select from OPTS where not opt in exec opt from .arg.config.i.opts[]};

.arg.opts:{[sOpt]
  .is.error.any[`none`s;sOpt];
  OPTS $[.is.none[sOpt]; ::; .arg.resolve[sOpt]]
 };


.arg.args:{[sOpt]
  .is.error.any[`none`s;sOpt];
  ARGS $[.is.none[sOpt]; ::; .arg.resolve[sOpt]]
 };

// Checks
.arg.is.option   :{[sOpt] $[.is.s[sOpt]; (sOpt in raze (0!OPTS[])[`alias]) | sOpt in key OPTS; 0b]};
.arg.is.supplied :{[sOpt] $[.arg.is.option[sOpt]; .arg.resolve[sOpt] in key ARGS; 0b]};
.arg.is.mandatory:{[sOpt] $[.arg.is.option[sOpt]; OPTS[.arg.resolve[sOpt];`mandatory]; 0b]};
.arg.is.optional :{[sOpt] $[.arg.is.option[sOpt]; not OPTS[.arg.resolve[sOpt];`mandatory]; 0b]};
.arg.is.alias    :{[sAlias] $[.is.s[sAlias]; sAlias in key .arg.alias.list[]; 0b]};

// ====================
//  Arguments handling
// ====================
// Argument override and retrieval
.arg.set:{[sOpt;content]
  .is.error.s[sOpt];
  .arg.is.error.option[sOpt];

  opt:.arg.resolve[sOpt];
  .arg.i.verify[opt;OPTS[opt][`cast];content];

  if[not .arg.is.supplied[opt];
    $[C.SET.STRICT[];
        '.arg.h.log.error "Cannot set option: ",-3!opt;
        .arg.inject[opt;content]
    ]
  ];
  ARGS set ARGS[],(1#opt)!enlist content;
  content
 };

.arg.inject:{[sOpt;content]
  .is.error.s[sOpt];
  if[.arg.is.supplied[sOpt]; '.arg.h.log.error "Cannot override option: ",-3!sOpt];

  opt:.arg.resolve[sOpt];
  .arg.i.verify[opt;OPTS[opt][`cast];content];

  .arg.h.log.info "Injecting option value: ",-3!(1#sOpt)!enlist content;
  ARGS set ARGS[],(1#sOpt)!enlist content;
  content
 };

.arg.get:{[sOpt]
  .is.error.s[sOpt];
  .arg.is.error.option[sOpt];

  opt:.arg.resolve[sOpt];
  $[OPTS[opt;`cast]~`b; opt in key ARGS; ARGS[opt]]
 };

/.arg.type:{[sOpt]
/  opt:.arg.resolve[sOpt];
/  t:OPTS[opt][`cast];
/  l:`$last string t;
/  f:`$-1_string t;
/  $[t in .miq.u.miq.atoms         ; `atom;
/    t in .miq.u.miq.lists         ; `list;
/    (f in .miq.u.miq.types) & `r~l; `range;
/                                     t
/  ]
/ };

// Mandatory arguments
.arg.mandatory.list:{[] select from OPTS where mandatory};

.arg.mandatory.add:{[sOpt]
  .is.s[sOpt];
  .arg.is.error.option[sOpt];

  $[`mandatory in key CFG.EXPLICIT; @[CFG.EXPLICIT;`mandatory;,;sOpt]; CFG.EXPLICIT set CFG.EXPLICIT[],(1#`mandatory)!enlist(),sOpt];
  update mandatory:1b from OPTS where opt=.arg.resolve[sOpt];
  sOpt
 };

.arg.mandatory.remove:{[sOpt]
  .is.s[sOpt];
  .arg.is.error.option[sOpt];

  $[`mandatory in key CFG.EXPLICIT; @[CFG.EXPLICIT;`mandatory;except;sOpt]; CFG.EXPLICIT set CFG.EXPLICIT[],(1#`mandatory)!enlist(),sOpt];
  update mandatory:0b from OPTS where opt=.arg.resolve[sOpt];
  sOpt
 };

// Aliases
.arg.alias.list:{[sAlias]
  .is.error.any[`none`s;sAlias];
  aliases:(1!ungroup select alias,opt,cast from OPTS);
  aliases $[.is.none[sAlias]; ::; .arg.is.error.alias[sAlias]; sAlias]
 };

.arg.alias.resolve:{[slAlias]
  .is.error.any[`s`sl;slAlias];
  if[C.STRICT[]; .arg.is.error.option each slAlias];
  $[all .is[`sl`empty]@\:slAlias;
      slAlias;
      slAlias^.arg.alias.list[]'[slAlias][`opt]
  ]
 };

.arg.alias.add:{[sOpt;sAlias]
  .is.error[`s`s]@'(sOpt;sAlias);
  .arg.is.error.option[sOpt];

  .arg.h.log.info "Adding new option alias: ",-3!`opt`alias!sOpt,sAlias;
  $[.arg.is.alias[sAlias];
      '.arg.h.log.error "Alias already exists: ",-3!sAlias;
      OPTS upsert `opt`alias!(sOpt;OPTS[sOpt;`alias],sAlias)
  ];
  sAlias
 };

.arg.alias.remove:{[sAlias]
  .is.error.s[sAlias];
  .arg.is.error.alias[sAlias];

  opt:.arg.alias.list[sAlias][`opt];
  .arg.h.log.info "Removing alias: ",-3!`opt`alias!opt,sAlias;
  OPTS upsert `opt`alias!(opt;OPTS[opt;`alias] except sAlias);
  sAlias
 };

// Options & processing
.arg.describe:{[sOpt;clDescription]
  .is.error.cl[clDescription];
  .arg.is.error.option[sOpt];

  .arg.h.log.info "Describing option: ",-3!sOpt;
  OPTS upsert `opt`description!(.arg.resolve[sOpt];clDescription);
  sOpt
 };

.arg.add:{[sOpt;sCast]
  .is.error[`s`s]@'(sOpt;sCast);

  .arg.h.log.info "Adding new argument definition: ",-3!(sOpt;sCast;`$());
  $[.arg.is.option[sOpt];
      '.arg.h.log.error "Option already exist: ",-3!sOpt;
      OPTS upsert `opt`cast`alias!(sOpt;sCast;`$())
  ];

  sOpt
 };

.arg.remove:{[sOpt]
  .is.error.s[sOpt];
  .arg.is.error.option[sOpt];

  .arg.h.log.info "Removing argument definition: ",-3!exec from OPTS where opt=sOpt;
  delete from OPTS where opt=sOpt;
  sOpt
 };

.arg.i.verify:{[sOpt;sCast;arg]
  $[.miq.fn.is.address[is:` sv `.arg.z.is,sCast];
      is (sOpt;arg);
      $[C.STRICT[]; eval"'",enlist(.arg.h.log.error;)@; .arg.h.log.warn] "No hook to verify argument type: ",-3!`option`cast`argument!(sOpt;sCast;arg)
  ];
  arg
 };

/ Process input according to configuration, show them or inject a fake one
.arg.process:{[zx] ARGS set ARGS[],.arg.parse[zx]};

/ Register and process startup options
.arg.parse:{[zx]
  if[(not .is.none[zx]) & not all .is.cl'[zx]; '.arg.h.log.error "Invalid zx: ",-3!zx];

  opts:" " sv'.Q.opt $[.is.none[zx];.z.X;zx];
  if[not count opts; :opts];

  if[not all m:.arg.is.option'[key opts];
    $[C.STRICT[]; eval"'",enlist(.arg.h.log.error;)@; .arg.h.log.warn] "Not a recognised option or alias: ",-3!key[opts] where not m;
    opts:(key[opts] where m)#opts;
  ];
  opts:.arg.alias.resolve[key opts]!get opts;

  opts,k[`opt]!{[o;k]
    opt:k[`opt]; t:k[`cast]; arg:o[opt];

    if[.is.empty[arg] & not `b~t;
      $[C.STRICT[]; eval"'",enlist(.arg.h.log.error;)@; .arg.h.log.warn] "Option missing argument: ",-3!opt
    ];

    h:` sv SELF,`z`parse,t;
    .arg.i.verify[opt;t] $[
      .miq.hook.is.defined h; h (arg);
      .is.miq.atom[t]       ; .arg.z.parse.atom  (t;arg);
      .is.miq.list[t]       ; .arg.z.parse.list  (t;arg);
      "r"~last string t     ; .arg.z.parse.range (t;arg);
                              '.arg.h.log.error "Option parsing not implemented: ",-3!k
    ]
  }[opts] each k:0!select from OPTS where opt in key opts
 };

/ Build a command line argument list from interned options
.arg.compose:{[qdOpts]
  .is.error.qd[qdOpts];

  if[not all m:.arg.is.option'[key qdOpts];
    $[C.STRICT[]; eval"'",enlist(.arg.h.log.error;)@; .arg.h.log.warn] "Not a recognised option or alias: ",-3!key[qdOpts] where not m;
    qdOpts:(key[qdOpts] where m)#qdOpts     / TODO fugly!!!
  ];
  opts:.arg.alias.resolve[key qdOpts]!get qdOpts;

  opts:{[o;k]
    opt:k; t:OPTS[opt;`cast]; arg:o[opt];

    if[null[t] & not C.STRICT[]; :"-",string[opt]," ",$[.is.cl[arg];::;string] arg];
    .arg.i.verify[opt;t;arg];
    h:` sv SELF,`z`compose,t;

    s:"-",string[opt]," ",
    $[.miq.hook.is.defined h; h (arg);
      .is.miq.atom[t]       ; .arg.z.compose.atom  (t;arg);
      .is.miq.list[t]       ; .arg.z.compose.list  (t;arg);
      "r"~last string t     ; .arg.z.compose.range (t;arg);
                              '.arg.h.log.error "Option composition not implemented: ",-3!k
    ];
    neg[(reverse null s)?0b]_s
  }[opts] each where not all each null opts;

  " " sv opts where not opts~\:""
 };

// Utils
/ Splits character list using separator
.arg.i.split:{[cSep;clArg] .is.error.cl[clArg]; $[()~clArg; enlist""; cSep vs clArg]};
.arg.split.list :.arg.i.split[" "];
.arg.split.range:.arg.i.split["-"];

/ Converts options into their original form
.arg.i.join:{[cSep;cllArg] .is.error.cl'[cllArg]; $[1~count cllArg; raze cllArg; cSep sv cllArg]};
.arg.join.list :.arg.i.join[" "];
.arg.join.range:.arg.i.join["-"];

// =======
//  Hooks
// =======
/ User logging
.arg.z.dd.message:{[sOpt;sCast;arg]
  $[C.STRICT[]; eval"'",enlist(.arg.h.log.error;)@; .arg.h.log.warn] "Option argument type incorrect: ",-3!`option`cast`argument!(sOpt;sCast;arg)
 }.;

/ user type
.arg.z.dd.parse.user:{[clArg] $[first[clArg] in "01"; "J"$first clArg; -1!`$clArg]};
.arg.z.dd.compose.user:{[arg] $[is.j[arg]; string arg; -1_string arg]};
.arg.z.dd.is.user:{[sOpt;arg] $[.is.any[`j`fs;arg]; 1b; [.arg.z.message (sOpt;`user;arg); 0b]]}.;

/ port type
.arg.z.dd.parse.port:{[clArg]
  rp:("rp,"~3#clArg);
  arg:$[rp;3_;::] clArg;

  $[count[arg]~c:first arg?":";
    [host:""   ; port:arg      ];
    [host:c#arg; port:(1+c)_arg]
  ];

  host:`$host;
  port:$[all port in .Q.n; "I"$port; `$port];

  (rp;host;port)
 };

.arg.z.dd.compose.port:{[arg]
  $[1~count arg; string arg;
    2~count arg; $[.is.b[arg 0]; $[arg[0];"rp,";""]; $[.is.null[arg 0];"";string[arg 0],":"]],string arg 1;
    3~count arg; $[arg[0];"rp,";""],$[null arg 1; ""; string[arg 1],":"],string arg 2;
                 '.arg.h.log.error "Unable to compose port: ",-3!arg
  ]
 };

.arg.z.dd.is.port:{[sOpt;arg]
  b:$[
    1~count arg; .is.any[`whole`s;arg];
    2~count arg; .is.any[`b`s;arg 0] & .is.any[`whole`s;arg 1];
    3~count arg; all (.is.b[arg 0]; .is.s[arg 1]; .is.any[`whole`s;arg 2]);
                 0b;
  ];
  $[b; 1b; [.arg.z.message (sOpt;`port;arg); 0b]]
 }.;

/ Atom
.arg.z.dd.parse.atom:{[sCast;clArg]
  t:neg abs first .is.miq2q[sCast];
  if[1<count t; t:0h];

  r:$[
    t in 0N 0h; '.arg.h.log.error "Unable to cast: ",-3!sCast;
    t ~ -10h  ; first clArg;
                $[t<0h; t$; get] clArg
  ];

  .is.warn[sCast;r];
  r
 }.;

.arg.z.dd.compose.atom:{[sCast;arg] $[.is.c[arg]; ::; .is.cl[arg]; first; string] arg}.;

/ List
.arg.z.dd.parse.list:{[sCast;clArg]
  t:neg abs first .is.miq2q[sCast];
  if[1<count t; t:0h];

  r:$[
    t in 0N 0h; '.arg.h.log.error "Unable to cast to: ",-3!sCast;
    t ~ -10h  ; clArg;
                $[t<0h; t$; get each] .arg.split.list[clArg]
  ];

  .is.warn[sCast;r];
  r
 }.;

.arg.z.dd.compose.list:{[sCast;arg] $[.is.cl[arg]; arg; .arg.join.list string arg]}.;

/ Range
  / TODO generate full list of values; numbers, strings, symbols, ...
.arg.z.dd.parse.range:{[sCast;clArg]
  miqt:`$-1_string sCast;
  t:neg abs first .is.miq2q[miqt];
  if[1<count t; t:0h];

  .is.warn[miqt] each r:$[.is.q.atom[t]; t$; get each] .arg.split.range[clArg];
  r
 }.;

.arg.z.dd.compose.range:{[sCast;arg] .arg.join.range string arg}.;

/ Targeted
.arg.z.dd.parse.b:{[clArg] 1b};
.arg.z.dd.compose.b:{[arg] ""};

.arg.z.dd.parse.bl:{[clArg] "B"$'clArg};
.arg.z.dd.compose.bl:{[arg] -1_-3!arg};

.arg.z.dd.parse.fs:{[clArg] -1!`$clArg};
.arg.z.dd.compose.fs:{[arg] 1_string arg};

.arg.z.dd.parse.fsl:{[clArg] -1!'`$.arg.split.list[clArg]};
.arg.z.dd.compose.fsl:{[arg] .arg.join.list 1_'string arg};

.arg.z.dd.parse.cs:{[clArg] -1!`$clArg};
.arg.z.dd.compose.cs:{[arg] string arg};

.arg.z.dd.parse.csl:{[clArg] -1!'`$.arg.split.list[clArg]};
.arg.z.dd.compose.csl:{[arg] .arg.join.list string arg};

/
NOTE !!!!! Stop thinking that you can support positional arguments -> just have the user name them !!!!!
TODO Generate range of values?? A.K.A. expand logic for range... is it even desired?
IDEA List of ranges? i.e. jrl to handle 200-260 270-300?
TODO Figure out how to expose type "classes", i.e. list, ranges, how would one expand on that
IDEA Handle `c and `cl passing using .miq.u.text.qw[]?
IDEA Handle mixed lists using get?
TODO .arg.set[] and .arg.get[] are not handling boolean well, i.e. set to 0b would mess up the logic!
IDEA Upgrade atom and list hooks to handle non-basic types?
IDEA Type checking and casting should rely on .is and .to miQ capabilities!!!
     .arg should only allow to override / extend that

