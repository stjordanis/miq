// =============== \\
//  Spawn workers  \\
// =============== \\

\d .worker
/ Meta
M.FAMILY  :`stl;
M.NAME    :`worker;
M.VERSION :`0.001.0;
M.MODULES :enlist(`stl.ipc`0.001.0);
M.COMMANDS:`nohup`kill`eval`printf;

.miq.system.is.warn.command'[M.COMMANDS];

/ Special
SELF:system"d";
BB  :.miq.bb.new[SELF];

/ Config
CFG.EXPLICIT:.miq.cfg.explicit[SELF];
CFG.DEFAULT :` sv SELF,`config`default;
CFG.APPLIED :.miq.bb.new[` sv SELF,`config];
CFG.CONSTANT:` sv SELF,`C;

// ===========
//  Constants
// ===========
INIT:(` sv BB,`init) set 0b;

ORIGIN.ID:` sv BB,`origin`id;
WORKER.ID:` sv BB,`worker`id;
WORKERS  :` sv BB,`workers;

THREADS.MAX    :(` sv BB,`threads`max    ) set system"s 0N";
THREADS.CURRENT:(` sv BB,`threads`current) set 0i|system"s";

/ Table template
TT.WORKERS:1!flip`id`active`handle`address`pid`created`killed`requestor!"JBISIPPI"$\:();

// ========
//  Config
// ========
.miq.fn.copy[`.worker.config.set     ;.miq.cfg.set[CFG.EXPLICIT;CFG.DEFAULT]];
.miq.fn.copy[`.worker.config.assemble;.miq.cfg.assemble[CFG.EXPLICIT;CFG.DEFAULT]];
.miq.fn.copy[`.worker.config.get     ;.miq.cfg.get[CFG.APPLIED]];

.miq.cfg.default[CFG.DEFAULT].'(
  (`envvar            ;`$"__MIQ_WORKER__" )
 ;(`process.cleanup   ;0b                 )   / TODO .worker.i.cleanup[jId] will kill all parent PIDs but 1
 ;(`port.restrict     ;1b                 )
 ;(`port.range        ;49152 65535        )   / IANA recommendation for dynamic or private ports
 ;(`connect.attempts  ;10                 )
 ;(`connect.timeout   ;100                )
 );

.worker.config.apply:{[]
  .miq.cfg.apply[CFG.CONSTANT;CFG.APPLIED;.worker.config.assemble]
    each `envvar`process.cleanup`port.restrict`port.range`connect.attempts`connect.timeout;

    .miq.hook.register[`.z.pc;`.worker.pc;.worker.pc];
 };

// ==========
//  Messages
// ==========
.worker.i.messages:{[]
  .is.generate[`warn`error;;1;]./:\:.[;(::;0);` sv SELF,`is,](
    (`present ;"No such worker: "   )
   ;(`active  ;"Worker not active: ")
  );
 };

// ===========
//  Interface
// ===========
.worker.i.interface:{[]
  api:.miq.ns.prune[`h`i`t`z`C`L`config] .miq.ns.trim[``init] .miq.ns.tree[`.worker];
  api:exec name from group api;
  api:api where not .miq.syntax.name.is.constant'[api];

  handler    :` sv'`stl,'`$1_'string api;
  implementer:api;
  arity      :.miq.fn.arity'[api];

  / TODO Make this nice and describe every function!
  .miq.handler.define[1b;;;;"!!! Description missing !!! WORKER module public interface dump"]'[handler;implementer;arity]
 };

// =========
//  General
// =========
.worker.init:{[fnConfigLoader]
  .miq.handler.import[;` sv SELF,`h]'[`stl.log`stl.trap`stl.ipc];

  .worker.h.log.info "Initialising: ",-3!SELF;

  .worker.i.messages[];
  .worker.i.interface[];

  ORIGIN.ID set @[get;ORIGIN.ID;{256 sv 4#0x0 vs first -1?0Ng}];
  WORKER.ID set @[get;WORKER.ID;{0}                           ];
  WORKERS   set @[get;WORKERS  ;{TT.WORKERS}                  ];

  .miq.cfg.loader[fnConfigLoader;last` vs SELF];
  .worker.config.apply[];

  INIT set 1b;
  .worker.h.log.info "Initialisation completed: ",-3!SELF;
 };

// Accessors
.worker.origin :{[] ORIGIN.ID[]};
.worker.workers:{[] WORKERS[]};
.worker.active :{[] select from WORKERS where active};
.worker.handles:{[] exec handle from WORKERS where active};

// =================
//  Worker handling
// =================
/ Checks
.worker.is.present:{[jId] .is.j[jId]; jId in key WORKERS};
.worker.is.active :{[jId] .worker.is.present[jId]; WORKERS[jId][`active]};

/ .z.pd handling
.worker.zpd.add:{[ch] .is.error.ch[ch]; .z.pd:@[get;`.z.pd;`int$()],`int$ch; ch};
.worker.zpd.remove:{[ch] .is.error.any[`j`i;ch] ; .z.pd:@[get;`.z.pd;`int$()] except `int$ch; ch};

.worker.info   :{[jId] .worker.is.error.active[jId]; WORKERS[jId]};
.worker.handle :{[jId] .worker.info[jId][`handle]};
.worker.address:{[jId] .worker.info[jId][`address]};
.worker.pid    :{[jId] .worker.info[jId][`pid]};

/ Worker handling
.worker.add:{[jId;ch;iPid]
  .is.error[`j`ch`i]@'(jId;ch;iPid);
  .worker.h.log.info "Registering new worker: ",-3!`id`active`handle`pid!(jId;1b;ch;iPid);
  .worker.zpd.add[ch];
  WORKERS upsert `id`active`handle`pid!(jId;1b;ch;iPid);
  jId
 };

/ TODO implement .z.exit on workers to report their demise
/.worker.remove:{[jId] .worker.zpd.remove[]; WORKERS upsert `id`active!(jId;0b); jId};

.worker.connect:{[jId]
  .is.error.j[jId];

  .worker.h.log.info "Connecting to worker: ",-3!jId;

  address:WORKERS[jId][`address];
  if[null address; '.worker.h.log.error "Worker ID has no address to connect to: ",-3!jId];

  handle:.worker.zpd.add .miq.u.wait[C.CONNECT.ATTEMPTS[];C.CONNECT.TIMEOUT[];.worker.h.ipc.connect;address];
  pid:.worker.h.ipc.sync[handle;".z.i"];
  WORKERS upsert `id`active`handle`pid!(jId;1b;handle;pid);
  jId
 };

.worker.disconnect:{[jId]
  .is.error.j[jId];

  .worker.h.log.info "Disconnecting worker: ",-3!jId;
  handle:.worker.zpd.remove WORKERS[jId][`handle];
  if[.is.ch[handle]; .worker.h.ipc.disconnect[handle]];

  WORKERS upsert `id`active!(jId;0b);
  jId
 };

/ TODO manager resolution to be part of clan hooks
.worker.manager:{[]

 };

/ Worker spawning and killing
.worker.new:{[jWorkers;sMethod;address]
  .is.error[`j`s]@'(jWorkers;sMethod);

  if[jWorkers<1; .worker.h.log.warn "Attempt to create an invalid number of new workers: ",-3!jWorkers; :0#0];
  if[not sMethod in `port`portless; '.worker.h.log.error "Invalid worker connection method: ",-3!sMethod];

  / Below will have to be moved to sMethod implementation when updating for MANAGER
  list:$[
    (sMethod~`port) & .is.none[address] | (|). .worker.h.ipc.is[`address`listener]@\:address       ; 0b;
    (sMethod~`port) & all .is.none'[address] | (|). .worker.h.ipc.is[`address`listener]@\:/:address; 1b;
    (sMethod~`portless) & .worker.h.ipc.is.address[address]                                        ; 0b;
    (sMethod~`portless) & all .worker.h.ipc.is.address'[address]                                   ; 1b;
                                                                                                     '.worker.h.log.error "Invalid worker address(es) supplied: ",-3!address
  ];

  if[list & not jWorkers~count address;
    '.worker.h.log.error "Invalid number of worker addresses: ",-3!`jWorkers`address!(jWorkers;count address)
  ];

  id:$[list;
    .worker.i.new[sMethod]'[address];
    .worker.i.new[;address]'[jWorkers#sMethod]
  ];

  / TODO do not auto-connect?
  /      causes problems when starting up multiple workers using port sharding
  connect:id where WORKERS'[id][`address]<>`;
  if[not .is.empty[connect]; .worker.connect'[connect]];
  id
 };

.worker.i.new:{[sMethod;address]
  / my global ID ~ gid:4#0x0 vs first -1?0Ng
  / my worker ID ~ `$raze string gid,0x0 vs 1i
  id:-6!(+:;WORKER.ID;1);

  WORKERS upsert `id`requestor!(id;.z.w);
  addr:.worker.i[sMethod][id;address];          / MANAGER to enhance local address with hostname
  addr:$[.is.none[addr]; `; .worker.h.ipc.to.address[addr]];
  WORKERS upsert `id`created`address!(id;.z.p;addr);

  id
 };

.worker.i.port:{[jId;address]
  .miq.system.is.error.command'[`nohup`ps`grep];

  listener:.ipc.to.listener $[
    .is.none[address];
      first .worker.h.ipc.port.random[1] $[C.PORT.RESTRICT[]; C.PORT.RANGE[]; ::];
      address
  ];

  /.worker.i.system[jId] "system\"p ",listener,"\"; .z.pc:{$[count .z.W;::;exit 0]}";
  .worker.i.system[jId] "system\"p ",listener,"\"; .z.pc:{[x] $[x~0i;::;count .z.W;::;exit 0]}";
  listener
 };

/ How to make handles to register and ensure correctness?
.worker.i.portless:{[jId;address]
  .miq.system.is.error.command'[`nohup`eval`printf];

  .worker.i.system[jId] "neg[hopen `$\"",(string .worker.h.ipc.to.address[address]),"\"] ({.worker.add[x;.z.w;y]};\"J\"$.z.X 1+.z.X?\"-worker-id\";.z.i); .z.pc:{[x] $[x~0i;::;count .z.W;::;exit 0]}";
 };

/ TODO exec module should handle this
/ TODO exec should also allow to redirect output to a useful location
.worker.i.system:{[jId;clCommand]
  C.ENVVAR[] setenv clCommand;

  command:"{ nohup q -origin-id ",string[ORIGIN.ID[]]," -worker-id ",string[jId]," </dev/null &>/dev/null <<'EOF'\n"
   ,"-6!parse getenv`$\"",(string C.ENVVAR[]),"\"\n"
   ,"EOF\n"
   ,"} &";

  / Remember that you escape to preserve escapes for printf!
  command:ssr[command;"\n";"\\n"];
  command:ssr[command;"\"";"\\\""];
  command:ssr[command;"`";"\\`"];

  system"eval \"$(printf \"",command,"\")\"";
  C.ENVVAR[] setenv "";
 };

.worker.kill:{[jId]
  .is.error.j[jId];
  if[not jId in key WORKERS[]; '.worker.h.log.error "No such worker: ",-3!jId];
  .miq.system.is.error.command[`kill];

  pid:WORKERS[jId][`pid];
  .worker.h.log.info "Killing worker ",-3!`id`pid!(jId;pid);
  out:.worker.h.trap.apply[system;"kill -9 ",string[pid]," &>/dev/null"];
  if[out[0]; '.worker.h.log.error "Kill failed: '",out[1;0]];

  WORKERS upsert `id`active`killed`requestor!(jId;0b;.z.p;.z.w);
  jId
 };

/ Hooks
/ connect to all workers when in MANAGER mode, and register their demise when they are closed
.worker.pc:{[ch]
  if[not .is.empty id:first exec id from WORKERS where handle=ch; .worker.disconnect[id]];
 };

// =================
//  Thread handling
// =================
.worker.threads.max    :{[] THREADS.MAX[]};
.worker.threads.current:{[] THREADS.CURRENT[]};
.worker.threads.disable:{[] .worker.h.log.info "Disabling threading"; system"s 0"};

.worker.threads.set:{[threads]
  .is.error.whole[threads];

  .worker.h.log.info "Setting number of threads to: ",string threads;
  system"s ",string threads;
  get THREADS.CURRENT set `int$threads
 };

/
TODO peach override to allow
     - specifying the number of workers
     - asynchronous work distribution (maybe using -30!)
TODO .worker.is.active / present / used (in .z.pd)
TODO WORKERS cleanup, handle cleanup, .z.pc
TODO allow pre-spawning of workers, setting number of workers to be maintained and allow to lease them to other processes
TODO spawning of processes should be done using exec module to allow for:
     - control of q command (q, q32, ...)
     - control of stream redirect
     - execution on remote
     - possibly handling of HEREDOCs by wrapping the commands in { ... } &
TODO allow threaded workers
TODO spawn workers for:
     - self, connects on its own (if I have port)
     - self, spawning process connects (requires child to listen on port)
     - others, either connecting on its own or returning addresses
TODO MANAGER mode: serving workers for other processes (disable .z.pd when in MANAGER mode)
NOTE should threading be even done at all? given it is so simple
IDEA processor afinity?
IDEA custom limits (ulimits) per process?
IDEA prevent workers from accessing system? (restricted workers)

