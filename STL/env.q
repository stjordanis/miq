// ============= \\
//  Environment  \\
// ============= \\
\d .env
/ Meta
M.FAMILY  :`stl;
M.NAME    :`env;
M.VERSION :`0.001.0;
M.COMMANDS:(),`env;

.miq.system.is.warn.command'[M.COMMANDS];

/ Special
SELF:system"d";               / My namespace
BB  :.miq.bb.new[SELF];       / My blackboard section

// ===========
//  Constants
// ===========
INIT:(` sv BB,`init) set 0b   / Init flag
VARS:` sv BB,`vars;           / Where to store the environment variables

// ==========
//  Messages
// ==========
.env.i.messages:{[]
  .is.generate[`warn`error;;1;]./:\:.[;(::;0);` sv SELF,`is,](
    (`variable    ;"No such environment variable: "       )
   ;(`interned    ;"Environment variable not interned: "  )
  );
 };

// =========
//  General
// =========
.env.init:{[]
  .miq.handler.import[`stl.log;` sv SELF,`h];
  .miq.handler.import[`stl.ipc;` sv SELF,`h];
  .env.i.messages[];

  .env.h.log.info "Initialising: ",-3!SELF;
  .env.build[];
  INIT set 1b;
  .env.h.log.info "Initialisation completed: ",-3!SELF;
 };

/ Accessors
.env.vars:{[] VARS[]};

// ==========
//  Handling
// ==========
/ Checks
.env.is.variable:{[s] $[.is.s[s]; not ""~getenv s; 0b]};
.env.is.interned:{[s] $[.is.s[s]; s in key VARS; 0b]};

/ Generate a dict holding all system variables
.env.build:{[]
  .miq.system.is.error.command[`env];
  .env.h.log.info $[()~key VARS;"B";"Reb"],"uilding environment variables cache";
  VARS set .env.i.toDict system"env";
 };

/ Turn system"env" into dictionary
.env.i.toDict:{[systemenv] (!)."S=*\000"0:raze systemenv,\:"\000"};

.env.get:{[sName]
  .is.error.s[sName];
  .env.is.error.variable[sName];
  if[not .env.is.interned[sName]; .env.build[]];
  VARS[sName]
 };

.env.set:{[sName;clContent]
  .is.error[`s`cl]@'(sName;clContent);
  .env.h.log.info "New environment variable: ",-3!`name`content(sName;clContent);
  sName setenv clContent;
  .env.build[];
 };

/ NOTE Is this obsolete?
/ Sends missing environment variables to target
.env.send:{[ch]
  .is.error.ch[ch];

  remote:.env.i.toDict .env.h.ipc.sync (ch;(system;"env"));
  vars  :key[VARS] except key[remote];
  if[0~count vars; .env.h.log.info "No environment variables to send: ",-3!ch; :vars];

  .env.h.log.info "Sending environment variables to handle: ",-3!ch;
  .env.h.log.show vars#VARS[];
  /.env.h.ipc.sync (ch;({[x;y]if[x~key x;x[]]}[`.env.build]@{x setenv y}.'flip@;(vars;VARS[vars])));
  .env.h.ipc.sync (ch;({@[.env.build;::;{}]}@{x setenv y;@[.env.build;::;{}]}.'flip@;(vars;VARS[vars])));
  .env.h.log.info "Environment variables delivered";

  vars
 };


/
TODO This looks rather obsolete and requires a facelift
IDEA This should be a part of miq base?
IDEA Should .env.send receive dict of process def,handle,vars to send?
     `    - missing only
     `var - overwrite var
IDEA Turn this into generic system module?

