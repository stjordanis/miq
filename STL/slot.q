// ============================ \\
//  Slot and module management  \\
// ============================ \\
/ Definition of terms:
/ * Slot   - directory holding modules
/ * Module - any named file within a slot whose extension is recognised using ext2int mapping
/   * There are multiple forms of modules, i.e. `module`config`role
/ * Link   - namespace named after slot, it's function allowing interaction with it's modules
/:
/ Code flow:
/ * .LINK.load[] accepting a module's `name or (`name`ext), turning it into MODULE object
/ * .slot.load[] accepting MODULE object
/ *
/: ========================= :\

\d .slot
/ Meta
M.FAMILY :`stl;
M.NAME   :`slot;
M.VERSION:`0.001.0;
M.OBJECTS:(),`slot.module;

/ Special
SELF   :system"d";
BB     :.miq.bb.new[SELF];

/ Object definition
O.MODULE:.miq.object.define[1b;`slot.module;`slot`name`extension;3#`s];
.miq.object.import[`slot.;` sv SELF,`o];

/ Config
CFG.EXPLICIT:.miq.cfg.explicit[SELF];
CFG.DEFAULT :` sv SELF,`config`default;
CFG.APPLIED :.miq.bb.new[` sv SELF,`config];
CFG.CONSTANT:` sv SELF,`C;


// ===========
//  Constants
// ===========
INIT   :(` sv BB,`init) set 0b;

CLASSES  :` sv BB,`classes;
EXT2INT  :` sv BB,`ext2int;
SLOTS    :` sv BB,`slots;
MODULES  :` sv BB,`modules;
PARSED   :` sv BB,`parsed;
FUNCTIONS:` sv BB,`functions;
RULES    :` sv BB,`rules;

SNAP.CURRENT :` sv BB,`snap`current;
SNAP.ITERATOR:` sv BB,`snap`iterator;

/ Table templates
TT.CLASSES:1!flip`class`interpreter!"S*"$\:();
TT.EXT2INT:1!flip`extension`interpreter!"S*"$\:();
TT.SLOTS  :1!flip`name`class`directory`linked`lzp!"SSSBP"$\:();

TT.MODULES:o.module.tt[]!flip`class`loaded`lzp`request!"SBP*"$\:();
TT.REQUEST:flip`requestor`handle`func`code`file`line`namespace`l`zp!"SIS*SJSBP"$\:();

TT.PARSED :o.module.tt[]^flip`snapshot`enriched`lzp`content!"JBP*"$\:();
TT.CONTENT:(0#enlist .miq.qk.inspect "")^flip(1#`code)!"*"$\:();

TT.FUNCTIONS :2!flip`class`function`enabled!"SSB"$\:();
TT.RULES:o.module.tt[]!flip`active`init`args!"BS*"$\:();

// ========
//  Config
// ========
.miq.fn.copy[`.slot.config.set     ;.miq.cfg.set[CFG.EXPLICIT;CFG.DEFAULT]];
.miq.fn.copy[`.slot.config.assemble;.miq.cfg.assemble[CFG.EXPLICIT;CFG.DEFAULT]];
.miq.fn.copy[`.slot.config.get     ;.miq.cfg.get[CFG.APPLIED]];

.slot.config.i.classes:{[] TT.CLASSES upsert flip(`module`config`role`so;((3#enlist"qk"),(2:)))};
.slot.config.i.ext2int:{[] TT.EXT2INT upsert flip(`k`k_`q`q_`so;"kkqq",(2:))};

/ Helper to return default `slots configuration item
.slot.config.i.slots:{[]
  slots:([]
    envvars:`MIQ_MOD`MIQ_CFG`MIQ_ROLE`MIQ_SO
   ;class:`module`config`role`so
  );

  slots:update directory:getenv each envvars from slots;
  slots:delete from slots where ""~/:directory;
  slots:update {$["`:"~2#x;(),get x;-1!'`$":"vs x]} each directory from slots;
  slots:update name:{upper last each` vs'x} each directory from slots;

  TT.SLOTS upsert ungroup`name`class`directory#slots
 };

/ Helper to return default `functions configuration item
.slot.config.i.functions:{[]
  lib:conf:role:`resolve`path`load`reload`definition`push`init`refresh;
  so:`resolve`path`load`refresh;

  TT.FUNCTIONS upsert ungroup
    flip`class`function`enabled!flip(
      (`module    ;lib ;1b)
     ;(`config    ;conf;1b)
     ;(`role      ;role;1b)
     ;(`so        ;so  ;1b)
    )
 };

.miq.cfg.default[CFG.DEFAULT].'(
  (`manager.enable   ;0b            )
 ;(`manager.identity ;`             )
 ;(`manager.fallback ;0b            )
 ;(`load.strict      ;1b            )
 ;(`load.snap        ;0b            )
 ;(`parse.enrich     ;1b            )
 ;(`parse.auto       ;1b            )
 ;(`snap.seed        ;0             )
 ;(`snap.default     ;0Wj           )
 ;(`snap.strict      ;0b            )
 ;(`classes          ;TT.CLASSES    )
 ;(`ext2int          ;TT.EXT2INT    )
 ;(`slots            ;TT.SLOTS      )
 ;(`functions        ;TT.FUNCTIONS  )
 ;(`auto.enable      ;0b            )
 ;(`rules            ;TT.RULES      )
  );

.slot.config.apply:{[]
  .miq.cfg.apply[CFG.CONSTANT;CFG.APPLIED;.slot.config.assemble]
    each `manager.enable`manager.identity`manager.fallback
   ,`load.strict`load.snap`parse.enrich`parse.auto
   ,`snap.seed`snap.default`snap.strict
   ,`classes`ext2int`slots`functions`auto.enable`rules;

  / Update internal state as per configuration
  CLASSES   upsert C.CLASSES[];
  EXT2INT   upsert C.EXT2INT[];
  SLOTS     upsert C.SLOTS[];
  FUNCTIONS upsert C.FUNCTIONS[];
  RULES     upsert C.RULES[];

  slots:key[SLOTS][`name];
  linked:slots where .slot.is.linked each slots;
  .slot.refresh each linked;
  .slot.link each slots except linked;
 };

// =========
//  General
// =========
.slot.init:{[fnConfigLoader]
  .miq.handler.import[`stl.log;` sv SELF,`h];
  .miq.handler.import[`stl.ipc;` sv SELF,`h];

  .slot.h.log.info "Initialising: ",-3!SELF;

  CLASSES   set @[get;CLASSES  ;.slot.config.i.classes[]  ];
  EXT2INT   set @[get;EXT2INT  ;.slot.config.i.ext2int[]  ];
  SLOTS     set @[get;SLOTS    ;.slot.config.i.slots[]    ];
  MODULES   set @[get;MODULES  ;TT.MODULES                ];
  PARSED    set @[get;PARSED   ;TT.PARSED                 ];
  FUNCTIONS set @[get;FUNCTIONS;.slot.config.i.functions[]];
  RULES     set @[get;RULES    ;TT.RULES                  ];

  .is.generate[`warn`error;;1;]./:\:.[;(::;0);` sv SELF,](
    (`is.defined        ;"Slot not defined: "   )
   ;(`is.linked         ;"Slot not linked: "    )
   ;(`module.is.present ;"No such module: "     )
   ;(`module.is.loaded  ;"Module not loaded: "  )
   ;(`module.is.class   ;"Not a module class: " )
  );

  .miq.cfg.loader[fnConfigLoader;last` vs SELF];
  .slot.config.apply[];

  SNAP.CURRENT  set @[get;SNAP.CURRENT ;C.SNAP.DEFAULT[]];
  SNAP.ITERATOR set @[get;SNAP.ITERATOR;C.SNAP.SEED[]   ];

  / Integrate miQ library history if running for the first time
  .slot.i.integrateMiqLibrary[];

  / Reimplement `miq.module handlers
  .miq.handler.set .'(
    (`miq.module.classes        ;`.slot.classes                 )
   ;(`miq.module.library        ;`.slot.modules                 )
   ;(`miq.module.load           ;`.slot.module.miq.load         )
   ;(`miq.module.reload         ;`.slot.module.miq.reload       )
   ;(`miq.module.path           ;`.slot.module.path             )
   ;(`miq.module.resolve        ;`.slot.module.resolve          )
   ;(`miq.module.is.loaded      ;`.slot.module.is.loaded        )
   ;(`miq.module.is.class       ;`.slot.module.is.class         )
   ;(`miq.module.is.warn.class  ;`.slot.module.is.warn.class    )
   ;(`miq.module.is.error.class ;`.slot.module.is.error.class   )
  );

  INIT set 1b;
  .slot.h.log.info "Initialisation completed: ",-3!SELF;
 };

.slot.i.integrateMiqLibrary:{[]
  if[.miq.bb.is.item[BB;`integrate.miq]; if[.miq.bb.get[BB;`integrate.miq]; :()]];

  / TODO improve performance! Can I save some system calls here?
  miqLibs:update .miq.fs.path.real'[.miq.fs.to.absolute directory] from select from .miq.module.library[] where loaded;
  slots:distinct(0!SLOTS[]),0!update .miq.fs.path.real'[.miq.fs.to.absolute directory] from SLOTS[];
  modules:select slot,name,extension,class,loaded,lzp,request from miqLibs
    lj `directory xkey delete loaded,lzp from `slot xcol slots;

  modules:{
    x[`request]:MODULES[o.module.construct[x]][`request] upsert ([]requestor:x[`request]);x
    } each modules;

  MODULES upsert modules;
  .miq.bb.set[BB;`integrate.miq;1b];
 };

/ Accessors
.slot.classes  :{[] CLASSES[]  };
.slot.ext2int  :{[] EXT2INT[]  };
.slot.slots    :{[] SLOTS[]    };
.slot.functions:{[] FUNCTIONS[]};
.slot.modules  :{[] MODULES[]  };
.slot.parsed   :{[] PARSED[]   };
.slot.rules    :{[] RULES[]    };

// ===============
//  Slot handling
// ===============
/ Checks
.slot.is.defined:{[sName] .is.error.s[sName]; sName in key[SLOTS][`name]};
.slot.is.linked:{[sName] .is.error.s[sName]; SLOTS[sName][`linked]};

/ Linking
.slot.link:{[sName]
  .is.error.s[sName];
  .slot.is.error.defined[sName];
  if[.slot.is.linked[sName]; '.slot.h.log.error "Slot already linked: ",-3!sName];

  .slot.h.log.info "Linking slot: ",-3!sName;
  .slot.i.link exec from SLOTS where name=sName
 };

.slot.refresh:{[sName]
  .is.error.s[sName];
  .slot.is.error.defined[sName];
  .slot.is.error.linked[sName];

  .slot.h.log.info "Refreshing slot: ",-3!sName;
  .slot.i.link exec from SLOTS where name=sName
 };

.slot.unlink:{[sName]
  .is.error.s[sName];
  .slot.is.error.defined[sName];
  .slot.is.error.linked[sName];

  .slot.h.log.info "Unlinking slot: ",-3!sName;
  .slot.i.set.unlinked exec from SLOTS where name=sName;
  if[exec count name from MODULES where slot=sName; .slot.purge[sName]];
 };

/ TODO qdSlot should be an object?
.slot.add:{[qdSlot]
  k:3#cols TT.SLOTS;
  if[not all k in key qdSlot;
    '.slot.h.log.error "Missing key: ",-3!k except key qdSlot
  ];

  .slot.h.log.info "Adding new slot: ",-3!qdSlot;
  SLOTS upsert enlist k#qdSlot;
  qdSlot`name
 };

.slot.remove:{[sName]
  .is.error.s[sName];
  .slot.is.error.defined[sName];

  .slot.h.log.info "Removing slot: ",-3!sName;
  if[.slot.is.linked[sName]; .slot.unlink[sName]];
  slot:exec from SLOTS where name=sName;
  delete from SLOTS where name=sName;

  slot
 };

.slot.purge:{[sName]
  .is.error.s[sName];
  if[not count modules:.slot.resolve[sName][];
    .slot.h.log.warn "No modules to purge: ",-3!sName; :();
  ];

  .slot.h.log.info "Purging modules of slot: ",-3!sName;
  .slot.h.log.show modules;
  delete from MODULES where slot=sName, not loaded;
  update slot:` from MODULES where slot=sName;
 };

/ TODO Improve error logging
/ Resolves module name for particular slot
/ * (`name`ext)   - select from modules
/ * `name.q       - derive extension from name and select from modules
/ * `name         - select without an extension
.slot.resolve:{[sSlot;slModule]
  .is.error.s[sSlot];
  .is.error.any[`none`s`sl;slModule];
  .slot.is.error.defined[sSlot];

  if[.is.none[slModule]; :?[key MODULES;enlist(=;`slot;(),sSlot);0b;()]];
  if[not .is.s[slModule] | .is.sl[slModule] & 2=count slModule; '.slot.h.log.error "Invalid name: ",-3!slModule];

  module:$[
    2=count slModule      ; o.module.construct sSlot,slModule;
    "." in string slModule; o.module.construct sSlot,.miq.fs.file.split[slModule];
                            `extension _o.module.construct sSlot,slModule,`
  ];

  ?[key MODULES;.miq.fq.d2cond[=;module];0b;()]
 };

// =========================
//  Link function templates
// =========================
/ TODO .slot... resolve unique? or rework .slot.i.resolve to accept bSingle flag?
.slot.i.template.resolve:{[sSlot;sModule]
  .is.error.s[sSlot];
  .is.error.any[`s`sl;sModule];
  module:.slot.resolve[sSlot;sModule];

  $[1=count module; :first module;
    0=count module; '.slot.h.log.error "No matching module: ",-3!`slot`name!(sSlot;sModule);
                    '.slot.h.log.error "Ambiguous module name: ",-3!`slot`name!(sSlot;sModule)
  ]
 };

/ TODO move to .slot.action.* ... to allow override and / or easy extensibility
.slot.i.template.functions.resolve:{[sSlot;sModule] .slot.resolve[sSlot;sModule]};
.slot.i.template.functions.path:{[sSlot;sModule] .slot.module.path .slot.i.template.resolve[sSlot;sModule]};
.slot.i.template.functions.load:{[sSlot;sModule] .slot.module.load .slot.i.template.resolve[sSlot;sModule]};
.slot.i.template.functions.reload:{[sSlot;sModule] .slot.module.reload .slot.i.template.resolve[sSlot;sModule]};
.slot.i.template.functions.definition:{[sSlot;sModule;sDefName] .slot.module.definition[.slot.i.template.resolve[sSlot;sModule];sDefName]};
.slot.i.template.functions.push:{[sSlot;sModule;ch] .slot.module.push[ch] .slot.i.template.resolve[sSlot;sModule]};
.slot.i.template.functions.init:{[sSlot;sModule] .slot.module.init .slot.i.template.resolve[sSlot;sModule]};
.slot.i.template.functions.refresh:{[sSlot;dummy] .slot.refresh[sSlot]};

// ==============
//  Snapshotting
// ==============
.slot.snap.new:{[slSlot]
  .is.error.sl[slots:(),slSlot];
  if[not all defined:.slot.is.defined'[slots];
    '.slot.h.log.error "Slot not defined: ",-3!slots where not defined
  ];

  modules:slots!.slot.resolve'[slots]'[];
  missing:where 0=count each modules;
  slots:slots except missing;
  if[not 0~count missing; .slot.h.log.warn "Slot empty: ",-3!missing];
  if[0~count slots; .slot.h.log.warn "No modules to snapshot: ",-3!slots; :0N];

  snap:.slot.snap.id[];
  .slot.module.i.snap[;snap] each raze modules;
  snap
 };

.slot.snap.extend:{[oModule]
  .miq.object.is.error.class[O.MODULE;oModule];
  .slot.module.is.error.present[oModule];

  .slot.module.i.snap[;.slot.snap.get[]];
  snap
 };

.slot.snap.set:{[jSnap]
  .is.error.j[jSnap];
  if[not jSnap in .miq.u.num.inf.j,exec distinct snapshot from PARSED;
    '.slot.h.log.error "Snapshot does not exist: ",-3!jSnap;
  ];
  get SNAP.CURRENT set jSnap
 };

.slot.snap.id:    {[] eval(+:;SNAP.ITERATOR;1)};
.slot.snap.get:   {[] SNAP.CURRENT[]};
.slot.snap.delete:{[jSnap] .is.error.j[jSnap]; delete from PARSED where snapshot=jSnap; jSnap};
.slot.snap.reset: {[] get SNAP.CURRENT set C.SNAP.DEFAULT[]};

// Internal
.slot.i.link:{[qdSlot]
  name:string qdSlot[`name];
  funcs:exec function from FUNCTIONS where class=qdSlot[`class], enabled;

  if[0=count funcs;
    .slot.h.log.error "Error linking slot:",-3!qdSlot;
    '.slot.h.log.error "No functions enabled: ",-3!qdSlot`class;
  ];

  / Define plain function, add file and line details
  funcNames:"." sv'("";name),/:enlist each string funcs;
  functions:funcNames,'":",/:string[.slot.i.template.functions funcs];
  code:.miq.qk.process[`$".slot.i.link@@";-1;functions];
  .slot.module.i.qk.interpret["q";code];

  / Projection has to be made after original function has been defined.
  / You are projecting an unnamed lambda instead of named function otherwise.
  / That will cause function name to not be in the details (first -4#get`fnc).
  .slot.module.i.qk.interpret["q"] funcNames,'":",'funcNames,\:"[`",name,"]";

  .slot.i.set.linked[qdSlot];
  .slot.i.populate[qdSlot];
 };

/ Discover and record modules
.slot.i.populate:{[qdSlot]
  if[not qdSlot[`class] in key[CLASSES]; '.slot.h.log.error "Class not defined: ",-3!qdSlot];

  files:.slot.i.explore[qdSlot];
  if[0=count files; .slot.h.log.warn "Slot empty: ",-3!qdSlot; :()];

  nameExt:flip .miq.fs.file.split'[files];
  modules:flip `slot`name`extension`class!(qdSlot[`name];nameExt[0];nameExt[1];qdSlot[`class]);
  present:select from MODULES where slot=qdSlot[`name];
  $[0<count present; modules:(present lj 3!modules) uj 3!modules;
                     modules[`request]:count[modules]#enlist TT.REQUEST
  ];

  MODULES upsert modules
 };

/ Returns list of modules for given slot
.slot.i.explore:{[qdSlot]
  files:key qdSlot[`directory];
  files:files where {x~key x} each` sv'qdSlot[`directory],/:files;

  interpreter:CLASSES[qdSlot`class][`interpreter];
  cond:enlist({any each flip x~\:/:(),y};`interpreter;interpreter);
  files where (` vs'files)[;1] in ?[EXT2INT;cond;();`extension]
 };

/ Discover and record slots
.slot.i.set.linked  :{[qdSlot] SLOTS upsert qdSlot,`linked`lzp!(1b;.z.p)};
.slot.i.set.unlinked:{[qdSlot] SLOTS upsert qdSlot,`linked`lzp!(0b;.z.p)};

// =================
//  Module handling
// =================
/ miQ hook replacements wrapping around slot routines
.slot.module.miq.i.resolve:{[sClass;sName]
  modules:.slot.module.resolve[sClass;sName];
  $[0~count modules; '.slot.h.log.error "No matching module: ",-3!`class`name!(sClass;sName);
    1<count modules; [.slot.h.log.show modules; '.slot.h.log.error "Module name ambiguous: ",-3!modules];
                     first modules
  ]
 };

.slot.module.miq.load    :{[sClass;sName] .slot.module.load .slot.module.miq.i.resolve[sClass;sName]};
.slot.module.miq.reload  :{[sClass;sName] .slot.module.reload .slot.module.miq.i.resolve[sClass;sName]};
.slot.module.miq.loaded  :{[sClass;sName] MODULES[;`loaded] .slot.module.miq.i.resolve[sClass;sName]};

/ Checks
.slot.module.is.present    :{[oModule] .miq.object.is.error.class[O.MODULE;oModule]; oModule in key MODULES};
.slot.module.is.loaded     :{[oModule] .miq.object.is.error.class[O.MODULE;oModule]; MODULES[oModule][`loaded]};
.slot.module.is.class      :{[sClass] .is.error.s[sClass]; sClass in key[CLASSES]};
.slot.module.is.warn.class :{[sClass] $[.slot.module.is.class[sClass]; 1b; [.slot.h.log.warn "Not a module class: ",-3!sClass; 0b]]};
.slot.module.is.error.class:{[sClass] $[.slot.module.is.class[sClass]; 1b; ['.slot.h.log.error "Not a module class: ",-3!sClass; 0b]]};

/ Code loading and management
.slot.module.load:{[oModule]
  .miq.object.is.error.class[O.MODULE;oModule];
  .slot.module.is.error.present[oModule];

  .slot.h.log.info "Loading module: ",-3!oModule;
  .slot.module.i.load[0b;oModule]
 };

.slot.module.reload:{[oModule]
  .miq.object.is.error.class[O.MODULE;oModule];
  .slot.module.is.error.present[oModule];
  if[C.LOAD.STRICT[]; .slot.module.is.error.loaded[oModule]];

  .slot.h.log.info "Reloading module: ",-3!oModule;
  .slot.module.i.load[1b;oModule]
 };

/ TODO parse if not parsed yet
.slot.module.definition:{[oModule;sDefName]
  .is.error.any[`s`ns;sDefName];
  .miq.object.is.error.class[O.MODULE;oModule];
  .slot.module.is.error.present[oModule];

  content:.slot.module.i.qk.retrieve[oModule][`content];
  defs:?[content;enlist(in/:;(),sDefName;`definition);0b;()];
  if[0~count defs; '.slot.h.log.error "Definition ",(-3!sDefName)," unknown in module: ",-3!oModule];
  if[1<count defs; .slot.h.log.warn "Definition ",(-3!sDefName)," on lines ",(-3!defs[`code][;2])," ambiguous in module: ",-3!oModule];

  .slot.h.log.info "Sourcing definition ",(-3!sDefName)," from module: ",-3!oModule;
  def:last defs;
  / TODO another miQ.pp use case
  tree:.miq.qk.parse[$[.z.K<4;first;last] def`code];
  idx:(";"~first tree)+def[`definition]?sDefName;
  (sDefName;eval tree[idx])
 };

.slot.module.init:{[oModule]
  .miq.object.is.error.class[O.MODULE;oModule];
  .slot.module.is.error.present[oModule];
  .slot.module.is.error.loaded[oModule];
  if[not oModule in key RULES; .slot.h.log.warn "No auto rule: ",-3!oModule; :()];

  .slot.h.log.info "Initialising Module: ",-3!oModule;
  .slot.module.i.init[oModule];
 };

/ Module initialisation from RULES table
.slot.module.i.init:{[oModule]
  rule:RULES oModule;
  if[not rule`active; .slot.h.log.info "Auto rule disabled: ",-3!oModule,rule; :()];

  rule[`init]@rule[`args][];
 };

.slot.module.drop:{[oModule]
  .slot.module.is.error.present[oModule];
  .slot.h.log.info "Dropping module: ",-3!oModule;
  ![MODULES;.miq.fq.d2cond[=;oModule];0b;`$()]
 };

.slot.module.path:{[oModule]
  .slot.module.is.error.present[oModule];
  .slot.module.i.path[oModule]
 };

.slot.module.i.path:{[oModule]
  ` sv SLOTS[oModule`slot;`directory],.miq.fs.file.join oModule[`name`extension]
 };

// Snap
/ IDEA Deprecate this functionality?
.slot.module.snap:{[olModule]
  modules:$[.is.qd[olModule]; enlist olModule; olModule];
  .miq.object.is.error.class[O.MODULE]'[modules];
  if[not all present:modules in key MODULES;
    '.slot.h.log.error "No such module: ",-3!modules where not present;
  ];

  snap:.slot.snap.id[];
  .slot.module.i.snap[;snap] each modules;
  snap
 };

.slot.module.is.snapped:{[oModule]
  .miq.object.is.error.class[O.MODULE;oModule];
  .slot.module.is.error.present[oModule];

  .slot.module.i.is.snapped[oModule]
 };

.slot.module.i.is.snapped:{[oModule]
  0<count?[PARSED;.miq.fq.d2cond[=;oModule];0b;()]
 };
// =========
//  Parsing
// =========
.slot.module.parse:{[oModule]
  .miq.object.is.error.class[O.MODULE;oModule];
  .slot.module.is.error.present[oModule];

  .slot.h.log.info "Parsing module: ",-3!oModule;
  .slot.module.i.snap[oModule;0N]
 };

.slot.module.i.snap:{[oModule;jSnap]
  .slot.module.i.qk.store[oModule;;jSnap] .slot.module.i.qk.parse[oModule]
 };

.slot.module.push:{[ch;oModule]
  .is.error.ch[ch];
  .miq.object.is.error.class[O.MODULE;oModule];
  .slot.module.is.error.present[oModule];

  code:.slot.module.i.qk.retrieve[oModule][`content][`code];
  .slot.h.log.info "Pushing module to handle: ",-3!(ch;oModule);
  .slot.h.ipc.async[ch;(.miq.qk.interpret["q"]';code)]
 };

.slot.module.path:{[oModule]
  .miq.object.is.error.class[O.MODULE;oModule];
  .slot.module.is.error.present[oModule];

  dir:SLOTS[oModule[`slot]][`directory];
  ` sv dir,.miq.fs.file.join[oModule`name`extension]
 };

.slot.module.content:{[oModule]
  .miq.object.is.error.class[O.MODULE;oModule];
  .slot.module.is.error.present[oModule];

  if[not .slot.module.is.snapped[oModule]; .slot.module.snap[oModule]];
  update code:code[;0],file:-1!'`$code[;1],line:code[;2] from last ?[PARSED;.miq.fq.d2cond[=;oModule];();`content]
 };

.slot.module.resolve:{[sClass;slModule]
  .is.error.s[sClass];
  .is.error.any[`none`s`sl;slModule];
  .slot.module.is.error.class[sClass];

  slots:exec name from SLOTS where class=sClass;
  modules:raze .slot.resolve[;slModule] each slots;
  $[.is.empty modules; 0#key MODULES; modules]
 };

// Internal
.slot.module.i.set.loaded   :{[oModule] MODULES upsert oModule,`loaded`lzp!(1b;.z.p)};
.slot.module.i.set.unloaded :{[oModule] MODULES upsert oModule,`loaded`lzp!(0b;.z.p)};

/ Tracking of requests
/ TODO abstract and move stack / frame inspection elsewhere
.slot.module.i.request.store:{[oModule]
  zp:.z.p;
  stack:.miq.u.stack[];
  frame:$[0~count frame:where stack[;1;1] like"<load>"; 0; 1+last frame];
  info:stack[frame;1];

  handle:.z.w;          / ("q";`) hack on info[0] on new kdb 4.0 behaviour
  requestor:$[handle<>0i; `remote; (2~count info[0])|()~info[0]; `process; ""~info[0]; `user; `$info[0]];
  func:first`$stack[;1;0] $[stack[frame+1;1;0] like".q.*"; frame+2; frame+1];
  code:info[3];
  file:-1!`$info[1];
  line:info[2];
  namespace:system"d";
  l:0<>frame;

  request:TT.REQUEST upsert (requestor;handle;func;code;file;line;namespace;l;zp);
  request:(first ?[MODULES;.miq.fq.d2cond[=;oModule];();`request]),request;
  ![MODULES;.miq.fq.d2cond[=;oModule];0b;enlist[`request]!enlist[enlist enlist request]];
 };

/ Loading of files
.slot.module.i.load:{[bReload;oModule]
  .slot.module.i.request.store[oModule];
  interpreter:EXT2INT[oModule[`extension];`interpreter];

  $[any interpreter~/:"qk";
      .slot.module.i.qk.load[bReload;interpreter;oModule];
    interpreter~(2:);
      .slot.module.i.so.load[oModule];
    null interpreter;
      '.slot.h.log.error "No interpreter for extension: ",-3!oModule[`extension];
      '.slot.h.log.error "Interpreter not implemented: ",-3!interpreter
  ]
 };

// qk
/ Parses q/k code into memory and executes it
.slot.module.i.qk.load:{[bReload;cInterpreter;oModule]
  if[MODULES[oModule;`loaded]&not bReload; :()];

  code:$[
      /LOAD.MANAGER[]; .slot.module.i.qk.remote[oModule];
      C.LOAD.SNAP[]   ; .slot.module.i.qk.retrieve[oModule][`content][`code];
                        .slot.module.i.qk.parse[oModule]
  ];
  .slot.module.i.qk.interpret[cInterpreter;code];
  .slot.module.i.qk.store[oModule;code;0N];
  .slot.module.i.set.loaded[oModule];

  if[C.AUTO.ENABLE[]&oModule in key RULES[];
    .slot.h.log.info "Auto initialising: ",-3!oModule;
    .slot.module.i.init[oModule]
  ];
 };

.slot.module.i.qk.parse:{[oModule]
  .miq.qk.load .slot.module.i.path[oModule]
 };

.slot.module.i.qk.interpret:{[cLanguage;code]
  ns:system"d";
  .miq.qk.interpret[cLanguage] each code;
  system"d ",string ns;
 };

.slot.module.i.qk.snap:{[oModule]
  if[not C.PARSE.AUTO[]; '.slot.h.log.error "Module not parsed: ",-3!oModule];
  $[C.LOAD.SNAP[];
      $[C.SNAP.STRICT[];
          '.slot.h.log.error "Module not part of snapshot: ",-3!`oModule`jSnap!(oModule;SNAP.CURRENT[]);
          .slot.module.i.snap[oModule;SNAP.CURRENT[]]
      ];
      .slot.module.i.snap[oModule;0N]
  ];
 };

.slot.module.i.qk.retrieve:{[oModule]
  snap:$[SNAP.CURRENT[] in .miq.u.num.inf.j;
      oModule;
      oModule,enlist[`snapshot]!enlist[SNAP.CURRENT[]]
  ];

  parsed:?[PARSED;.miq.fq.d2cond[=;snap];0b;()];
  if[not count parsed; .slot.module.i.qk.snap[oModule]];

  parsed:?[PARSED;.miq.fq.d2cond[=;snap];0b;()];
  if[-0W~SNAP.CURRENT[]; :first[parsed]];
  if[ 0W~SNAP.CURRENT[]; : last[parsed]];

  if[1<>count parsed; '.slot.h.log.error "Snapshot corrupted: ",-3!`oModule`jSnap!(oModule;SNAP.CURRENT[])];
  raze[parsed]
 };

/ Store parsed content for later re-use, enrich if possible
.slot.module.i.qk.store:{[oModule;code;jSnap]
  qtContent:TT.CONTENT upsert $[C.PARSE.ENRICH[];
      / TODO another use case for miQ.pp
      .miq.qk.inspect $[0h~type code[0]; $[.z.K<4;code[;0];code[;2]];code];
      .miq.qk.inspect count[code]#enlist""
  ]^([]code);

  PARSED upsert get[oModule],(jSnap;C.PARSE.ENRICH[];.z.p;qtContent)
 };

// so
/ Returns projection of dynamic load against requested so
.slot.module.i.so.load:{[oModule]
  .slot.module.i.set.loaded[oModule];
  2:[` sv SLOTS[oModule[`slot];`directory],oModule[`name]]
 };

/ Inspects so and stores it's defined symbols?
/ TODO We can store library as a binary in memory and save it down too
/.slot.module.i.so.store:{[oModule]
/flip`symbolValue`symbolType`symbolName!("*CS";" ")0:system"nm -D --defined-only so/tz.so"
/ };



/
TODO This whole mess needs a proper rewrite!!!
IDEA .slot.module.info[`.clan.init] will show when it was loaded, from what file, and maybe some extra info given by dev if we want to?
TODO Error trap module loading
IDEA Allow errors during module parsing, and display them in the library table
TODO Scan namespace tree pre and post loading to write down what has been created
IDEA Inspect every statement and declaration during parsing to compile a list of used namespaces?
IDEA for namespace clashing, any 1#.q namespace is considered empty and as if unused
IDEA Namespace collision detection, provide option to control behaviour when conflict detected
IDEA multiple classes allowed for each slot? That would mean that module definition has to be expanded to include class of it's own...
IDEA .slot.module.import to seek a particular function definition out and load it?

