Handler
===
---
Routines interacting with shell the process is running in.

**Errors**

| Message                                               | Explanation                                                                       |
| ----------------------------------------------------- | --------------------------------------------------------------------------------- |
| `Handler already defined`                             | Command unavailable in the current shell environment                              |
| `Invalid handler arity`                               | Environment variable not defined in the current shell environment                 |
| `Handler arity does not match that of implementer`    | Given implementer definition's arity is not matching handler's definition arity   |
| `Not a valid implementer`                             | Given implementer definition is not valid                                         |
| `Calling an unimplemented handler`                    | Attached handler is not implemented yet                                           |
| `Handler cannot be attached to null address`          | Attempt to attach null address to handler                                         |
| `Handler cannot be detached from null address`        | Null address cannot be attached to handler                                        |
| `Handler not defined`                                 | Failure of `.miq.handler.is.error.defined[]`                                      |
| `Handler not implemented`                             | Failure of `.miq.handler.is.error.implemented[]`                                  |
| `Handler implementer stale`                           | Failure of `.miq.handler.is.error.stale[]`                                        |
| `Handler not attached`                                | Failure of `.miq.handler.is.error.attached[]`                                     |

**Warnings**

| Message                                               | Explanation                                                                       |
| ----------------------------------------------------- | --------------------------------------------------------------------------------- |
| `Arity of executable implementer not defined`         | Allowing definition of handlers without arity for experimental reasons            |

**Examples**
```
/ Define handlers
q).miq.handler.define[`common.test.a;1;"add 1 to any number"]
`common.test.a
q).miq.handler.define[`common.test.b;2;"add 1 to sum of two numbers"]
`common.test.b

q)-2#.miq.handlers[]
name         | implementer arity description                   attached
-------------| --------------------------------------------------------
common.test.a|             1     "add 1 to any number"
common.test.b|             2     "add 1 to sum of two numbers"


/ Import and export common.test handlers
/ Because the handlers aren't implemented, addresses will be filled with placeholder functions accepting any number of arguments
q).miq.handler.import[`common.test;`.custom.h]
2021.03.17 | 17:46:57.766 | [INFO]  | <.miq.handler.i.attach> | Attaching to `common.test.a: `.custom.h.test.a
2021.03.17 | 17:46:57.766 | [INFO]  | <.miq.handler.i.attach> | Attaching to `common.test.b: `.custom.h.test.b
`.custom.h.test.a`.custom.h.test.b

q).custom.h.test
 | ::
a| {[x;y;z] '.miq.h.log.error "Calling an unimplemented handler: ",-3!`handler`caller!(x;y);}[`common.test.a;`.custom.h.test.a]enlist
b| {[x;y;z] '.miq.h.log.error "Calling an unimplemented handler: ",-3!`handler`caller!(x;y);}[`common.test.b;`.custom.h.test.b]enlist

q).miq.handler.export[`common.test;`.custom.e]
2021.03.17 | 19:37:28.089 | [INFO]  | <.miq.handler.i.export> | Exporting `common.test.a to: `.custom.e.test.a
2021.03.17 | 19:37:28.090 | [INFO]  | <.miq.handler.i.export> | Exporting `common.test.b to: `.custom.e.test.b
`.custom.e.test.a`.custom.e.test.b

q).custom.e.test
 | ::
a| {[x;y;z] if[not .miq.handler.is.implemented[x]; '.miq.h.log.error "Calling an unimplemented handler: ",-3!`handler`caller!(x;y)]; ...
b| {[x;y;z] if[not .miq.handler.is.implemented[x]; '.miq.h.log.error "Calling an unimplemented handler: ",-3!`handler`caller!(x;y)]; ...


/ Define implementers
q)fn:{1+x}
q)gn:{1+x+y}


/ Set implementer for handler `common.test.a
q).miq.handler.set[`common.test.a;`..fn]
2021.03.17 | 19:33:27.262 | [INFO]  | <.miq.handler.set> | New handler implementer: `handler`implementer!`common.test.a`..fn
2021.03.17 | 19:33:27.263 | [INFO]  | <.miq.handler.update> | Updating handler: `common.test.a
2021.03.17 | 19:33:27.263 | [INFO]  | <.miq.handler.i.attach> | Attaching to `common.test.a: `.custom.h.test.a
,`.custom.h.test.a


/ Imported handler was resolved
q).custom.h.test.a
`..fn
q).custom.h.test.a[1]
2

/ Exported handler will be resolved on the first execution
q).custom.e.test.a
{[x;y;z] if[not .miq.handler.is.implemented[x]; '.miq.h.log.error "Calling an unimplemented handler: ",-3!`handler`caller!(x;y)];
q).custom.e.test.a[1]
2
q).custom.e.test.a
{1+x}


/ Addresses attached to unimplemented handlers will fail to execute
q).custom.h.test.b[1]
2021.03.17 | 19:52:08.064 | [ERROR] | <.miq.handler.i.attach@> | Calling an unimplemented handler: `handler`caller!`common.test.b`.custom.h.test.b
'Calling an unimplemented handler: `handler`caller!`common.test.b`.custom.h.test.b
  [0]  .custom.h.test.b[1]
       ^

q).custom.e.test.b[1;2;3]
2021.03.17 | 19:51:24.411 | [ERROR] | <.miq.handler.i.export@> | Calling an unimplemented handler: `handler`caller!`common.test.b`.custom.e.test.b
'Calling an unimplemented handler: `handler`caller!`common.test.b`.custom.e.test.b
  [0]  .custom.e.test.b[1;2;3]
       ^


/ Set implementer for second handler
q).miq.handler.set[`common.test.b;`..gn]
2021.03.17 | 21:50:38.014 | [INFO]  | <.miq.handler.set> | New handler implementer: `handler`implementer!`common.test.b`..gn
2021.03.17 | 21:50:38.015 | [INFO]  | <.miq.handler.update> | Updating handler: `common.test.b
2021.03.17 | 21:50:38.015 | [INFO]  | <.miq.handler.i.attach> | Attaching to `common.test.b: `.custom.h.test.b
,`.custom.h.test.b


/ Detach address from handler, this will delete .custom.h.test.b name
q).miq.handler.detach[`common.test.b;`.custom.h.test.b]
2021.03.17 | 21:52:36.007 | [INFO]  | <.miq.handler.i.detach> | Detaching from `common.test.b: `.custom.h.test.b
2021.03.17 | 21:52:36.007 | [INFO]  | <.miq.ns.delete> | Deleting name: `.custom.h.test.b
`.custom.h.test.b

q).custom.h.test.b
'.custom.h.test.b
  [0]  .custom.h.test.b
       ^


/ Unset handler
q).miq.handler.unset[`common.test.a]
2021.03.17 | 21:53:05.732 | [INFO]  | <.miq.handler.unset> | Removing handler implementer: `handler`implementer!`common.test.a`..fn
2021.03.17 | 21:53:05.732 | [INFO]  | <.miq.handler.update> | Updating handler: `common.test.a
2021.03.17 | 21:53:05.732 | [INFO]  | <.miq.handler.i.attach> | Attaching to `common.test.a: `.custom.h.test.a
,`.custom.h.test.a


/ Info on handler
q).miq.handler.info'[`common.test.a`common.test.b]
implementer arity description                   attached
------------------------------------------------------------------
            1     "add 1 to any number"         ,`.custom.h.test.a
..gn        2     "add 1 to sum of two numbers" `symbol$()


/ .custom.h.test.a filled with stand-in function to unimplemented handler again
q).custom.h.test.a
{[x;y;z] '.miq.h.log.error "Calling an unimplemented handler: ",-3!`handler`caller!(x;y);}[`common.test.a;`.custom.h.test.a]enlist
```

## Checks
### .miq.handler.is.defined
| parameter | Type | Description            |
| --------- | ---- | ---------------------- |
| `s`       | s    | Handler name           |

Return `1b` if handler named `s` is defined, `0b` otherwise.

### .miq.handler.is.implemented
| parameter | Type | Description            |
| --------- | ---- | ---------------------- |
| `s`       | s    | Handler name           |

Return `1b` if handler named `s` is implemented, `0b` otherwise.

### .miq.handler.is.stale
| parameter | Type | Description            |
| --------- | ---- | ---------------------- |
| `s`       | s    | Handler name           |

Return `1b` if implementer of handler `s` is stale, `0b` otherwise.

### .miq.handler.is.attached
| parameter | Type | Description            |
| --------- | ---- | ---------------------- |
| `ns`      | ns   | Address                |

Return `1b` if address `ns` is attached to a handler, `0b` otherwise.

## .miq.handler.define
| parameter         | Type | Description            |\*| Interface           | Purpose                               |
| ----------------- | ---- | ---------------------- |--| ------------------- | ------------------------------------- |
| `sName`           | s    | Handler name           |\*| `` `s`j`cl ``       | Plain handler definition              |
| `nsImplementer`   | ns   | Implementer address    |\*| `` `s`ns`j`cl ``    | Define handler & set implementer      |
| `jArity`          | j    | Handler arity          |\*| `` `b`s`j`cl ``     | Plain handler redefinition            |
| `clDescription`   | cl   | Handler description    |\*| `` `b`s`ns`j`cl ``  | Redefine handler & set implementer    |
| `bRedefine`       | b    | Redefinition flag      |\*|                     |                                       |

Define handler `sName` of arity `jArity` and description `clDescription`. Once defined, it is possible to attach other addresses to it. If supplied with `nsImplementer`, it will also set it as the handler's implemented via `.miq.handler.set[]` call. Supply `bRedefine` as `1b` if you are fine with redefining an existing handler definition and don't want to signal if it does.

## .miq.handler.set
| parameter         | Type | Description            |
| ----------------- | ---- | ---------------------- |
| `sName`           | s    | Handler name           |
| `nsImplementer`   | ns   | Implementer address    |

Set implementer of `sName` handler to `nsImplementer`.

## .miq.handler.unset
| parameter         | Type | Description            |
| ----------------- | ---- | ---------------------- |
| `sName`           | s    | Handler name           |

Clear handler `sName` of any implementer it may have, returning it to unimplemented state.

## .miq.handler.get
| parameter         | Type | Description            |
| ----------------- | ---- | ---------------------- |
| `sName`           | s    | Handler name           |

Return implementer of handler `sName`.

## .miq.handler.info
| parameter         | Type | Description            |
| ----------------- | ---- | ---------------------- |
| `sName`           | s    | Handler name           |

Return information on handler `sName`.

## .miq.handler.list
Return the list of all defined handlers.

## .miq.handler.attach
| parameter         | Type | Description            |
| ----------------- | ---- | ---------------------- |
| `sName`           | s    | Handler name           |
| `nsAddress`       | ns   | Address to attach to   |

Attach `nsAddress` to handler `sName`. If `nsAddress` is not present, it will be automatically created.

## .miq.handler.detach
| parameter         | Type | Description            |
| ----------------- | ---- | ---------------------- |
| `nsAddress`       | ns   | Address to detach from |

Detach `nsAddress` from its handler. It will result in `nsAddress` deletion.

## .miq.handler.update
| parameter         | Type | Description            |
| ----------------- | ---- | ---------------------- |
| `sName`           | s    | Handler name           |

Update all addresses attached to `sName`, making sure the correct implementer is used.

## .miq.handler.import
| parameter         | Type | Description                    |
| ----------------- | ---- | ------------------------------ |
| `sName`           | s    | Handler name                   |
| `nsContext`       | ns   | Context to store addresses     |

Attach every matched handler `sName` to an auto-generated name under `nsContext`. If `sName` matches a handler name exactly, drop first part of the `sName` and imported into `nsContext` under this name. Alternatively, `sName` has to be a partial match to a handler name, where each dot-delimited part is compared in full. The part of `sName` after the last dot will be appended to `nsContext` and form the address that will be attached to `sName` itself.

## .miq.handler.export
| parameter         | Type | Description                    |
| ----------------- | ---- | ------------------------------ |
| `sName`           | s    | Handler name                   |
| `nsContext`       | ns   | Context to store addresses     |

Export works in the same manner that import does, but instead of attaching address under `nsContext` to handlers matched by `sName`, it translates the addresses to actual function definitions, completely detached from handlers from that point onwards.

## .miq.handler.resolve
| parameter         | Type | Description                    |
| ----------------- | ---- | ------------------------------ |
| `sName`           | s    | (Partial) handler name         |

Resolve `sName` to a list of matching handlers.

