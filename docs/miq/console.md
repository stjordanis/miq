Console
=======
---
Built in console to provide both essential and advanced functionality to operate on local or remote processes using REPL. Able to execute asynchronously against multiple remote processes at once. It opens its own output stream to to not pollute STDOUT and STDERR streams.

**Errors**

All error messages from console are prefixed with `Console:`.

| Message                               | Explanation                                                   |
| ------------------------------------- | ------------------------------------------------------------- |
| `Can only run on local terminal!`     | `handle` command was given invalid connection handle          |
| `Running already!`                    | `handle` command was given invalid connection handle          |
| `Cannot async locally`                | `handle` command was given invalid connection handle          |
| `Terminating q process on handle`     | `handle` command was given invalid connection handle          |
| `Invalid console.quit configuration`  | `console.quit` misconfigured                                  |
| `Internal failure!`                   | Console has failed to execute your command                    |
| `Sync request failed!`                | Something went wrong during sync execution                    |
| `Handle not valid`                    | You have supplied an invalid connection handle                |
| `Cannot mix local and remote handles` | When executing against multiple handles, `0i` is not accepted |
| `Label undefined`                     | `load` command attempting to load undefined `label`           |

**Examples**
```
michal@ams-agellus `. > 2+3
5
michal@ams-agellus `. > mli
 > longfunc:{[a;b]
 >   a*b*(a+b)
 >  };
 > save fn
 > 1+2
 > run
3
 > mli
michal@ams-agellus `. > load fn
michal@ams-agellus `. > longfunc[3;4]
84
michal@ams-agellus `. > exit
q)
```

## Commands
### \\
Switch between `q` and `k` language input.

### \\\\
> [!ATTENTION] Executing `\\` on local handle while `console.quit` is set to `local` will exit your session!

Exit from console when `console.quit` set to `local`, or execute `\\` on given handle if set to `remote`.

### mli / code
Multi-line input mode. While console is in `mli` mode, it accumulate input in buffer until it is instructed to execute it or until `mli` mode is exited.
* `mli` - switch between multi-line input and back
* `code` - show unexecuted code in current buffer

### handle / handles
Configure handle(s) the console operates on, with `0` meaning *self*.
* `handles` - print available handles
* `handle` - print current handle in use
* `handle [ch|chl]` - console will execute commands against given handles

### sync / async
Switch between synchronous and asynchronous execution when targetting remote processes. Cannot `async` when running code locally, and `async` is switched on automatically when executing against multiple handles.

### save / load
Feature to enable storing of unexecuted code for later retrieval and execution.
* `save [label]` - save the current un-executed code to `label`
* `load [label]` - load code saved under `label`, overwriting any unexecuted code in buffer

### reset / clear
State control commands.
* `reset` - reset state, identical to starting *console* anew
* `clear` - clear buffer, removing all unexecuted code from it

### run / done / exit
Commands to control `mli` mode and to exit from console.
* `run` - execute code in buffer when in `mli` mode
* `done` - same as `done`, but also exit *console*
* `exit` - exit *console* without executing code in the buffer

### debug
WIP

### block
WIP

