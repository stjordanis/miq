trap
====
---
Subsystem implementing minimalistic trap capabilities. Default implementer of `` `stl.trap `` handlers.

**Examples**
```
q).miq.trap.apply[{2#x+y}.;(1;2)]
0b
3 3

q).miq.trap.apply[{x+1};1]
0b
2

q).miq.trap.apply[{x+1};`]
1b
("type";((({x+1};2);(();"";-1;"{x+1}");2;3);

q)-1 .miq.trap.print . last .miq.trap.apply[{x+1};`];
'type
  [1]  {x+1}
         ^

q)-1 .miq.trap.print . last .miq.trap.apply[get;"NotHere`"];
'NotHere
  [0]  NotHere`
       ^
```

## .miq.trap.apply
| parameter | Type | Description          |
| --------- | ---- | -------------------- |
| `cmd`     | -    | Command to execute   |
| `arg`     | -    | Argument(s) to `cmd` |

Executes `cmd@arg`. Return:
* `(0b;output)` on success
* `(1b;(clErr;stack))` on failure; `stack` includes the `.Q.trp` frames

## .miq.trap.print
| parameter | Type | Description       |
| --------- | ---- | ----------------- |
| `clErr`   | cl   | Error string      |
| `stack`   | -    | Stack to print    |

Return formated string ready for printing, that contains error and stack trace, but excludes the `.Q.trp` frames. This amounts to near perfect output emulation of the default `.z.pi` implementation.

