Utility
=======
---
Interesting functionality and collections of useful values that does not need it's own namespace can be found here.

**Errors**

| Message                  | Explanation                               |
| ------------------------ | ----------------------------------------- |
| `Wait attempts exceeded` | `.miq.u.wait` has reached it's time limit |

**Examples**
```
q).miq.u.num.null.e, .miq.u.num.inf.e
0N -0w 0we

q).miq.u.num.o2d 644
420
q).miq.u.num.d2o 420
644

q).miq.u.q.info 10h
c    | "c"
name | `char
empty| ""
null | " "

q).miq.u.ops
n| u  b t
-| -------
0| :: : '
1| +: + /
2| -: - \
3| *: * ':
4| %: % /:
5| &: & \:

/ Explicit elision
q){[x;y] 1+2}[.miq.u.elision]
{[x;y] 1+2}[]
q){[x;y] 1+2}[.miq.u.elision] . 1 2
3

q)q:m:1
q)-16!q
2i
q)q:.miq.u.copy m
q)-16!q
1i

q).miq.u.wait[3;200;{[x;y]x+y;'oh}.;1 2]
22:52:31.623 [ERROR]  <.miq.u.wait> Wait attempts exceeded: `jTimes`jInterval`fn`args!(3;200;.[{[x;y]x+y;'oh}];1 2)
'Wait attempts exceeded: `jTimes`jInterval`fn`args!(3;200;.[{[x;y]x+y;'oh}];1 2)
  [0]  .miq.u.wait[3;200;{[x;y]x+y;'oh}.;1 2]
       ^
q)
```
# Textual
| Function                  | Description                                           |
| ------------------------- | ----------------------------------------------------- |
| `.miq.u.text.A`           | Upper case characters (`.Q.A`)                        |
| `.miq.u.text.a`           | Lower case characters (`.Q.a`)                        |
| `.miq.u.text.n`           | Number characters (`.Q.n`)                            |
| `.miq.u.text.Aa`          | Upper and lower case characters (`.Q.A,.Q.a`)         |
| `.miq.u.text.An`          | Upper case and number characters (`.Q.A,.Q.n`)        |
| `.miq.u.text.an`          | Lower case and number characters (`.Q.a,.Q.n`)        |
| `.miq.u.text.Aan`         | Upper, lower and number characters (`.Q.A,.Q.a,.Q.n`) |
| `.miq.u.text.ascii`       | All ASCII characters                                  |
| `.miq.u.text.printable`   | Printable ASCII characters                            |

## .miq.u.text.clean
| parameter | Type | Description |
| --------- | ---- | ----------- |
| `cl`      | cl   | Any cl      |

Removes all non-printable characters from `cl`.

## .miq.u.text.uncomment
| parameter | Type  | Description               |
| --------- | ----- | ------------------------- |
| `c`       | c\|cl | Comment character         |
| `clLine`  | cl    | Line to uncomment         |

Find first `c` character in `clLine` and return all characters to that point. If `c` is consists of multiple characters, comment matching will become more nuanced:
* `last[c]` is the comment character
* `1_c` are characters which are required to precede the comment character

This is to prevent treating valid syntax characters as comment characters.

## .miq.u.text.dedup
| parameter | Type  | Description               |
| --------- | ----- | ------------------------- |
| `chars`   | c\|cl | Characters to deduplicate |
| `clLine`  | cl    | Line to deduplicate       |

Remove repeated sequential occurrences of `chars` from `clLine` and reduce them to first character of `chars`.

## .miq.u.text.upper, .miq.u.text.lower

> [!WARNING] Not tested on types 77-97h (anymap and nested sym enum)

Forgiving versions of `upper` and `lower`:
* Handle both keyed and unkeyed tables
* Does not hit `'stack`, unlike its q counterpart
* If value can't be processed, return as is instead of returning `'type`

## .miq.u.text.qw
| parameter | Type | Description |
| --------- | ---- | ----------- |
| `cl`      | cl   | Any cl      |

Quote word `cl` in double quotes. For the ocassions when you need to quote your string and are getting lost in the escaped double quotes.

# Numeric
| Function              | Description                                                                       |
| --------------------- | --------------------------------------------------------------------------------- |
| `.miq.u.num.null`     | Dictionary of all nulls, keyed by type                                            |
| `.miq.u.num.pinf`     | Dictionary of positive infinities, keyed by type                                  |
| `.miq.u.num.ninf`     | Dictionary of negative infinities, keyed by type                                  |
| `.miq.u.num.inf`      | Dictionary of both negative and positive infinities, in this order, keyed by type |
| `.miq.u.num.o2d`      | Convert octal number to decimal                                                   |
| `.miq.u.num.d2o`      | Convert decimal number to octal                                                   |

# q & miQ types
| Function              | Description                                                                                                       |
| --------------------- | ----------------------------------------------------------------------------------------------------------------- |
| `.miq.u.q.types`      | List of all native q types using short representation                                                             |
| `.miq.u.q.atoms`      | List of all atomic q types using short representation                                                             |
| `.miq.u.q.lists`      | List of all list q types using short representation                                                               |
| `.miq.u.q.info`       | Table describing the native q datatypes in detail                                                                 |
| `.miq.u.miq.atoms`    | List of miQ fundamental atomic types                                                                              |
| `.miq.u.miq.lists`    | List of miQ fundamental list types                                                                                |
| `.miq.u.miq.special`  | List of miQ special types                                                                                         |
| `.miq.u.miq.types`    | List of all miQ types                                                                                             |

# Operators
| Function                  | Description                                                                                           |
| ------------------------- | ----------------------------------------------------------------------------------------------------- |
| `.miq.u.ops`              | Table of unary `u`, binary `b` and ternary `t` operators keyed by the operator's internal number `n`  |
| `.miq.u.op.u`             | List of all unary operators                                                                           |
| `.miq.u.op.b`             | List of all binary operatos                                                                           |
| `.miq.u.op.t`             | List of all ternary operators                                                                         |

# Other
## .miq.u.elision
Elision value is stored conveniently within this variable.

## .miq.u.ltrim, .miq.u.rtrim, .miq.u.trim
Slightly faster versions of `ltrim`, `rtrim` and `trim` that does not fail on atomic values.

> [!NOTE] Native `ltrim` and `rtrim` apparently operates over enum types 20-76h, but it is non-obvious to me why.

## .miq.u.copy
Perform a deep copy of any object.

## .miq.u.sleep
| parameter | Type | Description       |
| --------- | ---- | ----------------- |
| `jMilis`  | j    | New function name |

Sleep the process for `jMilis` miliseconds.

## .miq.u.seed
Set seed `\S` value to a random number derived from `-1?0Ng`.

## .miq.u.wait
| parameter   | Type | Description               |
| ----------- | ---- | ------------------------- |
| `jTimes`    | j    | Number of attempts        |
| `jInterval` | j    | Interval between attempts |
| `fn`        | fn   | Function to be executed   |
| `args`      | -    | As accepted by `fn`       |

Attempts `jTimes` to execute function `fn` using `args` every `jInterval` miliseconds. To execute variadit function `fn`, pass in a dot apply projection, i.e. `{[x+y] 1+2}.`.

## .miq.u.stack
Return full stack, printed.

## .miq.u.nFrame
| parameter | Type | Description  |
| --------- | ---- | ------------ |
| `n`       | j    | Frame number |

Return `n`-th frame relative from top of the execution stack.

## .miq.u.self
Return name of function from caller's perspective.

## .miq.u.caller
Return name of caller from caller's perspective.

## .miq.u.nCaller
| parameter | Type | Description  |
| --------- | ---- | ------------ |
| `n`       | j    | Frame number |

Return the name of `n`-th caller from caller's perspective. If the caller turns out to be a function from `.q` namespace, dive deeper once and return the name found.

## .miq.u.nCallerSkip
| parameter | Type | Description        |
| --------- | ---- | ------------------ |
| `n`       | j    | Frame number       |
| `cll`     | cll  | List of patterns   |

Return the name of `n`-th caller from caller's perspective, skipping names that match any of the `cll` patterns once at most.

## .miq.u.nCallerStrict
| parameter | Type | Description  |
| --------- | ---- | ------------ |
| `n`       | j    | Frame number |

Return the name of `n`-th caller from caller's perspective, skipping over any number of nested `.q` functions.

