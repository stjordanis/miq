Blackboard
==========
---
Blackboard is a centralised and managed storage space for modules to use during runtime. This enables developers writing functional and stateless libraries and to separate any configuration and other values required at runtime from the library's own namespace. That, in turn, can be utilised to preserve the state of a process to clone or duplicate it.

**Errors**

| Message                           | Explanation                                               |
| --------------------------------- | --------------------------------------------------------- |
| `Not a valid blackboard address`  | Invalid blackboard path                                   |
| `No such blackboard address`      | Blackboard or blackboard item does not exist              |
| `No such blackboard`              | Blackboard does not exist                                 |
| `No such blackboard item`         | Item not found on blackboard                              |
| `Blackboard context taken`        | Attempted creation of an existing blackboard              |

**Examples**
```
q).miq.bb.root[]
`.bb
q)bb:0N!.miq.bb.new[`test]
2021.03.13 | 18:46:35.003 | [INFO]  | <.miq.bb.new> | Creating new blackboard: `.bb.test
`.bb.test
q).miq.bb.is.bb[bb]
1b
q).miq.bb.is.error.bb[`.bb.fake]
2021.03.13 | 18:46:41.674 | [ERROR] | <.miq.bb.is.error.bb> | No such blackboard: `.bb.fake
'No such blackboard: `.bb.fake
  [0]  .miq.bb.is.error.bb[`.bb.fake]
       ^
q)-3!get 0N!.miq.bb.set[bb;`item;(0N;`;2%4)]
`.bb.test.item
"(0N;`;0.5)"
q)get 0N!.miq.bb.set[`test;`init;1b]
`.bb.test.init
1b
q).miq.bb.is.item[`test;`item]
1b
q).miq.bb.delete[`test]
2021.03.13 | 18:47:38.828 | [INFO]  | <.miq.bb.delete> | Deleting blackboard: `test
`.bb


q).miq.bb.root[]
`.bb
q)bb:0N!.miq.bb.new[`test]
22:24:57.168 [INFO]   <.miq.bb.new> Creating new blackboard: `.bb.test
`.bb.test
q).miq.bb.is.bb[bb]
1b
q).miq.bb.is.error.bb[`.bb.fake]
22:25:17.547 [ERROR]  <.miq.bb.is.error.bb> No such blackboard: `.bb.fake
'No such blackboard: `.bb.fake
  [0]  .miq.bb.is.error.bb[`.bb.fake]
       ^

q)-3!get 0N!.miq.bb.set[bb;`item;(0N;`;2%4)]
`.bb.test.item
"(0N;`;0.5)"

/ `test will expand to `.bb.test (which is identical to 'bb' variable)
q)get 0N!.miq.bb.set[`test;`init;1b]
`.bb.test.init
1b
q).miq.bb.is.item[`test;`item]
1b
q).miq.bb.delete[bb]
22:25:45.034 [INFO]   <.miq.bb.delete> Deleting blackboard: `test
`.bb
```

## Checks
### .miq.bb.is.valid
| parameter | Type | Description        |
| --------- | ---- | ------------------ |
| `s`       | ns   | Blackboard address |

Return `1b` if address `s` belongs to a blackboard, `0b` otherwise.

### .miq.bb.is.present
| parameter | Type  | Description        |
| --------- | ----- | ------------------ |
| `s`       | ns    | Blackboard address |

Return `1b` if `s` is present on blackboard, `0b` otherwise.

### .miq.bb.is.bb
| parameter | Type  | Description     |
| --------- | ----- | --------------- |
| `sBoard`  | s\|ns | Blackboard name |

Return `1b` if `sBoard` is a blackboard, otherwise `0b`.

### .miq.bb.is.item
| parameter | Type  | Description     |
| --------- | ----- | --------------- |
| `sBoard`  | s\|ns | Blackboard name |
| `sItem`   | s     | Item            |

Return `1b` if item `sItem` is defined on blackboard `sBoard`, `0b` otherwise.

## .miq.bb.root
Return blackboard root as configured via `root.bb`.

## .miq.bb.realpath
| parameter | Type  | Description     |
| --------- | ----- | --------------- |
| `s`       | s\|ns | Blackboard name |

Return full address of blackboard identified by `s`.

## .miq.bb.new
| parameter | Type  | Description     |
| --------- | ----- | --------------- |
| `sBoard`  | s\|ns | Blackboard name |

Create a new blackboard named `sBoard`, seeded with `1#.q`. Skip of the blackboard exists already, and signal if the blackboard address is taken.

## .miq.bb.delete
| parameter | Type  | Description     |
| --------- | ----- | --------------- |
| `sBoard`  | s\|ns | Blackboard name |

Delete a blackboard identified by `sBoard`.

## .miq.bb.set
| parameter | Type  | Description     |
| --------- | ----- | --------------- |
| `sBoard`  | s\|ns | Blackboard name |
| `sItem`   | s     | Item            |
| `content` | -     | Anything goes   |

Set `sItem` to value `content` on `sBoard` blackboard. Create `sItem` if it does not exist, overwrite otherwise.

## .miq.bb.get
| parameter | Type  | Description     |
| --------- | ----- | --------------- |
| `sBoard`  | s\|ns | Blackboard name |
| `sItem`   | s     | Item            |

Return the value of `sItem` from `sBoard` blackboard.

## .miq.bb.drop
| parameter | Type  | Description     |
| --------- | ----- | --------------- |
| `sBoard`  | s\|ns | Blackboard name |
| `sItem`   | s     | Item            |

Remove `sItem` from `sBoard` blackboard.

