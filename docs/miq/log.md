log
===
---
Placing useful information on the output while program is running is one hallmark of well written code. Log subsystem is a default implementer of `` `stl.log `` handlers.

**Log severity in ascending order**
* `debug`
* `show`
* `info`
* `warn`
* `error`
* `fatal`

**Errors**

| Message             | Explanation                                    |
| ------------------- | ---------------------------------------------- |
| `Log level invalid` | Attempt to intialise using undefined log level |

**Examples**
```
q).miq.config.set[`log.level;`show]
`show
q).miq.config.apply[]
q).miq.log.debug "I don't want to see all of this"
q).miq.log.show `this`is!2#`nice
this| nice
is  | nice
q).miq.log.info "Show me this"
22:01:04.970 [INFO]   <> Show me this
q).miq.log.warn "ouch"
22:01:17.620 [WARN]   <> ouch
q)f:{'.miq.log.error "You shouldn't do this!"}
q)f[]
22:02:26.753 [ERROR]  <..f> You shouldn't do this!
'You shouldn't do this!
  [0]  f[]
       ^
q)ohshi:{.miq.log.fatal "Something has gone batshit! Terminating!"}
q)ohshi[]
22:03:15.661 [FATAL]  <..ohshi> Something has gone batshit! Terminating!
```

## .miq.log.init
Action `log.level` and `log.caller` configuration.

## .miq.log.debug
| parameter | Type | Description       |
| --------- | ---- | ----------------- |
| `clMsg`   | cl   | Message to output |

Output `clMsg` message to `STDOUT` stream on debug level.

## .miq.log.show
| parameter | Type | Description            |
| --------- | ---- | ---------------------- |
| `obj`     | -    | Object to pretty print |

Pretty printing passed `obj` object to `STDOUT` stream.

## .miq.log.info
| parameter | Type | Description       |
| --------- | ---- | ----------------- |
| `clMsg`   | cl   | Message to output |

Output `clMsg` message to `STDOUT` stream on information level.

## .miq.log.warn
| parameter | Type | Description       |
| --------- | ---- | ----------------- |
| `clMsg`   | cl   | Message to output |

Output `clMsg` message to `STDOUT` stream on warning level.

## .miq.log.error
| parameter | Type | Description       |
| --------- | ---- | ----------------- |
| `clMsg`   | cl   | Message to output |

Outputs provided `clMsg` message to `STDERR` stream and returns it unchanged. Useful for throwing signal immediately after.miq.logging error.

## .miq.log.fatal
| parameter | Type | Description       |
| --------- | ---- | ----------------- |
| `clMsg`   | cl   | Message to output |

Output `clMsg` message to `STDERR` stream. Exiting is not enforced, however fatal error is by definition non-recoverable and you are expected to handle killing your own application in graceful manner after encountering fatal error.

