miQ
===
---
Microkernel that abstracts and standardises common functionality.

## Consumable variables
Used only once and cleared after use, these variables affect the miQ kernel in the following way:
* `QINIT` - use to embed miQ kernel into q shell (by assigning it a path to `miq.q`)
* `MIQ_MODULE_PATH` - parse to a list of filesymbols and use the output to configure `module.module.directory`
* `MIQ_SILENT_BOOT` - if not empty, miQ kernel will startup using *warn* log level
* `MIQ_LOAD_TIME` - if not empty, print out the time it took to load \& initialise miQ kernel

## Reserved namespaces
Namespaces always taken by miQ kernel:
* `.miq` - miQ implementation lives here, this documentation covers that
* `.is`  - implementation of miQ types, see [.is documentation](../is)
* `.to`  - type casting using miQ type notations, see [.to documentation](../to)

[Configurable](#preconfiguration) namespaces by the user:
* `.bb`  - used by blackboard to store state
* `.cfg` - explicit (user) configuration is stored here

## Pre-configuration
Special configuration that is required before miQ base is fully loaded. The values are hardcoded, but can be overriden through shell environment variables, as seen in table below.

> [!WARNING] Only a namespace of depth 1 are allowed!

| Name          | Type  | Default       | ENVVAR override   | Definition                                                                        |
| ------------- | ----- | ------------- | ----------------- | --------------------------------------------------------------------------------- |
| `ROOT.BB`     | `ns`  | `` `.bb``     | `MIQ_ROOT_BB`     | Root namespace for blakcboard                                                     |
| `ROOT.CFG`    | `ns`  | `` `.cfg``    | `MIQ_ROOT_CFG`    | Root namespace for user configuration                                             |

## Configuration
| Name                          | Type  | Default                               | Definition                                                                            |
| ----------------------------- | ----- | ------------------------------------- | ------------------------------------------------------------------------------------- |
| module.classes                | `sl`  | `` ` ``                               | Module classes enabled                                                                |
| module.*CLASS*                | `qd`  | -                                     | Class definition, see below                                                           |
| ns.reserved.list              | `sl`  | `` `.q`.Q`.h`.j`.o`.m ``              | KX reserved namespaces                                                                |
| ns.reserved.enable            | `b`   | `1b`                                  | If enabled, `.miq.ns.reserved[]` will return a list of reserved namespaces            |
| tmp.enable                    | `b`   | `0b`                                  | Temporary file storage flag                                                           |
| tmp.root                      | `fs`  | `` `:/tmp ``                          | Tmp directory root                                                                    |
| tmp.dir                       | `s`   | `.miq.config.i.tmp.dir[]`             | Process tmp directory of shape `miQ.<XXXXXXXX>.<PID>`, located in `tmp.root`          |
| tmp.autocreate                | `b`   | `0b`                                  | Automatic creation of tmp directory                                                   |
| tmp.autoclear                 | `b`   | `0b`                                  | Automatic clearing of tmp directory on disable                                        |
| console.debug                 | `b`   | `0b`                                  | Print out the stracktrace of console internals                                        |
| console.input.lang            | `c`   | `"q"`                                 | Language to be interpreted by console                                                 |
| console.input.quit            | `s`   | `` `local ``                          | `` `local`` or `` `remote``, defines behaviour of `\\`                                |
| console.input.block           | `b`   | `0b`                                  | Use blocking `read0` call instead of `.z.pi` override                                 |
| console.remember.lang         | `b`   | `0b`                                  | Use last used `lang` when starting console again                                      |
| console.remember.ch           | `b`   | `0b`                                  | Target last used `ch` when starting console again                                     |
| console.remember.d            | `b`   | `1b`                                  | Set execution context to last used context, per handle                                |
| colour.codes                  | `qd`  | `.miq.config.i.colour.codes[]`        | Dictionary of colour names and their RGB definitions                                  |
| colour.modes                  | `qd`  | `.miq.config.i.colour.modes[]`        | Dictionary of modes that can alter printed text                                       |
| fd.STDIN                      | `j`   | `0i`                                  | STDIN file descriptor                                                                 |
| fd.STDOUT                     | `j`   | `1i`                                  | STDOUT file descriptor                                                                |
| fd.STDERR                     | `j`   | `2i`                                  | STSERR file descriptor                                                                |
| fd.redirect.append            | `b`   | `1b`                                  | Control `.miq.fd.redirect[]` behaviour                                                |
| qk.variable                   | `sl`  | `.miq.config.i.qk.variable`           | List of functions whose return type is variable                                       |
| qk.function                   | `sl`  | `.miq.config.i.qk.function`           | List of functions whose return type is function                                       |
| hook.remove.safe              | `b`   | `1b`                                  | Will cause `.miq.hook.remove[]` to throw a signal on error                            |
| log.level                     | `s`   | `` `show ``                           | Output log message of `log.level` severity and higher                                 |
| log.caller                    | `b`   | `1b`                                  | Prepend message with function name initiating logging                                 |
| log.skip                      | `cll` | `(".q.*";"*.is.*")`                   | List of patterns used to match function names to skip over during stack look-behind   |
| ipc.flush                     | `b`   | `0b`                                  | Automatic flushing of asynchronous ipc requests                                       |

**Class definition**
* at least one of `directory` and `envvar` has to be defined,
* `directory` takes precedence over `envvar`,
* `extension` is mandatory and used to match files
| Key               | Type | Definition                                                |
| ----------------- | ---- | --------------------------------------------------------- |
| *CLASS*.envvar    | s    | Name of environment variable storing directory of *CLASS* |
| *CLASS*.directory | fsl  | List of directories holding *CLASS* files                 |
| *CLASS*.extension | cll  | List of patterns used by like to match *CLASS* files      |

**Default class definitions**

| *CLASS*   | module        | config        | role           | so           |
| --------- | ------------- | ------------- | -------------- | ------------ |
| envvar    | `` `MIQ_MOD`` | `` `MIQ_CFG`` | `` `MIQ_ROLE`` | `` `MIQ_SO`` |
| extension <td colspan=3 align=center>`("*.[kq]";"*.k_";"*.q_")` <td colspan=1>`,"*.so"`</table>


## Constants
| Name              | Definition                                                |
| ----------------- | --------------------------------------------------------- |
| `SELF`            | Module's namespace                                        |
| `BB`              | Blackboard namespace                                      |
| `INIT`            | Initiation status                                         |
| `CFG.EXPLICIT`    | User configuration values                                 |
| `CFG.DEFAULT`     | Default configuration values                              |
| `CFG.APPLIED`     | Applied configuration values                              |
| `LIBRARY`         | Available modules and their status                        |
| `QK.DESCRIPTION`  | Table holding function's name, body and return type       |
| `GIST`            | All captured namespace gists                              |
| `OBJECTS`         | All known objects and their characteristics               |
| `HANDLERS`        | Currently defined handlers and related information        |
| `TEMPLATES`       | Currently templated functions and related information     |
| `ITER.GIST`       | Incremental component of gist names                       |
| `TMP.PATH`        | Filesystem path to process tmp directory                  |
| `CONSOLE.CODE`    | Buffer for unexecuted code                                |
| `CONSOLE.ACTIVE`  | Indicates whether console is currently running            |
| `CONSOLE.OUT`     | Output stream for console messages                        |
| `CONSOLE.CH`      | Connection handle(s) console is currently operating on    |
| `CONSOLE.D`       | Last used directory `\d` per connection handle            |
| `HOOK.HOOKS`      | Registered routines to be executed for a particlar hook   |
| `HOOK.PRIMARY`    | Routines registered as primary for a particlar hook       |
| `HOOK.STACK`      | Used by `.miq.hook.depth` to determine depth of stack     |

**Special constants**

| Name           | Definition                                                                 |
| -------------- | -------------------------------------------------------------------------- |
| `LOADED`       | Stores file from which miQ has been loaded and a timestamp indicating when |
| `cfg.LOADED`   | Same as above, but for configuration file                                  |

## Table templates
* `TT.LIBRARY`
| path | name | extension |!| class | loaded | lzp | request |
| ---- | ---- | --------- |-| ----- | ------ | --- | ------- |
| fs   | s    | s         |!| s     | b      | p   | *       |

* `TT.DESCRIPTION`
| name |!| body | return |
| ---- |-| ---- | ------ |
| s    |!| \*   | s      |

* `TT.GISTS`
| name |!| context | zp | gist |
| ---- |-| ------- | -- | ---- |
| s    |!| nsl     | p  | \*   |

* `TT.OBJECTS`
| class |!| signature | types |
| ----- |-| --------- | ----- |
| s     |!| \*        | \*    |

* `TT.HANDLERS`
| name |!| implementer | arity | description | attached |
| ---- |-| ----------- | ----- | ----------- | -------- |
| s    |!| ns          | j     | \*          | nsl      |

* `TT.TEMPLATES`
| name | interface |!| order |implementer | arity |
| ---- | --------- |-| ----- |----------- | ----- |
| ns   | sl        |!| j     | ns         | j     |

## Hooks
| Name                      | Default implementation        | Description                                                                       |
| ------------------------- | ----------------------------- | --------------------------------------------------------------------------------- |
| `.miq.z.console.prompt`   | `.miq.z.dd.console.prompt`    | Console prompt implementation                                                     |
| `.miq.z.p`                | `.miq.z.dd.p`                 | Replacement of `.z.p` hook                                                        |
| `.miq.z.d`                | `.miq.z.dd.d`                 | Replacement of `.z.d` hook                                                        |
| `.miq.z.z`                | `.miq.z.dd.z`                 | Replacement of `.z.z` hook                                                        |
| `.miq.z.n`                | `.miq.z.dd.n`                 | Replacement of `.z.n` hook                                                        |
| `.miq.z.t`                | `.miq.z.dd.t`                 | Replacement of `.z.t` hook                                                        |
| `.miq.z.LAST`             | -                             | Contains a 2-item list of `(nsHook;returnValue)` of last executed primary hook    |

## Accessors
| Name              | target                  | Note                                                            |
| ----------------- | ----------------------- | --------------------------------------------------------------- |
| `.miq.gists`      | `.miq.ns.gist.info[]`   | Table of all defined objects                                    |
| `.miq.objects`    | `OBJECTS`               | Table of all defined objects                                    |
| `.miq.handlers`   | `HANDLERS`              | Table of all defined handlers and their implementers            |
| `.miq.templates`  | `TEMPLATES`             | Table of all defined templates and their interfaces             |
| `.miq.hooks`      | `.miq.hook.info[]`      | Shorthand to return known hooks and their defaults              |

## System commands
| Command           | Used by                               | Comment                                                           |
| ----------------- | ------------------------------------- | ----------------------------------------------------------------- |
| `command`         | `.miq.system.is.command`              | Without this, no system command can be checked                    |
| `env`             | `.miq.system.env`                     | Cannot list environment variables without this command            |
| `ls`              | `.miq.fd.info`                        | Required to tease out details of open file descriptors            |
| `gdb`             | `.miq.fd.i.redirect`                  | Will be unable to redirect file descriptors without it            |
| `realpath`        | `.miq.fs.path.real`                   | Will speed up path cannonicalisation by a factor of 10            |

## Routine summary
| Name                      | Namespace         | Description                                                                                                           |
| ------------------------- | ----------------- | --------------------------------------------------------------------------------------------------------------------- |
| [Console](console)        | `.miq.console`    | Built-in console with many advanced capabilities                                                                      |
| [Colour](colour)          | `.miq.colour`     | Text colourisation                                                                                                    |
| [System](system)          | `.miq.system`     | Manage dependencies on system commands and environment variables                                                      |
| [File descriptor](fd)     | `.miq.fd`         | File descriptor fun and redirection of fd's in use (requires gdb)                                                     |
| [Module](module)          | `.miq.module`     | Loading of `q` and `k` code, binding to dynamically linked libraries                                                  |
| [Blackboard](bb)          | `.miq.bb`         | Storage and retrieval of run-time values                                                                              |
| [Configuration](config)   | `.miq.config`     | Assembly, storage, application and retrieval of configuration items                                                   |
| [Functional query](fs)    | `.miq.fq`         | [WIP] Functional query building utilities                                                                             |
| [Namespace](ns)           | `.miq.ns`         | Routines to manage namespaces                                                                                         |
| [Filesystem](fs)          | `.miq.fs`         | Routines to manage files and directories                                                                              |
| [Temporary storage](tmp)  | `.miq.tmp`        | TMP storage handling                                                                                                  |
| [QK](qk)                  | `.miq.qk`         | Parsing, interpretation and inspection of `q` and `k` code                                                            |
| [Syntax](syntax)          | `.miq.syntax`     | [WIP] Name classification of functions and variables                                                                  |
| [Function](function)      | `.miq.fn`         | Function composition and decomposition                                                                                |
| [Utility](u)              | `.miq.u`          | Number of routines helpful when working with text, numbers, types, ops, sleep, wait, seed & stack and its frames      |
| [Object](object)          | `.miq.object`     | Implementation of objects (named dictionaries of pre-defined shape & value types)                                     |
| [Handler](handler)        | `.miq.handler`    | Implementation of handlers (formal inter-module API)                                                                  |
| [Template](template)      | `.miq.template`   | Implementation of templates (multiple code paths under a single roof)                                                 |
| [Hook](hook)              | `.miq.hook`       | Native and custom hook support                                                                                        |
| [log](log)                | `.miq.log`        | Logging subsystem                                                                                                     |
| [ipc](ipc)                | `.miq.ipc`        | IPC subsystem                                                                                                         |
| [trap](trap)              | `.miq.trap`       | Trapping subsystem                                                                                                    |

