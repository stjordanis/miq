Function
========
---
Function and projection magic. Useful for projecting functions by parameters and disassembling projections on the fly.

**Errors**

| Message                               | Explanation                                                           |
| ------------------------------------- | --------------------------------------------------------------------- |
| `Function locked`                     | function is locked                                                    |
| `Not an address to executable object` | Address not pointing to an executable object                          |
| `Not an executable object`            | Passed object is not executable                                       |
| `Invalid arity`                       | Arity has to fall within <0;8> range                                  |
| `Function of arity 1 required`        | Only monadic functions can be executed atomically by `.miq.fn.atomic` |

**Examples**
```
q).miq.fn.flatten 0N!.miq.fn.compose (+[10];+[5];+)
+[10]+[5]+
(+;10)
(+;5)
+
q).miq.fn.compose 0N!.miq.fn.decompose {[a;b;c;d;e] a+b+c+d+e}[1;][3]
({[a;b;c;d;e] a+b+c+d+e};(1;::);,3)
{[a;b;c;d;e] a+b+c+d+e}[1;][3]
q)system~0N!.miq.fn.compose 0N!.miq.fn.decompose system
(.:;,,["\\"])
.,["\\"]
1b
q).miq.fn.arity {[a;b;c;d;e] a+b+c+d+e}[1;][3]
2
q).miq.fn.locked .miq.fn.locked
0b


// Projection
q).miq.fn.info {[a;b;c;d;e] a+b+c+d+e}[1;][3]
name      | `symbol$()
signature | `b`d`e
parameters| `a`b`c`d`e
namespace | `
locals    | `symbol$()
globals   | `symbol$()
file      | `
line      | -1
source    | "{[a;b;c;d;e] a+b+c+d+e}"
filled    | `a`c
arguments | 1 3
function  | {[a;b;c;d;e] a+b+c+d+e}[1;][3]

// Composition (projection)
q)show qc:.miq.fn.compose {x*y}[3],{x+2},{x+y*z}[1]
{x*y}[3]{x+2}{x+y*z}[1]
q).miq.fn.info qc[;2]
name      | `symbol$()
signature | ,`y
parameters| `y`z
namespace | `
locals    | `symbol$()
globals   | `symbol$()
file      | `
line      | -1
source    | "'[;]/[{x*y}[3],{x+2},{x+y*z}[1]]"
filled    | ,`z
arguments | ,2
function  | {x*y}[3]{x+2}{x+y*z}[1][;2]

// Iterator
q).miq.fn.info (/:)[,][`A`B]
name      | `symbol$()
signature | ,`y
parameters| `x`y
namespace | `
locals    | `symbol$()
globals   | `symbol$()
file      | `
line      | 0N
source    | ,/:
filled    | ,`x
arguments | ,`A`B
function  | ,/:[`A`B]


q).miq.fn.print {[a;b;c;d;e] a+b+c+d+e}[1;][3]
"lambda[,1;,::;,3]"
q).miq.fn.copy[`fn] {[a;b;c;d;e] a+b+c+d+e}[1;][3];
q).miq.fn.info[fn][`name]
`..fn
q).miq.fn.print fn
"`..fn[,,1;,::;,,3]"

q).miq.fn.copy[`qc;.miq.fn.compose {x+1},{x+y+2}[1],{x*y}]
{x+1}{x+y+2}[1]{x*y}
q).miq.fn.info each first each .miq.fn.flatten qc
name parameters signature namespace locals globals file line source    filled arguments arity function class
---------------------------------------------------------------------------------------------------------------
..qc ,`x        ,`x                                     -1   "{x+1}"                    1     {x+1}    function
..qc `x`y       `x`y                                    -1   "{x+y+2}"                  2     {x+y+2}  function
..qc `x`y       `x`y                                    -1   "{x*y}"                    2     {x*y}    function

q).miq.fn.atomic[.is.type][([]a:1 2 3;b:`a`b`c;c:(1#"a";"b";1#"c"))]
a b c
------
j s cl
j s c
j s cl
```

## Checks
### .miq.fn.is.locked
| parameter | Type          | Description       |
| --------- | ------------- | ----------------- |
| `fn`      | executable    | Executable object |

Return `1b` if function source code is locked, otherwise `0b`.

### .miq.fn.is.address
| parameter | Type      | Description               |
| --------- | --------- | ------------------------- |
| `fn`      | ns        | Executable object         |

Return `1b` if `fn` is an address to an executable object, `0b` otherwise.

### .miq.fn.is.executable
| parameter | Type          | Description       |
| --------- | ------------- | ----------------- |
| `fn`      | executable    | Executable object |

Return `1b` if `fn` is an executable object, otherwise `0b`.

> [!INFO] This includes q types from 100h to 112h, but excludes the special single letter evaluators, i.e. the hardcoded `"q"` and `"k"`.

### .miq.fn.is.arity
| parameter | Type      | Description               |
| --------- | --------- | ------------------------- |
| `j`       | j         | Arity                     |

Return `1b` if number `j` represents a valid arity of an executable object. That means it has to be a number between 0 and 8 inclusive.

## .miq.fn.flatten
| parameter | Type          | Description       |
| --------- | ------------- | ----------------- |
| `fn`      | executable    | Executable object |

If `fn` is a projection, return the projected function and each argument individually in the form of `(fn;arg1;...;argN)`. If `fn` is a composition, decompose and flatten in depth, returning possibly a nested list of flattened compositions and projections. Return `fn` for `fn`,`op` and `qi`.

## .miq.fn.decompose
| parameter | Type          | Description       |
| --------- | ------------- | ----------------- |
| `fn`      | executable    | Executable object |

> [!NOTE] Reversible via `.miq.fn.compose[]`.

If `fn` is a projection, return the projected function and its every projection in the form of `(fn;proj1;...;projN)`. If `fn` is a composition, break it and return individual constituents as a list. Return `fn` for `fn`,`op` and `qi`.

## .miq.fn.compose
| parameter | Type          | Description                   |
| --------- | ------------- | ----------------------------- |
| `fn`      | executable    | List of `(fn;arg1;...;argN)`  |

> [!WARNING] Don't feed with output from `.miq.fn.flatten[]`.

If `fn` looks like decomposed composition, re-compose it and return. If `fn` looks like decomposed projection, build projection and return it. Return unchanged input if no arguments are part of `fn`.

## .miq.fn.info
| parameter | Type          | Description       |
| --------- | ------------- | ----------------- |
| `fn`      | executable    | Executable object |

Return description of `fn`. For composition, it is the entry function that gets described. Description is a dictionary of following shape:
* `name`       - a single symbol for named functions, null symbol otherwise
* `parameters` - list of names of all formal parameters
* `signature`  - list of names of all unfilled formal parameters
* `namespace`  - effective namespace name for function execution, including leading dot
* `locals`     - list of locally defined objects
* `globals`    - list of globally defined objecst used within function
* `file`       - source file where this function originates from
* `line`       - line of source file where this function is defined
* `source`     - source code as a string
* `address`    - the location of object if `fn` is given as `s` or `ns`, or if `fn` is `code`
* `filled`     - list of names of all filled parameters; relevant for projection
* `arguments`  - list of arguments supplied to `filled` parameters, respecting their order
* `arity`      - number of parameters accepted by `fn`; `0` when explicitly defined without parameters
* `function`   - q object that has been inspected
* `class`      - output of `.miq.fn.class[fn]`

### .miq.fn.name
Optimised version of ``.miq.fn.info[fn][`name]``.

### .miq.fn.arity
Optimised version of ``.miq.fn.info[fn][`arity]``.

### .miq.fn.arguments
Shorthand for ``exec filled!arguments from .miq.fn.info[fn][`filled`arguments]``.

### .miq.fn.namespace
Shorthand for ``.miq.fn.info[fn][`namespace]``.

### .miq.fn.source
Shorthand for ``.miq.fn.info[fn][`source]``.

## .miq.fn.class
| parameter | Type          | Description       |
| --------- | ------------- | ----------------- |
| `fn`      | executable    | Executable object |

Return a class (type) of `fn`.

## .miq.fn.print
| parameter | Type          | Description       |
| --------- | ------------- | ----------------- |
| `fn`      | executable    | Executable object |

Converts `fn` into string representation, using it's name in a symbol form, followed by argument indexation. If `fn` is unnamed, use name `lambda`.

## .miq.fn.copy
| parameter | Type          | Description       |
| --------- | ------------- | ----------------- |
| `sName`   | s\|ns         | New function name |
| `fn`      | executable    | Executable object |

Deep copy of `fn` function definition to new `sName` label, returning the function assigned to `sName`. Use to force functions to acquire the name you want, including functions that are part of projection or composition. Useful for enhancing stack visibility for debug scenarios.

## .miq.fn.atomic
| parameter | Type          | Description           |
| --------- | ------------- | --------------------- |
| `fn`      | executable    | Executable object     |
| `arg`     | -             | Any argument to `fn`  |

Applies function `fn` atomically against `arg`. `fn` has to have arity of **1**.

