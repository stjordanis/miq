QK
==
---
Loading, parsing, interpretation and inspection of code. `.miq.qk.load` and `.miq.qk.process` routines together identical to behaviour of native q binary loading routing, from where the code has been taken. Hence the original implementation in `k`, and only slight enhancments to support additional features were introduced.

`.miq.qk.interpret` is making use of `.miq.syntax` routines to explain non-functional aspects of code.

**Errors**

| Message                        | Explanation                                                      |
| ------------------------------ | ---------------------------------------------------------------- |
| `Cannot interpret language`    | Only q and k supported at the moment                             |
| `Failed to change directory!`  | `.miq.qk.i.inspect` could not execute the `\d` system command    |

**Examples**
```
q)show code:.miq.qk.load `:../miq/miq.q
q)show code:.miq.qk.load `:../miq/miq.q
"/work/michal/repo/miq/examples/app/bin/../miq/miq.q" 1  "// ========== \\\\"
"/work/michal/repo/miq/examples/app/bin/../miq/miq.q" 2  "//  miQ base  \\\\"
"/work/michal/repo/miq/examples/app/bin/../miq/miq.q" 3  "// ========== \\\\"
"/work/michal/repo/miq/examples/app/bin/../miq/miq.q" 4  ""
"/work/michal/repo/miq/examples/app/bin/../miq/miq.q" 5  "\\d .miq"
"/work/michal/repo/miq/examples/app/bin/../miq/miq.q" 6  "/ Meta"
"/work/michal/repo/miq/examples/app/bin/../miq/miq.q" 7  "M.FAMILY  :`kernel;"
"/work/michal/repo/miq/examples/app/bin/../miq/miq.q" 8  "M.NAME    :`miq;"
"/work/michal/repo/miq/examples/app/bin/../miq/miq.q" 9  "M.VERSION :`0.001.0;"
"/work/michal/repo/miq/examples/app/bin/../miq/miq.q" 10 "M.COMMANDS:`command`env`ls`gdb`realpath;"
"/work/michal/repo/miq/examples/app/bin/../miq/miq.q" 11 ""
"/work/michal/repo/miq/examples/app/bin/../miq/miq.q" 12 "LOADED:`file`lzp!(-1!`$first -3#(get {});.z.p);"
"/work/michal/repo/miq/examples/app/bin/../miq/miq.q" 13 ""
"/work/michal/repo/miq/examples/app/bin/../miq/miq.q" 14 "/ miQ pre-configuration"
"/work/michal/repo/miq/examples/app/bin/../miq/miq.q" 15 "ROOT.BB :`.bb  ^ `$getenv`MIQ_ROOT_BB ;"
"/work/michal/repo/miq/examples/app/bin/../miq/miq.q" 16 "ROOT.CFG:`.cfg ^ `$getenv`MIQ_ROOT_CFG;"

/ this will be code[;0] for kdb version lower than 4.0
q).miq.qk.process[`fake.q;-1] code[;2]
q).miq.qk.process[`fake.q;-1] code[;2]
"fake.q" -1 "// ========== \\\\"
"fake.q" -1 "//  miQ base  \\\\"
"fake.q" -1 "// ========== \\\\"
"fake.q" -1 ""
"fake.q" -1 "\\d .miq"
"fake.q" -1 "/ Meta"
"fake.q" -1 "M.FAMILY  :`kernel;"
"fake.q" -1 "M.NAME    :`miq;"
"fake.q" -1 "M.VERSION :`0.001.0;"
"fake.q" -1 "M.COMMANDS:`command`env`ls`gdb`realpath;"
"fake.q" -1 ""
"fake.q" -1 "LOADED:`file`lzp!(-1!`$first -3#(get {});.z.p);"
"fake.q" -1 ""
"fake.q" -1 "/ miQ pre-configuration"
"fake.q" -1 "ROOT.BB :`.bb  ^ `$getenv`MIQ_ROOT_BB ;"
"fake.q" -1 "ROOT.CFG:`.cfg ^ `$getenv`MIQ_ROOT_CFG;"

q).miq.qk.fn.wrap[1#`] 0N!.miq.qk.fn.strip "k){[] 1+2}"
"k) 1+2"
"k){[] 1+2}"

q).miq.qk.parse code[7;0]
";"
(:;`LOADED;(!;,`file`lzp;(enlist;(!;-1;($;,`;(*:;(#;-3;(.:;{})))));`.z.p)))
::

q).miq.qk.interpret["q"] "a:0N!`a`b!1 2;a:string a;-3!a"
`a`b!1 2
"`a`b!(,\"1\";,\"2\")"
q)a
a| ,"1"
b| ,"2"

/ inspect is context aware, see definition
q).miq.qk.inspect code[7;0]
context   | .        .
definition| ..LOADED
datatype  | variable
class     | constant
q).miq.qk.inspect[code[6 7;0]]^([]code:code[6 7;0])
context    definition    datatype   class      code
------------------------------------------------------------------------------------------------
`.miq      ,`            ,`function ,`system   "\\d .miq"
`.miq`.miq `.miq.LOADED` `variable` `constant` "LOADED:`file`lzp!(-1!`$first -3#(get {});.z.p);"
```

## .miq.qk.load
| parameter | Type  | Description  |
| --------- | ----- | ------------ |
| `x`       | s\|fs | Path to file |

Load code from file `x`, stripping it of shebang line if present, and passes the code over to `.miq.qk.process` together with full path to file `x`.

## .miq.qk.process
| parameter | Type   | Description    |
| --------- | ------ | -------------- |
| `l`       | s\|fs  | Path to file   |
| `n`       | j      | Line numbering |
| `x`       | cl|cll | Code           |

Process code `x`, split by new line if in form of `cl`, by collapsing lines into interpretable code and removing comments. Include file `l` and line `n` info, if provided. If `l` is null, don't include extra information. If `n` is null, use natural line numbering, otherwise replace all line references to the given value.

## .miq.qk.fn.wrap
| parameter | Type  | Description                  |
| --------- | ----- | ---------------------------- |
| `slParam` | s     | Parameters to new function   |
| `clCode`  | cl    | Code to make a function from |

Wrap `clCode` in curly brackets and add definition of `slParam` formal parameters.

## .miq.qk.fn.strip
| parameter | Type  | Description         |
| --------- | ----- | ------------------- |
| `clCode`  | cl    | Function definition |

Remove curly brackets and formal parameter definitions from function's source `clCode`.

## .miq.qk.fn.break
| parameter | Type  | Description         |
| --------- | ----- | ------------------- |
| `fn`      | fn    | Function definition |

Break down function into a list of statements.

## .miq.qk.parse
| parameter | Type | Description  |
| --------- | ---- | ------------ |
| `clLine`  | cl   | Line of code |

Return a k-tree of code, excluding the magic semicolon and implanted generic null returns.

## .miq.qk.interpret
| parameter   | Type | Description |
| ----------- | ---- | ----------- |
| `cLanguage` | c    | Language    |
| `code`      | cll  | Code        |

Interpret `code` as `cLanguage`. Output mimics native q REPL.

## .miq.qk.inspect
| parameter | Type | Description |
| --------- | ---- | ----------- |
| `code`    | cll  | Code        |

Will attempt to recognise objects and definitions within the `code` and return the results in a form of dictionary `(context;,definition;,datatype;,class)`.

