miQ
===
---
- Dreaming of not having to develop every basic thing from scratch?
- Annoyed of hiring large, expensive teams just to reinvent the wheel?
- Tired of spending years on code that cannot be ever used on anything else?

Then welcome to miQ project! Here you will find information on miQ kernel, how it underpins an ecosystem of shareable modules, and how you can write your own module or make use of the modules already written.

## Project partitioning
* **miQ** - a single file implementing [miQ kernel](miq/miq) full of goodies, [miQ type system](is) and [miQ type casting](to)
* **STL** - Collection of generic and pluggable [modules](modules) of varying complexity (term borrowed from C++)

## Embedding
```
git clone https://gitlab.com/Shahdee/miq.git
cd miq/
MIQ_SILENT_BOOT=true MIQ_MODULE_PATH=STL/ QINIT=miq/miq.q q
```

* `MIQ_SILENT_BOOT` - optional, supress logging during until miQ self-initialises
* `MIQ_MODULE_PATH` - optional, paths to directories containing modules
* `QINIT` - use to embed miQ into your q

You can also quick-boot miQ kernel by simply executing it:
```
q miq/miq.q
```

And put into a file named `miq`, you can call `miq` command from your shell to start an miQ enabled console:
```
#!/bin/bash
Q=<__PATH_TO_Q_BINARY__>
MIQ=<__PATH_TO_MIQ_REPO__>

export QINIT=${MIQ}/miq/miq.q
export MIQ_MODULE_PATH=${MIQ}/STL
export MIQ_SILENT_BOOT=true

rlwrap -r ${Q} $@
```

## Author
Michal Širochman / [LinkedIn](https://www.linkedin.com/in/sirochman)

