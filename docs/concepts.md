Concepts
========
---
## Module classes
Module is any executable code, and the code is divided into classes based on their purpose. Classes implemented out of the box by miQ can be found in the table below. Developer can add own class easily by simply defining it's configuration and enabling it.

| Class        | Purpose                           |
| ------------ | --------------------------------- |
| `module`     | Extension of code base            |
| `config`     | Altering behaviour of loaded code |
| `definition` | Object and packet definition      |
| `role`       | Application level logic           |
| `so`         | Source of C dynamic libraries     |

## Blackboard
Sometimes for module to function properly and fulfill it's purpose, it will need to store runtime values to preserve it's state. This requirement is abstracted via blackboard functionality, allowing for state to be stored in a dedicated context. It makes implementing a recovery codepath or safe code reloading much easier and possibly generic for all modules. For documentation of routines and their purpose, please see the [blackboard documentation](miq/bb).

| Term              | Meaning                                                   |
| ----------------- | --------------------------------------------------------- |
| `blackboard`      | *Any* context nested under `BB.ROOT`                      |
| `blackboard item` | Item of `blackboard` that is not a `blackboard` itself    |

## Configuration management
Configuration is not passed to initialisation function as is common, but stored independently in a separate context. Therefore, module defines default values of configuration items it requires to operate, while user is [given tools](miq/cfg) to alter the configuration.

| Term                  | Meaning                                                   |
| --------------------- | --------------------------------------------------------- |
| `configuration item`  | Name of a configuration option                            |
| `default`             | Pre-set value of `configuration item`                     |
| `explicit`            | User set value of `configuration item`                    |
| `applied`             | Currently applied value of `configuration item`           |

## Objects
Object is a named dictonary whose keys (signature) and their expected types are defined in advance during class definition. We can think of objects as higher-order variables of fixed shape. Nesting is allowed, so object can be a superposition of multiple objects and types.

| Term                  | Meaning                                                                               |
| --------------------- | ------------------------------------------------------------------------------------- |
| `class`               | Name of object definition                                                             |
| `signature`           | Expected keys of an object of a given class                                           |
| `types`               | Types acceptable by respective signature keys (miQ types, class name, null symbol)    |
| `definition`          | Dictionary of `signature!types`                                                       |
| `constructor`         | Function taking a list of values and returning an object of desired class             |
| `tt`                  | Function returning an empty table formed based on object definition (table template)  |

## Handlers
Handlers form a part of global named virtual interface that underpins dynamic dependency resolution. Once a handler is defined, one can attach any address to it to start using its implementer. Implementer changes are propagated to all attached addresses automatically, ensuring that every attached address is pointing at the right code. Read more about handlers in [the dedicated documentation](miq/handler).

| Term                  | Meaning                                                               |
| --------------------- | --------------------------------------------------------------------- |
| `handler`             | Name of handler                                                       |
| `implementer`         | Address to function implementing the `handler`                        |
| `attach`              | To link context name to handler's `implementer`                       |
| `import`              | Automated creation of addresses attached to chosen `handler`(s)       |
| `export`              | Automated translation of implementers to local function definitions   |

## Templates
Templated functions are special compositions that act as a central interface to many underlying functions. When you invoke the templated function, it will note the number of arguments and their types and compare those with implemented interfaces. Every `interface` has a single `implementer`, and null symbol `` ` `` servers as **catch all** type that matches anything.

| Term                  | Meaning                                                               |
| --------------------- | --------------------------------------------------------------------- |
| `template`            | A single function that routes arguments to one of `implementers`      |
| `interface`           | Description of formal parameters that `implementer` processes         |
| `implementer`         | An actual function that processes arguments passed to template        |
| `arity`               | Arity refers to the number of arguments an interface takes            |

## Generic hooks
Hooks are nothing new under the Sun. KDB uses hooks to allow developers to modify default behaviour, as with `.z.pg`, or implement altogether new functionality, as with `.z.ws`. This concept has been taken further by miQ, which generalises this concepts by allowing definition of custom, ad-hoc hooks, and implements routines to handle both native KDB and user defined routines in the same manner.

| Term                  | Meaning                                                                                                   |
| --------------------- | --------------------------------------------------------------------------------------------------------- |
| `default definition`  | Default hook implementation found under `ns.z.dd.hook`                                                    |
| `hook definition`     | Hook can have multiple definitions registered under distinct `label`s                                     |
| `label`               | Name of a registered hook definition                                                                      |
| `primary`             | First to be executed hook definition                                                                      |
| `secondary`           | Non-primary hook definition, executed after `primary` and only when hook has a `primary` definition       |

## miQ types
You may have noticed that q wrapper is promoting numeric values to explicit boolean in a number of functions. miQ is taking this 2 steps further, refining the types and implementing new pseudo-types, together with respective tests. More on this topic can be found in [documentation page dedicated to types](types).

| Term                  | Meaning                                                                                           |
| --------------------- | ------------------------------------------------------------------------------------------------- |
| `miQ type`            | Name of miQ type, as seen in `Atom` and `List` columns on [Types](types#miq-type-notations) page  |
| `q type`              | Number of native q type, as seen in `q n` column on [Types](types#miq-type-notations) page        |

## Constants
Constant is, by definition, a value that does not change over time. This is ipredominantly true for miQ constants, albeit some exception are allowed for implementation specific reasons. Constants are used throughout module's code base to reference common resources. Constants were introduced in order to:
* Make code more readable by abstracting common references
* Make code changes simpler as every common value is referrenced by a single name
* Make code easily configurable thanks to above properties

## Table templates
Table templates are technically constants, as they are empty tables and, in essence, table schemas. Their purpose is to define table in a single place, removing some hardcoding and making it easier to maintain.

## Objects & Packets
Purpose of object is to define a strict, typed, named dictionary structure. It is by no means a classic OOP object known from C++ or Python. Objects are useful constructs as they essentialy are a custom datatype, which can be taken advantage of in module design.

Packets are, as the name suggests, intended to transmit data between processes. They are implemented as higher order objects, and can leverage the same support offered to objects.

For ease of use, objects come with constructors, and miQ provides tools to identify the object or verify the object is a valid member of object class. Their documentation can be found [here](miq/op).

