to
==
---
Set of functions to convert fundamental and temporal type values using miQ type notation. Only miQ types that are derived from q types 1 - 19h are supported, [see the table below](#miq-type-casting).

> [!ATTENTION] Experimental! Will require a number of improvements to become relevant and intuitive, especially distinction between converting to atomic and list types.

**Errors**

| Message                                                   | Explanation                                                   |
| --------------------------------------------------------- | ------------------------------------------------------------- |
| `Only miQ fundamental types are supported at the moment`  | Attempt to cast to one of `.miq.u.miq.special` types          |
| `Casting returned unexpected type`                        | Printed out when `warn` or `error` casting variation fails    |
| `Not a recognised part of temporal`                       | Not one of `.miq.to.temporalParts` recognised temporal parts  |

**Examples**
```
// miQ types
q)(,//).to.c (`109;(105;(81.123f;0x21)))
"miQ!"
q).to.cl (`109;(105;(81.123f;0x21)))
"109"
("105";("81.123";"21"))

q)-3!.to.x (0x0133;("0xff00";`123;2020.12m))
"(0x0133;(0xff00;0x7b;0xfb))"

// Temporal
q)-3!.to.ss (.z.p;(.z.n;.z.t))
"(53i;53 53i)"

q)-3!.to.dd (.z.d;(.z.z;.z.p))
"(8i;8 8i)"
```

## miQ type casting
* Functions casting to atomic miQ types:
  * operate on lists atomicaly
  * do not narrow the type
* Functions casting to list miQ types:
  * operate on lists at depth 1
  * attempt to return a list even if given an atom

If casting is not successful, return that element unchanged. To throw a signal, use the *.to.error* flavour of casting functions.

| Type | Atom     | List      || Type | Atom     | List      || Type | Atom     | List      |
| ---- | -------- | --------- || ---- | -------- | --------- || ---- | -------- | --------- |
| b    | `.to.b`  | `.to.bl`  || s    | `.to.s`  | `.to.sl`  || ns   | `.to.ns` | `.to.nsl` |
| g    | `.to.g`  | `.to.gl`  || p    | `.to.p`  | `.to.pl`  || fd   | `.to.fd` | `.to.fdl` |
| x    | `.to.x`  | `.to.xl`  || m    | `.to.m`  | `.to.ml`  || fs   | `.to.fs` | `.to.fsl` |
| h    | `.to.h`  | `.to.hl`  || d    | `.to.d`  | `.to.dl`  || fh   | `.to.fh` | `.to.fhl` |
| i    | `.to.i`  | `.to.il`  || z    | `.to.z`  | `.to.zl`  || cs   | `.to.cs` | `.to.csl` |
| j    | `.to.j`  | `.to.jl`  || n    | `.to.n`  | `.to.nl`  || ch   | `.to.ch` | `.to.chl` |
| e    | `.to.e`  | `.to.el`  || u    | `.to.u`  | `.to.ul`  || op   | -        | -         |
| f    | `.to.f`  | `.to.fl`  || v    | `.to.v`  | `.to.vl`  || fn   | -        | -         |
| c    | `.to.c`  | `.to.cl`  || t    | `.to.t`  | `.to.tl`  || fp   | -        | -         |
|      |          |           ||      |          |           || qc   | -        | -         |
|      |          |           ||      |          |           || qi   | -        | -         |
|      |          |           ||      |          |           || qs   | `.to.qs` | `.to.qsl` |
|      |          |           ||      |          |           || qd   | -        | -         |
|      |          |           ||      |          |           || qt   | -        | -         |
|      |          |           ||      |          |           || qk   | -        | -         |
|      |          |           ||      |          |           || qp   | -        | -         |

## Temporal part extraction
Atomic functions implementing the extraction of temporal parts from temporal values as well, as defined by [.miq.to.temporalParts](#miqtotemporalParts).

| Part | Function   |
| ---- | ---------- |
| year | `.to.year` |
| mm   | `.to.mm`   |
| dd   | `.to.dd`   |
| hh   | `.to.hh`   |
| uu   | `.to.uu`   |
| ss   | `.to.ss`   |

## .to.warn.\*, .to.error.\*
Generate by [.to.init](#toinit) to standardise and simplify logging out inconsistencies.
* `.to.warn` - returns `1b` on success, otherwise `0b` and log a warn level message
* `.to.error` - returns `1b` on success, otherwise log an error level message and signal

## .miq.to.init
Generates `.to.warn` and `.to.error` routines for all implemented casting and temporal extraction routines.

## .miq.to.type
| parameter | Type | Description       |
| --------- | ---- | ----------------- |
| `sType`   | s    | miQ type notation |
| `arg`     | -    | Any value         |

Internal implementation for casting `arg` value to `sType`. Use individual [miQ type casting](#miq-type-casting) functions instead.

## .miq.to.temporalParts
List of temporal parts implemented by kdb.

## .miq.to.temporal
| parameter | Type | Description       |
| --------- | ---- | ----------------- |
| `sPart`   | s    | Part to extract   |
| `arg`     | -    | Temporal value(s) |

Internal implementation for extracting `sPart` from temporal value `arg`. User individual [Temporal part extraction](#temporal-part-extraction) functions instead.

