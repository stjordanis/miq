Examples
========
--------

## Enabling all features
```
q).prof.config.set'[`stack`parent`depth`user`context`input;1b]
111111b
q).prof.config.apply[]
```

## Enable profiling on set of functions
```
q).prof.enable each `.miq.hook.info`.miq.ns.tree`.miq.ns.i.tree`.miq.ns.user`.miq.ns.reserved`.miq.ns.is.context`.miq.hook.z`.miq.hook.i.default`.miq.ns.is.present`.miq.fq.d2cond;
2021.01.14 | 23:46:10.633 | [INFO]  | <.prof.i.enable> | Profiling: `.miq.hook.info
2021.01.14 | 23:46:10.633 | [INFO]  | <.prof.i.enable> | Profiling: `.miq.ns.tree
2021.01.14 | 23:46:10.634 | [INFO]  | <.prof.i.enable> | Profiling: `.miq.ns.i.tree
2021.01.14 | 23:46:10.634 | [INFO]  | <.prof.i.enable> | Profiling: `.miq.ns.user
2021.01.14 | 23:46:10.635 | [INFO]  | <.prof.i.enable> | Profiling: `.miq.ns.reserved
2021.01.14 | 23:46:10.635 | [INFO]  | <.prof.i.enable> | Profiling: `.miq.ns.is.context
2021.01.14 | 23:46:10.636 | [INFO]  | <.prof.i.enable> | Profiling: `.miq.hook.z
2021.01.14 | 23:46:10.637 | [INFO]  | <.prof.i.enable> | Profiling: `.miq.hook.i.default
2021.01.14 | 23:46:10.638 | [INFO]  | <.prof.i.enable> | Profiling: `.miq.ns.is.present
2021.01.14 | 23:46:10.639 | [INFO]  | <.prof.i.enable> | Profiling: `.miq.fq.d2cond

/ We can see that 2 functions are called by .prof.enable, hence they appeared in our runs as soon as their profiling initiates
/ stack is removed for readability
q)delete stack from .prof.runs[]
id step| function           parent depth user   context input               start                         end                           total                own                  space
-------| ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
1  1   | .miq.ns.is.context        0     michal .       .miq.hook.z         2021.01.14D23:46:10.636498000 2021.01.14D23:46:10.636518000 0D00:00:00.000020000 0D00:00:00.000020000 288
2  1   | .miq.ns.is.context        0     michal .       .miq.hook.i.default 2021.01.14D23:46:10.637282000 2021.01.14D23:46:10.637298000 0D00:00:00.000016000 0D00:00:00.000016000 288
3  1   | .miq.ns.is.context        0     michal .       .miq.ns.is.present  2021.01.14D23:46:10.638060000 2021.01.14D23:46:10.638074000 0D00:00:00.000014000 0D00:00:00.000014000 288
4  1   | .miq.ns.is.context        0     michal .       .miq.fq.d2cond      2021.01.14D23:46:10.639027000 2021.01.14D23:46:10.639044000 0D00:00:00.000017000 0D00:00:00.000017000 288
5  1   | .miq.ns.is.present        0     michal .       .miq.fq.d2cond      2021.01.14D23:46:10.639358000 2021.01.14D23:46:10.639372000 0D00:00:00.000014000 0D00:00:00.000014000 288
```

## Viewing definition of profiled code
```
q).prof.original `.miq.ns.is.context
{[ns] .is.error.ns[ns]; $[11h~type key ns; $[any key[ns] like "*.*"; 0b; 0h~type get get ns]; 0b]}
```

## Inspecting a particular run
```
q).miq.hook.info[];

/ We can see that the run ID we are interested in is 6
q)delete stack from .prof.runs[]
id step| function           parent         depth user   context input                start                         end                           total                own                  space
-------| -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
1  1   | .miq.ns.is.context                0     michal .       `.miq.hook.z         2021.01.15D00:05:40.634960000 2021.01.15D00:05:40.634970000 0D00:00:00.000010000 0D00:00:00.000010000 288
2  1   | .miq.ns.is.context                0     michal .       `.miq.hook.i.default 2021.01.15D00:05:40.635388000 2021.01.15D00:05:40.635396000 0D00:00:00.000008000 0D00:00:00.000008000 288
3  1   | .miq.ns.is.context                0     michal .       `.miq.ns.is.present  2021.01.15D00:05:40.635714000 2021.01.15D00:05:40.635720000 0D00:00:00.000006000 0D00:00:00.000006000 288
4  1   | .miq.ns.is.context                0     michal .       `.miq.fq.d2cond      2021.01.15D00:05:40.636047000 2021.01.15D00:05:40.636053000 0D00:00:00.000006000 0D00:00:00.000006000 288
5  1   | .miq.ns.is.present                0     michal .       `.miq.fq.d2cond      2021.01.15D00:05:40.636141000 2021.01.15D00:05:40.636146000 0D00:00:00.000005000 0D00:00:00.000005000 288
6  1   | .miq.hook.info                    0     michal .       ::                   2021.01.15D00:05:54.668757000 2021.01.15D00:05:55.372652000 0D00:00:00.703895000 0D00:00:00.083029000 9566144
6  2   | .miq.ns.tree       .miq.hook.info 1     michal .       ::                   2021.01.15D00:05:54.668947000 2021.01.15D00:05:55.286524000 0D00:00:00.617577000 0D00:00:00.000611000 9147904
6  3   | .miq.ns.i.tree     .miq.ns.tree   2     michal .       ::                   2021.01.15D00:05:54.668989000 2021.01.15D00:05:55.285955000 0D00:00:00.616966000 0D00:00:00.590669000 9146592
6  4   | .miq.ns.user       .miq.ns.i.tree 3     michal .       ::                   2021.01.15D00:05:54.669023000 2021.01.15D00:05:54.669553000 0D00:00:00.000530000 0D00:00:00.000497000 12112
6  5   | .miq.ns.reserved   .miq.ns.user   4     michal .       ::                   2021.01.15D00:05:54.669065000 2021.01.15D00:05:54.669098000 0D00:00:00.000033000 0D00:00:00.000033000 64
..

/ Filter out run number 6
q)delete stack from .prof.runs[6]
id step| function           parent         depth user   context input        start                         end                           total                own                  space
-------| ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
6  1   | .miq.hook.info                    0     michal .       ::           2021.01.15D00:05:54.668757000 2021.01.15D00:05:55.372652000 0D00:00:00.703895000 0D00:00:00.083029000 9566144
6  2   | .miq.ns.tree       .miq.hook.info 1     michal .       ::           2021.01.15D00:05:54.668947000 2021.01.15D00:05:55.286524000 0D00:00:00.617577000 0D00:00:00.000611000 9147904
6  3   | .miq.ns.i.tree     .miq.ns.tree   2     michal .       ::           2021.01.15D00:05:54.668989000 2021.01.15D00:05:55.285955000 0D00:00:00.616966000 0D00:00:00.590669000 9146592
6  4   | .miq.ns.user       .miq.ns.i.tree 3     michal .       ::           2021.01.15D00:05:54.669023000 2021.01.15D00:05:54.669553000 0D00:00:00.000530000 0D00:00:00.000497000 12112
6  5   | .miq.ns.reserved   .miq.ns.user   4     michal .       ::           2021.01.15D00:05:54.669065000 2021.01.15D00:05:54.669098000 0D00:00:00.000033000 0D00:00:00.000033000 64
6  6   | .miq.ns.is.context .miq.ns.i.tree 3     michal .       `.miq.       2021.01.15D00:05:54.670014000 2021.01.15D00:05:54.670036000 0D00:00:00.000022000 0D00:00:00.000022000 288
6  7   | .miq.ns.is.context .miq.ns.i.tree 3     michal .       `.miq.LOADED 2021.01.15D00:05:54.670233000 2021.01.15D00:05:54.670255000 0D00:00:00.000022000 0D00:00:00.000022000 288
6  8   | .miq.ns.is.context .miq.ns.i.tree 3     michal .       `.miq.SELF   2021.01.15D00:05:54.670437000 2021.01.15D00:05:54.670447000 0D00:00:00.000010000 0D00:00:00.000010000 288
6  9   | .miq.ns.is.context .miq.ns.i.tree 3     michal .       `.miq.CFG    2021.01.15D00:05:54.670627000 2021.01.15D00:05:54.670707000 0D00:00:00.000080000 0D00:00:00.000080000 288
6  10  | .miq.ns.is.context .miq.ns.i.tree 3     michal .       `.miq.TT     2021.01.15D00:05:54.671002000 2021.01.15D00:05:54.671017000 0D00:00:00.000015000 0D00:00:00.000015000 288
..
```

## Figuring out heavy hitters
```
/ Apparently, running .miq.ns.i.tree once took ~27x more time than running .miq.ns.is.context 3414 times!
/ Or did it?
q).prof.total[6]
function           | runs time                 ratio
-------------------| -------------------------------------
.miq.ns.i.tree     | 1    0D00:00:00.590669000 83.91436
.miq.hook.info     | 1    0D00:00:00.083029000 11.79565
.miq.ns.is.context | 3414 0D00:00:00.025767000 3.660631
.miq.ns.is.present | 286  0D00:00:00.001756000 0.249469
.miq.hook.i.default| 162  0D00:00:00.001484000 0.2108269
.miq.ns.tree       | 1    0D00:00:00.000611000 0.08680272
.miq.ns.user       | 1    0D00:00:00.000497000 0.07060712
.miq.fq.d2cond     | 1    0D00:00:00.000038000 0.005398532
.miq.ns.reserved   | 1    0D00:00:00.000033000 0.004688199
.miq.hook.z        | 2    0D00:00:00.000011000 0.001562733

/ Remember that enabling additional features introduces performance penalty and distorts numbers.
/ Lets disable the goodies!
q).prof.config.set'[`stack`parent`depth`user`context`input;0b]
000000b
q).prof.config.apply[]      / Apply the new configuration
q).miq.hook.info[];         / Throw output away
q).prof.run.id[]            / Last run ID
39

/ And what do we see? That it took actually ~7x longer, instead of ~27x!
q).prof.total[39]
function           | runs time                 ratio
-------------------| ------------------------------------
.miq.ns.i.tree     | 1    0D00:00:00.177265000 75.42132
.miq.hook.info     | 1    0D00:00:00.028941000 12.31359
.miq.ns.is.context | 3414 0D00:00:00.024842000 10.56958
.miq.ns.is.present | 286  0D00:00:00.001702000 0.7241536
.miq.hook.i.default| 162  0D00:00:00.001240000 0.5275855
.miq.ns.tree       | 1    0D00:00:00.000556000 0.2365625
.miq.ns.user       | 1    0D00:00:00.000378000 0.1608285
.miq.ns.reserved   | 1    0D00:00:00.000062000 0.02637927
.miq.fq.d2cond     | 1    0D00:00:00.000033000 0.01404058
.miq.hook.z        | 2    0D00:00:00.000014000 0.00595661
```

## Disabling profiling and resetting run information
```
/ This will disable profiling on all functions
q).prof.disable each .prof.enabled[]
`.miq.hook.info`.miq.ns.tree`.miq.ns.i.tree`.miq.ns.user`.miq.ns.reserved`.miq.ns.is.context`.miq.hook.z`.miq.hook.i.default`.miq.ns.is.present`.miq.fq.d2cond

/ And this will reset all run information
q).prof.clear[]
q)count .prof.runs[]
0

/ Use reset when going for both!
q).prof.reset[]
```

