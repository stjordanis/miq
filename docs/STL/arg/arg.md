arg
===
---
Handling of script arguments. Able to parse passed options into a required format, or revert the process using compose call. Options can have aliases, are stored with their arguments in appropriate format, and can be easily extended to handle *any* argument format you can imagine.

## Configuration
| Name                  | Type  | Default                                           | Definition                                                                |
| --------------------- | ------| ------------------------------------------------- | ------------------------------------------------------------------------- |
| `opts`                | `qk`  | [opts](#default-options)                          | Store context (after function execution)                                  |
| `mandatory`           | `sl`  | `` `$() ``                                        | Arguments in this list will be made mandatory                             |
| `strict`              | `b`   | `0b`                                              | Signal on encountering an unrecognised option                             |
| `set.strict`          | `b`   | `1b`                                              | Disallow setting an unsupplied option                                     |
| `assert.auto`         | `b`   | `0b`                                              | Verify mandatory arguments are supplied                                   |
| `assert.exit`         | `b`   | `0b`                                              | Exit on assertion failure                                                 |
| `assert.exitcode`     | `j`   | `1`                                               | Code to exit with when `assert.exit` is true and `.arg.assert[]` fails    |
| `assert.usage`        | `b`   | `1b`                                              | Call `.arg.usage[]` on failure                                            |
| `program.description` | `cl`  | `<No program description given>`                  | Default program description to use when none supplied to `.arg.usage[]`   |
| `program.usage`       | `cl`  | ``(string last` vs -1!.z.f)," [options] ..."``    | Default program usage string to use when none supplied to `.arg.usage[]`  |
| `program.options`     | `sl`  | `::`                                              | Program options to print out when none supplied to `.arg.usage[]`         |
| `help.option`         | `s`   | `` `help ``                                       | Name of help option                                                       |
| `help.auto`           | `b`   | `0b`                                              | Introduce `help.option` to the list of defined options when true          |
| `help.exitcode`       | `j`   | `0`                                               | Exit with this code when `help.option` is supplied                        |

### Native options
| Option | Cast   | Description                                                                         |
| ------ | ------ | ----------------------------------------------------------------------------------- |
| `b`    | `b`    | Block write-access to a kdb+ database for any handle context other than 0           |
| `c`    | `jl`   | Set console maximum rows and columns, default 25 80                                 |
| `C`    | `jl`   | Set HTTP display maximum rows and columns                                           |
| `e`    | `j`    | Set error-trapping mode, default to 0 (off)                                         |
| `E`    | `j`    | TLS Server Mode                                                                     |
| `g`    | `j`    | Set garbage-collection mode                                                         |
| `l`    | `b`    | Log updates to filesystem                                                           |
| `L`    | `b`    | As `l`, but sync logging                                                            |
| `m`    | `fs`   | Memory backed by a filesystem                                                       |
| `o`    | `j`    | Set local time offset as N hours from UTC, or minutes if abs[N]>23                  |
| `p`    | `port` | Set listening port                                                                  |
| `P`    | `j`    | Display precision for floating-point numbers                                        |
| `q`    | `b`    | Quiet mode                                                                          |
| `r`    | `cs`   | Replicate from `:host:port`                                                         |
| `s`    | `j`    | Number of secondary threads or processes available for parallel processing          |
| `t`    | `j`    | Period in milliseconds between timer ticks, default to 0 for no timer               |
| `T`    | `j`    | Timeout in seconds for client queries, default to 0 for no timeout                  |
| `u`    | `user` | Disable access to system commands and restrict file access                          |
| `U`    | `fs`   | Set a password file for user authentication and disable `\x`                        |
| `w`    | `j`    | Workspace limit in MB for the heap per thread, default to 0 for no limit            |
| `W`    | `j`    | Set the start-of-week offset, where 0 is Saturday, default to 2 for Monday          |
| `z`    | `j`    | Set the format for `"D"$` date parsing: 0 for *mm/dd/yyyy* and 1 for *dd/mm/yyyy*   |


### Default casting capabilities
* atoms  - `` `b`g`x`h`i`j`e`f`c`s`p`m`d`z`n`u`v`t`ns`fs`cs`qs ``
* lists  - `` `bl`gl`xl`hl`il`jl`el`fl`cl`sl`pl`ml`dl`zl`nl`ul`vl`tl`nsl`fsl`csl`qsl ``
* ranges - any atomic cast followed by `r`, i.e. `jr` or `qsr`
* port   - special cast to handle `-p` option, representing it in the form of  `(rp;host;port)`, where:
  * `rp` is boolean,
  * `host` is a symbol,
  * `port` is a whole number when port, or a symbol when using service name instead of port,
* user   - special cast to handle `-u` option, which is either represented as `j` or `fs` type

## Constants
| Name   | Definition                               |
| ------ | ---------------------------------------- |
| `INIT` | Module's initialisation status           |
| `OPTS` | Configured options and aliases           |
| `ARGS` | Processed options and their arguments    |

## Table templates
* `TT.OPTS`
| opt |!| mandatory | cast | alias | description |
| --- |-| --------- | ---- | ----- | ----------- |
| s   |!| b         | s    | \*    | \*          |

## Accessors
| Name             | target               | Note                       |
| ---------------- | -------------------- | -------------------------- |
| `.arg.resolve`   | `.arg.alias.resolve` | Resolve option             |
| `.arg.opts`      | `OPTS`               | All option definitions     |
| `.arg.native`    | `OPTS`               | Native option definitions  |
| `.arg.custom`    | `OPTS`               | Custom option definitions  |
| `.arg.args`      | `ARGS`               | Processed option arguments |

## Hooks
| Name               | Default implementation                       | Description             |
| ------------------ | -------------------------------------------- | ----------------------- |
| `.arg.z.message`   | [`.prof.z.dd.message`](api#argzmessage)      | Log output              |
| `.arg.z.parse.*`   | [`.prof.z.dd.parse.*`](api#argzparse)        | Family of parse hooks   |
| `.arg.z.compose.*` | [`.prof.z.dd.compose.*`](api#argzcompose)    | Family of compose hooks |
| `.arg.z.is.*`      | [`.prof.z.dd.is.*`](api#argzis)              | Family of is hooks      |

## Errors
| Message                                       | Explanation                                                                                                       |
| --------------------------------------------- | ----------------------------------------------------------------------------------------------------------------- |
| `Some mandatory options were not supplied`    | `.arg.assert` has failed due to mandatory options not being supplied on the command line                          |
| `Cannot set option`                           | Can only set options that have not been supplied. Set `set.strict` to false to allow override.                    |
| `No such option`                              | Not an option or alias                                                                                            |
| `Option not supplied`                         | Option or alias has not been supplied to script                                                                   |
| `Option not mandatory`                        | Option or alias has not been configured as mandatory                                                              |
| `Option not optional`                         | Option or alias has not been configured as optional                                                               |
| `No such alias`                               | Not a known alias to any of configured options                                                                    |
| `Alias already exists`                        | Attempting creation of extant alias                                                                               |
| `Option already exist`                        | Attempting creation of extant option                                                                              |
| `Cannot override option`                      | `.arg.inject` is not allowed to override options, use `.arg.set`                                                  |
| `Invalid zx`                                  | `.arg.parse` is complaining about invalid (script) input                                                          |
| `Not a recognised option or alias`            | `.arg.parse` & `.arg.compose` will output a list of unknown options and aliases. Will signal if `strict` is true. |
| `Option missing argument`                     | `.arg.parse` complains about missing argument to option. Will signal if `strict` is true.                         |
| `Option parsing not implemented`              | `.arg.parse` is missing cast parse implementation                                                                 |
| `Option composition not implemented`          | `.arg.compose` is missing cast compose implementation                                                             |
| `Unable to compose port`                      | `.arg.z.compose.port` has been fed an unexpected `-p` definition                                                  |
| `Option argument type incorrect`              | Argument is not conforming to `cast` type of option                                                               |

