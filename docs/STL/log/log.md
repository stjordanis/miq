log
===
---
Logging library with built-in support for custom log levels, log rotation and colours.

## Configuration
| Name                | Type    | Default                                   | Definition                                                        |
| ------------------- | ------- | ----------------------------------------- | ----------------------------------------------------------------- |
| `so.class`          | `s`     | `` `so``                                  | C library module class                                            |
| `so.lib`            | `s`     | `` `log``                                 | C library name                                                    |
| `level`             | `s`     | `` `info``                                | Minimum severity of log messages to output                        |
| `levels`            | `s`     | `` `debug`show`info`warn`error`fatal``    | Available log levels, from lowest to highest                      |
| `STDOUT`            | `j`     | `-1`                                      | STDOUT file descriptor                                            |
| `STDERR`            | `j`     | `-2`                                      | STDERR file descriptor                                            |
| `delimiter`         | `c\|cl` | `"\|"`                                    | Character(s) used to delimit individual parts of the log line     |
| `caller.enable`     | `b`     | `1b`                                      | Prepend function's name to log message                            |
| `file.directory`    | `s`     | `` `:.``                                  | Log file location                                                 |
| `file.name`         | `s`     | `` ` ``                                   | Log file name                                                     |
| `file.extension`    | `s`     | `` `log``                                 | Log file extension                                                |
| `file.mode`         | `j`     | `0N`                                      | Permission mode of newly created log files                        |
| `dir.autocreate`    | `j`     | `0b`                                      | When set to true, auto-create log directory if it does not exist  |
| `colour.default`    | `b`     | `1b`                                      | Apply default warn and error levelity colours                     |
| `rotate.enable`     | `b`     | `0b`                                      | Flag to enable or disable log rotation                            |
| `rotate.mode`       | `s`     | `` `day``                                 | Determines when to rotate to a new log file                       |
| `rotate.modes`      | `s`     | `` `none`day`line`size``                  | Implemented log rotation modes                                    |
| `rotate.threshold`  | `j`     | `1`                                       | Frequency of log rotation, as per `rotate.mode`                   |
| `rotate.method`     | `s`     | `` `inode``                               | Method used to rotate the log file                                |
| `rotate.methods`    | `s`     | `` `inode`dup2``                          | Implemented log rotation methods                                  |
<!--| rules             | qk    | `TT.RULES`  | Per-process logging rules                                                                    |-->

## Constants
#### Standard
| Name             | Definition                                |
| ---------------- | ----------------------------------------- |
| `SELF`           | Module's namespace                        |
| `BB`             | Blackboard namespace                      |
| `INIT`           | Initiation status                         |
| `DATE`           | Current log date                          |
| `SO.LOADED`      | True if C library has been loaded         |
| `CFG.EXPLICIT`   | User configuration values                 |
| `CFG.DEFAULT`    | Default configuration values              |
| `CFG.APPLIED`    | Applied configuration values              |
| `FILE.PATH`      | `.miq.fd.stdout[]`                        |

#### Local
| Name             | Definition                                         |
| ---------------- | -------------------------------------------------- |
| `L.SEVERITY`     | Mapping of log level to printable string           |
| `L.THRESHOLD`    | `rotate.threshold` configuration                   |
| `L.COUNTER`      | Counter of `rotate.mode` units                     |
| `L.DELIMITER`    | `delimiter` configuration                          |
| `L.DATE`         | `DATE` constant                                    |
| `L.STR_DATE`     | String `DATE` constant                             |
| `L.FIXED_SIZE`   | Adjusted byte size of pre-log-message header       |
| `L.CALLER.DEPTH` | Frames to `.log.z.caller`+1 from `.log.<LEVEL>`    |

## Hooks
| Name                   | Default implementation                               | Description                                                       |
| ---------------------- | ---------------------------------------------------- | ----------------------------------------------------------------- |
| `.log.z.generate.name` | [`.log.z.dd.generate.name`](api#logzgeneratename)    | Generate name of the logging process                              |
| `.log.z.generate.file` | [`.log.z.dd.generate.file`](api#logzgeneratefile)    | Generate name of the log file                                     |
| `.log.z.tally`         | -                                                    | Keeps track of log output so threshold configuration is respected |
| `.log.z.print`         | -                                                    | Prints messages to output streams                                 |
| `.log.z.rotate`        | -                                                    | Rotates log file into a new one                                   |
| `.log.z.caller`        | `{""}`                                               | Figures out function name calling log routine                     |
| `.log.z.message`       | [`.log.z.dd.message`](api#logzmessage)               | Compiles the log line that is printed to log file                 |
| `.log.z.<LEVEL>`       | `::`                                                 | Default definition of any log level hook is identity `::`         |

## Accessors
| Name             | target             | Note                                      |
| ---------------- | ------------------ | ----------------------------------------- |
| `.log.date`      | `DATE`             | Constant                                  |
| `.log.rotate`    | `ROTATE.*`         | Dict of `ROTATE` constants                |
| `.log.file`      | `FILE.*`           | Dict of `FILE` constants                  |
| `.log.<LEVEL>`   | `.log.z.<LEVEL>`   | Auto-generated for every log level hook   |

## Errors
| Message                                                           | Explanation                                   |
| ----------------------------------------------------------------- | --------------------------------------------- |
| `Invalid log level`                                               | Log level not one of `levels`                 |
| `Invalid log rotate mode`                                         | Log rotate mode not one of `rotate.modes`     |
| `Invalid log rotate method`                                       | Log rotate method not one of `rotate.methods` |
| `Log rotate aborted! New log file is identical to current one`    | Attempt to log rotate into current file       |

