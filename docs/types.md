Types
=====
---
miQ implements it's own type philosophy to make coding more productive and code more readable, in a similar fashion that `q` promotes numeric values to `boolean` where necessary. This page is dealing with type definitions only. For more details on implemented type tests, including other useful tests of non-types, see [`is` documentation](is).

Few highlights:
* Atomic and list types have distinct notation
* Symbol type is split into multiple types based on use case
* Connection handles and file handles can be told apart and tested
* Dictionary and table types are mutualy exclusive

## miQ types
Legend:
* `Name` - name of type
* `Atom` - type of atomic values
* `List` - type of list values
* `q n` - native q short int value equivalent (multiple values signify "confluence of")
* `q c` - native q type character equivalent
* `Note` - necessary information for completeness

### Fundamental
| Name                  | Atom | List       | q n            | q c | Note                                                                   |
| --------------------- | ---- | ---------- | -------------- | --- | ---------------------------------------------------------------------- |
| *generic list*        | -    | `generic`  | 0              | `*` | Only list can be generic                                               |
| *boolean*             | `b`  | `bl`       | 1              | `b` | -                                                                      |
| *guid*                | `g`  | `gl`       | 2              | `g` | -                                                                      |
| *byte*                | `x`  | `xl`       | 4              | `x` | -                                                                      |
| *short*               | `h`  | `hl`       | 5              | `h` | -                                                                      |
| *int*                 | `i`  | `il`       | 6              | `i` | -                                                                      |
| *long*                | `j`  | `jl`       | 7              | `j` | -                                                                      |
| *real*                | `e`  | `el`       | 8              | `e` | -                                                                      |
| *float*               | `f`  | `fl`       | 9              | `f` | -                                                                      |
| *char*                | `c`  | `cl`       | 10             | `c` | -                                                                      |
| *symbol*              | `s`  | `sl`       | -              | -   | Symbol that is not `ns`, `fs` or `cs`                                  |
| *timestamp*           | `p`  | `pl`       | 12             | `p` | -                                                                      |
| *month*               | `m`  | `ml`       | 13             | `m` | -                                                                      |
| *date*                | `d`  | `dl`       | 14             | `d` | -                                                                      |
| *datetime*            | `z`  | `zl`       | 15             | `z` | -                                                                      |
| *timespan*            | `n`  | `nl`       | 16             | `n` | -                                                                      |
| *minute*              | `u`  | `ul`       | 17             | `u` | -                                                                      |
| *second*              | `v`  | `vl`       | 18             | `v` | -                                                                      |
| *time*                | `t`  | `tl`       | 19             | `t` | -                                                                      |
| *namespace*           | `ns` | `nsl`      | -              | -   | Address symbol that starts with `.`                                    |
| *file descriptor*     | `fd` | `fdl`      | 6, 7           | -   | File descriptors in use by current process                             |
| *file symbol*         | `fs` | `fsl`      | -              | -   | Starts with `` `: ``, is at least 2 character long and is not `cs`     |
| *file handle*         | `fh` | `fhl`      | 6, 7           | -   | Open file descriptors to a file                                        |
| *connection symbol*   | `cs` | `csl`      | -              | -   | Starts with `:` and includes `:` at least twice                        |
| *connection handle*   | `ch` | `chl`      | 6, 7           | -   | Open file descriptors to a socket                                      |
| *operator*            | `op` | `opl`      | 101, 102, 103  | -   | Unary, binary and ternary primitives                                   |
| *function*            | `fn` | `fnl`      | 100            | -   | Named & unnamed lambda only                                            |
| *projection*          | `fp` | `fpl`      | 104            | -   | Any elided object                                                      |
| *composition*         | `qc` | `qcl`      | 105            | -   | -                                                                      |
| *iterator*            | `qi` | `qil`      | 106 - 111      | -   | -                                                                      |
| *native k symbol*     | `qs` | `qsl`      | 11             | `s` | -                                                                      |
| *dictionary*          | `qd` | `qdl`      | -              | -   | Does not include `qk`                                                  |
| *unkeyed table*       | `qt` | `qtl`      | -              | -   | Unkeyed in-memory table only                                           |
| *keyed table*         | `qk` | `qkl`      | -              | -   | Keyed in-memory table only                                             |
| *partitioned table*   | `qp` | `qpl`      | -              | -   | Any partitioned table                                                  |
| *in-memory table*     | `qm` | `qml`      | -              | -   | Any in-memory table, be it `qt` or `qk`                                |

### Special
| Name                      | Atom      | List      | q n               | q c | Note                                                                        |
| ------------------------- | --------- | --------- | ----------------- | --- | --------------------------------------------------------------------------- |
| *not yet implemented*     | `nyi`     | -         | -                 | -   | Convenience function that always return `1b`                                |
| *enumeration*             | `enum`    | `enum`    | -76 - 20, 20 - 76 | -   | Atom / vector using enumeration domain                                      |
| *anymap*                  | `anymap`  | -         | 77                | -   | Modern mapped nested list since kdb 3.6, meant to replace `mapped`          |
| *mapped list of lists*    | `mapped`  | -         | 78 - 96           | -   | Previous to kdb 3.6 you would use `mapped` type to splay nested columns     |
| *nested sym enumeration*  | `nested`  | -         | 97                | -   | Nested sym enumeration domain                                               |
| *table*                   | `table`   | -         | -                 | -   | All table-like objects resolve to this type (`qt`, `qk`, `qp`)              |
| *elision*                 | `elision` | -         | 101               | -   | Special unary operator number 255, excluded from *operator* type            |
| *dynamic load*            | `code`    | -         | 112               | -   | Dynamically loaded code, i.e. using `2:`                                    |
| *foreign object*          | `foreign` | -         | 112               | -   | Foreign objects, i.e `` -100!` ``                                           |

## True, False, None, Null, Empty
miQ introduces pseudo-types similar to that of Python.
* `.is.true` return `1b` for everything that `.is.false` evaluates as `0b`
* `.is.false` return `1b` for `0b`, any numeric `0`, empty list of any type and null atomic values
* `.is.none` return `1b` for a generic null `::` or an empty generic list `()`
* `.is.null` return `1b` for values that are atomic and null
* `.is.empty` return `1b` for lists that have a length of `0`

## Symbols
> [!NOTE] All symbol-derive miQ types share a common null `` ` `` and an empty list `` `$() `` values.

> [!ATTENTION] miQ's `s` type is not q's `s` type `-11h`!

Symbol type is overloaded in q to mean many things. in miQ, use cases are distinguished and given their own type.
* `s`  - Plain symbol is any symbol that's not `ns`, `cs` or `fs`
* `ns` - Address symbol starts with `.`
* `cs` - Connection symbol starts with `` `: `` and has at least 2 `:`
* `fs` - File symbol starts with `` `: `` and is at least 2 characters long
* `qs` - This is the native q symbol type (-11h) that everybody is familiar with

That means that `.is.qs` test will return true in the same manner as `-11h~type`. However, `.is.s` will only return true for `s` type, just as other symbol-derived types return true only for their own domain.

## Descriptors & Handles
* **File descriptor** - miQ provides ability to obtain open file descriptors of current process, thus making `fd` type possible
* **Connection handle** - thanks to `.z.W` holding the list of open handles, `ch` type is trivial to implement
* **File handle** - given that we already have a list of file descriptors and a list of connection handles available, `fd except ch` yields `fh` type

> [!ATTENTION]
> * File descriptor, file handle & connection handle domains will differ between processes and over time.
> * File descriptor `0i` is special in that it is *both* file handle and connection handle.
> * `fh` type is imperfect as it will also returns true for currently open listener socket. I need to find a reliable way how to determine listener socket of current process.

## Infinities, Attributes, Type translation, Numeric & Temporal values
In addition to standard miQ types, [`.is`](is) implements multitude of other tests to convey intention in meaningful way, i.e. testing for an infinity of particular type or for object attributes.

