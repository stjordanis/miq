Modules
===
---
Here you can find a collection of official miQ modules. These are intended to be used as provided, and are suitable for use as building blocks for higher level modules.

| Name                        | Version | Tested    | Description                                       | Objects defined | Module dependencies   | System dependencies                         |
| --------------------------- | ------- | --------- | ------------------------------------------------- | --------------  | --------------------- | ------------------------------------------- |
| [arg](STL/arg/arg)          | 0.000.1 | Yes       | Process input parsing and composition             |                 |                       | `ip`, `timeout`, `bash`, `echo`, `lsof`     |
|  clan                       | 0.000.1 | No        | Application level communication between processes | `process`       | `arg`                 |                                             |
|  env                        | 0.000.0 | No        | System environment management                     |                 |                       | `env`                                       |
|  exec                       | 0.000.0 | No        | Execute commands on OS                            |                 |                       |                                             |
| [ipc](STL/ipc/ipc)          | 0.000.1 | Yes       | Inter process communication                       |                 |                       |                                             |
| [log](STL/log/log)          | 0.000.1 | No        | Feature rich logging                              |                 |                       |                                             |
|  mat                        | 0.000.0 | No        | Manager                                           |                 | `arg`,`clan`,`exec`   |                                             |
| [prof](STL/prof/prof)       | 0.000.1 | No        | Profiling capabilities                            |                 |                       |                                             |
|  seq                        | 0.000.0 | No        | Generator of values of given type                 |                 |                       |                                             |
|  schema                     | 0.000.1 | No        | Table schema handling                             |                 |                       |                                             |
|  slot                       | 0.000.0 | No        | Module organisation and management                | `module`        |                       |                                             |
|  timer                      | 0.000.0 | No        | Cyclical job execution at specific intervals      |                 |                       |                                             |
|  tz                         | 0.000.0 | No        | Time & Timezone capabilities                      |                 |                       | `timedatectl`                               |
|  tp                         | 0.000.0 | No        | Tickerplant                                       |                 |                       |                                             |
|  worker                     | 0.000.1 | No        | Slave / threading                                 |                 | `ipc`, `trap`         | `nohup`, `kill`, `eval`, `printf`           |

