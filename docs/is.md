is
==
---
Type checking and validation to support both native q and miQ type resolution. All functions take a single input, unless communicated otherwise.

> [!TIP] Shorthand to resolve multiple types in one go: ``.is[`s`cl`fn]@'(sVar;clVar;fpFunc)``

**Errors**

| Message                               | Explanation                                                           |
| ------------------------------------- | --------------------------------------------------------------------- |
| `Form includes unimplemented checks`  | Attempt to resolve type not in `.miq.is.functions`                    |

**Examples**
```
/ Generate tests with standardised log messages
q)test:{[p1;p2] $[.is.atom[p2]; .[in;(p2;p1);0b]; 0b]}
q).is.generate.warn[`..test;"[${p2}] not in [${p1}]"]
q).warn.test
{[p1;p2] $[test[p1;p2];1b;[.log.warn "[",(-3!p2),"] not in [",(-3!p1),"]";0b]]}
q).warn.test[`a`b`c;`a]
1b
q).warn.test[`a`b`c;`a`b]
2021.03.03 | 14:27:58.960 | [WARN]  | <.warn.test> | [`a`b] not in [`a`b`c]
0b

q).is.generate.error[`..test;"${p2} not in: "]
q).error.test
{[p1;p2] $[test[p1;p2];1b;'.log.error "",(-3!p2)," not in: ",-3!p1]}
q).error.test[`a`b`c;`a]
1b
q).error.test[`a`b`c;`a`b]
2021.03.03 | 14:28:57.727 | [ERROR] | <.error.test> | `a`b not in: `a`b`c
'`a`b not in: `a`b`c
  [0]  .error.test[`a`b`c;`a`b]
       ^


/ Common type testing
q).is.xl 0x1234
1b
q).is.c "q"
1b
q).is.neg 0N!.is.type 0N!.miq.fs.file.handles[]
0 1 2 3 4 5i
`il
`i

q)hopen`:file.txt
8i
q).is.fh each 7 8 9
010b
q).is.fhl 0 5 8
1b
q).is.fhl 6 8
0b

q)(type;.is.type)@\:([a:`a`b];b:1 2)
99h
`qk
q).is[`qd`qt`qk]@\:([k:`a`b]v:1 2)
011b

q).is[`j`fn]@'(1;{})
11b

q).is.any[`s`sl;`sym]
1b
q).is.any[`s`sl;`sym`bol]
1b
q).is.any[`s`sl;`sym,1]
0b

q).is.one[(`none;`fs;`fs`j;`fs`j`j)] each (12;::;`:file;`file,1;`:file,1 2)
01101b

q).is.temporal .z.d
1b
q).is.year 9999
1b
q).is.dd 32
0b
q).is.hh 24
0b
```

## miQ type tests
### Fundamental
Following functions are implementing checks for the identically named miQ types. See [Types](types) for more details on individual types.

| Type | Atom     | List      || Type | Atom     | List      || Type | Atom     | List      || Type | Atom     | List      |
| ---- | -------- | --------- || ---- | -------- | --------- || ---- | -------- | --------- || ---- | -------- | --------- |
| b    | `.is.b`  | `.is.bl`  || s    | `.is.s`  | `.is.sl`  || ns   | `.is.ns` | `.is.nsl` || qc   | `.is.qc` | `.is.qcl` |
| g    | `.is.g`  | `.is.gl`  || p    | `.is.p`  | `.is.pl`  || fd   | `.is.fd` | `.is.fdl` || qi   | `.is.qi` | `.is.qil` |
| x    | `.is.x`  | `.is.xl`  || m    | `.is.m`  | `.is.ml`  || fs   | `.is.fs` | `.is.fsl` || qs   | `.is.qs` | `.is.qsl` |
| h    | `.is.h`  | `.is.hl`  || d    | `.is.d`  | `.is.dl`  || fh   | `.is.fh` | `.is.fhl` || qd   | `.is.qd` | `.is.qdl` |
| i    | `.is.i`  | `.is.il`  || z    | `.is.z`  | `.is.zl`  || cs   | `.is.cs` | `.is.csl` || qt   | `.is.qt` | `.is.qtl` |
| j    | `.is.j`  | `.is.jl`  || n    | `.is.n`  | `.is.nl`  || ch   | `.is.ch` | `.is.chl` || qk   | `.is.qk` | `.is.qkl` |
| e    | `.is.e`  | `.is.el`  || u    | `.is.u`  | `.is.ul`  || op   | `.is.op` | `.is.opl` || qp   | `.is.qp` | `.is.qpl` |
| f    | `.is.f`  | `.is.fl`  || v    | `.is.v`  | `.is.vl`  || fn   | `.is.fn` | `.is.fnl` ||      |          |           |
| c    | `.is.c`  | `.is.cl`  || t    | `.is.t`  | `.is.tl`  || fp   | `.is.fp` | `.is.fpl` ||      |          |           |

### Special
| Type      | Atom          | List          | Note                      |
| --------- | ------------- | ------------- | ------------------------- |
| nyi       | `.is.nyi`     | -             | Never return false        |
| generic   | -             | `.is.generic` | Generic list type         |
| enum      | `.is.enum`    | `.is.enum`    | Enumeration               |
| anymap    | `.is.anymap`  | -             | [Anymap](types#special)   |
| mapped    | `.is.mapped`  | -             | [Mapped](types#special)   |
| nested    | `.is.nested`  | -             | [Nested](types#special)   |
| elision   | `.is.elision` | -             | Elision magic value       |
| code      | `.is.code`    | -             | Dynamically loaded code   |
| foreign   | `.is.foreign` | -             | Foreign object            |

## Additional tests
| Function              | True if given                             |
| --------------------- | ----------------------------------------- |
| `.is.none`            | One of `::`, `()`                         |
| `.is.null`            | Any atomic `null`                         |
| `.is.empty`           | Empty list, dictionary or table           |
| `.is.true`            | Any value `.is.false` evaluates as false  |
| `.is.false`           | 0b, 0, empty list or null                 |
| `.is.any`             | [Explained here](#isany)                  |
| `.is.one`             | [Explained here](#isone)                  |
| `.is.form`            | [Explained here](#isform)                 |
| `.is.atom`            | Any q atomic type                         |
| `.is.list`            | Any q list type                           |
| `.is.unary(List)`     | Any unary operator (primitive)            |
| `.is.binary(List)`    | Any binary operator (operator)            |
| `.is.ternary(List)`   | Any ternary iperator (iterator)           |
| `.is.number(List)`    | Any whole or decimal type                 |
| `.is.whole(List)`     | Either short, int or long type            |
| `.is.decimal(List)`   | Either real or float type                 |
| `.is.pinf(List)`      | Positive infinity value                   |
| `.is.ninf(List)`      | Negative infinity value                   |
| `.is.inf(List)`       | Any infinity value                        |

## Temporal tests
| Function              | True if given                             |
| --------------------- | ----------------------------------------- |
| `.is.temporal`        | Any atom or list of temporal data type    |
| `.is.year(List)`      | Any whole number between 1000 and 9999    |
| `.is.mm(List)`        | Any whole number between 1 and 12         |
| `.is.dd(List)`        | Any whole number between 1 and 31         |
| `.is.hh(List)`        | Any whole number between 0 and 23         |
| `.is.uu(List)`        | Any whole number between 0 and 59         |
| `.is.ss(List)`        | Any whole number between 0 and 59         |

## Attribute tests
| Function              | True if given                                     |
| -------------------   | ------------------------------------------------- |
| `.is.sorted`          | Value with `` `s`` attribute                      |
| `.is.unique`          | Value with `` `u`` attribute                      |
| `.is.parted`          | Value with `` `p`` attribute                      |
| `.is.grouped`         | Value with `` `g`` attribute                      |
| `.is.step`            | A dict or keyed table with `` `s`` attribute      |

## .miq.is.functions
List of functions that implement plain type checking (= check type & take single parameter).

## .miq.is.init
Generate `.is.warn` and `.is.error` routines for all functions performing type checking.

## .is.generate.warn, .is.generate.error
| parameter   | Type | Description        |
| ----------- | ---- | ------------------ |
| `nsFunction`| ns   | Function to wrap   |
| `jDepth`    | j    | Context base depth |
| `clMessage` | cl   | Log message        |

Generate a wrapper of `nsFunction`, in context `jDepth` below it, that will log `clMessage` on `nsFunction` returning `0b`.
* `nsFunction` has to return either `1b` or `0b`.
* `clMessage` will have any `${paramName}` substituted for actual parameter values
* `clMessage` will have a parameter value appended if there's only one parameter to print out (excluding the ones substituted above)
* `clMessage` will have a printout of dictionary of parameters and their values appended if there's more than one parameter to print out (again excluding substituted parameters)

Both will assign the function to address that has either `warn` or `error` subcontext prepended to `nsFunction` leaf name. In addition:
* `.is.generate.warn` will generate a function that **does not** throw a signal
* `.is.generate.error` will generate a function that **does** throw a signal

## .is.warn.\*, .is.error.\*
Generated by [`.is.init`](#isinit) using [`.is.generate`](#isgeneratewarn-isgenerateerror) family of functions to standardise output logging and prevent inconsistent messaging.
* `.is.warn` - return `1b` on success, otherwise `0b` and log a warn level message
* `.is.error` - return `1b` on success, otherwise log an error level message and signal

## .is.q.\*
This set of functions accept short number, and return:
* `type` - true if it represents a q type
* `atom` - true if it represents an atomic q type
* `list` - true if it represents a non-atomic q type

## .is.miq.\*
This set of functions accept a symbol, and return:
* `type` - true if it represents an miQ type
* `atom` - true if it represents an atomic miQ type
* `list` - true if it represents a non-atomic miQ type

## .is.miq2q
Mapping of miQ type symbols to the q type numbers employed by them.

## .is.q2miq
Mapping of q type numbers to miQ type symbols that make use of it.

## .is.any
| parameter | Type | Description       |
| --------- | ---- | ----------------- |
| `slType`  | sl   | Types to match    |
| `x`       | -    | Value to check    |

Return true if value `x` resolves to at least one of `slType` types.

## .is.one
| parameter | Type | Description            |
| --------- | ---- | ---------------------- |
| `form`    | sll  | List of forms to match |
| `x`       | -    | Value to check         |

Return `1b` if `x` conforms to at least one type description from from the list of `form`s. If `form` element is an atom, perform direct type check, otherwise make use of `.is.form[]`.

## .is.form
| parameter | Type | Description            |
| --------- | ---- | ---------------------- |
| `form`    | sl   | List of types to match |
| `x`       | -    | List value to check    |

Return `1b` if the type of elements of the list `x` map exactly to `form`. That means that both the length of `x` and `form` have to be the same, and order of types in `form` has to match the order of types in `x`.

## .is.type
Return [miQ type notation](types#miq-types) of supplied data.

## .is.meta
Return `meta` of table object using [miQ type notation](types#miq-types).

## .is.neg
Given an miQ type that is:
* `atomic` - turn it to list type
* `list` - turn it to atomic type
* `generic`- keep unchanged

If given a q type:
* `-19 19h` - apply `neg` to it and return
* `20h 112h` - return `0h`

